<cfscript>
 public string function getLabel(required any keyword, numeric languageId=0) {
 	if(languageId==0){
 		languageId=SESSION.languageId;
 	}
 	for(item in application.labels) {
 	   if(item.keyword == keyword and item.languageId == languageId) {
 	   		return item.value;
 	   }
 	}
 	return keyword;
 }
</cfscript>