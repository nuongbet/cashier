<cfinclude template="/#application.sPath#/header.cfm">
<cfset stChart = {
            chart: {
                renderTo:"container",
                type: 'area'
            },
            title: {
                text: 'Area chart with negative values'
            },
            xAxis: {
                categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'John',
                data: [5, 3, 4, 7, 2]
            }, {
                name: 'Jane',
                data: [2, -2, -3, 2, 1]
            }, {
                name: 'Joe',
                data: [3, 4, 4, -2, 5]
            }]
        }>

    <cfinclude template="/#application.sPath#/menu.cfm">
    <div id="container"></div>
    <cfhighchart attributeCollection="#stChart#" createContainer="false">

    </body>
</html>
