<cfinclude template="/#application.sPath#/header.cfm">
<cfscript>
    
    categories = ['MSIE', 'Firefox', 'Chrome', 'Safari', 'Opera'];
    name = 'Browser brands';
    data = [{
            y: 55.11,
            color: colors[1],
            drilldown: {
                name: 'MSIE versions',
                categories: ['MSIE 6.0', 'MSIE 7.0', 'MSIE 8.0', 'MSIE 9.0'],
                data: [10.85, 7.35, 33.06, 2.81],
                color: colors[1]
            }
        }, {
            y: 21.63,
            color: colors[2],
            drilldown: {
                name: 'Firefox versions',
                categories: ['Firefox 2.0', 'Firefox 3.0', 'Firefox 3.5', 'Firefox 3.6', 'Firefox 4.0'],
                data: [0.20, 0.83, 1.58, 13.12, 5.43],
                color: colors[2]
            }
        }, {
            y: 11.94,
            color: colors[3],
            drilldown: {
                name: 'Chrome versions',
                categories: ['Chrome 5.0', 'Chrome 6.0', 'Chrome 7.0', 'Chrome 8.0', 'Chrome 9.0',
                    'Chrome 10.0', 'Chrome 11.0', 'Chrome 12.0'],
                data: [0.12, 0.19, 0.12, 0.36, 0.32, 9.91, 0.50, 0.22],
                color: colors[3]
            }
        }, {
            y: 7.15,
            color: colors[4],
            drilldown: {
                name: 'Safari versions',
                categories: ['Safari 5.0', 'Safari 4.0', 'Safari Win 5.0', 'Safari 4.1', 'Safari/Maxthon',
                    'Safari 3.1', 'Safari 4.1'],
                data: [4.55, 1.42, 0.23, 0.21, 0.20, 0.19, 0.14],
                color: colors[4]
            }
        }, {
            y: 2.14,
            color: colors[5],
            drilldown: {
                name: 'Opera versions',
                categories: ['Opera 9.x', 'Opera 10.x', 'Opera 11.x'],
                data: [ 0.12, 0.37, 1.65],
                color: colors[5]
            }
        }];
    
        // Build the data arrays
        browserData = [];
        versionsData = [];
        for (i=1; i <= data.len(); i++) {
    
            // add browser data
            browserData.append({
                name: categories[i],
                y: data[i].y,
                color: data[i].color
            });
    
            // add version data
            for (j=1; j<=data[i].drilldown.data.len(); j++) {
                brightness = 0.4 - (j / data[i].drilldown.data.len()) / 5 ;
                versionsData.append({
                    name: data[i].drilldown.categories[j],
                    y: data[i].drilldown.data[j],
                    color: brighten(data[i].color, brightness)
                });
            }
        }
</cfscript>
   
<cfset stChart = {
            chart: {
                renderTo:"container",
                type: 'pie'
            },
            title: {
                text: 'Browser market share, April, 2011'
            },
            yAxis: {
                title: {
                    text: 'Total percent market share'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    center: ['50%', '50%']
                }
            },
            tooltip: {
        	    valueSuffix: '%'
            },
            series: [{
                name: 'Browsers',
                data: browserData,
                size: '60%',
                dataLabels: {
                    formatter: "function() {
                        return this.y > 5 ? this.point.name : 'null';
                    }",
                    color: 'white',
                    distance: -30
                }
            }, {
                name: 'Versions',
                data: versionsData,
                size: '80%',
                innerSize: '60%',
                dataLabels: {
                    formatter: "function() {
                        // display only if larger than 1
                        return this.y > 1 ? '<b>'+ this.point.name +':</b> '+ this.y +'%'  : 'null';
                    }"
                }
            }]
        }>

    <cfinclude template="/#application.sPath#/menu.cfm">
    <div id="container"></div>
    <cfhighchart attributeCollection="#stChart#" createContainer="false">

    </body>
</html>

<cfscript>
    string function brighten(required string sColor, required numeric iFactor) {
        local.sColor = replace(arguments.sColor, "##", "", "ALL");
        local.sRed   = inputBaseN(left(sColor, 2), 16);
        local.sGreen = inputBaseN(mid(sColor, 3, 2), 16);
        local.sBlue  = inputBaseN(right(sColor, 2), 16);
        sRed   = formatBaseN(round(min(255, sRed * (1+arguments.iFactor))), 16);
        sGreen = formatBaseN(round(min(255, sGreen * (1+arguments.iFactor))), 16);
        sBlue  = formatBaseN(round(min(255, sBlue * (1+arguments.iFactor))), 16);
        return "##" & sRed & sGreen & sBlue;
    }
</cfscript>
