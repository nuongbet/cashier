<cfinclude template="/#application.sPath#/header.cfm">
<cfset stChart = {
            
            chart: {
                renderTo:"container"
            },
            
            title: {
                text: 'Logarithmic axis demo'
            },
            
            xAxis: {
                tickInterval: 1
            },
            
            yAxis: {
                type: 'logarithmic',
                minorTickInterval: 0.1
            },
            
            tooltip: {
                headerFormat: '<b>{series.name}</b><br />',
                pointFormat: 'x = {point.x}, y = {point.y}'
            },
            
            series: [{            
                data: [1, 2, 4, 8, 16, 32, 64, 128, 256, 512],
                pointStart: 1
            }]
        }>

    <cfinclude template="/#application.sPath#/menu.cfm">
    <div id="container"></div>
    <cfhighchart attributeCollection="#stChart#" createContainer="false">

    </body>
</html>
