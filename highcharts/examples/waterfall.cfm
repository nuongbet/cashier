<cfinclude template="/#application.sPath#/header.cfm">
<cfset stChart = {
        chart: {
            renderTo:"container",
            type: 'waterfall'
        },

        title: {
            text: 'Highcharts Waterfall'
        },

        xAxis: {
            type: 'category'
        },

        yAxis: {
            title: {
                text: 'USD'
            }
        },

        legend: {
            enabled: false
        },

        tooltip: {
            pointFormat: '<b>${point.y:,.2f}</b> USD'
        },

        series: [{
            upColor: colors[3],
            color: colors[4],
            data: [{
                name: 'Start',
                y: 120000
            }, {
                name: 'Product Revenue',
                y: 569000
            }, {
                name: 'Service Revenue',
                y: 231000
            }, {
                name: 'Positive Balance',
                isIntermediateSum: true,
                color: colors[2]
            }, {
                name: 'Fixed Costs',
                y: -342000
            }, {
                name: 'Variable Costs',
                y: -233000
            }, {
                name: 'Balance',
                isSum: true,
                color: colors[2]
            }],
            dataLabels: {
                enabled: true,
                formatter: "function () {
                    return Highcharts.numberFormat(this.y / 1000, 0, ',') + 'k';
                }",
                style: {
                    color: '##FFFFFF',
                    fontWeight: 'bold',
                    textShadow: '0px 0px 3px black'
                }
            },
            pointPadding: 0
        }]
        }>

    <cfinclude template="/#application.sPath#/menu.cfm">
    <div id="container"></div>
    <cfhighchart attributeCollection="#stChart#" createContainer="false">

    </body>
</html>
