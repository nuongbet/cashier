<cfinclude template="/#application.sPath#/header.cfm">
		<cfscript>
    	
    aColors = ["##2f7ed8", "##0d233a", "##8bbc21", "##910000", "##1aadce", "##492970", "##f28f43", "##77a1e5", "##c42525", "##a6c96a"];
    colors = [];
    aColors.each(function(color) {
            colors.append({
                    radialGradient: { cx: 0.5, cy: 0.5, r: 0.7 },
                    stops: [
                        [0, color],
                        [1, brighten(color, -0.4)]
                    ]
                });
            }
        );
    </cfscript>

<cfset stChart = {
            chart: {
                renderTo:"container",
                plotBackgroundColor: 'null',
                plotBorderWidth: 'null',
                plotShadow: false
            },
            colors: colors,
            title: {
                text: 'Browser market shares at a specific website, 2010'
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '##000000',
                        connectorColor: '##000000',
                        formatter: "function() {
                            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                        }"
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data: [
                    ['Firefox',   45.0],
                    ['IE',       26.8],
                    {
                        name: 'Chrome',
                        y: 12.8,
                        sliced: true,
                        selected: true
                    },
                    ['Safari',    8.5],
                    ['Opera',     6.2],
                    ['Others',   0.7]
                ]
            }]
        }>

    <cfinclude template="/#application.sPath#/menu.cfm">
    <div id="container"></div>
    <cfhighchart attributeCollection="#stChart#" createContainer="false">

    </body>
</html>

<cfscript>
    string function brighten(required string sColor, required numeric iFactor) {
        local.sColor = replace(arguments.sColor, "##", "", "ALL");
        local.sRed   = inputBaseN(left(sColor, 2), 16);
        local.sGreen = inputBaseN(mid(sColor, 3, 2), 16);
        local.sBlue  = inputBaseN(right(sColor, 2), 16);
        sRed   = formatBaseN(round(min(255, sRed * (1+arguments.iFactor))), 16);
        sGreen = formatBaseN(round(min(255, sGreen * (1+arguments.iFactor))), 16);
        sBlue  = formatBaseN(round(min(255, sBlue * (1+arguments.iFactor))), 16);
        return "##" & sRed & sGreen & sBlue;
    }
</cfscript>
