<div id="menu">
	<h1>Highcharts Examples</h1>
	<div class="menu">
		<h4 class="smaller">Line charts</h4>
		<cfoutput>
		<ul>
			<li><a href="/#application.sPath#/examples/line-basic.cfm">Basic line</a></li>
			<li><a href="/#application.sPath#/examples/line-labels.cfm">With data labels</a></li>
			<li><a href="/#application.sPath#/examples/line-time-series.cfm">Time series, zoomable</a></li>
			<li><a href="/#application.sPath#/examples/spline-inverted.cfm">Spline with inverted axes</a></li>
			<li><a href="/#application.sPath#/examples/spline-symbols.cfm">Spline with symbols</a></li>
			<li><a href="/#application.sPath#/examples/spline-plot-bands.cfm">Spline with plot bands</a></li>
			<li><a href="/#application.sPath#/examples/spline-irregular-time.cfm">Time data with irregular intervals</a></li>
			<li><a href="/#application.sPath#/examples/line-log-axis.cfm">Logarithmic axis</a></li>
		</ul>
	</div>
	<div class="menu">
		<h4 class="smaller">Area charts</h4>
		<ul>
			<li><a href="/#application.sPath#/examples/area-basic.cfm">Basic area</a></li>
			<li><a href="/#application.sPath#/examples/area-negative.cfm">Area with negative values</a></li>
			<li><a href="/#application.sPath#/examples/area-stacked.cfm">Stacked area</a></li>
			<li><a href="/#application.sPath#/examples/area-stacked-percent.cfm">Percentage area</a></li>
			<li><a href="/#application.sPath#/examples/area-missing.cfm">Area with missing points</a></li>
			<li><a href="/#application.sPath#/examples/area-inverted.cfm">Inverted axes</a></li>
			<li><a href="/#application.sPath#/examples/areaspline.cfm">Area-spline</a></li>
			<li><a href="/#application.sPath#/examples/arearange.cfm">Area range</a></li>
			<li><a href="/#application.sPath#/examples/arearange-line.cfm">Area range and line</a></li>
		</ul>
	</div>
	<div class="menu">
		<h4 class="smaller">Column and bar charts</h4>
		<ul>
			<li><a href="/#application.sPath#/examples/bar-basic.cfm">Basic bar</a></li>
			<li><a href="/#application.sPath#/examples/bar-stacked.cfm">Stacked bar</a></li>
			<li><a href="/#application.sPath#/examples/bar-negative-stack.cfm">Bar with negative stack</a></li>
			<li><a href="/#application.sPath#/examples/column-basic.cfm">Basic column</a></li>
			<li><a href="/#application.sPath#/examples/column-negative.cfm">Column with negative values</a></li>
			<li><a href="/#application.sPath#/examples/column-stacked.cfm">Stacked column</a></li>
			<li><a href="/#application.sPath#/examples/column-stacked-and-grouped.cfm">Stacked and grouped column</a></li>
			<li><a href="/#application.sPath#/examples/column-stacked-percent.cfm">Stacked percentage column</a></li>
			<li><a href="/#application.sPath#/examples/column-rotated-labels.cfm">Column with rotated labels</a></li>
			<li><a href="/#application.sPath#/examples/column-drilldown.cfm">Column with drilldown</a></li>
			<li><a href="/#application.sPath#/examples/column-parsed.cfm">Data defined in a HTML table</a></li>
			<li><a href="/#application.sPath#/examples/columnrange.cfm">Column range</a></li>
		</ul>
	</div>
	<div class="menu">
		<h4 class="smaller">Pie charts</h4>
		<ul>
			<li><a href="/#application.sPath#/examples/pie-basic.cfm">Pie chart</a></li>
			<li><a href="/#application.sPath#/examples/pie-gradient.cfm">Pie with gradient fill</a></li>
			<li><a href="/#application.sPath#/examples/pie-donut.cfm">Donut chart</a></li>
			<li><a href="/#application.sPath#/examples/pie-legend.cfm">Pie with legend</a></li>
		</ul>
	</div>
	<div class="menu">
		<h4 class="smaller">Scatter and bubble charts</h4>
		<ul>
			<li><a href="/#application.sPath#/examples/scatter.cfm">Scatter plot</a></li>
			<li><a href="/#application.sPath#/examples/bubble.cfm">Bubble chart</a></li>
			<li><a href="/#application.sPath#/examples/bubble-3d.cfm">3D bubbles</a></li>
		</ul>
	</div>
	<div class="menu">
		<h4 class="smaller">Combinations</h4>
		<ul>
			<li><a href="/#application.sPath#/examples/combo.cfm">Column, line and pie</a></li>
			<li><a href="/#application.sPath#/examples/combo-dual-axes.cfm">Dual axes, line and column</a></li>
			<li><a href="/#application.sPath#/examples/combo-multi-axes.cfm">Multiple axes</a></li>
			<li><a href="/#application.sPath#/examples/combo-regression.cfm">Scatter with regression line</a></li>
		</ul>
	</div>
	<div class="menu">
		<h4 class="smaller">More chart types</h4>
		<ul>
			<li><a href="/#application.sPath#/examples/dynamic-click-to-add.cfm">Click to add a point</a></li>
			<li><a href="/#application.sPath#/examples/polar.cfm">Polar chart</a></li>
			<li><a href="/#application.sPath#/examples/polar-spider.cfm">Spiderweb</a></li>
			<li><a href="/#application.sPath#/examples/polar-wind-rose.cfm">Wind rose</a></li>
			<li><a href="/#application.sPath#/examples/box-plot.cfm">Box plot</a></li>
			<li><a href="/#application.sPath#/examples/error-bar.cfm">Error bar</a></li>
			<li><a href="/#application.sPath#/examples/waterfall.cfm">Waterfall</a></li>
			<li><a href="/#application.sPath#/examples/funnel.cfm">Funnel chart</a></li>
			<li><a href="/#application.sPath#/examples/renderer.cfm">General drawing</a></li>
		</ul>
	</div>
	</cfoutput>
</div>
<div style="clear:both"></div>