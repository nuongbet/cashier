<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Highcharts Example</title>
    </head>
	<body>
    <cfset colors = ["##2f7ed8", "##0d233a", "##8bbc21", "##910000", "##1aadce", "##492970", "##f28f43", "##77a1e5", "##c42525", "##a6c96a"]>
	<style>
		body {
			font-family: "verdana";
			font-size: 11px;
		}

		h1 {
			margin: 10px 0;
		}

		h4.smaller {
			margin: 0;
		}

		ul {
			margin-top: 5px;
			padding-left: 0px;
			list-style: none; 
			margin-bottom: 5px;
		}
		#container {
			margin-top:25px;
			min-width: 800px;
			min-height: 600px;
		}

		.menu {
			float:left;
			padding:0 20px;
			border-right:1px solid silver;
			height:220px;
		}

		a, a:visited {
			text-decoration:none;
			color:grey;
		}

		a:hover {
			text-decoration: underline;
		}

	</style>
