component {
	this.name="highcharts";

	function onApplicationStart() {
		application.sPath = contractPath(getDirectoryFromPath(getCurrentTemplatePath()));
		if (left(application.sPath, 1) eq "/") {
			application.sPath = mid(application.sPath, 2);
		}
	}

}