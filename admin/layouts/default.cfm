<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta charset="utf-8" />
		<title>Cashier</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="/assets/css/ace.min.css" id="main-ace-style" />
		<link rel="stylesheet" href="/assets/css/colorpicker.css" />
		<link rel="stylesheet" href="/css/style.css" id="main-ace-style" />
		<link rel="stylesheet" href="/css/bootstrap-select.css" />

		<link rel="stylesheet" href="/assets/css/jquery-ui.custom.min.css" />
		<link rel="stylesheet" href="/assets/css/chosen.css" />
		<link rel="stylesheet" href="/assets/css/datepicker.css" />
		<link rel="stylesheet" href="/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="/assets/css/bootstrap-datetimepicker.css" />
		<link rel="stylesheet" href="/assets/css/jquery.dataTables.css" />
		<link rel="stylesheet" href="/assets/css/dataTables.tableTools.css" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/assets/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
		<!--[if lte IE 9]>
		<script src="/assets/js/highcharts.js"></script>
		  <link rel="stylesheet" href="/assets/css/ace-ie.min.css" />
		<![endif]-->
		<script src="/js/jquery.js"></script>
		<script src="/assets/js/ace-extra.min.js"></script>
		<script src="/assets/js/jquery.dataTables.min.js"></script>
		<script src="/assets/js/dataTables.tableTools.js"></script>
		<script src="/assets/js/jquery.dataTables.bootstrap.js"></script>
		<script src="/assets/js/bootstrap-colorpicker.min.js"></script>

	</head>
	<cfoutput>
	<body class="no-skin">
        #view("common/header")#
        <div class="main-container" id="main-container">
            <!--- #view("common/menu")# --->
            <div class="main-content main-nopadding">
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12 wraper">
                            #body#
                        </div>
                    </div>
                </div>
              </div>
              #view( "common/footer" )#
            </div>
        </div>
	</cfoutput>
	</body>
</html>
