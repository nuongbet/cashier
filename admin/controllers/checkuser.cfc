<cfcomponent>

<cffunction name="chkUser" access="remote" returnformat="json" returntype="boolean" output="false">
    <cfargument name="user" type="string" required="true">
    <cfquery name="qchkUser">
    SELECT *
    FROM user 
    WHERE username = <cfqueryparam value="#arguments.user#" sqltype="varchar" />
    </cfquery>
    <cfreturn yesNoFormat(qchkUser.recordCount) />
</cffunction>

<cffunction name="chkEmail" access="remote" returnformat="json" returntype="boolean" output="false">
    <cfargument name="email" type="string" required="true">
    <cfquery name="qchkEmail">
    SELECT *
    FROM user 
    WHERE email = <cfqueryparam value="#arguments.email#" sqltype="varchar" />
    </cfquery>
    <cfreturn yesNoFormat(qchkEmail.recordCount) />
</cffunction>

<cffunction name="chkCode" access="remote" returnformat="json" returntype="boolean" output="false">
    <cfargument name="fcode" type="string" required="true">
    <cfquery name="qchkCode">
    SELECT *
    FROM food 
    WHERE fcode = <cfqueryparam value="#arguments.fcode#" sqltype="varchar" />
    AND shopId= <cfqueryparam value="#SESSION.ShopId#" sqltype="integer" />
    </cfquery>
    <cfreturn yesNoFormat(qchkCode.recordCount) />
</cffunction>

<cffunction name="chkKeyCode" access="remote" returnformat="json" returntype="boolean" output="false">
    <cfargument name="kcode" type="string" required="true">
    <cfquery name="qchkCode">
    SELECT *
    FROM food 
    WHERE keycode = <cfqueryparam value="#arguments.kcode#" sqltype="varchar" />
    AND shopId= <cfqueryparam value="#SESSION.ShopId#" sqltype="integer" />
    </cfquery>
    <cfreturn yesNoFormat(qchkCode.recordCount) />
</cffunction>

</cfcomponent>