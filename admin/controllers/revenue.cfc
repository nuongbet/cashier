component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}
	public any function getTotalByDate(struct rc) {
		var total = QueryExecute("SELECT ifnull(sum(total),0) as total from bill where DATE_FORMAT(datecreate,'%d-%m-%Y') ='"&idate&"'");
		variables.fw.RenderData("json",total.total[1]);
	}
	public any function getTotalByRangeDate(struct rc) {
		var fdate=#dateformat(fdate,'yyyy-mm-dd')#;
		var tdate=#dateformat(tdate,'yyyy-mm-dd')#;
		var total = QueryExecute("SELECT ifnull(sum(total),0) as total from bill where date(datecreate) BETWEEN '"&fdate&"' AND '"&tdate&"' ");
		variables.fw.RenderData("json",total.total[1]);
	}
}