component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}


	public  function daily(struct rc) {

		rc.liststockneed=QueryExecute("SELECT material.*,unit.name as uname FROM material 
									inner join unit on material.unitId=unit.unitId 
									where actualstock < needstock 
									and actualstock > ministock
									and isactive=1
									and material.shopId=:shopId",{shopId=SESSION.ShopId});
		rc.liststockmini=QueryExecute("SELECT material.*,unit.name as uname FROM material 
									inner join unit on material.unitId=unit.unitId 
									where actualstock < ministock
									and isactive=1
									and material.shopId=:shopId",{shopId=SESSION.ShopId});

		rc.topFoodSell = QueryExecute("
  			Select `order` .fname as name, sum(`order`.quantity) as quantity 
  			FROM  `order` 
  			WHERE `order`.quantity is not null and `order`.starttime BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE() and shopId=#SESSION.shopId# group by `order`.foodId
		");	
		param name="URL.idate" default=#now()#;
		param name="URL.rdate" default="1";
		rc.totalcost=QueryExecute("select ifnull(sum(amount),0) + (select ifnull(sum(amount),0) as payment 
												from receipt where year(datereceipt)=year(:idate) 
												and date(datereceipt)=:idate
												and shopId=#SESSION.shopId# and isreceipt=0 and tag=0) as inputtotal 
												from import where year(datecreate)=year(:idate) 
												and date(datecreate)=:idate
												and shopId=#SESSION.shopId#
											union all
											select ifnull(sum(amount),0) + (select ifnull(sum(amount),0) as payment 
												from receipt where year(datereceipt)=year(:idate) 
												and date(datereceipt)=:idate - interval 1 day
												and shopId=#SESSION.shopId# and isreceipt=0 and tag=0) as inputtotal 
												from import where year(datecreate)=year(:idate) 
												and date(datecreate)=:idate - interval 1 day
												and shopId=#SESSION.shopId#",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		

		rc.revenue=QueryExecute("select ifnull(sum(total),0) + (select ifnull(sum(amount),0) as receipt 
												from receipt where year(datereceipt)=year(:idate) 
												and date(datereceipt)=:idate
												and shopId=#SESSION.shopId# and isreceipt=1 and tag=0) as revenue,count(*) as turnover 
											from bill where year(datecreate)=year(:idate) 
											and date(datecreate)=:idate
											and shopId=#SESSION.shopId#
											union all
											select ifnull(sum(total),0) + (select ifnull(sum(amount),0) as receipt 
												from receipt where year(datereceipt)=year(:idate) 
												and date(datereceipt)=:idate - interval 1 day
												and shopId=#SESSION.shopId# and isreceipt=1 and tag=0) as revenue,count(*) as turnover 
											from bill where year(datecreate)=year(:idate) 
											and date(datecreate)=:idate - interval 1 day
											and shopId=#SESSION.shopId#",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});


		rc.SaleFoodType=QueryExecute("select c.categoryId, c.categoryname, cat.quantity, highest.name, highest.quantity as quantityItem
									from  category c 
									inner join (select sum(quantity) as quantity,f.categoryId
									from cashier.order o 
									inner join food f on f.foodId=o.foodId 
									where date(starttime)=:idate
									and f.shopid=#SESSION.shopId#
									group by f.categoryId) as cat on cat.categoryId=c.categoryId 
									inner join (
									select items.name, items.foodId,items.quantity, @row_number:=CASE WHEN @categoryId=categoryId THEN @row_number+1 ELSE 1 END AS row_number,@categoryId:=categoryId AS categoryId
									from (
									select sum(quantity) as quantity, f.foodid, f.name, f.categoryId  
									 from cashier.order o 
									inner join food f on f.foodId=o.foodId  
									where date(starttime)=:idate
									and f.shopid=#SESSION.shopId#
									group by f.foodId, f.categoryId , f.name ) as items,  (SELECT @row_number:=0,@categoryId:=0) AS t
									ORDER BY items.categoryId,quantity desc) as highest on highest.row_number =1  and highest.categoryId=c.categoryId",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		rc.ListStatisticSheet = QueryExecute("select statisticsheet.statisticsheetId,statisticsheet.materialId,round(sum(statisticsheet.beginbalance),2) as beginbalance,round(sum(statisticsheet.endingbalance),2) as endingbalance,round(sum(inputquantity),2) as inputquantity,round(sum(statisticsheet.adjustquantity),2) as adjustquantity,round(sum(statisticsheet.outputquantity),2) as outputquantity,statisticsheet.userId,statisticsheet.datecreate,material.name,user.username 
			from statisticsheet,material,user 
			where statisticsheet.materialId=material.materialId 
			and statisticsheet.userId=user.userId 
			and year(datecreate)=year(:idate)
			and case when :rdate=1 then date(datecreate)=:idate 
					 when :rdate=2 then week(datecreate,1)=week(:idate,1)
					 when :rdate=3 then month(datecreate)=month(:idate) end
			and statisticsheet.shopId=#SESSION.shopId#
			and material.isactive=1
			group by statisticsheet.materialId 
			order by statisticsheet.materialId desc",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		rc.DailyRevenueByShift=QueryExecute("select shift,ifnull(sum(total),0)as dailyrevenue from bill 
											where date(datecreate)=:idate 
											and shopId=#SESSION.shopId#
											group by shift",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		rc.DailyRevenueByOrderType=QueryExecute("select name as ordername,ifnull(sum(total),0)as dailyrevenue from bill inner join type on bill.typeId=type.typeId
												where date(datecreate)=:idate 
												and bill.shopId=#SESSION.shopId#
												group by bill.typeId",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});


		rc.dailydashboard = QueryExecute("select * from dailydashboard where date(datecreate)=date(#now()#) and shopId=#SESSION.shopId#");

		var checkdate = QueryExecute("select datecreate from dailydashboard WHERE date(datecreate) =:idate and shopId=#SESSION.shopId#",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		if(CGI.REQUEST_METHOD EQ "POST" and isNull(rc.username)){
			if(checkdate.recordcount == 0){
				QueryExecute("insert into dailydashboard(note,datecreate,shopId) values(:note,:idate,#SESSION.shopId#)",{note=rc.txtNote,idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT")});
				endingStatisticForOneShop(SESSION.ShopId);
				
			} else {
				QueryExecute("update dailydashboard set note=:note, datecreate=:idate where id=:id",{note=rc.txtNote,idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),id=rc.dailydashboard.id[1]});
				endingStatisticForOneShop(SESSION.ShopId);
				
			}	
		}
	}
	
	public any function endingStatisticForOneShop(required any shopId) {

				//update inputquantity
				var ListInput=QueryExecute("select materialId,sum(quantity) as quantity from importsheet where date(datecreate)=date(:dNow) and shopId=#shopId# group by materialId",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				for(iInput in ListInput){
					QueryExecute("update statisticsheet set inputquantity=:iquantity where materialId=:MId and date(datecreate)=date(:dNow)",{iquantity=iInput.quantity,MId=iInput.materialId,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}

				//update outputquantity
				var ListOutput=QueryExecute("select materialId,(actualtemp-ordertemp)as quantity from material where actualtemp <> ordertemp and shopId=#shopId#");
				for(iOutput in ListOutput){
					QueryExecute("update statisticsheet set outputquantity = outputquantity + :iquantity where materialId=:MId and date(datecreate)=date(:dNow)",{iquantity=iOutput.quantity,MId=iOutput.materialId,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
					QueryExecute("update material m inner join material temp on
									    m.materialId = temp.materialId
										set m.ordertemp = temp.actualtemp where m.materialId=:MId",{MId=iOutput.materialId});
				}

				//update adjustquantity
				var ListAdjust=QueryExecute("select materialId,sum(fixnumber)as quantity from adjustsheet where date(datecreate)=date(:dNow) and shopId=#shopId# group by materialId",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				for(iAdjust in ListAdjust){
					QueryExecute("update statisticsheet set adjustquantity=:iquantity where materialId=:MId and date(datecreate)=date(:dNow)",{iquantity=iAdjust.quantity,MId=iAdjust.materialId,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}
				//update endingbalance
				QueryExecute("update statisticsheet s inner join statisticsheet temp on
									    s.statisticsheetId = temp.statisticsheetId
										set s.endingbalance = round(temp.beginbalance+temp.inputquantity+temp.adjustquantity-temp.outputquantity,2) where date(temp.datecreate)=date(:dNow)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				//checkEnding
				var checkEnding=QueryExecute("select * from statisticsheet where date(datecreate)=date(:dNow + interval 1 day) and shopId=#shopId#",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				//render statisticsheet next day.
				if(checkEnding.recordcount==0){
					QueryExecute("insert into statisticsheet(materialId,beginbalance,endingbalance,inputquantity,outputquantity,adjustquantity,userId,datecreate,shopId)
					select materialId,endingbalance,0,0,0,0,:UId,:dNow + interval 1 day,#shopId# from statisticsheet where date(datecreate)=date(:dNow) and shopId=#shopId#",{UId=4,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}
				else{
					//it's exist so update it.
					QueryExecute("update statisticsheet a 
						inner join statisticsheet b on
						    a.materialID = b.materialID and date(a.datecreate)  = date(:dNow  +  interval 1 day) and date( b.datecreate)  = date(:dNow) and a.shopId=#shopId# and a.shopId=b.shopId
						set
						    a.beginbalance = b.endingbalance",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}

	}

	public  function weekly(struct rc) {
		param name="URL.idate" default=#now()#;
		param name="URL.rdate" default="2";
		rc.totalcost=QueryExecute("select ifnull(sum(amount),0) + (select ifnull(sum(amount),0) as payment 
												from receipt where year(datereceipt)=year(:idate) 
												and week(datereceipt,1)=week(:idate,1)
												and shopId=#SESSION.shopId# and isreceipt=0 and tag=0) as inputtotal 
												from import where year(datecreate)=year(:idate) 
												and week(datecreate,1)=week(:idate,1)
												and shopId=#SESSION.shopId#
											union all
											select ifnull(sum(amount),0) + (select ifnull(sum(amount),0) as payment 
												from receipt where year(datereceipt)=year(:idate) 
												and week(datereceipt,1)=week(:idate,1)-1
												and shopId=#SESSION.shopId# and isreceipt=0 and tag=0) as inputtotal 
												from import where year(datecreate)=year(:idate) 
												and week(datecreate,1)=week(:idate,1)-1
												and shopId=#SESSION.shopId#",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});
		

		rc.revenue=QueryExecute("select ifnull(sum(total),0) + (select ifnull(sum(amount),0) as payment 
												from receipt where year(datereceipt)=year(:idate) 
												and week(datereceipt,1)=week(:idate,1)
												and shopId=#SESSION.shopId# and isreceipt=1 and tag=0) as revenue,count(*) as turnover 
											from bill where year(datecreate)=year(:idate) 
											and week(datecreate,1)=week(:idate,1)
											and shopId=#SESSION.shopId#
											union all
											select ifnull(sum(total),0) + (select ifnull(sum(amount),0) as payment 
												from receipt where year(datereceipt)=year(:idate) 
												and week(datereceipt,1)=week(:idate,1)-1
												and shopId=#SESSION.shopId# and isreceipt=1 and tag=0) as revenue,count(*) as turnover 
											from bill where year(datecreate)=year(:idate) 
											and week(datecreate,1)=week(:idate,1)-1
											and shopId=#SESSION.shopId#",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		rc.SaleFoodType=QueryExecute("select c.categoryId, c.categoryname, cat.quantity, highest.name, highest.quantity as quantityItem
									from  category c 
									inner join (select sum(quantity) as quantity,f.categoryId
									from cashier.order o 
									inner join food f on f.foodId=o.foodId 
									where week(starttime,1)=week(:idate,1)
									and year(starttime)=year(:idate)
									and f.shopid=#SESSION.shopId#
									group by f.categoryId) as cat on cat.categoryId=c.categoryId 
									inner join (
									select items.name, items.foodId,items.quantity, @row_number:=CASE WHEN @categoryId=categoryId THEN @row_number+1 ELSE 1 END AS row_number,@categoryId:=categoryId AS categoryId
									from (
									select sum(quantity) as quantity, f.foodid, f.name, f.categoryId  
									 from cashier.order o 
									inner join food f on f.foodId=o.foodId  
									where week(starttime,1)=week(:idate,1)
									and year(starttime)=year(:idate)
									and f.shopid=#SESSION.shopId#
									group by f.foodId, f.categoryId , f.name ) as items,  (SELECT @row_number:=0,@categoryId:=0) AS t
									ORDER BY items.categoryId,quantity desc) as highest on highest.row_number =1  and highest.categoryId=c.categoryId",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		rc.ListStatisticSheet = QueryExecute("
			select *,round(beginbalance+inputquantity+adjustquantity-outputquantity,2) as endingbalance from (
			select statisticsheet.statisticsheetId,statisticsheet.materialId,ifnull((select ifnull(beginbalance,0) from statisticsheet as temp where date(datecreate)=SUBDATE(:idate, WEEKDAY(:idate)) and temp.materialId=statisticsheet.materialId),0) as beginbalance
			,round(sum(inputquantity),2) as inputquantity
			,round(sum(statisticsheet.adjustquantity),2) as adjustquantity
			,round(sum(statisticsheet.outputquantity),2) as outputquantity
			,statisticsheet.userId,statisticsheet.datecreate,material.name,user.username 
			from statisticsheet,material,user 
			where statisticsheet.materialId=material.materialId 
			and statisticsheet.userId=user.userId 
			and year(datecreate)=year(:idate)
			and case when :rdate=1 then date(datecreate)=:idate 
					 when :rdate=2 then week(datecreate,1)=week(:idate,1)
					 when :rdate=3 then month(datecreate)=month(:idate) end
			and statisticsheet.shopId=#SESSION.shopId#
			and material.isactive=1
			group by statisticsheet.materialId 
			order by statisticsheet.materialId desc
			)newtable",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		rc.DailyRevenueByShift=QueryExecute("select shift,ifnull(sum(total),0)as dailyrevenue from bill 
											where week(datecreate,1)=week(:idate,1) and year(datecreate)=year(:idate)
											and shopId=#SESSION.shopId# 
											group by shift",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		rc.DailyRevenueByOrderType=QueryExecute("select name as ordername,ifnull(sum(total),0)as dailyrevenue from bill inner join type on bill.typeId=type.typeId
												where week(datecreate,1)=week(:idate,1) and year(datecreate)=year(:idate)
												and bill.shopId=#SESSION.shopId#
												group by bill.typeId",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});

		
	}
	

	public any function monthly(struct rc) {
		param name="URL.idate" default=#now()#;
		rc.ListStatisticSheet = QueryExecute("
			select *,round(beginbalance+inputquantity+adjustquantity-outputquantity,2) as endingbalance from (
			select statisticsheet.statisticsheetId,statisticsheet.materialId,
			ifnull((select ifnull(beginbalance,0) from statisticsheet as temp where date(datecreate)=DATE_SUB(:idate, INTERVAL DAYOFMONTH(:idate)-1 DAY) and temp.materialId=statisticsheet.materialId),0) as beginbalance
			,round(sum(inputquantity),2)as inputquantity
			,round(sum(statisticsheet.adjustquantity),2)as adjustquantity
			,round(sum(statisticsheet.outputquantity),2)as outputquantity
			,statisticsheet.userId,statisticsheet.datecreate,material.name,user.username 
				from statisticsheet,material,user 
				where statisticsheet.materialId=material.materialId 
				and statisticsheet.userId=user.userId 
				and year(datecreate)=year(:idate)
				and month(datecreate)=month(:idate)
				and statisticsheet.shopId=#SESSION.shopId#
				and material.isactive=1
				group by statisticsheet.materialId 
				order by statisticsheet.materialId desc
			)newtable
		",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT")});

	}
	
}