component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}


	public any function default(struct rc) {
		rc.shop = QueryExecute("select * from shop");
	}
	
	public any function form(struct rc){

		param name="URL.shopid" default=0;
		rc.user=QueryExecute("select user.userId, user.firstname, user.lastname, user.phone, user.username, user.isactive, type.name as typeUser 
		from user, type
		where user.typeId = type.typeId
		and user.shopId=#URL.shopid#
		and user.shopId!=0");

		if (URL.shopid > 0) rc.shop = entityLoad("shop",rc.shopId,true);
		if (isNull(rc.shop)) { 
			rc.shop = EntityNew("shop");
		}
		else 
		{
			rc.shop=entityLoad("shop",{shopId=rc.shopId},true);
		}
		if(CGI.REQUEST_METHOD=="POST")
		{		
			rc.shop.setname(rc.name);
			rc.shop.setaddress(rc.address);
			rc.shop.setphone(rc.phone);
			rc.shop.setemail(rc.email);
			rc.shop.settaxcode(rc.taxcode);
			rc.shop.setmanagername(rc.managername);
			if(rc.fileimage != "")
			{
				var f1=fileupload("#expandpath('/images/shop')#","logo","","makeunique");
				rc.shop.setlogo(f1.serverfile);
			}
			if(rc.fileimage1 != "")
			{
				var f2=fileupload("#expandpath('/images/shop')#","logobill","","makeunique");
				rc.shop.setlogobill(f2.serverfile);
			}
			entitySave(rc.shop);
			if(#URL.shopId# == 0){
				location(url="/index.cfm/admin:shop.form?shopid=#rc.shop.getshopid()#", addtoken="no");
			}
			else{
				location(url="/index.cfm/admin:shop", addtoken="no");
			}
		}
	}

}