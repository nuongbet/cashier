component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

    public array function queryToArray( required query qry ) {
	    var columns = arguments.qry.getColumnNames();
	    var ofTheJedi = [];

	    for( var i = 1; i LTE qry.recordCount; i++ ) {
	        var obj = {};

	        for( var k = 1; k LTE arrayLen( columns ); k++ ) {
	            structInsert( obj, columns[ k ], arguments.qry[ columns[ k ] ][ i ] );
	        }

	        arrayAppend( ofTheJedi, obj );
	    }

	    return ofTheJedi;
	}


	public any function default(struct rc){
		param name="URL.loadDelItems" default=1;
		rc.food = QueryExecute("select f.categoryId,c.categoryname,f.foodId, f.name,f.price,f.isactive,f.estimateprice,f.margin,f.percentage,f.fsorted,f.fcode
								from category as c , food as f 
								where c.categoryId=f.categoryId
								and c.shopId=#SESSION.shopId#
								and f.isactive=#URL.loadDelItems#
								order by f.fsorted");
		}

	public any function getestimatecurrent(required any Fid, required any check) {
		var result = arrayNew(1);
		var estimatecurrent = QueryExecute("
			SELECT foodId,ifnull(round(sum(estimate*m.unitprice),2),0) as estimateprice 
			FROM foodmaterial fm inner join material m on fm.materialId=m.materialId and fm.shopId=m.shopId
			where fm.foodId=:Fid
			group by fm.foodId
		",{Fid=FId});
		var es = 0;
		if(estimatecurrent.recordCount != 0){
			es = estimatecurrent.estimateprice[1];
		}
		QueryExecute("update food set estimateprice=#es# where foodid=:Fid",{Fid=FId});
		result[1]=check;
		result[2]=es;
		return result;
	}

	public any function delete(struct rc) {
		var food=entityLoad("food",{foodId=rc.id},true);
		entityDelete(food);
		variables.fw.redirect("food");
	}

	public any function updateFoodMaterial(struct rc) {
		var rs = arrayNew(1);
		QueryExecute("update foodmaterial set unitid=:UId,estimate=:estimate where foodmaterialId=:FMId",{UId=UId,estimate=estimate,FMId=FMId});
		rs = getestimatecurrent(Fid,true);
		variables.fw.RenderData("json",rs);
	}

	public any function insertFoodMaterial(struct rc) {
		var check=false;
		var fm=entityLoad("foodmaterial",{foodId:FId,materialId:MId},true);
		var rs = arrayNew(1);
		if(isnull(fm)){
			fm=entityNew("foodmaterial");
			fm.setFoodId(FId);
			fm.setMaterialId(MId);
			fm.setUnitId(UId);
			fm.setEstimate(estimate);
			fm.setShopId(SESSION.shopId);
			entitySave(fm);
			check = fm.getfoodmaterialId();
			rs = getestimatecurrent(FId,check);
		} else {
			rs[1] = check;
		}
		variables.fw.RenderData("json",rs);
	}

	public any function deleteMaterial(struct rc) {
		QueryExecute("delete from foodmaterial where foodmaterialId=:FMId",{FMId=FMId});
		var rs = arrayNew(1);
		rs = getestimatecurrent(Fid,true);
		variables.fw.RenderData("json",rs);
	}
	
	
	public any function form(struct rc){
		param name="URL.id" default=0;
		param name="rc.isactive" default =0;

		rc.ListUnit=QueryExecute("select * from type where tag='unit' and isActive=1 and shopId=#SESSION.shopId#");

		rc.ListMaterial=QueryExecute("
			select m.materialId, m.name as materialname, u.name as unitname, u.unitid, m.unitprice
			from material as m
			inner join unit as u on m.unitId = u.unitId 
			and m.shopId=#SESSION.shopId#
			and m.isactive=1
		");

		rc.ListType = QueryExecute("select * from category as c where c.parentId=6 and c.isActive=1 and c.shopId=#SESSION.shopId#");
		rc.getcolor = QueryExecute("select foodId, color from food where color is not null and color != '' and shopId=#SESSION.shopId#  group by color limit 10");
		
		if(rc.ListType.recordCount == 0){
			location(url="/index.cfm/admin:category.form?ca=6&id=0",addtoken="no");
		}
		rc.food = entityLoad("food",{foodId=rc.id},true);
		rc.FoodMaterial=QueryExecute("select foodmaterial.*,material.name,material.isactive as misactive,unit.name as uname, unitprice, estimate * unitprice as estimateprice 
										from foodmaterial,material ,unit
										where foodmaterial.materialId=material.materialId 
										and foodmaterial.unitId=unit.unitId 
										and foodmaterial.foodId=:fId 
										and foodmaterial.shopId=#SESSION.shopId#",{fId=rc.id});
	
		if (URL.id > 0) rc.food = entityLoad("food",{foodId=rc.id},true);
		if (isNull(rc.food)) { 
			rc.food = EntityNew("food");
			rc.foodId=0;
		}
		else{
			rc.foodId=rc.food.getfoodId();
		}

		if(CGI.REQUEST_METHOD=="POST")
		{
			rc.food.setname(rc.name);
			rc.food.setcategoryId(rc.type);
			rc.food.setprice(replace(rc.price,",","","All"));
			rc.food.setisactive(rc.isactive);
			rc.food.setcolor(rc.color);
			rc.food.setShopId(SESSION.shopId);
			rc.food.setmargin(replace(rc.margin,",","","All"));
			rc.food.setpercentage(rc.percentage);
			rc.food.setfsorted(rc.fsorted);
			rc.food.setfcode(rc.fcode);
			rc.food.setkeycode(rc.keycode);
			rc.food.setfunit(rc.funit);
			entitySave(rc.food);
			rc.foodId=rc.food.getfoodId();

			if(structKeyExists(rc,"_addAnother")) {
				variables.fw.redirect("food.form?id=0&_addAnother=true");
			}
			else {
				variables.fw.redirect("food");
			}
		}
	}

	public any function getunitvsprice(struct rc) {
		var MaterialById=QueryExecute("
			select m.materialId, m.name as materialname, u.name as unitname, u.unitid, m.unitprice
			from material as m
			inner join unit as u on m.unitId = u.unitId and m.shopId=#SESSION.shopId#
			where m.materialId =:materialId
		",{materialId=materialId});
		variables.fw.renderData('JSON',queryToArray(MaterialById));
	}	
}