component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =fw;
		return this;
	}

	function default(struct rc)
	{
		rc.lang = QueryExecute("SELECT * FROM label");
		rc.list = QueryExecute("SELECT * FROM language");
	}

	function changekeyword(struct rc)
	{
		if(SESSION.isLogin){
			var query=QueryExecute("UPDATE label 
									SET keyword = '"&rc.key&"' 
									WHERE  labelId = "&id);
			variables.fw.renderData("text",true);
		}
		else variables.fw.renderData("text",false);
	}

	function changevalue(struct rc)
	{
		if(SESSION.isLogin){
			var query=QueryExecute("UPDATE label 
									SET value = '"&rc.key&"' 
									WHERE  labelId = "&id);
			variables.fw.renderData("text",true);
		}
		else variables.fw.renderData("text",false);
	}

	function changelanguageid(struct rc)
	{
		if(SESSION.isLogin){
			var query=QueryExecute("UPDATE label 
									SET languageId = '"&rc.languageId&"' 
									WHERE  labelId = "&id);
			variables.fw.renderData("text",true);
		}
		else variables.fw.renderData("text",false);
	}

	function addnewlabel(struct rc)
	{
		if(SESSION.isLogin){
			// var query=QueryExecute("INSERT INTO label(keyword, value, languageId) VALUES('"&rc.keyword&"','"&rc.value&"',"&rc.languageId&")");
			var label 		 = entityNew("label",{
				keyword 	 = rc.keyword,
				lastModified = now(),
				createDate	 = now(),
				value 		 = rc.value,
				language 	 = entityLoadbyPK("language",rc.languageId)
			});
			entitySave(label);
			var rs = {success:true, labelId:label.labelId}
			variables.fw.renderData("json",rs);
		}
		else variables.fw.renderData("text",false);
	}

	function deletelabel(struct rc)
	{
		if(SESSION.isLogin){
			var query=QueryExecute("DELETE FROM label WHERE  labelId = "&rc.labelId);
			variables.fw.renderData("text",true);
		}
		else variables.fw.renderData("text",false);
	}
}