component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw = arguments.fw;
		return this;
	}

	public function default(struct rc) {
		param name="URL.fdate" default="#now()#";
		param name="URL.tdate" default="#now()#";
		param name="URL.userId" default="All";
		rc.ListUser=QueryExecute("select * from user where shopId=#SESSION.ShopId# and isactive=1");
		rc.ListBill = QueryExecute("select bill.*,type.name as ordername,concat(user.lastname,' ',user.firstname)as staff
									from bill,type,user
									where bill.typeId=type.typeId 
									and bill.userId=user.userId
									and bill.shopId=#SESSION.ShopId#
									and date(bill.datecreate) between date(:fdate) and date(:tdate)
									and if(:id='All',1=1,bill.userId=:id)
									order by datecreate desc",{fdate=lsdateformat(URL.fdate,"yyyy/mm/dd","pt_PT"),tdate=lsdateformat(URL.tdate,"yyyy/mm/dd","pt_PT"),id=URL.userId});

		rc.amount=QueryExecute("select ifnull(sum(bill.total),0)as amount
									from bill,type,user
									where bill.typeId=type.typeId 
									and bill.userId=user.userId
									and bill.shopId=#SESSION.ShopId#
									and date(bill.datecreate) between date(:fdate) and date(:tdate)
									and if(:id='All',1=1,bill.userId=:id)
									group by date(datecreate)",{fdate=lsdateformat(URL.fdate,"yyyy/mm/dd","pt_PT"),tdate=lsdateformat(URL.tdate,"yyyy/mm/dd","pt_PT"),id=URL.userId});

	}

	public function detail(struct rc) {
		param name="URL.id" default="";
		rc.ListOrder = QueryExecute("select o.*,d.name as dname,o.quantity*o.paymentprice as total 
									from cashier.order o,desk d
									where o.deskId=d.deskId
									and o.shopId=#SESSION.ShopId#
									and o.billId=:billId",{billId=#URL.id#});
		rc.Bill = QueryExecute("select bill.*,type.name as ordername,concat(user.lastname,' ',user.firstname)as staff 
								from bill,type,user
								where bill.typeId=type.typeId
								and bill.userId=user.userId
								and bill.billId=:billId
								and bill.shopId=#SESSION.ShopId#
								order by datecreate desc",{billId:#URL.id#});
	}
	
	
	
}