component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc) {
		rc.ListShift=entityLoad("shift",{shopId=#SESSION.ShopId#});
	}


	public any function form(struct rc) {
		param name="URL.id" default=0;
		if (URL.id > 0) rc.shift = entityLoad("shift",{shiftId:URL.id},true);
		if (isNull(rc.shift)) { 
			rc.shift = EntityNew("shift");
		} 
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.shift.setname(rc.name);
			rc.shift.settimestart(rc.starttime);
			rc.shift.settimeend(rc.endtime);
			rc.shift.setshopId(#SESSION.ShopId#);
			entitySave(rc.shift);
			variables.fw.redirect("shift");
		}
	}

	public any function delete(struct rc) {
		var shift=entityLoad("shift",{shiftId=rc.id},true);
		entityDelete(shift);
		variables.fw.redirect("shift");
	}
	
}