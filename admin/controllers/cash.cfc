component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw = arguments.fw;
		return this;
	}

	public function default(struct rc) {
		
	}

	public function tonghopdoanhthu(struct rc) {
		param name="rc.dateselected" default=now();
		var fDate = lsDateFormat(rc.dateselected, "yyyy-mm-dd", "pt_PT");
		rc.doanhthungay= QueryExecute("select * from cashe where Date(ngaychot) = '#fDate#'");
		rc.doanhthuthang = QueryExecute("select * from cash where Month(ngaychot) = Month('#fDate#') and Year(ngaychot) = Year('#fDate#')");
		rc.doanhthunam = QueryExecute("select * from cash where Month(ngaychot) = Month('#fDate#') and Year(ngaychot) = Year('#fDate#')");
	}

	public function capnhatthucconyes(numeric tienlayrayes, numeric thuccondauyes, numeric thucconcuoiyes, date ngaychotyes, string ghichu) {
		
		var ngaychotformated = lsDateFormat( ngaychotyes, "yyyy-mm-dd", "pt_PT");
		var thuccon = QueryExecute("select * from cash where Date(ngaychot) = '#ngaychotformated#'");
		if(thuccon.recordcount > 0) {
			QueryExecute("update cash set thuccontondau = #LSParseNumber(thuccondauyes)#, ghichu = '#ghichu#', tienlayra = #LSParseNumber(tienlayrayes)#, thucconcuoi = #LSParseNumber(thucconcuoiyes)# where Date(ngaychot) = '#ngaychotformated#'");
		}
		else {
			var newthuccon = entitynew("cash");
			newthuccon.setngaychot(lsDateFormat( ngaychotyes, "yyyy-mm-dd", "pt_PT"));
			newthuccon.setthuccontondau(LSParseNumber(thuccondauyes));
			newthuccon.setthucconcuoi(LSParseNumber(thucconcuoiyes));
			newthuccon.settienlayra(LSParseNumber(tienlayrayes));
			newthuccon.setghichu(ghichu);
			entitysave(newthuccon);
		}

		variables.fw.renderData("json", true);
	}

	public function capnhatthuccontoday(numeric thucconcuoitoday, date ngaychottoday, string ghichu) {
		
		var ngaychotformated = lsDateFormat( ngaychottoday, "yyyy-mm-dd", "pt_PT");
		var thuccon = QueryExecute("select * from cash where Date(ngaychot) = '#ngaychotformated#'");
		if(thuccon.recordcount > 0) {
			QueryExecute("update cash set thuccontondau = #LSParseNumber(thucconcuoitoday)#, ghichu = '#ghichu#', thucconcuoi = #LSParseNumber(thucconcuoitoday)# where Date(ngaychot) = '#ngaychotformated#'");
		}
		else {
			var newthuccon = entitynew("cash");
			newthuccon.setngaychot(lsDateFormat( ngaychottoday, "yyyy-mm-dd", "pt_PT"));
			newthuccon.setthuccontondau(LSParseNumber(thucconcuoitoday));
			newthuccon.setthucconcuoi(LSParseNumber(thucconcuoitoday));
			newthuccon.setghichu(ghichu);
			entitysave(newthuccon);
		}

		variables.fw.renderData("json", true);
	}

	public function getcashbydate (date datecash) {
		var fdate = lsDateFormat( datecash, "yyyy-mm-dd", "pt_PT");
		var cash = QueryExecute("select * from cash where Date(ngaychot) = '#fdate#'");
		variables.fw.renderData("json", cash);
	}
}