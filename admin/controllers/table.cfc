component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc){
		param name="URL.loadDelItems" default=1;
		rc.table = QueryExecute("select d.categoryId,c.categoryname,d.deskId,d.status,d.type,d.isactive, d.name  
									from category as c 
									join desk as d 
									where c.categoryId=d.categoryId 
									and d.shopId=#SESSION.ShopId#
									and d.isactive=#URL.loadDelItems#");
		}

	public any function form(struct rc){
		rc.listPos = QueryExecute("select * from  category  
									where isActive=1 
									and parentId=2 
									and shopId=#SESSION.ShopId# 
									group by categoryId");
		if(rc.listPos.recordCount == 0){
			location(url="/index.cfm/admin:category.form?ca=2&id=0",addtoken="no");
		}
		rc.desk = entityLoad("desk",rc.id,true);

		if(CGI.REQUEST_METHOD=="POST")
		{
			if(rc.id ==0)
			{      		
				var desk=entityNew("desk");
				var msort=QueryExecute("SELECT ifnull(max(sorted),0)as msorted 
			 						FROM cashier.desk 
			 						WHERE categoryId=:ca",{ca=rc.pos});
				var sorted=1;
				if(msort.recordcount > 0){
					sorted=msort.msorted+1;
				}
				desk.setsorted(sorted);	
			}
			else 
			{
				var desk=entityLoad("desk",{deskId=rc.id},true);
			}
			desk.setname(rc.name);
			desk.setcategoryId(rc.pos);
			desk.settype(rc.type);
			if(not structKeyExists(rc, "isActive")) {
			   desk.setisactive(0);
			   }
			   else {
			    desk.setisactive(rc.isactive);
			   }	
			 desk.setShopId(SESSION.ShopId);
			entitySave(desk);
			variables.fw.redirect("table");
		}
	}

	public any function delete(struct rc) {
		var desk=entityLoad("desk",{deskId=rc.id},true);
		entityDelete(desk);
		variables.fw.redirect("table");
	}
}