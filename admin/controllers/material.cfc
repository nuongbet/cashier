component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc) {
		param name="URL.loadDelItems" default=1;
		
		//0:load all,1:load list needed,2:load list min
		param name="URL.type" default=0;
		rc.ListMaterial=QueryExecute("select material.*,unit.name as unitName 
			from material,unit 
			where material.unitId=unit.unitId 
			and material.shopId=:shopId
			and material.isactive=#URL.loadDelItems#
			and case when :tp = 1 then actualstock < needstock 
									and actualstock > ministock
									and isactive=1
									and material.shopId=:shopId
					  when :tp=2 then actualstock < ministock
									and isactive=1
									and material.shopId=:shopId
					else 1=1 end
			order by material.materialId desc",{shopId=#SESSION.ShopId#,tp=URL.type});
	}


	public any function form(struct rc) {
		param name="URL.id" default=0;
		param name="rc.isactive" default =0;
		rc.ListUnit=entityLoad("unit");
		if (URL.id > 0) rc.material = entityLoad("material",{materialId:URL.id},true);
		if (isNull(rc.material)) { 
			rc.material = EntityNew("material");
		} 
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.material.setname(rc.name);
			rc.material.setunitId(rc.unitId);
			rc.material.setneedstock(rc.need);
			rc.material.setministock(rc.mini);
			rc.material.setunitprice(replace(rc.unitprice,",","","All"));
			if(URL.id ==0){
				rc.material.setactualstock(0);
				rc.material.setactualtemp(10000);
				rc.material.setordertemp(10000);
			}
			rc.material.setshopId(SESSION.shopId);
			rc.material.setisactive(rc.isactive);
			entitySave(rc.material);
		
			//render materialId for statisticsheet if have new materialId in material
			QueryExecute("insert into statisticsheet(materialId,beginbalance,inputquantity,outputquantity,adjustquantity,endingbalance,datecreate,userId,shopId) 
										select materialId,0,0,0,0,0,:dNow,:UId,#SESSION.shopId# from material where material.materialId not in
										(select materialId from statisticsheet where  date(datecreate)=date(:dNow) and shopId=#SESSION.shopId# group by materialId) and shopId=#SESSION.shopId#",{UId=SESSION.UserID,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
			if(URL.id > 0){
				QueryExecute("update foodmaterial set unitId=:uId where materialId=:mId",{uId=rc.unitId,mId=URL.id});
				QueryExecute("update material set unitprice=:up where materialId=:mId",{up=replace(rc.unitprice,",","","All"),mId=URL.id});
				var listfood=QueryExecute("select foodId from foodmaterial 
											where materialId=:mId and shopId=:shopId
											group by foodId",{mId=URL.id,shopId=SESSION.ShopId});
				for(iFood in listfood){
					QueryExecute("update food f inner join 
									(SELECT foodId,ifnull(round(sum(estimate*m.unitprice),2),0) as estimatetemp 
									FROM foodmaterial fm inner join material m 
									on fm.materialId=m.materialId 
									and fm.shopId=m.shopId
									and fm.shopId=:shopId
									where fm.foodId=:fId
									group by fm.foodId)temp 
									on f.foodId=temp.foodId
									set estimateprice=temp.estimatetemp",{fId=iFood.foodId,shopId=SESSION.ShopId});
				}
			}
			variables.fw.redirect("material");
		}
	}

	public any function changeunitprice(struct rc) {
		var material = entityLoad('material',{materialId:id}, true);
		material.setunitprice(replace(unitprice,",","","All"));
		entitySave(material);
		variables.fw.renderData("JSON","true");
	}
	
	
}