component output="false" displayname=""  {
	
	public function init(required any fw){
		variables.fw =fw;
		return this;
	}
	function default(struct rc){
		if(structKeyExists(URL,"t")){
			var type=URL.t;
		}else{
			var type=1;
		}
		if(CGI.REQUEST_METHOD eq "POST" AND SESSION.isLogin){
			var query=QueryExecute(sql:"UPDATE email SET subject=:sub,container=:cont WHERE emailID="&type,
						params:{cont:{value=rc.contain,CFSQLType='string'},
								sub:{value=rc.subject,CFSQLType='string'}});
		}
		rc.listOption=QueryExecute("SELECT * FROM email");
		if(rc.listOption.recordCount eq 0){
			getDefaultData();
		}else{
		for(item in rc.listOption) {
			if(!structKeyExists(rc,"type")){
				rc.type=item;
			}else{
				if(item.emailID eq type){
		   			rc.type=item;
		   		}
			}
		}
		}
	}
	public void function getDefaultData(struct rc){
		var newEmailTemplate1=entityNew("email");
		newEmailTemplate1.setEmailKey("DailyReport");
		newEmailTemplate1.setSubject("Report $date$");
		newEmailTemplate1.setContainer("<h1>This email for report.</h1><p>$container$</p>");

		var newEmailTemplate2=entityNew("email");
		newEmailTemplate2.setEmailKey("Stock");
		newEmailTemplate2.setSubject("Alert stock $date$");
		newEmailTemplate2.setContainer("<p>$container$</p>");

		entitySave(newEmailTemplate1);
		entitySave(newEmailTemplate2);
	}
	
}	