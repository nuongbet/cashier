component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw = arguments.fw;
		return this;
	}

	public function default(struct rc) {
		var finaldate = lsDateFormat( now(), "yyyy-mm-dd", "pt_PT");
		var tdcashin 	= 0;
		var ttcashout 	= 0;
		var cashtondau 	= 0;

		rc.todaycashIn = QueryExecute("select * from cashin where Date(datecreate) = '#finaldate#'");
		if(rc.todaycashIn.recordcount > 0) {
			tdcashin 	=  rc.todaycashIn.cashin;
		}
		rc.todaycashOut = QueryExecute("select * from cashout where cashinId = #rc.todaycashIn.recordcount > 0 ? rc.todaycashIn.cashinId : 0#");
		rc.totalcashout = QueryExecute("select sum(cash) as total from cashout where cashinId = #rc.todaycashIn.recordcount > 0 ? rc.todaycashIn.cashinId : 0#");
		if(rc.totalcashout.total != '') {
			ttcashout = rc.totalcashout.total;
		}
		rc.tondau	 = QueryExecute("select thucconcuoi, ngaychot from cash where datediff(cash.ngaychot,'#finaldate#') = -1");
		if(rc.tondau.recordcount > 0) {
			cashtondau 	=  rc.tondau.thucconcuoi;
		}
		rc.equalcash = 	LSParseNumber(tdcashin) - LSParseNumber(ttcashout) + LSParseNumber(cashtondau);

		rc.thucconyes = QueryExecute("select * from cash where datediff(cash.ngaychot,now()) = -1");
		rc.thuccontoday = QueryExecute("select * from cash where datediff(cash.ngaychot,now()) = 0");
	}

	public function form(struct rc) {
		
	}

	public function addcashin(numeric cashintoday, date datecashin) {
		var finaldate = lsDateFormat( datecashin, "yyyy-mm-dd", "pt_PT");
		var checkcashtoday = QueryExecute("select cashinId, cashin from cashin where Date(datecreate) = '#finaldate#'");
		if(checkcashtoday.recordcount > 0) {
			QueryExecute("update cashin set cashin = cashin + #LSParseNumber(cashintoday)# where cashinId = #checkcashtoday.cashinId#");
		}
		else {
			var cashinto = entitynew("cashin");
			cashinto.setcashin(LSParseNumber(cashintoday));
			cashinto.setdatecreate(finaldate);
			entitysave(cashinto);
		}

		var data = getallcash(finaldate);
		variables.fw.renderData("json",data);
	}

	public function getdetailbydate(date datecash) {
		var finaldate = lsDateFormat( datecash, "yyyy-mm-dd", "pt_PT");
		var data = getallcash(finaldate);
		variables.fw.renderData("json",data);
	}

	private struct function getallcash(date finaldate) {
		var tdcashin 	= 0;
		var ttcashout 	= 0;
		var cashtondau 	= 0;

		rc.todaycashIn = QueryExecute("select * from cashin where Date(datecreate) = '#finaldate#'");
		if(rc.todaycashIn.recordcount > 0) {
			tdcashin 	=  rc.todaycashIn.cashin;
		}
		rc.todaycashOut = QueryExecute("select * from cashout where cashinId = #rc.todaycashIn.recordcount > 0 ? rc.todaycashIn.cashinId : 0#");
		rc.totalcashout = QueryExecute("select sum(cash) as total from cashout where cashinId = #rc.todaycashIn.recordcount > 0 ? rc.todaycashIn.cashinId : 0#");
		if(rc.totalcashout.total != '') {
			ttcashout = rc.totalcashout.total;
		}
		var tondau	 = QueryExecute("select thucconcuoi, ngaychot from cash where datediff(cash.ngaychot,'#finaldate#') = -1");
		if(tondau.recordcount > 0) {
			cashtondau 	=  tondau.thucconcuoi;
		}
		rc.equalcash = 	LSParseNumber(tdcashin) - LSParseNumber(ttcashout) + LSParseNumber(cashtondau);

		var data = {};
		data.cashin = rc.todaycashIn;
		data.cashout = rc.todaycashOut;
		data.equalcash = rc.equalcash;
		data.tondau = tondau;
		return data;
	}
} 