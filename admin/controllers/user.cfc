component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc) {
		rc.users = QueryExecute("
			select user.userId, user.firstname, user.lastname, user.phone, user.username, user.isactive, type.name as typeUser 
			from user, type
			where user.typeId = type.typeId
			and user.shopId=#SESSION.ShopId#
		");
	}


	public any function form(struct rc) {
		param name="URL.id" default=0;
		param name="rc.isActive" default=0;
		rc.ListLanguage=entityLoad("language");
		rc.shop=entityLoad("shop",{shopId:SESSION.shopId},true);
		if(isnull(rc.shop)){
			rc.shop=entityNew("shop");
		}
		rc.ListType=entityLoad("type",{tag:"user",isactive:1});
		if (URL.id > 0) rc.users = entityLoad("user",{userId:URL.id},true);
		if (isNull(rc.users)) { 
			rc.users = EntityNew("user");
		} 
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.users.setfirstname(rc.firstname);
			rc.users.setlastname(rc.lastname);
			rc.users.setphone(rc.phone);
			rc.users.setemail(rc.email);
			rc.users.setusername(rc.username);
			if(rc.users.getpassword() != rc.password){
				rc.users.setpassword(#hash(rc.password)#);
			}
			rc.users.settypeId(rc.tag);
			rc.users.setisActive(rc.isactive);
			rc.users.setshopId(SESSION.ShopId);
			rc.users.setlanguageId(rc.languageId);
			rc.users.setdateofbirth(rc.dateofbirth);
			rc.users.setdatestart(rc.datestart);
			rc.users.setcmnd(rc.cmnd);
			rc.users.setcurrentsalary(replace(rc.currentsalary,',','','ALL'));
			rc.users.setholdsalary(replace(rc.holdsalary,',','','ALL'));
			if(SESSION.UserID == URL.id){
				SESSION.languageId=rc.languageid;
			}
			
			entitySave(rc.users);
			variables.fw.redirect("user");
		}
	}

	public any function formadmin(struct rc) {
		param name="URL.userid" default=0;
		param name="rc.isActive" default=0;
		rc.shop=entityLoad("shop",{shopId:URL.shopid},true);
		if(isnull(rc.shop)){
			rc.shop=entityNew("shop");
		}
		rc.ListType=entityLoad("type",{tag:"user",isactive:1});
		if (URL.userid > 0) rc.users = entityLoad("user",{userId:URL.userid},true);
		if (isNull(rc.users)) { 
			rc.users = EntityNew("user");
		} 
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.users.setfirstname(rc.firstname);
			rc.users.setlastname(rc.lastname);
			rc.users.setphone(rc.phone);
			rc.users.setemail(rc.email);
			rc.users.setusername(rc.username);
			if(rc.users.getpassword() != rc.password){
				rc.users.setpassword(#hash(rc.password)#);
			}
			rc.users.settypeId(rc.tag);
			rc.users.setisActive(rc.isactive);
			rc.users.setshopId(URL.shopid);
			entitySave(rc.users);
			location(url="/index.cfm/admin:shop.form?shopid=#URL.shopid#", addtoken="no");
		}
	}

}