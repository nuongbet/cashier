component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc){
		param name="URL.loadDelItems" default=1;
		rc.category = QueryExecute("SELECT * From category 
									where parentId=:ca
									and shopId=:shopId
									and isactive=#URL.loadDelItems# order by categoryId ",{shopId=#SESSION.ShopId#,ca=URL.ca});
		}

	public any function delete(struct rc) {
		var category=entityLoad("category",{categoryId=rc.id},true);
		entityDelete(category);
		variables.fw.redirect("category");
	}

	public any function form(struct rc){
		param name="rc.cost" default=0;
		param name="rc.profitmargin" default=0;
		rc.category = entityLoad("category",{categoryId=rc.id},true);
		if(isnull(rc.category)){
			rc.category=entityNew("category");
		}
		if(CGI.REQUEST_METHOD=="POST")
		{
			if(rc.id ==0)
			{      		
				var category=entityNew("category");
			}
			else 
			{
				var category=entityLoad("category",{categoryId=rc.id},true);
			}
			category.setcategoryname(rc.name);
			category.setdescription(rc.description);
			category.setparentId(URL.ca);
			category.setsorted(rc.Sorted);
			if(not structKeyExists(rc, "isActive")) {
			   category.setisActive(0);
			   }
			   else {
			    category.setisActive(rc.isActive);
			   }
			
			if(URL.ca==1)
			{
				category.setilevel(2);
			}
			else
			{
				category.setilevel(3);
			}
			category.setShopId(SESSION.ShopId);
			category.setcost(replace(rc.cost,",","","All"));
			category.setprofitmargin(replace(rc.profitmargin,",","","All"));
			entitySave(category);

			if(structKeyExists(rc,"_addAnother")) {
				variables.fw.redirect("category.form?ca=#URL.ca#&id=0&_addAnother=true");
			}
			else {
				location(url="/index.cfm/admin:category?ca=#URL.ca#",addtoken="no");
			}
		}
	}

}