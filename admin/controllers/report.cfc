component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw = arguments.fw;
		return this;
	}

	public  function default(struct rc) {
		param name="URL.fdate" default="#now()#";
		param name="URL.tdate" default="#now()#";
		rc.ListReport=QueryExecute("select * from (select foodId,if(isnull(foodId),'',fname)as fname,if(isnull(foodId),'',name)as name,categoryname,categoryId,sum(quantity)as quantity,sum(promo)as promo,sum(discountitems)as discountitems,sum(amount)as revenue,(sum(amount)+sum(discountitems) )as amounttotal
									from (
									SELECT type.name,category.categoryname,category.categoryId,order.foodId,starttime,status,
									sum(quantity)as quantity,sum(quantity)*paymentprice as amount,paymentprice,
									fname,foc as promo,foc*paymentprice as discountitems
									FROM `order`
									inner join food on order.foodId=food.foodId
									inner join type on food.funit=type.typeId
									inner join category on food.categoryId=category.categoryId
									where order.shopId=:shopId
									and status=1 
									and date(starttime) between date(:fdate) and date(:tdate) group by order.foodId,paymentprice
									)temp group by categoryId,foodId with rollup )abc order by categoryId ,foodId  
									",{shopId=SESSION.shopId,fdate=lsdateformat(URL.fdate,"yyyy/mm/dd","pt_PT"),tdate=lsdateformat(URL.tdate,"yyyy/mm/dd","pt_PT")});

	}


	public  function reportcash(struct rc) {
		param name="URL.fdate" default="#now()#";
		param name="URL.tdate" default="#now()#";
		rc.ListCash = QueryExecute("SELECT note,datereceipt,datecreate,
									amount,if(isreceipt !=0,person1,person2)as receiver,if(isreceipt !=0,person2,person1)as payer,isreceipt,tag,if(isreceipt=0,amount,0)as pamount,if(isreceipt=1,amount,0)as ramount ,case when (tag=0 and isreceipt=1) then 'Receipt' when (tag=0 and isreceipt=0) then 'Payment' when tag=1 then 'Bill' when tag=2 then 'Import' end as type
									FROM receipt where shopId=:shopId
									and  date(datereceipt) between date(:fdate) and date(:tdate)
									order by isreceipt,datereceipt",{shopId=SESSION.shopId,fdate=lsdateformat(URL.fdate,"yyyy/mm/dd","pt_PT"),tdate=lsdateformat(URL.tdate,"yyyy/mm/dd","pt_PT")});

	}


	public  function setDefaultData(struct rc) {
		var trackdefault=EntityNew("cashtracking");
		trackdefault.setDatecreate(now());
		trackdefault.setBeginbalance(0);
		trackdefault.setInputcash(0);
		trackdefault.setOutputcash(0);
		trackdefault.setEndingbalance(0);
		trackdefault.setUserId(4);
		trackdefault.setshopId(SESSION.shopId);
		entitySave(trackdefault);
	}
	public  function endingCashForOneShop(required any shopId) {
		// //update

		QueryExecute("update cashtracking ct set ct.inputcash=ifnull((select ifnull(sum(amount),0) from receipt where isreceipt=1 and shopId=#shopId# and date(datereceipt)=date(:dNow) group by isreceipt,date(datereceipt) ),0) where shopId=#shopId# and date(datecreate)=date(:dNow)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});

		QueryExecute("update cashtracking ct set ct.outputcash=ifnull((select ifnull(sum(amount),0) from receipt where isreceipt=0 and shopId=#shopId# and date(datereceipt)=date(:dNow) group by isreceipt,date(datereceipt)),0) where shopId=#shopId# and date(datecreate)=date(:dNow)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});

		QueryExecute("update cashtracking a 
						inner join cashtracking b on
						    date(a.datecreate)  = date(:dNow) and date( b.datecreate)  = date(:dNow) and a.shopId=#shopId# and a.shopId=b.shopId
						set
						    a.endingbalance = b.beginbalance+ifnull(b.inputcash,0)-ifnull(b.outputcash,0)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});

				//checkEnding
				var checkEnding=QueryExecute("select * from cashtracking where date(datecreate)=date(:dNow + interval 1 day) and shopId=#shopId#",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				//render statisticsheet next day.
				if(checkEnding.recordcount==0){
					QueryExecute("insert into cashtracking(beginbalance,endingbalance,inputcash,outputcash,userId,datecreate,shopId)
					select endingbalance,0,0,0,:UId,:dNow + interval 1 day,1 from cashtracking where date(datecreate)=date(:dNow) and shopId=#shopId#",{UId=4,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}
				else{
					//it's exist so update it.
					QueryExecute("update cashtracking a 
						inner join cashtracking b on
						    date(a.datecreate)  = date(:dNow  +  interval 1 day) and date( b.datecreate)  = date(:dNow) and a.shopId=#shopId# and a.shopId=b.shopId
						set
						    a.beginbalance = b.endingbalance",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}

	}
	public  function summarycash(struct rc) {
		param name="URL.fdate" default="#now()#";
		param name="URL.tdate" default="#now()#";

		var getCashtracking=QueryExecute("select cashtrackingId 
											from cashtracking 
											where shopId=:shopId limit 0,1",{shopId:SESSION.shopId});
	
		if(getCashtracking.recordcount eq 0){
			setDefaultData();
		}
		endingCashForOneShop(SESSION.shopId);

		rc.ListCashtracking=QueryExecute("select * from cashtracking where shopId=:shopId  and date(datecreate) between date(:fdate) and date(:tdate) ",{shopId=SESSION.shopId,fdate=lsdateformat(URL.fdate,"yyyy/mm/dd","pt_PT"),tdate=lsdateformat(URL.tdate,"yyyy/mm/dd","pt_PT")});
		
	}


	public any function autodata(struct rc) {
		
		
	}
	
	
	
	
	
	
	
	
}