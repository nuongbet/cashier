component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc) {
		rc.ListType=entityLoad("type",{tag:'#URL.tag#',shopId=#SESSION.ShopId#});
	}


	public any function form(struct rc) {
		param name="URL.id" default=0;
		param name="rc.isActive" default=0;
		if (URL.id > 0) rc.types = entityLoad("type",{typeId:URL.id},true);
		if (isNull(rc.types)) { 
			rc.types = EntityNew("type");
		} 
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.types.setname(rc.name);
			// rc.types.settag(rc.tag);
			rc.types.settag('#URL.tag#');
			rc.types.setisActive(rc.isActive);
			rc.types.setshopId(#SESSION.ShopId#);
			entitySave(rc.types);
			location(url="/index.cfm/admin:type?tag=#URL.tag#",addtoken=false);
		}
	}

	public any function delete(struct rc) {
		var type=entityLoad("type",{typeId=rc.id},true);
		entityDelete(type);
		location(url="/index.cfm/admin:type?tag=#URL.tag#",addtoken=false);
	}
	
}