component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw = arguments.fw;
		return this;
	}

    public array function queryToArray( required query qry ) {
	    var columns = arguments.qry.getColumnNames();
	    var ofTheJedi = [];

	    for( var i = 1; i LTE qry.recordCount; i++ ) {
	        var obj = {};

	        for( var k = 1; k LTE arrayLen( columns ); k++ ) {
	            structInsert( obj, columns[ k ], arguments.qry[ columns[ k ] ][ i ] );
	        }

	        arrayAppend( ofTheJedi, obj );
	    }

	    return ofTheJedi;
	}


	
	public function statisticsheet(struct rc) {
		//get list statisticsheet
		param name="URL.idate" default=#now()#;
		param name="URL.rdate" default="1";
		rc.ListStatisticSheet = QueryExecute("
			select *,round(beginbalance+inputquantity+adjustquantity-outputquantity,2) as endingbalance from (
			select statisticsheet.statisticsheetId,statisticsheet.materialId,
			case when :rdate=1 then round(sum(statisticsheet.beginbalance),2)
				when :rdate=2 then ifnull((select ifnull(beginbalance,0) from statisticsheet as temp where date(datecreate)=SUBDATE(:idate, WEEKDAY(:idate)) and temp.materialId=statisticsheet.materialId),0)
				when :rdate=3 then ifnull((select ifnull(beginbalance,0) from statisticsheet as temp where date(datecreate)=DATE_SUB(:idate, INTERVAL DAYOFMONTH(:idate)-1 DAY) and temp.materialId=statisticsheet.materialId),0)  end as beginbalance
				,round(sum(inputquantity),2) as inputquantity
				,round(sum(statisticsheet.adjustquantity),2) as adjustquantity
				,round(sum(statisticsheet.outputquantity),2) as outputquantity
				,statisticsheet.userId,statisticsheet.datecreate,material.name,user.username 
			from statisticsheet,material,user 
			where statisticsheet.materialId=material.materialId 
			and statisticsheet.userId=user.userId 
			and year(datecreate)=year(:idate)
			and case when :rdate=1 then date(datecreate)=:idate 
					 when :rdate=2 then week(datecreate,1)=week(:idate,1)
					 when :rdate=3 then month(datecreate)=month(:idate) end
			and statisticsheet.shopId=#SESSION.shopId#
			and material.isactive=1
			group by statisticsheet.materialId 
			order by statisticsheet.materialId desc
			)newtable",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});
		rc.totalcost=QueryExecute("select ifnull(sum(amount),0) + (select ifnull(sum(amount),0) as payment 
									from receipt where year(datereceipt)=year(:idate) and case 
											 when :rdate=1 then date(datereceipt)=:idate 
											 when :rdate=2 then week(datereceipt,1)=week(:idate,1)
											 when :rdate=3 then month(datereceipt)=month(:idate) end
											 and shopId=#SESSION.shopId# and isreceipt=0 and tag=0) as inputtotal 
									from import where year(datecreate)=year(:idate) and case 
											 when :rdate=1 then date(datecreate)=:idate 
											 when :rdate=2 then week(datecreate,1)=week(:idate,1)
											 when :rdate=3 then month(datecreate)=month(:idate) end
											 and shopId=#SESSION.shopId#",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});
		rc.revenue=QueryExecute("select ifnull(sum(total),0) + (select ifnull(sum(amount),0) as receipt 
									from receipt where year(datereceipt)=year(:idate) and case 
											 when :rdate=1 then date(datereceipt)=:idate 
											 when :rdate=2 then week(datereceipt,1)=week(:idate,1)
											 when :rdate=3 then month(datereceipt)=month(:idate) end
											 and shopId=#SESSION.shopId# and isreceipt=1 and tag=0) as revenue 
							from bill where year(datecreate)=year(:idate) and case 
											 when :rdate=1 then date(datecreate)=:idate 
											 when :rdate=2 then week(datecreate,1)=week(:idate,1)
											 when :rdate=3 then month(datecreate)=month(:idate) end
											 and shopId=#SESSION.shopId#",{idate=lsdateformat(idate,"yyyy-mm-dd","pt_PT"),rdate=rdate});


		rc.CheckInsert=QueryExecute("select statisticsheet.*,material.name,user.username from statisticsheet,material,user 
			where statisticsheet.materialId=material.materialId 
			and statisticsheet.userId=user.userId 
			and statisticsheet.shopId=#SESSION.shopId#
			group by materialId 
			order by datecreate desc");
		if(rc.CheckInsert.recordcount==0){
			//starting statisticsheet.
			QueryExecute("insert into statisticsheet(materialId,beginbalance,inputquantity,outputquantity,adjustquantity,endingbalance,datecreate,userId,shopId)
							  select materialId,0,0,0,0,0,:dNow,:UId,#SESSION.shopId# from material where shopId=#SESSION.shopId#",{UId=SESSION.UserID,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
		}

		
		// //render materialId for statisticsheet if have new materialId in material
		// QueryExecute("insert into statisticsheet(materialId,beginbalance,inputquantity,outputquantity,adjustquantity,endingbalance,datecreate,userId,shopId) 
		// 								select materialId,0,0,0,0,0,:dNow,:UId,#SESSION.shopId# from material where material.materialId not in
		// 								(select materialId from statisticsheet where  date(datecreate)=date(:dNow) group by materialId) and shopId=#SESSION.shopId#",{UId=SESSION.UserID,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
		
		if(CGI.REQUEST_METHOD == "POST"){
			endingStatisticForOneShop(SESSION.ShopId);
			location(url ="/index.cfm/admin:stock.statisticsheet", addToken = "no");
		}
	}


	public any function getStatistics(struct rc) {
		rc.ListStatisticSheet = QueryExecute("select statisticsheet.*,material.name,user.username from statisticsheet,material,user where statisticsheet.materialId=material.materialId and statisticsheet.userId=user.userId and DATE_FORMAT(datecreate,'%d-%m-%Y')=:idate and statisticsheet.shopId=#SESSION.shopId# order by datecreate desc",{idate=idate});
		variables.fw.renderData('JSON',queryToArray(rc.ListStatisticSheet));
	}
	
	public any function endingStatisticForOneShop(required any shopId) {

				//update inputquantity
				var ListInput=QueryExecute("select materialId,sum(quantity) as quantity from importsheet where date(datecreate)=date(:dNow) and shopId=#shopId# group by materialId",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				for(iInput in ListInput){
					QueryExecute("update statisticsheet set inputquantity=:iquantity where materialId=:MId and date(datecreate)=date(:dNow)",{iquantity=iInput.quantity,MId=iInput.materialId,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}

				//update outputquantity
				var ListOutput=QueryExecute("select materialId,(actualtemp-ordertemp)as quantity from material where actualtemp <> ordertemp and shopId=#shopId#");
				for(iOutput in ListOutput){
					QueryExecute("update statisticsheet set outputquantity = outputquantity + :iquantity where materialId=:MId and date(datecreate)=date(:dNow)",{iquantity=iOutput.quantity,MId=iOutput.materialId,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
					QueryExecute("update material m inner join material temp on
									    m.materialId = temp.materialId
										set m.ordertemp = temp.actualtemp where m.materialId=:MId",{MId=iOutput.materialId});
				}

				//update adjustquantity
				var ListAdjust=QueryExecute("select materialId,sum(fixnumber)as quantity from adjustsheet where date(datecreate)=date(:dNow) and shopId=#shopId# group by materialId",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				for(iAdjust in ListAdjust){
					QueryExecute("update statisticsheet set adjustquantity=:iquantity where materialId=:MId and date(datecreate)=date(:dNow)",{iquantity=iAdjust.quantity,MId=iAdjust.materialId,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}
				//update endingbalance
				QueryExecute("update statisticsheet s inner join statisticsheet temp on
									    s.statisticsheetId = temp.statisticsheetId
										set s.endingbalance = round(temp.beginbalance+temp.inputquantity+temp.adjustquantity-temp.outputquantity,2) where date(temp.datecreate)=date(:dNow)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				//checkEnding
				var checkEnding=QueryExecute("select * from statisticsheet where date(datecreate)=date(:dNow + interval 1 day) and shopId=#shopId#",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				//render statisticsheet next day.
				if(checkEnding.recordcount==0){
					QueryExecute("insert into statisticsheet(materialId,beginbalance,endingbalance,inputquantity,outputquantity,adjustquantity,userId,datecreate,shopId)
					select materialId,endingbalance,0,0,0,0,:UId,:dNow + interval 1 day,#shopId# from statisticsheet where date(datecreate)=date(:dNow) and shopId=#shopId#",{UId=4,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}
				else{
					//it's exist so update it.
					QueryExecute("update statisticsheet a 
						inner join statisticsheet b on
						    a.materialID = b.materialID and date(a.datecreate)  = date(:dNow  +  interval 1 day) and date( b.datecreate)  = date(:dNow) and a.shopId=#shopId# and a.shopId=b.shopId
						set
						    a.beginbalance = b.endingbalance",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}

	}


	public  function endingCashForOneShop(required any shopId) {
		//update

				QueryExecute("update cashtracking ct set ct.inputcash=ifnull((select ifnull(sum(amount),0) from receipt where isreceipt=1 and shopId=#shopId# and date(datereceipt)=date(:dNow) group by isreceipt,date(datereceipt) ),0) where shopId=#shopId# and date(datecreate)=date(:dNow)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});

				QueryExecute("update cashtracking ct set ct.outputcash=ifnull((select ifnull(sum(amount),0) from receipt where isreceipt=0 and shopId=#shopId# and date(datereceipt)=date(:dNow) group by isreceipt,date(datereceipt)),0) where shopId=#shopId# and date(datecreate)=date(:dNow)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});

				QueryExecute("update cashtracking a 
						inner join cashtracking b on
						    date(a.datecreate)  = date(:dNow) and date( b.datecreate)  = date(:dNow) and a.shopId=#shopId# and a.shopId=b.shopId
						set
						    a.endingbalance = b.beginbalance+ifnull(b.inputcash,0)-ifnull(b.outputcash,0)",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});

				//checkEnding
				var checkEnding=QueryExecute("select * from cashtracking where date(datecreate)=date(:dNow + interval 1 day) and shopId=#shopId#",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				//render statisticsheet next day.
				if(checkEnding.recordcount==0){
					QueryExecute("insert into cashtracking(beginbalance,endingbalance,inputcash,outputcash,userId,datecreate,shopId)
					select endingbalance,0,0,0,:UId,:dNow + interval 1 day,1 from cashtracking where date(datecreate)=date(:dNow) and shopId=#shopId#",{UId=4,dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}
				else{
					//it's exist so update it.
					QueryExecute("update cashtracking a 
						inner join cashtracking b on
						    date(a.datecreate)  = date(:dNow  +  interval 1 day) and date( b.datecreate)  = date(:dNow) and a.shopId=#shopId# and a.shopId=b.shopId
						set
						    a.beginbalance = b.endingbalance",{dNow=lsdateFormat(now(),"yyyy-mm-dd","pt_PT")});
				}

	}
	
	

	public any function endingStatistic(struct rc) {

			var ListShop=QueryExecute("select * from shop");
			for(iShop in ListShop){
				//endingStatistic
				endingStatisticForOneShop(iShop.shopId);
				//ending cashtracking
				endingCashForOneShop(iShop.shopId);
			}
			variables.fw.renderData('JSON',true);
	}

	public any function adjustlist(struct) {
		param name="URL.idate" default=#now()#;
		rc.adjustlist = QueryExecute("select * from adjust where date(datecreate)=:datesearch and shopId=#SESSION.shopId#",{datesearch=#lsdateFormat(idate,'yyyy-mm-dd',"pt_PT")#});
	}

	public any function adjustsheet(struct rc) {
		rc.ListNameMaterial = QueryExecute("
			select m.materialId, m.name as materialname, u.name as unitname, m.actualstock
			from material m 
			inner join unit u on m.unitId = u.unitId 
			and m.shopId=#SESSION.shopId#
			and m.isactive=1
			ORDER BY m.name");
		rc.ListShift=QueryExecute("select * from type where isActive=1 and tag='shift'");
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.adjust = EntityNew("adjust");
			rc.adjust.setnote(rc.txtNote);
			rc.adjust.setdatecreate(now());
			rc.adjust.setuserId(Session.UserID);
			rc.adjust.setchecker(rc.checker);
			rc.adjust.setshift(rc.shift);
			rc.adjust.setshopId(SESSION.shopId);
			entitySave(rc.adjust);
			for (i=1;i lte ArrayLen(rc.mId);i=i+1){
				rc.adjustsheet = EntityNew("adjustsheet");
				rc.adjustsheet.setmaterialId(rc.mId[i]);
				rc.adjustsheet.setfixnumber(rc.mFixnumber[i]);
				rc.adjustsheet.setdatecreate(now());
				rc.adjustsheet.setadjustId(rc.adjust.adjustId);
				rc.adjustsheet.setshopId(SESSION.shopId);
				entitySave(rc.adjustsheet);	
				QueryExecute("update material set actualstock = actualstock+:fixnumber where materialId=:materialID",{fixnumber=rc.adjustsheet.getfixnumber(), materialID=rc.adjustsheet.getmaterialId()});
			}	
		}
	}	
	
	public any function adjustdetail(struct) {
		rc.adjustdetail = QueryExecute("
			select u.username, date_format(ad.datecreate,'%d-%m-%Y') as datecreate, ad.checker, ad.shift, ad.note, m.name as materialname, ads.fixnumber, un.name as unitname
			from adjust ad
				inner join adjustsheet ads on ad.adjustId = ads.adjustId
				inner join material m on ads.materialId = m.materialId
				inner join unit un on un.unitId = m.unitId
				inner join user u on ad.userId = u.userId
			Where ad.adjustId=:adjustId and ad.shopId=#SESSION.shopId#
		",{adjustId=adjustId});
		variables.fw.renderData('JSON',queryToArray(rc.adjustdetail));
	}
	
	public any function importsheet(struct rc) {
		rc.ListNameMaterial = QueryExecute("select m.materialId, m.name as materialname, m.unitprice, u.name as unitname 
										from material m inner join unit u on m.unitId = u.unitId 
										and m.shopId=#SESSION.shopId# and m.isactive=1 ORDER BY m.name");	
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.import = EntityNew("import");
			rc.import.setdatecreate(now());
			rc.import.setnote(rc.txtNote);
			rc.import.setuserId(Session.UserID);
			rc.import.setdeliverycompany(rc.deliverycompany);
			rc.import.setdeliverydate(#dateformat(rc.deliverydate,'yyyy-mm-dd')#);
			rc.import.setbuyer(rc.buyer);
			rc.import.setamount(replace('#rc.amount#',',','','ALL'));
			rc.import.setactualcost(replace('#rc.actualcost#',',','','ALL'));
			rc.import.setshopId(SESSION.shopId);
			entitySave(rc.import);
			for (i=1;i lte ArrayLen(rc.mId);i=i+1){
				rc.importsheet = EntityNew("importsheet");
				rc.importsheet.setmaterialId(rc.mId[i]);
				rc.importsheet.setquantity(rc.mQuantity[i]);
				rc.importsheet.setimportId(rc.import.getimportId());
				rc.importsheet.setdatecreate(now());
				rc.importsheet.setprice(replace('#rc.iPrice[i]#',',','','ALL'));
				rc.importsheet.settotal(replace('#rc.iTotal[i]#',',','','ALL'));
				rc.importsheet.setshopId(SESSION.shopId);
				entitySave(rc.importsheet);
				QueryExecute("update material set actualstock = actualstock+:quantity where materialId=:materialID",{quantity=rc.importsheet.getquantity(), materialID=rc.importsheet.getmaterialId()});
			}

			//save import to receipt
			savebilltoreceipt(rc.deliverycompany,rc.buyer,rc.actualcost,rc.txtNote,rc.deliverydate,rc.import.getimportId());
		}	
	}
	private function savebilltoreceipt(required any person1,required any person2,required any amount,required any txtNote,required any datereceipt,required any importId){
		receipt = EntityNew("receipt");
		receipt.setDatereceipt(lsdateFormat(#datereceipt#,'yyyy-mm-dd',"pt_PT"));
		receipt.setPerson1(person1);
		receipt.setPerson2(person2);
		receipt.setDatecreate(now());
		receipt.setAmount(replace('#amount#',',','','ALL'));
		receipt.setNote(txtNote);
		receipt.setIsreceipt(0);
		receipt.setTag(2);
		receipt.setTempId(importId);
		receipt.setUserId(SESSION.UserID);
		receipt.setshopId(SESSION.shopId);
		entitySave(receipt);
	}
	public any function importlist(struct) {
		param name="URL.idate" default=#now()#;
		rc.importlist = QueryExecute("select * from import where date(datecreate)=:datesearch and shopId=#SESSION.shopId#",{datesearch=#lsdateFormat(idate,'yyyy-mm-dd',"pt_PT")#});
	}
	
	public any function importdetail(struct) {
		rc.importdetail = QueryExecute("
			select u.username, ip.datecreate, ip.actualcost, ip.note, ip.amount, date_format(ip.deliverydate,'%d-%m-%Y') as deliverydate, ip.deliverycompany, ip.buyer, m.name as materialname, ips.quantity, ips.price, ips.total, un.name as unitname
			from import ip
				inner join importsheet ips on ip.importId = ips.importId
				inner join material m on ips.materialId = m.materialId
				inner join unit un on un.unitId = m.unitId
				inner join user u on ip.userId = u.userId
			Where ip.importId=:importId and ip.shopId=#SESSION.shopId#
		",{importId=importId});
		variables.fw.renderData('JSON',queryToArray(rc.importdetail));
	}


	public any function getunitnameandbalance(struct rc) {
		var result = QueryExecute('
			select u.name, m.actualstock, m.unitprice
			from material m 
				inner join unit u on m.unitId = u.unitId 
			where m.materialId=:materialId and m.shopId=#SESSION.shopId#',{materialId=materialId});
		variables.fw.renderData('JSON',queryToArray(result));
	}

	public any function receiptlist(struct) {
		param name="URL.idate" default=#now()#;
		param name="URL.isreceipt" default=2;
		rc.receiptlist = QueryExecute("select * from receipt where date(datereceipt)=:datesearch and shopId=#SESSION.shopId# and if (:re=2 ,  tag=2 , isreceipt=:re) ",{datesearch=#lsdateFormat(idate,'yyyy-mm-dd',"pt_PT")#,re=URL.isreceipt});
	}

	public any function receiptsheet(struct rc) {
		param name="rc.acontinue" default=0;
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.receiptsheet = EntityNew("receipt");
			rc.receiptsheet.setDatereceipt(lsdateFormat(rc.datereceipt,'yyyy-mm-dd','pt_PT'));
			rc.receiptsheet.setPerson1(rc.person1);
			rc.receiptsheet.setPerson2(rc.person2);
			rc.receiptsheet.setDatecreate(now());
			rc.receiptsheet.setAmount(replace('#rc.amount#',',','','ALL'));
			rc.receiptsheet.setNote(rc.txtNote);
			rc.receiptsheet.setIsreceipt(URL.isreceipt);
			rc.receiptsheet.setUserId(SESSION.UserID);
			rc.receiptsheet.setshopId(SESSION.shopId);
			entitySave(rc.receiptsheet);
			if(rc.acontinue eq 0){
				location(url="/index.cfm/admin:stock.receiptlist?isreceipt=#URL.isreceipt#",addtoken="no");
			}
			else{
				location(url="/index.cfm/admin:stock.receiptsheet?isreceipt=#URL.isreceipt#",addtoken="no");
			}	
		}
	}



	
	
	
}