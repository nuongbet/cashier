component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc) {
		param name="URL.typeId" default=0;
		rc.typeprice=entityLoad("type",{isActive:1,tag:'price',shopId:#SESSION.ShopId#});
		if(URL.typeId == 0)
		{
			rc.ListFood=QueryExecute("SELECT foodprice.*,food.name as fname,type.name as tname,food.price as normalprice 
										FROM foodprice,food,type 
										where foodprice.foodId=food.foodId 
										and type.typeId=foodprice.typeId
										and foodprice.shopId=#SESSION.ShopId#");
		}
		else
		{
			rc.ListFood=QueryExecute("SELECT foodprice.*,food.name as fname,type.name as tname,food.price as normalprice 
				FROM foodprice,food,type 
				where foodprice.foodId=food.foodId 
				and type.typeId=foodprice.typeId 
				and foodprice.typeId=:id
				and foodprice.shopId=#SESSION.ShopId#",{id=URL.typeId});
		}
		
	}

	public  function form(struct rc) {
		param name="URL.Id" default=0;
		if (URL.id > 0){
		 rc.foodprice = entityLoad("foodprice",{foodpriceId:URL.id},true);
		 rc.FoodByID=entityLoad("food",{foodId:rc.foodprice.getfoodId()},true);
		 }
		rc.typeprice=entityLoad("type",{isActive:1,tag:'price',shopId:#SESSION.ShopId#});
		rc.ListFood=entityLoad("food",{isActive:1,shopId:#SESSION.ShopId#});
		if (isNull(rc.foodprice) && arrayLen(rc.ListFood)>0 && arrayLen(rc.typeprice)>0) 
		{ 
			rc.foodprice = EntityNew("foodprice");
			rc.FoodByID=entityLoad("food",{foodId:rc.ListFood[1].getfoodId()},true);
		} 
		else
		{
			if(arrayLen(rc.ListFood)<0){
				location(url="/index.cfm/admin:food.form?id=0",addtoken="no");
			}
			else{
				if(arrayLen(rc.typeprice)<0){
					location(url="/index.cfm/admin:type.form?id=0",addtoken="no");
				}
			}	
		}
		
		if(CGI.REQUEST_METHOD == "POST"){
			rc.fexists=0;
			if(URL.id>0){
				rc.foodprice.setprice(replace(rc.price,",","","All"));
				entitySave(rc.foodprice);
			}
			else{
				var foodExists=entityLoad("foodprice",{typeId:rc.typeId,foodId:rc.foodId},true);
				if(isnull(foodExists)){
					rc.foodprice.setfoodId(rc.foodId);
					rc.foodprice.setprice(replace(rc.price,",","","All"));
					rc.foodprice.settypeId(rc.typeId);
					rc.foodprice.setshopId(#SESSION.ShopId#);
					entitySave(rc.foodprice);
				}
				else{
					rc.fexists=1;
				}
				
			}
			
			
		}
	}
	
	public any function getFoodChange(struct rc) {
		var food=entityLoad("food",{foodId=fId},true);
		variables.fw.RenderData("json",food);
	}

	public any function delete(struct rc) {
		var foodprice=entityLoad("foodprice",{foodpriceId=rc.id},true);
		entityDelete(foodprice);
		variables.fw.redirect("foodprice");
	}



	
	
}