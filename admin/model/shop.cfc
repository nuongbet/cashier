component persistent="true" accessors="true" table="shop"{
 property name="shopId" fieldtype="id" generator="native"  column="shopId" ;
 property name="name" type ="string" default="";
 property name="address" type ="string" default="";
 property name="phone" type ="string" default="";
 property name="email" type ="string" default="";
 property name="logo" type ="string" default="";
 property name="logobill" type ="string" default="";
 property name="taxcode" type ="string" default="";
 property name="managername" type ="string" default="";
 property name="isactive" type ="boolean" default=0;
}