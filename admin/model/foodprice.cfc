component persistent="true" accessors="true" table="foodprice"{
property name="foodpriceId" fieldtype="id" generator="native"  column="foodpriceId" ;
 property name="foodId" type ="numeric" default="";
 property name="price" type ="numeric" ormType="big_decimal" default=0;
  property name="typeId" type ="numeric" ;
   property name="shopId" type ="numeric" ;
}