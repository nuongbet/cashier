component persistent="true" accessors="true" table="desk"{
property name="deskId" fieldtype="id" generator="native"  column="deskId" ;
 property name="name" type ="string" default="";
 property name="categoryId" type ="numeric" default="";
 property name="status" type ="numeric" default=0;
 property name="type" type ="string" default="";
 property name="isactive" type ="boolean" default=0;
  property name="shopId" type ="numeric" ;
  property name="sorted" type ="numeric" default="";
 property name="isPrintedReceipt" type="boolean" default=0;
}