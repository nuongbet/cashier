component persistent="true" accessors="true" table="user"{
property name="userId" fieldtype="id" generator="native"  column="userId" ;
 property name="firstname" type ="string" default="";
 property name="lastname" type ="string" default="";
 property name="phone" type ="string" default="";
 property name="email" type ="string" default="";
 property name="username" type ="string" default="";
 property name="password" type ="string" default="";
 property name="isactive" type ="boolean" default=0;
 property name="typeId" type ="numeric" ;
  property name="shopId" type ="numeric" ;
  property name="languageId" type ="numeric" default=2 ;
  property name="datestart" type ="timestamp" ;
  property name="dateofbirth" type ="timestamp" ;
  property name="cmnd" type ="string" ;
  property name="holdsalary" type ="numeric";
  property name="currentsalary" type ="numeric" default=2 ;
}