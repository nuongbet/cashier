component persistent="true" accessors="true" table="cashtracking"{
	 property name="cashtrackingId" fieldtype="id" generator="native" column="cashtrackingId";
	 property name="beginbalance" type ="numeric" default=0;
	 property name="endingbalance" type ="numeric" default=0;
	 property name="inputcash" type ="numeric" default=0;
	 property name="outputcash" type ="numeric" default=0;
	 property name="userId" type ="numeric" ;
	 property name="datecreate" type="timestamp" ;
	  property name="shopId" type ="numeric" ;
}