component persistent="true" accessors="true" table="foodmaterial"{
 property name="foodmaterialId" fieldtype="id" generator="native"  column="foodmaterialId" ;
  property name="foodId" type ="numeric" ;
  property name="materialId" type ="numeric" ;
  property name="unitId" type ="numeric";
property name="estimate" type ="numeric" ormType="big_decimal" default=0;
 property name="shopId" type ="numeric" ;
}