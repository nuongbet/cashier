component persistent="true" accessors="true" table="bill"{
 property name="billId" fieldtype="id"  column="billId" type="string" ;
 property name="total" type ="numeric" ormType="big_decimal" default=0;
 property name="discount" type ="numeric" ormType="big_decimal" default=0;
 property name="discountvalue" type ="numeric" ormType="big_decimal" default=0;
 property name="totalvalue" type ="numeric" ormType="big_decimal" default=0;
 property name="typediscount" type ="string" ;
 property name="typepriceId" type ="numeric" ;
 property name="typepriceName" type="string";
 property name="datecreate" type="timestamp" ;
 property name="userId" type="numeric";
 property name="note" type ="string" default="";
 property name="typeId" type="numeric";
 property name="shift" type ="string" default="";
 property name="shopId" type ="numeric" ;
 property name="tablename" type ="string" ;
 property name="waiter" type ="string" ;
}