component persistent="true" accessors="true" table="cashout"{
	property name="cashoutId" fieldtype="id"  column="cashoutId" generator="native";
	property name="cash" type ="numeric" ormType="big_decimal" default=0;
	property name="typeofcash" type ="string" default="";
	property name="datecreate" type="timestamp" ;
	property name="fkcashinout" fieldtype="many-to-one" cfc="cashin" fkcolumn="cashinId";
}