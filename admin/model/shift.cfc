component persistent="true" accessors="true" table="shift"{
 property name="shiftId" fieldtype="id" generator="native"  column="shiftId" ;
 property name="name" type ="string" default="";
 property name="timestart" type ="time";
 property name="timeend" type ="time" ;
  property name="shopId" type ="numeric" ;
}
