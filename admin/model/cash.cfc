component persistent="true" accessors="true" table="cash"{
	property name="cashId" fieldtype="id"  column="cashId" generator="native";
	property name="thucconcuoi" type ="numeric" ormType="big_decimal" default=0;
	property name="tienlayra" type ="numeric" ormType="big_decimal" default=0;
	property name="thuccontondau" type ="numeric" ormType="big_decimal" default=0;
	property name="ngaychot" type="timestamp" ;
	property name="ghichu" type="string" ;

}