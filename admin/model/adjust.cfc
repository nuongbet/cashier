component persistent="true" accessors="true" table="adjust"{
	 property name="adjustId" fieldtype="id" generator="native" column="adjustId";
	 property name="note" type ="string" ;
	 property name="datecreate" type="timestamp" ;
	 property name="userId" type ="numeric" ;
	 property name="checker" type ="string" ;
	 property name="shift" type ="string" ;
	 property name="shopId" type ="numeric" ;
}