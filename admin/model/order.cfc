component persistent="true" accessors="true" table="order"{
	property name="orderId" fieldtype="id" generator="native"  column="orderId" ;
	property name="deskId" type ="numeric" ;
	property name="foodId" type ="numeric" ;
	property name="userId" type ="numeric" ;
	property name="note" type ="string" default="";
	property name="starttime" type="timestamp" ;
	property name="status" type ="boolean" default=0;
	property name="billId" type ="string" default="";
	property name="quantity" type ="numeric";
	property name="paymentprice" type ="numeric" ormType="big_decimal" default=0;
	property name="fname" type ="string" default="";
	property name="foc" type ="numeric" default=0;
	 property name="shopId" type ="numeric" ;
	 property name="isDone" type="boolean" default=0;
}