component persistent="true" accessors="true" table="receipt"{
	 property name="receiptId" fieldtype="id" generator="native"  column="receiptId";
	 property name="userId" type ="numeric" ;
	 property name="note" type ="string" ;
	 property name="datecreate" type="timestamp" ;
	 property name="datereceipt" type="timestamp" ;
	 property name="amount" type ="numeric" ormType="big_decimal";
	 property name="person1" type ="string" ;
	 property name="person2" type ="string" ;
	 property name="isreceipt" type ="boolean" default=0;
	 property name="tag" type ="numeric" ;//0:cash 1:bill
	 property name="tempId" type ="string" ;//BillId/ImportId from bill/import
	 property name="shopId" type ="numeric" ;
}