component persistent="true" accessors="true" table="category"{
property name="categoryId" fieldtype="id" generator="native"  column="categoryId" ;
 property name="categoryname" type ="string" default="";
 property name="description" type ="string" default="";
 property name="parentId" type ="numeric" default=0;
 property name="isActive" type ="boolean" default=1;
 property name="ilevel" type ="numeric" default=0;
  property name="shopId" type ="numeric" ;
  property name="sorted" type ="string" default="";
  property name="cost" type ="numeric" ormType="big_decimal" default=0;
  property name="profitmargin" type ="numeric" ormType="big_decimal" default=0;
}