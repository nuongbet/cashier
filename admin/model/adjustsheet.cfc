component persistent="true" accessors="true" table="adjustsheet"{
	 property name="adjustsheetId" fieldtype="id" generator="native" column="adjustsheetId";
	 property name="materialId" type ="numeric" ;
	 property name="fixnumber" type ="numeric" ;
	 property name="datecreate" type="timestamp" ;
	 property name="adjustId" type ="numeric" ;
	 property name="shopId" type ="numeric" ;
}