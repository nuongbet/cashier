component persistent="true" accessors="true" table="importsheet"{
	 property name="importsheetId" fieldtype="id" generator="native"  column="importsheetId";
	 property name="materialId" type ="numeric" ;
	 property name="quantity" type ="numeric" ;
	 property name="importId" type ="numeric" ;
	 property name="datecreate" type="timestamp" ;
	 property name="price" type ="numeric" ormType="big_decimal" default=0;
	 property name="total" type ="numeric" ormType="big_decimal" default=0;
	  property name="shopId" type ="numeric" ;
}