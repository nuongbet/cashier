component persistent="true" accessors="true" table="material"{
 property name="materialId" fieldtype="id" generator="native"  column="materialId" ;
 property name="name" type ="string" default="";
 property name="unitId" type ="numeric" ;
 property name="actualstock" type ="numeric" ormType="big_decimal" default=0;
 property name="needstock" type ="numeric" ormType="big_decimal" default=0;
 property name="ministock" type ="numeric" ormType="big_decimal" default=0;
 property name="unitprice" type ="numeric" ormType="big_decimal" default=0;
 property name="actualtemp" type ="numeric" ormType="big_decimal" default=10000;
 property name="ordertemp" type ="numeric" ormType="big_decimal" default=10000;
 property name="isactive" type ="boolean" default=0;
  property name="shopId" type ="numeric" ;
}