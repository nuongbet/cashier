component persistent="true" accessors="true" table="food"{
	property name="foodId" fieldtype="id" generator="native"  column="foodId" ;
	property name="name" type ="string" default="";
	property name="categoryId" type ="numeric" default="";
	property name="price" type ="numeric" ormType="big_decimal" default=0;
	property name="estimateprice" type ="numeric" ormType="big_decimal" default=0;
	property name="isactive" type ="boolean" default=0;
	property name="shopId" type ="numeric" ;
	property name="color" type ="string" default="" ;
	property name="margin" type ="numeric" ormType="big_decimal" default=0;
	property name="percentage" type ="numeric" ormType="big_decimal" default=0;
	property name="fsorted" type ="string" default="";
	property name="fcode" type ="string" default="";
	property name="keycode" type ="string" default="";
	property name="funit" type ="numeric" ;

}