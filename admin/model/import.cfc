component persistent="true" accessors="true" table="import"{
	 property name="importId" fieldtype="id" generator="native"  column="importId";
	 property name="userId" type ="numeric" ;
	 property name="note" type ="string" ;
	 property name="datecreate" type="timestamp" ;
	 property name="amount" type ="numeric" ormType="big_decimal" default=0;
	 property name="actualcost" type ="numeric" ormType="big_decimal" default=0;
	 property name="deliverycompany" type ="string" ;
	 property name="deliverydate" type ="timestamp" ;
	 property name="buyer" type ="string" ;
	  property name="shopId" type ="numeric" ;
}