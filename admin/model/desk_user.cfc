component persistent="true" accessors="true" table="desk_user"{
	 property name="id" fieldtype="id" generator="native" column="id";
	 property name="isactive" type ="boolean" default=1;
	 property name="username" type ="string";
	 property name="deskId" type ="numeric" ;
	 property name="datecreate" type="timestamp" ;
}