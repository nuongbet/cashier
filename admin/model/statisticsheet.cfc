component persistent="true" accessors="true" table="statisticsheet"{
	 property name="statisticsheetId" fieldtype="id" generator="native" column="statisticsheetId";
	 property name="materialId" type ="numeric" ;
	 property name="beginbalance" type ="numeric" ;
	 property name="endingbalance" type ="numeric" ;
	 property name="inputquantity" type ="numeric" ;
	 property name="outputquantity" type ="numeric" ;
	 property name="adjustquantity" type ="numeric" ;
	 property name="userId" type ="numeric" ;
	 property name="datecreate" type="timestamp" ;
	  property name="shopId" type ="numeric" ;
}