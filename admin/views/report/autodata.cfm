<cfoutput>
<cfset dtStart = CreateDate(2014,11,12) />
<cfset dtEnd = Now() />
<cfloop 
    index="dtToday" 
    from="#dtStart#" 
    to="#dtEnd#" 
    step="#CreateTimeSpan( 1, 0, 0, 0 )#">

   <cfscript>
   

QueryExecute("update cashtracking ct set ct.inputcash=ifnull((select ifnull(sum(amount),0) from receipt where isreceipt=1 and shopId=1 and date(datereceipt)=date(:dNow) group by isreceipt,date(datereceipt) ),0) where shopId=1 and date(datecreate)=date(:dNow)",{dNow=lsdateFormat(#dtToday#,"yyyy-mm-dd","pt_PT")});

QueryExecute("update cashtracking ct set ct.outputcash=ifnull((select ifnull(sum(amount),0) from receipt where isreceipt=0 and shopId=1 and date(datereceipt)=date(:dNow) group by isreceipt,date(datereceipt)),0) where shopId=1 and date(datecreate)=date(:dNow)",{dNow=lsdateFormat(#dtToday#,"yyyy-mm-dd","pt_PT")});

QueryExecute("update cashtracking a 
						inner join cashtracking b on
						    date(a.datecreate)  = date(:dNow) and date( b.datecreate)  = date(:dNow) and a.shopId=1 and a.shopId=b.shopId
						set
						    a.endingbalance = b.beginbalance+ifnull(b.inputcash,0)-ifnull(b.outputcash,0)",{dNow=lsdateFormat(#dtToday#,"yyyy-mm-dd","pt_PT")});

				//checkEnding
				var checkEnding=QueryExecute("select * from cashtracking where date(datecreate)=date(:dNow + interval 1 day) and shopId=1",{dNow=lsdateFormat(#dtToday#,"yyyy-mm-dd","pt_PT")});
				//render statisticsheet next day.
				if(checkEnding.recordcount==0){
					QueryExecute("insert into cashtracking(beginbalance,endingbalance,inputcash,outputcash,userId,datecreate,shopId)
					select endingbalance,0,0,0,:UId,:dNow + interval 1 day,1 from cashtracking where date(datecreate)=date(:dNow) and shopId=1",{UId=4,dNow=lsdateFormat(#dtToday#,"yyyy-mm-dd","pt_PT")});
				}
				else{
					//it's exist so update it.
					QueryExecute("update cashtracking a 
						inner join cashtracking b on
						    date(a.datecreate)  = date(:dNow  +  interval 1 day) and date( b.datecreate)  = date(:dNow) and a.shopId=1 and a.shopId=b.shopId
						set
						    a.beginbalance = b.endingbalance",{dNow=lsdateFormat(#dtToday#,"yyyy-mm-dd","pt_PT")});
				}
   </cfscript>
</cfloop>
</cfoutput>