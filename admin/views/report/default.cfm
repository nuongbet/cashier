<cfoutput>
<cfscript>
	setLayout('fund');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
 function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}
function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}

function search(){
	var fdate=$('##fdate').val();
	var tdate=$('##tdate').val();
	location.href="/index.cfm/admin:report.default?fdate="+fdate+"&tdate="+tdate;
}

	$.fn.dataTable.TableTools.defaults.aButtons = [ "copy", "csv", "xls" ];
    jQuery(function($) {

    	$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});


		var oTable1 = $('##list_link')
		.dataTable( {
			"bSort": false,
			"bPaginate":false,
			dom: 'T<"clear">lfrtip',
			"oTableTools": {
			    "aButtons": [
			        {
			            "sExtends": "copy",
			            "sButtonText": "Copy",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        },
			         {
			            "sExtends": "print",
			            "sButtonText": "Print",
			            "oSelectorOpts": {
			                page: 'current',
			                filter: 'applied'
			            }
			        }
			        ,
			         {
			            "sExtends": "csv",
			            "sButtonText": "CSV",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			        ,
			         {
			            "sExtends": "xls",
			            "sButtonText": "Excel",
			            "sTitle": "Report sale #URL.fdate# to #URL.tdate#",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			        ,
			         {
			            "sExtends": "pdf",
			            "sButtonText": "PDF",
			           	"sTitle": "Report sale #URL.fdate# to #URL.tdate#",
			            "sPdfOrientation": "landscape",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			    ]
			}
	    } );

		$('.DTTT').css({"float":"right","margin-bottom":"5px"});
	})

</script>

<div class="page-header">
  <h1>
	#getLabel("Report Sale Total")#
  </h1>
</div>
<div class="col-xs-12">
<div class="col-md-12 form-inline">
		<input class="form-control date-picker" name="fdate" id="fdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.fdate,"dd-mm-yyyy","pt_PT")#" placeholder="From date" />
		<input class="form-control date-picker" name="tdate" id="tdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.tdate,"dd-mm-yyyy","pt_PT")#" placeholder="To date" />
		<p class="btn btn-primary btn-white" onclick='search();'>
			<i class="ace-icon fa fa-search"></i> 
		Search
		</p>
</div>
	<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th style="text-align:center;">#getLabel("No")#</th>
					<th style="text-align:center;">#getLabel("Goods Details")#</th>
					<th style="text-align:center;">#getLabel("Unit")#</th>
					<th style="text-align:center;">#getLabel("Quantity")#</th>
					<th style="text-align:center;">#getLabel("Promo")#</th>
					<th style="text-align:center;">#getLabel("Amount")#</th>
					<th style="text-align:center;">#getLabel("Revenue")#</th>
					<th style="text-align:center;">#getLabel("Discount Items")#</th>
					<th style="text-align:center;">#getLabel("Cost")#</th>
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>

		
			<cfloop query="#rc.ListReport#"  startrow="2"  endrow="#rc.ListReport.recordcount#">
				<tr>
					<td style="text-align:center;">
						<cfif rc.ListReport.foodId neq ''>
							#i#
						</cfif>
					</td>
					<cfif rc.ListReport.currentrow  eq 1>
						<td style="font-weight: bold;">TOTAL</td>
					<cfelse>
						<cfif rc.ListReport.foodId neq ''>
							<cfquery name="qGetNameFood">
								select name from food where foodId=<cfqueryparam value="#rc.ListReport.foodId#" cfsqltype="cf_sql_integer"/>
							</cfquery>
							<td>#qGetNameFood.name#
						<cfelse>
							<td style="font-weight: bold;">#UCase(rc.ListReport.categoryname)#</td>
						</cfif>
					</cfif>
					<td style="text-align:center;">
						#rc.ListReport.name#
					</td>
					<td style="text-align:center;">
						#NumberFormat(rc.ListReport.quantity)#
					</td>
					<td style="text-align:right;">
						#rc.ListReport.promo#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListReport.amounttotal)#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListReport.revenue)#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListReport.discountitems)#
					</td>
					<td>
						
					</td>
				</tr>
				<cfif rc.ListReport.foodId neq ''>
						<cfset i=i+1/>
				</cfif>
				
			</cfloop>
			<cfif rc.ListReport.recordcount>
				<tr>
					<td></td>
					<td style="font-weight: bold;">TOTAL</td>
					<td></td>
					<td style="text-align:center;">
						#NumberFormat(rc.ListReport.quantity)#
					</td>
					<td style="text-align:right;">
						#rc.ListReport.promo#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListReport.amounttotal)#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListReport.revenue)#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListReport.discountitems)#
					</td>
					<td></td>
				</tr>
			</cfif>
			</tbody>
		</table>
	</div>
</cfoutput>