<cfoutput>
<cfscript>
	setLayout('fund');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
 function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}
function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}

function search(){
	var fdate=$('##fdate').val();
	var tdate=$('##tdate').val();
	location.href="/index.cfm/admin:report.summarycash?fdate="+fdate+"&tdate="+tdate;
}

	$.fn.dataTable.TableTools.defaults.aButtons = [ "copy", "csv", "xls" ];
    jQuery(function($) {

    	$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		var oTable1 = $('##list_link')
		.dataTable( {
			"bSort": false,
			"bPaginate":false,
			dom: 'T<"clear">lfrtip',
			"oTableTools": {
			    "aButtons": [
			        {
			            "sExtends": "copy",
			            "sButtonText": "Copy",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        },
			         {
			            "sExtends": "print",
			            "sButtonText": "Print",
			            "oSelectorOpts": {
			                page: 'current',
			                filter: 'applied'
			            }
			        }
			        ,
			         {
			            "sExtends": "csv",
			            "sButtonText": "CSV",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			        ,
			         {
			            "sExtends": "xls",
			            "sButtonText": "Excel",
			            "sTitle": "Summary cash from #URL.fdate# to #URL.tdate#",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			        ,
			         {
			            "sExtends": "pdf",
			            "sTitle": "Summary cash from #URL.fdate# to #URL.tdate#",
			            "sButtonText": "PDF",
			            "sPdfOrientation": "landscape",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			    ]
			}
	    } );

		$('.DTTT').css({"float":"right","margin-bottom":"5px"});
	})

</script>

<div class="page-header">
  <h1>
	#getLabel("Summary cash")#
  </h1>
</div>
<div class="col-md-12 form-inline">
		<input class="form-control date-picker" name="fdate" id="fdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.fdate,"dd-mm-yyyy","pt_PT")#" placeholder="From date" />
		<input class="form-control date-picker" name="tdate" id="tdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.tdate,"dd-mm-yyyy","pt_PT")#" placeholder="To date" />
		<p class="btn btn-primary btn-white" onclick='search();'>
		<i class="ace-icon fa fa-search"></i> 
		Search
	</p>
</div>
<div class="col-xs-12">
	<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th style="text-align:center;">#getLabel("No")#</th>
					<th style="text-align:center;">#getLabel("Date")#</th>
					<th style="text-align:center;">#getLabel("Begin cash")#</th>
					<th style="text-align:center;">#getLabel("Receipt cash")#</th>
					<th style="text-align:center;">#getLabel("Payment cash")#</th>
					<th style="text-align:center;">#getLabel("Ending cash")#</th>
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>
		
			<cfloop query="#rc.ListCashtracking#" >
				<tr>
					<td style="text-align:center;">
						#i#
					</td>
					<td style="text-align:center;">
						#Dateformat(rc.ListCashtracking.datecreate,'dd/mm/yyyy')#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListCashtracking.beginbalance)#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListCashtracking.inputcash)#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListCashtracking.outputcash)#
					</td >
					<td style="text-align:right;">
						#NumberFormat(rc.ListCashtracking.endingbalance)#
					</td >
				</tr>
				<cfset i=i+1/>
			</cfloop>
			</tbody>
		</table>
	</div>
</cfoutput>