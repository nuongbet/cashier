<cfoutput>
<cfscript>
    setLayout('fund');
</cfscript>
<script type="text/javascript">

    $(document).ready(function(){
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        $('##datesearch').change(function(){
            var idate=$('##datesearch').val();
            location.href='/index.cfm/admin:dashboard.monthly?idate='+idate;
        });

    });
    jQuery(function($) {
        var oTable1 = $('##list_link')
        .dataTable( {
            bAutoWidth: false,
            "aoColumns": [null,null,null,null,null,null,null]
        } );
    });
</script>

<div class="col-md-12">
    #view('common/sidebar')#
</div>
<div class="col-md-12">
    <div class="col-md-4">
        <input class="form-control date-picker" id="datesearch" type="text" data-date-format="dd-mm-yyyy" placeholder="#getLabel("Date search")#" value="#lsdateFormat(URL.idate,"dd-mm-yyyy","pt_PT")#" style="margin-top:10px;"/>
    </div>
    <div class="col-md-12 row">
        <!--- report cost vs revenue in 30 recent day --->
   <!---      <cfquery name="qGetCostvsRevenue">
             select day(temp.datecreate)as datecreate,ifnull(ip.amount,0)as cost,ifnull(bo.total,0)as revenue 
            from
               ( select * from(  
               select test.datecreate as datecreate
                from
                (
                 SELECT date_sub(LAST_DAY(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>),interval 30 day) + INTERVAL a + b DAY datecreate,0 as amount
                  FROM
                  (SELECT 0 a UNION SELECT 1 a UNION SELECT 2 UNION SELECT 3
                  UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
                  UNION SELECT 8 UNION SELECT 9 ) d,
                  (SELECT 0 b UNION SELECT 10 UNION SELECT 20 
                  UNION SELECT 30 UNION SELECT 40) m
                  WHERE date_sub(LAST_DAY(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>),interval 30 day) + INTERVAL a + b DAY  < LAST_DAY(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>) + interval 1 day ORDER BY a + b)as test
                ) as abc
               where month(abc.datecreate)=month(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>))as temp

            left join 
            (select date(datecreate)as datecreate,sum(amount)as amount from import
            where shopId=#SESSION.ShopId# and datecreate  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datecreate))as ip
            on temp.datecreate=ip.datecreate
            left join 
            (select date(datecreate)as datecreate,sum(total)as total from bill
            where shopId=#SESSION.ShopId# and  datecreate  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datecreate))as bo
            on temp.datecreate=bo.datecreate
        </cfquery> --->

        <cfquery name="qGetCostvsRevenue">
             select day(temp.datecreate)as datecreate, ifnull(pa.payment,0)as cost, ifnull(re.receipt,0)as revenue 
            from
               ( select * from(  
               select test.datecreate as datecreate
                from
                (
                 SELECT date_sub(LAST_DAY(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>),interval 30 day) + INTERVAL a + b DAY datecreate,0 as amount
                  FROM
                  (SELECT 0 a UNION SELECT 1 a UNION SELECT 2 UNION SELECT 3
                  UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
                  UNION SELECT 8 UNION SELECT 9 ) d,
                  (SELECT 0 b UNION SELECT 10 UNION SELECT 20 
                  UNION SELECT 30 UNION SELECT 40) m
                  WHERE date_sub(LAST_DAY(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>),interval 30 day) + INTERVAL a + b DAY  < LAST_DAY(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>) + interval 1 day ORDER BY a + b)as test
                ) as abc
               where month(abc.datecreate)=month(<cfqueryparam sqltype="date" value="#lsdateFormat(URL.idate,"yyyy-mm-dd","pt_PT")#"/>))as temp

            -- left join 
            -- (select date(datecreate)as datecreate,sum(amount) as amount from import
            -- where shopId=#SESSION.ShopId# and datecreate  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datecreate))as ip
            -- on temp.datecreate=ip.datecreate

            left join (select date(datereceipt)as datereceipt,ifnull(sum(amount),0) as payment 
                                                from receipt where shopId=#SESSION.ShopId# and isreceipt=0 and datereceipt  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datereceipt))as pa
            on temp.datecreate=pa.datereceipt

            -- left join 
            -- (select date(datecreate)as datecreate,sum(total) as total from bill
            -- where shopId=#SESSION.ShopId# and  datecreate  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datecreate))as bo
            -- on temp.datecreate=bo.datecreate

            left join (select date(datereceipt)as datereceipt,ifnull(sum(amount),0) as receipt 
                                                from receipt where shopId=#SESSION.ShopId# and isreceipt=1 and datereceipt  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datereceipt))re
             on temp.datecreate=re.datereceipt
        </cfquery>

        <cfset ListDate=#ListToArray(valueList(qGetCostvsRevenue.datecreate, ','))#/>
        <cfset ListCost=#ListToArray(valueList(qGetCostvsRevenue.cost, ','))#/>
        <cfset ListRevenue=#ListToArray(valueList(qGetCostvsRevenue.revenue, ','))#/>
        <cfset stChart3 = {
                chart: {
                    renderTo:"costvsrevenue",
                    type: 'line'
                },
                title: {
                    text: '#getLabel("Cost vs Revenue in month")#',
                    x: -20 //center
                },
                subtitle: {
                    text: 'Source: Cashier',
                    x: -20
                },
                xAxis: {
                    categories: #ListDate#
                },
                yAxis: {
                    title: {
                        text: '#getLabel("Amount")# (#currency#)'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '##808080'
                    }]
                },
                tooltip: {
                    valueSuffix: '#currency#'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: [{
                    name: '#getLabel("Revenue")#',
                    data: #ListRevenue#
                }, {
                    name: '#getLabel("Cost")#',
                    data: #ListCost#
                }]
            }>
        <cfhighchart attributeCollection="#stChart3#" createContainer="false">
        <!--- end report cost vs revenue in 30 recent day --->
        <div id="costvsrevenue" class="col-md-12" style="margin-top:10px"></div>
    </div>
    <div class="col-md-12">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-tasks red"></i>
                    #getLabel("Stock Status")#
                </h4>
                <div class="widget-toolbar">
                    <a href="##" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>                
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main no-padding">
                    <table id="list_link" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#getLabel("ID")#</th>
                                <th>#getLabel("Material")#</th>
                                <th>#getLabel("Begin balance")#</th>
                                <th>#getLabel("Input")#</th>
                                <th>#getLabel("Adjust")#</th>
                                <th>#getLabel("Output")#</th>
                                <th>#getLabel("Ending balance")#</th>
                            </tr>
                        </thead>

                        <tbody>
                            <cfset id=1/>
                            <cfloop query="#rc.ListStatisticSheet#">
                                <tr>
                                    <td>
                                        #id#
                                    </td>
                                    <td>
                                        #rc.ListStatisticSheet.name#
                                    </td>
                                    <td>
                                        #rc.ListStatisticSheet.beginbalance#
                                        <cfset ebalance=#rc.ListStatisticSheet.beginbalance#/>
                                    </td>
                                    <cfif #dateFormat(rc.ListStatisticSheet.datecreate,"dd-mm-yyyy")# eq #dateFormat(now(),"dd-mm-yyyy")#>
                                        <td>
                                            <cfquery name="qGetInputNow">
                                                select sum(quantity) as quantity from importsheet where date(datecreate)=date(#now()#) and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#"> group by materialId
                                            </cfquery>
                                            <cfif #qGetInputNow.recordcount# eq 0>
                                                0
                                            <cfelse>
                                                #qGetInputNow.quantity[1]#
                                                <cfset ebalance=ebalance+#qGetInputNow.quantity[1]#/>
                                            </cfif>
                                        </td>
                                        <td>
                                            <cfquery name="qGetAdjustNow">
                                                select sum(fixnumber) as quantity from adjustsheet where date(datecreate)=date(#now()#) and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#"> group by materialId
                                            </cfquery>
                                            <cfif #qGetAdjustNow.recordcount# eq 0>
                                                0
                                            <cfelse>
                                                #qGetAdjustNow.quantity[1]#
                                                <cfset ebalance=ebalance+#qGetAdjustNow.quantity[1]#/>
                                            </cfif>
                                        </td>
                                        <td>
                                            <cfquery name="qGetOutputNow">
                                                select (actualtemp-ordertemp)as quantity from material where actualtemp <> ordertemp and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#"> group by materialId
                                            </cfquery>
                                            <cfif #qGetOutputNow.recordcount# eq 0>
                                                #rc.ListStatisticSheet.outputquantity#
                                                <cfset ebalance=ebalance-#rc.ListStatisticSheet.outputquantity#/>
                                            <cfelse>
                                                #rc.ListStatisticSheet.outputquantity+qGetOutputNow.quantity[1]#
                                                <cfset ebalance=ebalance-#rc.ListStatisticSheet.outputquantity+qGetOutputNow.quantity[1]#/>
                                            </cfif>
                                        </td>
                                        <td>
                                            #ebalance#
                                        </td>
                                    <cfelse>
                                        <td>
                                            #rc.ListStatisticSheet.inputquantity#
                                        </td>
                                        <td>
                                            #rc.ListStatisticSheet.adjustquantity#
                                        </td>
                                        <td>
                                            #rc.ListStatisticSheet.outputquantity#
                                        </td>
                                        <td>
                                            #rc.ListStatisticSheet.endingbalance#
                                        </td>
                                    </cfif>
                                </tr>
                                <cfset id += 1/>  
                            </cfloop>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</cfoutput>
