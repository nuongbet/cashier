<cfoutput>
<cfscript>
    setLayout('fund');
</cfscript>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        });

        $('##datesearch').change(function(){
            searchdate();
        });
    })

    function searchdate(){
        var rdate=2;
        var idate=$('##datesearch').val();
        location.href='/index.cfm/admin:dashboard.weekly?idate='+idate+'&rdate='+rdate;
    }
    jQuery(function($) {
        var oTable1 = $('##list_link')
        .dataTable( {
            bAutoWidth: false,
            "aoColumns": [null,null,null,null,null,null,null]
        } );
    });

</script>
<cfscript>
    public array function queryToArray( required query qry ) {
    var columns = arguments.qry.getColumnNames();
    var ofTheJedi = [];

    for( var i = 1; i LTE qry.recordCount; i++ ) {
        var obj = {};

        for( var k = 1; k LTE arrayLen( columns ); k++ ) {
            structInsert( obj, columns[ k ], arguments.qry[ columns[ k ] ][ i ] );
        }

        arrayAppend( ofTheJedi, obj );
    }

    return ofTheJedi;
}
</cfscript>
<div class="col-md-12">
    #view('common/sidebar')#
</div>
<div class="col-md-12">
    <div class="form-group">
        <div  class="col-sm-4">
            <input class="form-control date-picker" id="datesearch" type="text" data-date-format="dd-mm-yyyy" placeholder="#getLabel("Date search")#" value="#lsdateFormat(URL.idate,"dd-mm-yyyy","pt_PT")#" style="margin-top:10px;"/>
        </div>
    </div>
</div>
<div class="col-md-12 row">
    <div class="col-md-6">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-star red"></i>
                    #getLabel("Weekly")#
                </h4>
                <div class="widget-toolbar">
                    <a href="##" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>    
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main no-padding">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>#getLabel("Weekly Revenue")#</td>
                                <td>#numberformat(rc.revenue.revenue[1])#</td>
                                <td>
                                    <cfif rc.revenue.revenue[2] eq 0>
                                        <cfif rc.revenue.revenue[1] eq 0>
                                            <i class='fa fa-minus orange'></i>
                                        <cfelse>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                            100 %
                                        </cfif>
                                    <cfelse>
                                        <cfset revenue_c_l=#((rc.revenue.revenue[1]-rc.revenue.revenue[2])/rc.revenue.revenue[2])*100#/>
                                        <cfif revenue_c_l gt 0>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                        <cfelse>    
                                            <i class='fa fa-long-arrow-down red'></i>
                                        </cfif>
                                        #round(revenue_c_l,2)# %
                                    </cfif>
                                </td>
                            </tr>
                            <tr>
                                <td>#getLabel("Weekly Cost")#</td>
                                <td>#numberformat(rc.totalcost.inputtotal[1])#</td>
                                <td>
                                    <cfif rc.totalcost.inputtotal[2] eq 0>
                                        <cfif rc.totalcost.inputtotal[1] eq 0>
                                            <i class='fa fa-minus orange'></i>
                                        <cfelse>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                            100 %
                                        </cfif>
                                    <cfelse>
                                        <cfset cost_c_l=#((rc.totalcost.inputtotal[1]-rc.totalcost.inputtotal[2])/rc.totalcost.inputtotal[2])*100#/>
                                        <cfif cost_c_l gt 0>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                        <cfelse>    
                                            <i class='fa fa-long-arrow-down red'></i>
                                        </cfif>
                                        #round(cost_c_l,2)# %
                                    </cfif>
                                </td>
                            </tr>
                            <tr>
                                <td>#getLabel("Weekly P/L")#</td>
                                <td>#numberformat(rc.revenue.revenue[1]-rc.totalcost.inputtotal[1])#</td>
                                <td>
                                    <cfif (rc.revenue.revenue[2]-rc.totalcost.inputtotal[2]) eq 0>
                                        <cfif (rc.revenue.revenue[1]-rc.totalcost.inputtotal[1]) eq 0>
                                            <i class='fa fa-minus orange'></i>
                                        <cfelse>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                            100 %
                                        </cfif>
                                    <cfelse>
                                        <cfset daily_p_l=#(((rc.revenue.revenue[1]-rc.totalcost.inputtotal[1])-(rc.revenue.revenue[2]-rc.totalcost.inputtotal[2]))/(rc.revenue.revenue[2]-rc.totalcost.inputtotal[2]))*100#/>
                                        <cfif daily_p_l gt 0>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                        <cfelse>    
                                            <i class='fa fa-long-arrow-down red'></i>
                                        </cfif>
                                        #round(daily_p_l,2)# %
                                    </cfif>
                                </td>
                            </tr>
                            <tr>
                                <td>#getLabel("Weekly Turnover")#</td>
                                <td>#rc.revenue.turnover[1]#</td>
                                <td>
                                    <cfif rc.revenue.turnover[2] eq 0>
                                        <cfif rc.revenue.turnover[1] eq 0>
                                            <i class='fa fa-minus orange'></i>
                                        <cfelse>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                            100 %
                                        </cfif>
                                    <cfelse>
                                        <cfset turnover_c_l=#((rc.revenue.turnover[1]-rc.revenue.turnover[2])/rc.revenue.turnover[2])*100#/>
                                        <cfif turnover_c_l gt 0>
                                            <i class='fa fa-long-arrow-up blue'></i>
                                        <cfelse>    
                                            <i class='fa fa-long-arrow-down red'></i>
                                        </cfif>
                                        #round(turnover_c_l,2)# %
                                    </cfif>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>  
    <div class="col-md-6">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-flask red"></i>
                    #getLabel("Sales Volume")#
                </h4>
                <div class="widget-toolbar">
                    <a href="##" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>    
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main no-padding">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#getLabel("Category")#</th>
                                <th>#getLabel("Quantity")#</th>
                                <th>#getLabel("Highest")#</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <cfloop query="#rc.SaleFoodType#">
                                <tr>
                                    <td>
                                        #rc.SaleFoodType.categoryName#
                                    </td>
                                    <td>
                                        #rc.SaleFoodType.quantity#
                                    </td>
                                    <td>
                                        #rc.SaleFoodType.name#
                                    </td>
                                    <td>
                                        #rc.SaleFoodType.quantityItem#
                                    </td>
                                </tr>
                            </cfloop>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="col-md-12">
    <div class="widget-box transparent">
        <div class="widget-header widget-header-flat">
            <h4 class="widget-title lighter">
                <i class="ace-icon fa fa-tasks red"></i>
                #getLabel("Stock Status")#
            </h4>
            <div class="widget-toolbar">
                <a href="##" data-action="collapse">
                    <i class="ace-icon fa fa-chevron-up"></i>
                </a>
            </div>    
        </div>
        <div class="widget-body" style="display: block;">
            <div class="widget-main no-padding">
                <table id="list_link" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>#getLabel("ID")#</th>
                            <th>#getLabel("Material")#</th>
                            <th>#getLabel("Begin balance")#</th>
                            <th>#getLabel("Input")#</th>
                            <th>#getLabel("Adjust")#</th>
                            <th>#getLabel("Output")#</th>
                            <th>#getLabel("Ending balance")#</th>
                        </tr>
                    </thead>

                    <tbody>
                    <cfset id=1/>
                    
                    <cfloop query="#rc.ListStatisticSheet#">
                        <tr>
                            <td>
                                #id#
                            </td>
                            <td>
                                #rc.ListStatisticSheet.name#
                            </td>
                            <td>
                                #rc.ListStatisticSheet.beginbalance#
                                <cfset ebalance=#rc.ListStatisticSheet.beginbalance#/>
                            </td>
                            <cfif #dateFormat(rc.ListStatisticSheet.datecreate,"dd-mm-yyyy")# eq #dateFormat(now(),"dd-mm-yyyy")#>
                                <td>
                                    <cfquery name="qGetInputNow">
                                        select sum(quantity) as quantity from importsheet where date(datecreate)=date(#now()#) and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#">
                                        and shopId=#SESSION.ShopId# group by materialId
                                    </cfquery>
                                    <cfif #qGetInputNow.recordcount# eq 0>
                                        0
                                    <cfelse>
                                        #qGetInputNow.quantity[1]#
                                        <cfset ebalance=ebalance+#qGetInputNow.quantity[1]#/>
                                    </cfif>
                                </td>
                                <td>
                                    <cfquery name="qGetAdjustNow">
                                        select sum(fixnumber) as quantity from adjustsheet where date(datecreate)=date(#now()#) and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#">
                                        and shopId=#SESSION.ShopId# group by materialId
                                    </cfquery>
                                    <cfif #qGetAdjustNow.recordcount# eq 0>
                                        0
                                    <cfelse>
                                        #qGetAdjustNow.quantity[1]#
                                        <cfset ebalance=ebalance+#qGetAdjustNow.quantity[1]#/>
                                    </cfif>
                                </td>
                                <td>
                                    <cfquery name="qGetOutputNow">
                                        select (actualtemp-ordertemp)as quantity from material where actualtemp <> ordertemp and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#">
                                        and shopId=#SESSION.ShopId# group by materialId
                                    </cfquery>
                                    <cfif #qGetOutputNow.recordcount# eq 0>
                                        #rc.ListStatisticSheet.outputquantity#
                                        <cfset ebalance=ebalance-#rc.ListStatisticSheet.outputquantity#/>
                                    <cfelse>
                                        #rc.ListStatisticSheet.outputquantity+qGetOutputNow.quantity[1]#
                                        <cfset ebalance=ebalance-#rc.ListStatisticSheet.outputquantity+qGetOutputNow.quantity[1]#/>
                                    </cfif>
                                </td>
                                <td>
                                    #ebalance#
                                </td>
                            <cfelse>
                                <td>
                                    #rc.ListStatisticSheet.inputquantity#
                                </td>
                                <td>
                                    #rc.ListStatisticSheet.adjustquantity#
                                </td>
                                <td>
                                    #rc.ListStatisticSheet.outputquantity#
                                </td>
                                <td>
                                    #rc.ListStatisticSheet.endingbalance#
                                </td>
                            </cfif>
                        </tr>
                        <cfset id += 1/>
                    </cfloop>   
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!--- graph daily revenue by shift --->
<cfset data1=#replace(serializejson(queryToArray(rc.DailyRevenueByShift)),'"shift":',"","All")#>
<cfset data2=#replace(data1,'"dailyrevenue":',"","All")#>
<cfset data3=#replace(data2,'{',"[","All")#>
<cfset dataShift=#replace(data3,'}',"]","All")#>
        <cfset stChart = {
        chart: {
            renderTo:"containerShift",
            plotBackgroundColor: 'null',
            plotBorderWidth: 'null',
            plotShadow: false
        },
        title: {
            text: '#getLabel("Report Revenue By Shift")#'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: 'black',
                    connectorColor: 'black',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Cashier',
            data: 
                #dataShift#
            
        }]
    }>
    <cfhighchart attributeCollection="#stChart#" createContainer="false">
<!--- end shift --->
<!--- graph revenue by order type --->
<cfset ListOrderType=#ListToArray(valueList(rc.DailyRevenueByOrderType.ordername, ','))#/>
<cfset ListRevenue=#ListToArray(valueList(rc.DailyRevenueByOrderType.dailyrevenue, ','))#/>
<cfset stChart2 = {
            chart: {
                renderTo:"containerOderType",
                type: 'column'
            },
            title: {
                text: '#getLabel("Report Revenue By Order Type")#'
            },
            subtitle: {
                text: 'Source: Cashier'
            },
            xAxis: {
                categories: #ListOrderType#
            },
            yAxis: {
                min: 0,
                title: {
                    text: '#getLabel("Revenue")#'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: '#getLabel("Revenue")#',
                data: #ListRevenue#
    
            }]
        }>
    <cfhighchart attributeCollection="#stChart2#" createContainer="false">
<!--- end order type --->
<div class="col-md-12 row">
    <div class="col-md-6">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-pie-chart red"></i>
                    #getLabel("Break Sales Volume down by time of day")#
                </h4>
                <div class="widget-toolbar">
                    <a href="##" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>    
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main no-padding" id="containerShift">
                   
                </div>
            </div>
        </div>
    </div>
     <div class="col-md-6">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="widget-title lighter">
                    <i class="ace-icon fa fa-bar-chart-o red"></i>
                    #getLabel("Sales Volume show")#
                </h4>
                <div class="widget-toolbar">
                    <a href="##" data-action="collapse">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>    
            </div>
            <div class="widget-body" style="display: block;">
                <div class="widget-main no-padding" id="containerOderType">
                    
                </div>
            </div>
        </div>
    </div>
</div>
</cfoutput>

