<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
    jQuery(function($) {
		var oTable1 = $('##list_link')
		.dataTable( {
			"aaSorting": [[ 0, "desc" ]],
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,null,null,null,{ "bSortable": false }]
	    } );
	   })
</script>

<div class="page-header">
  <h1>
	#getLabel("Food")#
  </h1>
</div>
<div class="col-xs-12">
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:food.form?id=0">
		<i class="ace-icon fa fa-plus"></i> 
		#getLabel("Add New")#
	</a>
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:food?loadDelItems=0">
		<i class="ace-icon fa fa-list-ul"></i>#getLabel("List of Deleted Items")# 
	</a>
		<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("No.")#</th>
					<th>#getLabel("Name")#</th>
					<th>#getLabel("Type")#</th>
					<th>#getLabel("Price")#</th>
					<th>#getLabel("Cost")#</th>
					<th>#getLabel("Margin")#</th>
					<th>#getLabel("Percent(%)")#</th>
					<th>#getLabel("Sort Order")#</th>
					<th></th>
					<!--- <th></th> --->
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>
			<cfloop query="rc.food">
				<tr>
					<td>
						#rc.food.fcode#
					</td>
					<td>
						<a class="blue" href="/index.cfm/admin:food.form?id=#rc.food.foodId#">
						#rc.food.name#
					</td>
					<td>
						#rc.food.categoryname#
					</td>
					<td>
						#NumberFormat(rc.food.price)#
					</td>
					<td>
						#NumberFormat(rc.food.estimateprice)#
					</td>
					<td>
						#NumberFormat(rc.food.margin)#
					</td>
					<td>
						#NumberFormat(rc.food.percentage)#
					</td>
					<td>
						#rc.food.fsorted#
					</td>
					<td style="text-align:center">
						<a class="green" href="/index.cfm/admin:food.form?id=#rc.food.foodId#">
							<i class="ace-icon fa fa-pencil bigger-130"></i>
						</a>
					</td>	
					<!--- <td style="text-align:center">
						<a class="red" href="/index.cfm/admin:food.delete?id=#rc.food.foodId#" onclick="return checkDelete()">
							<i class="ace-icon fa fa-trash-o bigger-130"></i>
						</a>
					</td> --->
				</tr>
				<cfset i=i+1/>
			</cfloop>	
			</tbody>
		</table>
	</div>
</cfoutput>