<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	// window.oncontextmenu = function () { 
	// 	return false; 
	// } 
	// document.onkeydown = function (e) {	
	// 	if (window.event.keyCode == 123 || e.button==2)	
	// 		return false; 
	// } 
	var foodmateriaId=0;
	function editMaterial(fMId,Mid,unitId,estimate,unitprice){
		$('##showMaterial').show();
		foodmateriaId=fMId;
		$('##materialId').val(Mid);
		$('##unitId').val(unitId);
		$('##unitprice').val(commaSeparateNumber(unitprice));
		$('##estimate').val(estimate);
		$('##materialId').prop("disabled", true);
	}
	function addMaterial(){
		$('##showMaterial').show();
		$('##estimate').val('');
		foodmateriaId=0;
		$('##materialId').prop("disabled", false);
	}
	function deleteMaterial(FMId){
	    $.ajax({
	      type: 'POST',
	      url: '#getConTextRoot()#/index.cfm/admin:food.deleteMaterial/',
	      data: {'FMId':FMId,'Fid':#URL.id#},
	      dataType: 'JSON',
	      success: function(data) {
	      	console.log(data);
	      	 $('##del'+FMId).remove();
	      	 $('##estimateprice').val(commaSeparateNumber(data[1])); 
	      }
	    }); 
	}
	function isBlank(str) {
    return (!str || /^\s*$/.test(str));
	}
	function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
    }
    function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
	}
	function format(input)
	{
	    var nStr = input.value + '';
	    nStr = nStr.replace( /\,/g, "");
	    x = nStr.split( '.' );
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while ( rgx.test(x1) ) {
	        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
	    }
	    input.value = x1 + x2;
	}
	function SaveMaterial(){
		var MId=$('##materialId').val();
		var Mname=$('select[name="materialId"] option:selected').text();
		var UId= parseInt($('##unitid').val());
		var Uname=$('##unit').val();
		var estimate=parseFloat($('##estimate').val());
		var unitprice = parseFloat($('##unitprice').val().replace(/,/g , ""));
		var estimateprice = commaSeparateNumber(parseFloat(unitprice * estimate));


		if(isBlank(estimate)){
			alert('Please input estimate.');
			$('##estimate').focus();
		}
		else{
				var strURL="";
				if(foodmateriaId > 0){
					//update foodmaterial
					strURL='updateFoodMaterial/';
				}
				else{
					//insert foodmaterial
					strURL='insertFoodMaterial/';
				}
			    $.ajax({
			      type: 'POST',
			      url: '#getConTextRoot()#/index.cfm/admin:food.'+strURL,
			      data: {'FMId':foodmateriaId,'MId':MId,'UId':UId,'estimate':estimate,'FId':#URL.id#},
			      dataType: 'JSON',
			      success: function(data) {
			      	console.log(data[0]);
			      	if(data[0] != false){
			      		if(data[0] == true){
			      			//update
			      			$('##td_uname'+foodmateriaId).html(Uname);
			      			$('##td_estimate'+foodmateriaId).html(estimate);
			      			$('##td_edit'+foodmateriaId).attr('onclick','return editMaterial('+foodmateriaId+','+MId+','+UId+','+estimate+','+unitprice+');');
		      				$('##td_estimateprice'+foodmateriaId).html(estimateprice);
			      		}
			      		else{
			      			//insert
			      			$('##list_foodmaterial tbody').append('<tr id="del'+data[0]+'"><td>'+Mname+'</td id="td_uname'+data[0]+'"><td>'+Uname+'</td><td id="td_estimate'+data[0]+'">'+estimate+'</td><td id="td_estimateprice'+data[0]+'">'+estimateprice+'</td><td style="text-align:center"><div class="hidden-sm hidden-xs action-buttons"><a class="green" style="cursor:pointer;  id="td_edit'+data[0]+'""  onclick="return editMaterial('+data[0]+','+MId+','+UId+','+estimate+','+unitprice+')"><i class="ace-icon fa fa-pencil bigger-130"></i></a></div></td><td style="text-align:center"><div class="hidden-sm hidden-xs action-buttons"><a class="red" style="cursor:pointer;" onclick="return deleteMaterial('+data[0]+')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div></td></tr>');
			      		}
			      		$('##showMaterial').hide();
			      		$('##estimateprice').val(commaSeparateNumber(data[1]));
			      		var pricetemp=$('##price').val().replace(/,/g , "");
					    if(isBlank(pricetemp)){
					      pricetemp=0;
					    }
			      		$('##margin').val(commaSeparateNumber(pricetemp-data[1]));
			      		$('##percentage').val(Math.round((data[1]/pricetemp*100),2)); 
			      	}
			      	else{
			      		alert('#getLabel("Material is exist. Please choose another material.")#');
			      	}
			      }
			    }); 
		}
	}
	function clearcolor(){
		$('##color').val('');
	}

	function getcolor(id){
		var color = $('##color'+id).val();
		$('##color').val(color);
	}
	function clearkey(){
		$('##keycode').val('');
	}
	function checkCode(theCode){
	  $.getJSON("../admin/controllers/checkuser.cfc", {
	   method: 'chkCode',
	   fcode: theCode,
	   returnformat: 'json'
	   }, 

	   function(isCodeUnique){  
	   if (isCodeUnique == true) {
	   var lb='#getLabel("is exists, Please select a new Code")#';
	   $("##theErrorDivCode").html('['+theCode+'] '+lb);
	 	  $('##fcode').val("");
	   }
	   else {
	   $("##theErrorDivCode").html('');
	   }
	   });
	};

	function checkKeyCode(theCode){
	  $.getJSON("../admin/controllers/checkuser.cfc", {
	   method: 'chkKeyCode',
	   kcode: theCode,
	   returnformat: 'json'
	   }, 

	   function(isCodeUnique){  
	   if (isCodeUnique == true) {
	   var lb='#getLabel("is exists, Please select a new key Code")#';
	   $("##theErrorDivKeyCode").html('['+theCode+'] '+lb);
	 	  $('##keycode').val("");
	   }
	   else {
	   $("##theErrorDivKeyCode").html('');
	   }
	   });
	};


	$(document).ready(function(){
		$('##color').colorpicker();
		$('##showMaterial').hide();
		$('.selecticon').selectpicker({
			style: 'btn-info',
      		size: 'auto',
      	});


		$('##materialId').change(function(){
			var materialId = $(this).val();
			$.ajax({
				type: 'POST',
				url: '/index.cfm/admin:food.getunitvsprice/',
				data: {'materialId':materialId},
				dataType: 'JSON',
				success: function(data) {
					$('##unitid').val(data[0].unitid);
					$('##unit').val(data[0].unitname);
					$('##unitprice').val(commaSeparateNumber(data[0].unitprice));
				}
			}); 

		});

		$('##price').on('input',function(e){
		    var price=$(this).val().replace(/,/g , "");
		    if(isBlank(price)){
		      price=0;
		    }
		    var cost=$('##estimateprice').val().replace(/,/g , "");
		    $('##margin').val(commaSeparateNumber(price-cost));
		    $('##percentage').val(Math.round((cost/price*100),2));
	 	});

	    $('##keycode').on('keydown', function (e) {
	    	$(this).val(e.keyCode);
	    	checkKeyCode(e.keyCode);
	        e.preventDefault();
	    });

});


</script>

<cfif CGI.REQUEST_METHOD EQ 'POST'>
	<cfif #URL.id# gt 0>
		<cflocation url="/index.cfm/admin:food">
	<cfelse>
		<cflocation url="/index.cfm/admin:food.form?id=#rc.foodId#">
	</cfif>
</cfif>

<div class="page-header">
  <h1>
	#getLabel("Food")#
  </h1>
</div>
<form id="form1" class="form-horizontal" role="form" action="" method="POST" enctype="multipart/form-data">

	<div class="row">
		<div class="col-md-7">
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Name")#</label>
				<div class="col-sm-9">
					<input type="text" id="name" name="name" required placeholder="#getLabel("Name")#" class="col-xs-10 col-sm-10" value="#rc.food.name#" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Type")#</label>
				<div class="col-sm-9">
					<select name="type" id="type" class="col-xs-10 col-sm-10" >
						<cfloop query="#rc.ListType#">
							<cfif #rc.food.categoryId# eq #rc.ListType.categoryId#>
										<cfset selected="selected"/>
										<cfelse>
										<cfset selected=""/>
										</cfif>
							<option 		  	
								value="#rc.ListType.categoryId#" #selected#>#rc.ListType.categoryname#
							</option>
						</cfloop>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Price")#</label>
				<div class="col-sm-9">
					<input type="text" min="0" id="price" name="price" placeholder="#getLabel("Price")#" class="col-xs-10 col-sm-10" value="#NumberFormat(rc.food.price)#" onkeypress="return isNumberKey(event)" onkeyup="format(this)" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Cost")#</label>
				<div class="col-sm-9">
					<input type="text" min="0" id="estimateprice" name="estimateprice" placeholder="#getLabel("Cost")#" class="col-xs-10 col-sm-10" value="#NumberFormat(rc.food.estimateprice)#" readonly/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Margin")#</label>
				<div class="col-sm-9">
					<input type="text" min="0" id="margin" name="margin" placeholder="#getLabel("Margin")#" class="col-xs-10 col-sm-10" value="#NumberFormat(rc.food.margin)#" readonly/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Percent(%)")#</label>
				<div class="col-sm-9">
					<input type="text" min="0" id="percentage" name="percentage" placeholder="#getLabel("Percent(%)")#" class="col-xs-10 col-sm-10" value="#NumberFormat(rc.food.percentage)#" readonly/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Color")#</label>
				<div class="col-sm-8">
					<input type="text" id="color" name="color" placeholder="#getLabel("Color")#" class="col-xs-8 col-sm-8" value="#rc.food.color#"/>
					<button class="btn btn-info col-xs-3 col-sm-3 btn-clear" type="button" onclick="return clearcolor();">
						#getLabel("Clear color")#
					</button>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label"></label>
				<div class="col-sm-7">
					<div class="row">
						<cfloop query="#rc.getcolor#">
							<input class="btn col-xs-3 col-sm-3 colorboxselect" id="color#rc.getcolor.foodId#" style="background: #rc.getcolor.color# !important" type="button" value="#rc.getcolor.color#" onclick="return getcolor(#rc.getcolor.foodId#);"/>
						</cfloop>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Sort Order")#</label>
				<div class="col-sm-9">
					<input type="text" id="fsorted" name="fsorted" placeholder="#getLabel("Sort Order")#" class="col-xs-10 col-sm-10" value="#rc.food.fsorted#"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Product code")#</label>
				<div class="col-sm-9">
					<input type="text" id="fcode" name="fcode" placeholder="#getLabel("Product code")#" class="col-xs-10 col-sm-10" value="#rc.food.fcode#" onchange="return checkCode(this.value);"/>
				</div>
				<label class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
					<p id="theErrorDivCode"></p>
				</div>	
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Key code")#</label>
				<div class="col-sm-8">
					<input type="text" id="keycode" name="keycode" placeholder="#getLabel("Key code")#" class="col-xs-8 col-sm-8" value="#rc.food.keycode#"/>
					<button class="btn btn-info col-xs-3 col-sm-3 btn-clear" type="button" onclick="return clearkey();">
						#getLabel("Clear key")#
					</button>
				</div>
				<label class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
					<p id="theErrorDivKeyCode"></p>
				</div>	
			</div>		

			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Food unit")#</label>
				<div class="col-sm-9">
					<select name="funit" id="funit" class="col-xs-10 col-sm-10" >
						<cfloop query="#rc.ListUnit#">
							<cfif #rc.food.funit# eq #rc.ListUnit.typeId#>
										<cfset selected="selected"/>
										<cfelse>
										<cfset selected=""/>
										</cfif>
							<option 		  	
								value="#rc.ListUnit.typeId#" #selected#>#rc.ListUnit.name#
							</option>
						</cfloop>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">#getLabel("Active")#</label>
				<div class="col-sm-9">
					<label>
						<cfif #rc.food.isactive# EQ false>
							<cfset checkDel = ""/>
						<cfelse>
							<cfset checkDel = "checked"/>
						</cfif>
						<input #checkDel# name="isactive" id="isactive" class="ace ace-switch ace-switch-2" type="checkbox" value="1"/>
						<span class="lbl"></span>
					</label>
				</div>
			</div>
			<cfif url.id eq 0>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="_addAnother">
						Add another
					</label>
					<div class="col-sm-9">
						<div class="clearfix">
							<cfif structKeyExists(url,"_addAnother") and url._addAnother eq true>
								<cfset checked = "checked">
							<cfelse>
								<cfset checked = "">
							</cfif>
							<input class="ace ace-switch ace-switch-5" type="checkbox" id="_addAnother" name="_addAnother" value="1" #checked#>
							<span class="lbl"></span>
						</div>
					</div>
				</div>
			</cfif>
			<div class="form-group">
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						#getLabel("Submit")#
					</button>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">#getLabel("Material list")#
            	<cfif #rc.foodId# gt 0>
            		<a class="btn btn-primary btn-white" onclick="return addMaterial()">
					<i class="ace-icon fa fa-plus"></i> #getLabel("Add")#</a>
            	</cfif>
        	</div>

            <!-- Table -->
            <div class="table-responsive">
              <table class="table" id="list_foodmaterial">
                <thead>
                  <tr>
                    <th>#getLabel("Name")#</th>
                    <th>#getLabel("Unit")#</th>
                    <th>#getLabel("Quantity")#</th>
                    <th>#getLabel("Estimate Price")#</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <cfset i=1/>
                  <cfloop query="#rc.FoodMaterial#">
                  	<tr id="del#rc.FoodMaterial.foodmaterialId#">
                      	<td>
                      		#rc.FoodMaterial.name#
                     	 </td>
                      	<td id="td_uname#rc.FoodMaterial.foodmaterialId#">
                      		#rc.FoodMaterial.uname#
                      	</td>
                      	<td id="td_estimate#rc.FoodMaterial.foodmaterialId#">
                      		#rc.FoodMaterial.estimate#
                     	 </td>
						<td id="td_estimateprice#rc.FoodMaterial.foodmaterialId#">
							#numberFormat(rc.FoodMaterial.estimateprice)#
						</td>
						<cfif #rc.FoodMaterial.misactive# eq 1>
							<td style="text-align:center">
								<div class="hidden-sm hidden-xs action-buttons">
								<a class="green" style="cursor:pointer;"  id="td_edit#rc.FoodMaterial.foodmaterialId#"  onclick="return editMaterial(#rc.FoodMaterial.foodmaterialId#,#rc.FoodMaterial.materialId#,#rc.FoodMaterial.unitId#,#rc.FoodMaterial.estimate#,#rc.FoodMaterial.unitprice#)">
									<i class="ace-icon fa fa-pencil bigger-130"></i>
								</a>
								</div>
							</td>
						<cfelse>
							<td></td>
						</cfif>
						<td style="text-align:center">
							<div class="hidden-sm hidden-xs action-buttons">
								<a class="red" style="cursor:pointer;" onclick="return deleteMaterial(#rc.FoodMaterial.foodmaterialId#)">
									<i class="ace-icon fa fa-trash-o bigger-130"></i>
								</a>
							</div>
						</td>
                    </tr>
                    <cfset i=i+1/>
                  </cfloop>
                </tbody>
               </table>
             </div>
            </div>
            <div id="showMaterial">

            	<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right">#getLabel("Material")#</label>
					<div class="col-sm-10">
						<select name="materialId" id="materialId" class="col-xs-10 col-sm-10" >
							<cfloop query="#rc.ListMaterial#">
								<cfloop query="#rc.FoodMaterial#">
									<cfif #rc.ListMaterial.materialId# eq 'temp'>
										<cfset selected="selected"/>
									<cfelse>
										<cfset selected=""/>
									</cfif>
								</cfloop>
								<option value="#rc.ListMaterial.materialId#" #selected#>#rc.ListMaterial.materialname#</option>
							</cfloop>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label no-padding-right">#getLabel("Unit")#</label>
					<div class="col-sm-10">
						<input type="number" id="unitid" name="unitid" class="col-xs-10 col-sm-10" value="#rc.ListMaterial.unitid#" hidden/>
						<input type="text" min="0" id="unit" name="unit" class="col-xs-10 col-sm-10" value="#rc.ListMaterial.unitname#" readonly/>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">#getLabel("Unit price")#</label>
					<div class="col-sm-10">
						<input type="text" min="0" id="unitprice" name="unitprice" class="col-xs-10 col-sm-10" value="#numberFormat(rc.ListMaterial.unitprice)#" readonly/>
					</div>
				</div>				
				<div class="form-group">
					<label class="col-sm-2 control-label">#getLabel("Quantity")#</label>
					<div class="col-sm-10">
						<input type="number" step="any" min="0" id="estimate" name="estimate" placeholder="#getLabel("Quantity")#" class="col-xs-10 col-sm-10" value=""  />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-10">
						<button class="btn btn-info" type="button" id="btnSaveMaterial" onclick="return SaveMaterial();">
							<i class="ace-icon fa fa-check bigger-110"></i>
							#getLabel("Save")#
						</button>
					</div>
				</div>

            </div>
		</div>
	</div>
</form>
</cfoutput>