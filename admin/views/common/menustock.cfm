<script type="text/javascript">
	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
	try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	jQuery(function() {
       jQuery(function() {
            jQuery('ul.nav-list li').each(function() {
                  var href = jQuery(this).find('a').attr('href');
                  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                   var iurl=(window.location.pathname+window.location.search);
                  if (href === window.location.pathname+"?"+hashes) {
                  	jQuery(this).parent().parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                  if (href === window.location.pathname) {
                  	jQuery(this).parent().parent().addClass('active open');
                  	jQuery(this).parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }

                 if (window.location.pathname.indexOf(href)>-1) {
                  	jQuery(this).parent().parent().addClass('active open');
                  	jQuery(this).parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                 
                  if(iurl.indexOf('isreceipt=1')>-1){
                  	$('#lireceipt').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('isreceipt=0')>-1){
                  	$('#lipayment').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('importsheet')>-1){
                  	$('#liimport').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('adjustsheet')>-1){
                  	$('#liadjust').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }
            }); 
    	});
	});  
</script>
<cfoutput>

<div id="sidebar" class="sidebar responsive sidebar-fixed">
	<ul class="nav nav-list" style="top: 0px;">
		<cfif SESSION.shopId gt 0>
			<li class="">
				<a href="/index.cfm/admin:stock.statisticsheet">
					<i class="menu-icon fa fa-pie-chart"></i>
					<span class="menu-text"> #getLabel("Statistic Sheet")# </span>
					
				</a>
				<b class="arrow"></b>
			</li>
			<li class="" id="liimport">
				<a href="/index.cfm/admin:stock.importlist">
					<i class="menu-icon fa fa-plus"></i>
					
					<span class="menu-text"> #getLabel("Purchase Voucher")# </span>
				</a>
				<b class="arrow"></b>
			</li>
			<li class="" id="liadjust">
				<a href="/index.cfm/admin:stock.adjustlist">
					<i class="menu-icon fa fa-adjust"></i>
					
					<span class="menu-text"> #getLabel("Adjust")# </span>
				</a>
				<b class="arrow"></b>
			</li>	
	    <li>
			<a href="/index.cfm/admin:material">
				<i class="menu-icon fa fa-list-alt"></i>
				<span class="menu-text"> #getLabel("Material")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		</cfif>
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>
</cfoutput>