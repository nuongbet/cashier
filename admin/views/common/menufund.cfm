<script type="text/javascript">
	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
	try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	jQuery(function() {
       jQuery(function() {
            jQuery('ul.nav-list li').each(function() {
                  var href = jQuery(this).find('a').attr('href');
                  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                   var iurl=(window.location.pathname+window.location.search);
                  if (href === window.location.pathname+"?"+hashes) {
                  	jQuery(this).parent().parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                  if (href === window.location.pathname) {
                  	jQuery(this).parent().parent().addClass('active open');
                  	jQuery(this).parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                  
                  if (window.location.pathname.indexOf(href)>-1) {
                  	jQuery(this).parent().parent().addClass('active open');
                  	jQuery(this).parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }

                  if(iurl.indexOf('isreceipt=1')>-1){
                  	$('#lireceipt').addClass('active');
                  	// $('#submenu').css('display',"block");
                  	// $('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('isreceipt=0')>-1){
                  	$('#lipayment').addClass('active');
                  	// $('#submenu').css('display',"block");
                  	// $('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('isreceipt=2')>-1){
                  	$('#liimport').addClass('active');
                  	// $('#submenu').css('display',"block");
                  	// $('.hsub').addClass('active open');
                  }
                   if(iurl.indexOf('dashboard')>-1){
                  	$('#lidashboard').addClass('active');
                  }

            }); 
    	});
	});  
</script>
<cfoutput>

<div id="sidebar" class="sidebar responsive sidebar-fixed">
	<ul class="nav nav-list" style="top: 0px;">
		<cfif SESSION.shopId gt 0>
		 <li id="lidashboard">
			<a href="/index.cfm/admin:dashboard.daily">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> #getLabel("Dashboard")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li id="lireceipt">
			<a href="/index.cfm/admin:stock.receiptlist?isreceipt=1">
				<i class="menu-icon fa fa-money"></i>
				<span class="menu-text"> #getLabel("Receipt Voucher")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		 <li id="lipayment">
			<a href="/index.cfm/admin:stock.receiptlist?isreceipt=0">
				<i class="menu-icon fa fa fa-money"></i>
				<span class="menu-text"> #getLabel("Payment Voucher")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:bill">
				<i class="menu-icon fa fa-file-text-o"></i>
				<span class="menu-text"> #getLabel("Invoice")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		 <li id="liimport">
			<a href="/index.cfm/admin:stock.receiptlist?isreceipt=2">
				<i class="menu-icon fa fa-plus"></i>
				<span class="menu-text"> #getLabel("Purchase Voucher")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li>
			<a href="/index.cfm/admin:report.default">
				<i class="menu-icon fa fa-file-text-o"></i>
				<span class="menu-text"> #getLabel("Sale report")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li>
			<a href="/index.cfm/admin:report.reportcash">
				<i class="menu-icon fa fa-file-text-o"></i>
				<span class="menu-text"> #getLabel("Cash report")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li>
			<a href="/index.cfm/admin:report.summarycash">
				<i class="menu-icon fa fa-file-text-o"></i>
				<span class="menu-text"> #getLabel("Summary report")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li>
			<a href="/index.cfm/admin:cashin.default">
				<i class="menu-icon fa fa-file-text-o"></i>
				<span class="menu-text"> #getLabel("Cash In Out")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li>
			<a href="/index.cfm/admin:cash.default">
				<i class="menu-icon fa fa-file-text-o"></i>
				<span class="menu-text"> #getLabel("tonghopdoanhthu")# </span>
			</a>
			<b class="arrow"></b>
		</li>			
		</cfif>
	</ul>
	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>
</cfoutput>