<div class="footer">
  <div class="footer-inner">
    <div class="footer-content">
		<span class="bigger-120">
			<a href="http://www.rasia.info/" target="_blank">Cashier</a>
			Application &copy; 2014
		</span>
    </div>
  </div>
</div>  
<script type="text/javascript">
	window.jQuery || document.write("<script src='/assets/js/jquery.min.js'>"+"<"+"/script>");
</script>

<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='/assets/js/jquery1x.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

<script src="/js/bootstrap-select.js"></script>

<script src="/assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="/assets/js/date-time/bootstrap-timepicker.min.js"></script>
<script src="/assets/js/date-time/moment.min.js"></script>
<script src="/assets/js/date-time/daterangepicker.min.js"></script>
<script src="/assets/js/date-time/bootstrap-datetimepicker.min.js"></script>
<!--[if lte IE 8]>
<script src="/assets/js/html5shiv.min.js"></script>
<script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
	if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/ace-elements.min.js"></script>
<script src="/assets/js/ace.min.js"></script>
<link rel="stylesheet" href="/assets/css/ace.onpage-help.css" />
<script type="text/javascript"> ace.vars['base'] = ''; </script>
<script src="/assets/js/ace/elements.onpage-help.js"></script>
<script src="/assets/js/ace/ace.onpage-help.js"></script>
<script src="/assets/js/jquery.dataTables.min.js"></script>