<script type="text/javascript">
	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
	try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	jQuery(function() {
       jQuery(function() {
            jQuery('ul.nav-list li').each(function() {
                  var href = jQuery(this).find('a').attr('href');
                  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                   var iurl=(window.location.pathname+window.location.search);
                  if (href === window.location.pathname+"?"+hashes) {
                  	jQuery(this).parent().parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                  if (href === window.location.pathname.split(".")[0]) {
                  	jQuery(this).parent().parent().addClass('active open');
                  	jQuery(this).parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }

                   if (window.location.pathname.indexOf(href)>-1) {
                  	jQuery(this).parent().parent().addClass('active open');
                  	jQuery(this).parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                 
                  if(iurl.indexOf('ca=2')>-1){
                  	$('#litablecategory').addClass('active');
                  }

                  if(iurl.indexOf('tag=unit')>-1){
                  	$('#liunitprice').addClass('active');
                  }

                  if(iurl.indexOf('tag=price')>-1){
                  	$('#lipricetype').addClass('active');
                  }

                  if(iurl.indexOf('ca=6')>-1){
                  	$('#lifoodcategory').addClass('active');
                  }

                  if(iurl.indexOf('foodprice')>-1){
                  	$('#lifood').removeClass('active');
                  }

                   if(iurl.indexOf('dashboard')>-1){
                  	$('#lidashboard').addClass('active');
                  }
            }); 
    	});
	});  
</script>
<cfoutput>

<div id="sidebar" class="sidebar responsive sidebar-fixed">
	<ul class="nav nav-list" style="top: 0px;">
		<cfif SESSION.shopId gt 0>
	  <!---   <li id="lidashboard">
			<a href="/index.cfm/admin:dashboard.daily">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> #getLabel("Dashboard")# </span>
			</a>
			<b class="arrow"></b>
		</li> --->
	    <li id="lifood">
			<a href="/index.cfm/admin:food">
				<i class="menu-icon fa fa fa-cutlery"></i>
				<span class="menu-text"> #getLabel("Food")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li id="lifoodcategory">
			<a href="/index.cfm/admin:category?ca=6">
				<i class="menu-icon fa fa-bars"></i>
				<span class="menu-text"> #getLabel("Food Category")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:foodprice">
				<i class="menu-icon fa fa-money"></i>
				<span class="menu-text"> #getLabel("Food price")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li id="lipricetype">
			<a href="/index.cfm/admin:type?tag=price">
				<i class="menu-icon fa fa-dollar"></i>
				<span class="menu-text"> #getLabel("Price Type")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		 <li id="liunitprice">
			<a href="/index.cfm/admin:type?tag=unit">
				<i class="menu-icon fa fa-dollar"></i>
				<span class="menu-text"> #getLabel("Unit Type")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		 <li>
			<a href="/index.cfm/admin:shift">
				<i class="menu-icon fa fa-dollar"></i>
				<span class="menu-text"> #getLabel("Shift")# </span>
			</a>
			<b class="arrow"></b>
		</li>					
	    <li>
			<a href="/index.cfm/admin:table">
				<i class="menu-icon fa fa-sitemap"></i>
				<span class="menu-text"> #getLabel("Tables")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li id="litablecategory">
			<a href="/index.cfm/admin:category?ca=2">
				<i class="menu-icon fa fa-bars"></i>
				<span class="menu-text"> #getLabel("Table Category")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:user">
				<i class="menu-icon fa fa fa-users"></i>
				<span class="menu-text"> #getLabel("Users")# </span>
			</a>
			<b class="arrow"></b>
		</li>	
		</cfif>
		<cfif SESSION.shopId EQ 0>
	    <li>
			<a href="/index.cfm/admin:shop">
				<i class="menu-icon fa fa-university"></i>
				<span class="menu-text"> #getLabel("Company")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		 <li>
			<a href="/index.cfm/admin:language">
				<i class="menu-icon fa fa-comment"></i>
				<span class="menu-text"> #getLabel("Language")# </span>
			</a>
			<b class="arrow"></b>
		</li>		
		</cfif>
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>
</cfoutput>