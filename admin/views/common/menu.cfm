<script type="text/javascript">
	try{ace.settings.check('main-container' , 'fixed')}catch(e){}
	try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	jQuery(function() {
       jQuery(function() {
            jQuery('ul.nav-list li').each(function() {
                  var href = jQuery(this).find('a').attr('href');
                  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                   var iurl=(window.location.pathname+window.location.search);
                  if (href === window.location.pathname+"?"+hashes) {
                  	jQuery(this).parent().parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                  if (href === window.location.pathname) {
                  	jQuery(this).parent().parent().addClass('active open');
                  	jQuery(this).parent().addClass('active open');
                    jQuery(this).addClass('active');
                  }
                 
                  if(iurl.indexOf('isreceipt=1')>-1){
                  	$('#lireceipt').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('isreceipt=0')>-1){
                  	$('#lipayment').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('importsheet')>-1){
                  	$('#liimport').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }

                  if(iurl.indexOf('adjustsheet')>-1){
                  	$('#liadjust').addClass('active');
                  	$('#submenu').css('display',"block");
                  	$('.hsub').addClass('active open');
                  }
            }); 
    	});
	});  
</script>
<cfoutput>

<div id="sidebar" class="sidebar responsive sidebar-fixed">
	<ul class="nav nav-list" style="top: 0px;">
		<cfif SESSION.shopId gt 0>
	    <li>
			<a href="/index.cfm/admin:main">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> #getLabel("Dashboard")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="##" class="dropdown-toggle">
				<i class="menu-icon fa fa-tasks"></i>
				<span class="menu-text"> #getLabel("Stock")# </span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu nav-show" id="submenu">
				<li class="">
					<a href="/index.cfm/admin:stock.statisticsheet">
						<i class="ace-icon fa fa-pie-chart"></i>
						#getLabel("Statistic Sheet")#
					</a>
					<b class="arrow"></b>
				</li>
				<li class="" id="liimport">
					<a href="/index.cfm/admin:stock.importlist">
						<i class="ace-icon fa fa-plus"></i>
						#getLabel("Import")#
					</a>
					<b class="arrow"></b>
				</li>
				<li class="" id="liadjust">
					<a href="/index.cfm/admin:stock.adjustlist">
						<i class="ace-icon fa fa-adjust"></i>
						#getLabel("Adjust")#
					</a>
					<b class="arrow"></b>
				</li>
				<li class="" id="lireceipt">
					<a href="/index.cfm/admin:stock.receiptlist?isreceipt=1">
						<i class="ace-icon fa fa-money"></i>
						#getLabel("Receipt Voucher")#
					</a>
					<b class="arrow"></b>
				</li>	
				<li class="" id="lipayment">
					<a href="/index.cfm/admin:stock.receiptlist?isreceipt=0">
						<i class="ace-icon fa fa-bank"></i>
						#getLabel("Payment Voucher")#
					</a>
					<b class="arrow"></b>
				</li>				
			</ul>
		</li>
	    <li>
			<a href="/index.cfm/admin:material">
				<i class="menu-icon fa fa-list-alt"></i>
				<span class="menu-text"> #getLabel("Material")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:bill">
				<i class="menu-icon fa fa-file-text-o"></i>
				<span class="menu-text"> #getLabel("Bill")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:food">
				<i class="menu-icon fa fa fa-cutlery"></i>
				<span class="menu-text"> #getLabel("Food")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li>
			<a href="/index.cfm/admin:category?ca=6">
				<i class="menu-icon fa fa-bars"></i>
				<span class="menu-text"> #getLabel("Food Category")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:foodprice">
				<i class="menu-icon fa fa-money"></i>
				<span class="menu-text"> #getLabel("Food price")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:type">
				<i class="menu-icon fa fa-dollar"></i>
				<span class="menu-text"> #getLabel("Price Type")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		 <li>
			<a href="/index.cfm/admin:shift">
				<i class="menu-icon fa fa-dollar"></i>
				<span class="menu-text"> #getLabel("Shift")# </span>
			</a>
			<b class="arrow"></b>
		</li>					
	    <li>
			<a href="/index.cfm/admin:table">
				<i class="menu-icon fa fa-sitemap"></i>
				<span class="menu-text"> #getLabel("Tables")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		<li>
			<a href="/index.cfm/admin:category?ca=2">
				<i class="menu-icon fa fa-bars"></i>
				<span class="menu-text"> #getLabel("Table Category")# </span>
			</a>
			<b class="arrow"></b>
		</li>
	    <li>
			<a href="/index.cfm/admin:user">
				<i class="menu-icon fa fa fa-users"></i>
				<span class="menu-text"> #getLabel("Users")# </span>
			</a>
			<b class="arrow"></b>
		</li>	
		</cfif>
		<cfif SESSION.shopId EQ 0>
	    <li>
			<a href="/index.cfm/admin:shop">
				<i class="menu-icon fa fa-university"></i>
				<span class="menu-text"> #getLabel("Company")# </span>
			</a>
			<b class="arrow"></b>
		</li>
		 <li>
			<a href="/index.cfm/admin:language">
				<i class="menu-icon fa fa-comment"></i>
				<span class="menu-text"> #getLabel("Language")# </span>
			</a>
			<b class="arrow"></b>
		</li>		
		</cfif>
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>
</cfoutput>