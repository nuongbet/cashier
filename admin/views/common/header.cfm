<cfoutput>
<cfquery name="qGetnotice">
	select count(materialId) as count from material where actualstock <= ministock and shopId=#SESSION.ShopId# and isactive=1
</cfquery>
<cfquery name="qGetlogoandname">
	SELECT name,logo FROM shop WHERE shopId=#SESSION.ShopId#
</cfquery>
<div id="navbar" class="navbar navbar-default    navbar-collapse       h-navbar">
	<script type="text/javascript">
		try{ace.settings.check('navbar' , 'fixed')}catch(e){}
	</script>
	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
	</script>
	<div class="navbar-container" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="/index.cfm/admin:main" class="navbar-brand">
				<small>
					<cfif SESSION.shopId NEQ 0>
						<img src="/images/shop/#qGetlogoandname.logo#" class="logo">
						#qGetlogoandname.name#
						<cfelse>
						<i class="fa fa-paw"></i>
						Cashier
					</cfif>
				</small>
			</a>

			<button class="pull-right navbar-toggle navbar-toggle-img collapsed" type="button" data-toggle="collapse" data-target=".navbar-buttons,.navbar-menu">
				<span class="sr-only">Toggle user menu</span>
			</button>

			<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".sidebar">
				<span class="sr-only">Toggle sidebar</span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>

				<span class="icon-bar"></span>
			</button>
		</div>

		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">
				<cfif SESSION.ShopId NEQ 0>
					<li class="purple">
						<a href="/index.cfm/admin:material?type=2">
							<i class="ace-icon fa fa-bell icon-animated-bell"></i>
							<span class="badge badge-important">#qGetnotice.count#</span>
						</a>
					</li>	
				</cfif>
				<li class="light-blue user-min">
					<a data-toggle="dropdown" href="##" class="dropdown-toggle">
						<span>
							<small>#getLabel("Welcome,")#</small>
							#SESSION.UserName#
						</span>
						<i class="ace-icon fa fa-caret-down"></i>
					</a>
					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li>
							<a href="/index.cfm/admin:main">
								<i class="ace-icon fa fa-external-link"></i>
								#getLabel("Home page")#
							</a>
						</li>
						<li>
							<a href="/index.cfm/admin:user.form?id=#SESSION.UserId#">
								<i class="ace-icon fa fa-user"></i>
								#getLabel("Profile")#
							</a>
						</li>

						<li class="divider"></li>

						<li>
							<a href="/index.cfm/admin:common.logout">
								<i class="ace-icon fa fa-power-off"></i>
								#getLabel("Logout")#
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
</cfoutput>