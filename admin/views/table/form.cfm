<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
	<cfparam name="rc.desk.deskId"			default=""/>
	<cfparam name="rc.desk.name"			default=""/>
	<cfparam name="rc.desk.categoryId"		default=""/>
	<cfparam name="rc.desk.type"			default=""/>
	<cfparam name="rc.desk.isactive"		default=""/>
<div class="page-header">
  <h1>
	#getLabel("Tables")#
  </h1>
</div>
<form class="form-horizontal" role="form" action="" method="POST">
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Name")#</label>
		<div class="col-sm-10">
			<input type="text" id="name" name="name" required placeholder="#getLabel("Name")#" class="col-xs-10 col-sm-5" value="#rc.desk.name#" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Position")#</label>
		<div class="col-sm-10">
			<select name="pos" id="pos" class="col-xs-10 col-sm-5" >
				<cfloop query="#rc.ListPos#">
					<cfif #rc.desk.categoryId# eq #rc.ListPos.categoryId#>
								<cfset selected="selected"/>
								<cfelse>
								<cfset selected=""/>
								</cfif>
					<option 		  	
						value="#rc.ListPos.categoryId#" #selected#>#rc.ListPos.categoryname#
					</option>
				</cfloop>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Type")#</label>
		<div class="col-sm-10">
			<input type="text" id="type" name="type" placeholder="Table type" class="col-xs-10 col-sm-5" value="#rc.desk.type#" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Active")#</label>
		<div class="col-sm-10">
			<label>
				<cfif #rc.desk.isactive# EQ false>
					<cfset checkDel = ""/>
				<cfelse>
					<cfset checkDel = "checked"/>
				</cfif>
				<input #checkDel# name="isactive" id="isactive" class="ace ace-switch ace-switch-2" type="checkbox" value="1"/>
				<span class="lbl"></span>
			</label>
		</div>
	</div>
	<div class="form-group">

	</div>
	<div class="form-group">
		
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<button class="btn btn-info" type="submit">
				<i class="ace-icon fa fa-check bigger-110"></i>
				#getLabel("Submit")#
			</button>
		</div>
	</div>

</form>
</cfoutput>