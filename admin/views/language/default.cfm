<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<cfif SESSION.shopId NEQ 0>
	<cflocation url="/index.cfm/admin:"/>
</cfif>
	<div class="row clearfix">
		<div class="col-md-12" style="border-bottom:2px ##428bca solid;">
			<h1 class="blue">#getLabel("Language management")#</h1>			
		</div>
		<small class="">(<strong class="red">*</strong>) #getLabel("Detail have")# <abbr>#getLabel("underline")#</abbr> #getLabel("can be changed by click on it")#.</small>
	</div>
	<div class="row clearfix" style="margin-top:20px;">
		<div class="col-md-12">
			<button class="btn btn-sm btn-success pull-right" onclick="add();" onmouseover="addBig();" onmouseout="addNormal();">
				<i class="menu-icon glyphicon glyphicon-bookmark"><span id="aLang" style="font-family: 'Open Sans';"></span></i>
			</button>
		</div>
		<hr>
		<div class="col-md-12">
            <table id="language" class="datatable table-bordered col-md-12">
	            <thead>
					<tr>
	                    <th>&nbsp;#getLabel("Key word")#</th>
	                    <th>&nbsp;#getLabel("Value")#</th>
	                    <th>&nbsp;#getLabel("Language")#</th>
	                    <th>&nbsp;#getLabel("Actions")#</th>
	                </tr>
	            </thead>
	            <tbody id="tbLabel">
	                <cfloop query=#rc.lang# >
						<tr id="tr#rc.lang.labelId#">
	                        <td id="keyword#rc.lang.labelId#"> <abbr id="spankeyword#rc.lang.labelId#"  onClick='changekeyword("#rc.lang.labelId#")' title="Click to change key word">&nbsp;#rc.lang.keyword#</abbr><i class="ace-icon fa fa-pencil-square-o  bigger-110 icon-on-right" onClick='changekeyword("#rc.lang.labelId#")'></i>&nbsp;</td>
	                        <td id="value#rc.lang.labelId#"> <abbr id="spanvalue#rc.lang.labelId#"  onClick='changevalue("#rc.lang.labelId#")' title="Click to change value">&nbsp;#rc.lang.value#</abbr><i class="ace-icon fa fa-pencil-square-o  bigger-110 icon-on-right" onClick='changevalue("#rc.lang.labelId#")'></i>&nbsp;</td>
	                        <td class="col-md-2">
	                        	<select id="s#rc.lang.labelId#" onchange="changelanguageid(#rc.lang.labelId#)">
	                        		<cfloop query=#rc.list#>
	                        			<option value="#rc.list.languageId#" <cfif rc.list.languageId EQ rc.lang.languageId>SELECTED</cfif>>#rc.list.language#</option>
	                        			
	                        		</cfloop>
	                        	</select>
	                        </td>
	                        <td class="col-md-1">
								<div class="hidden-sm hidden-xs action-buttons">
									<a class="red" onclick="del(#rc.lang.labelId#)">
										<i class="ace-icon fa fa-trash-o bigger-130"></i>
									</a>
								</div>
							</td>
	                    </tr>
	                </cfloop>
	            </tbody>
	        </table>
	    </div>
	</div>
	<div class="modal fade" id="addNew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  	<div class="modal-dialog">
		    <div class="modal-content alert-info">
		      	<div class="modal-header alert-info">
		        	<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">#getLabel("Close")#</span></button>
		        	<h3 class="modal-title" id="myModalLabel">#getLabel("Add new label")#</h3>
		      	</div>
		      	<div class="modal-body">					
					<div class="row">
			            <label class="col-md-2 control-label" for="name">#getLabel("Key word")#</label>
			            <div class="col-md-10">
			                <input id="newkey" type="text" class="col-md-11">
			            </div>
		            </div>
		            <div class="row margin-top-20" id="rowResolution">
			            <label class="col-md-2 control-label" for="soSelect">#getLabel("Value")#</label>
			            <div class="col-md-10">
			                <input id="newvalue" type="text" class="col-md-11">
			            </div>
		            </div>									
					<div class="row margin-top-20">
						<label class="col-md-2 control-label">#getLabel("Language")#</label>
						<div class="col-md-10">
							<select id="newlangid">
	                        		<cfloop query=#rc.list#>
	                        			<option value="#rc.list.languageId#">#rc.list.language#</option>
	                        		</cfloop>
	                        	</select>
						</div>
					</div>					
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">#getLabel("Close")#</button>
		        	<button type="button" class="btn btn-info" onclick="addLabel();">#getLabel("Save changes")#</button>
		      	</div>
		    </div>
	  	</div>
	</div>
</cfoutput>
<script type="text/javascript">
    $(document).ready(function(){
        $('#language').dataTable();
    });

    function del(id)
    {
    	if(confirm("Are you want to delete this label?"))
    	{
	    	$.ajax({
		        url: '/index.cfm/admin:language.deletelabel',
		        method:'POST',
		        dataType: 'json',
		        data: {
		        	labelId : id
		        },
		        success: 	function(data) {
	        		if(data)
        			{
        				$("#tr"+id).remove();
        			}
	        	}	
			});
    	}
    }

    function addBig()
	{
		$("#aLang").text(" <cfoutput>#getLabel('Add Label')#</cfoutput>");
	}
	function addNormal()
	{
		$("#aLang").text("");
	}

    function add()
    {
    	$('#addNew').modal({show:true});
    }
    function changelanguageid(id)
    {
    	var langId = $("#s"+id).val();
    	$.ajax({
		        url: '/index.cfm/admin:language.changelanguageid',
		        method:'POST',
		        dataType: 'json',
		        data: {
		        	id: id,
		        	languageId:langId
		        }	
		    });
    }

    function addLabel()
    {
    	var newKey = $("#newkey").val();
    	var newVal = $("#newvalue").val();
    	var langId = $("#newlangid").val();
    	var selected="";
    	switch(parseInt(langId)) {
		    case 1:
		        selected="selected";
		        break;
		    case 2:
		        selected="selected";
		        break;
		    case 3:
		        selected="selected";
		        break;
		}
		console.log(selected);
		console.log(langId);
    	$.ajax({
		        url: '/index.cfm/admin:language.addnewlabel/',
		        method:'POST',
		        dataType: 'json',
		        data: {
		        	keyword : newKey,
		        	value : newVal,
		        	languageId : langId
		        },
		        success: 	function(data) {
		        		if(data.SUCCESS)
	        			{
	        				$("#tbLabel").append('\
	        					<tr id="tr'+data.LABELID+'">\
	        						<td id="keyword'+data.LABELID+'"> <abbr id="spankeyword'+data.LABELID+'" onClick="changekeyword('+data.LABELID+')" title="Click to change key word">&nbsp;'+newKey+'</abbr><i class="ace-icon fa fa-pencil-square-o  bigger-110 icon-on-right" onClick="changekeyword('+data.LABELID+')"></i>&nbsp;</td>\
	        						<td id="value'+data.LABELID+'"> <abbr id="spanvalue'+data.LABELID+'"  onClick="changevalue('+data.LABELID+')" title="Click to change value">&nbsp;'+newVal+'</abbr><i class="ace-icon fa fa-pencil-square-o  bigger-110 icon-on-right" onClick="changevalue('+data.LABELID+')"></i>&nbsp;</td>\
	        						<td class="col-md-2">\
			                        	<select id="s'+data.LABELID+'" onchange="changelanguageid('+data.LABELID+')">\
			                        		<option value="1" '+selected+'>German</option>\
			                        		<option value="2" '+selected+'>English</option>\
			                        		<option value="3" '+selected+'>VietNam</option>\
			                        	</select>\
			                        </td>\
			                        <td class="col-md-2">\
			                        	<div class="ui-pg-div col-md-1"><span class="ui-icon ace-icon fa fa-plus-circle purple" data-toggle="modal" onclick="add()"></span></div>\
			                        	<div class="hidden-sm hidden-xs action-buttons"><a class="red" onclick="del('+data.LABELID+')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>\
			                        </td>\
	        					');
								$("#newkey").val("");
								$("#newvalue").val("");
	        			}
		        	}	
		        });
    }
    

	function closeInput1(id)
	{
		var keyword = $("#input"+id).val();
		if($("#spankeyword"+id).text() != keyword){
			$("#spankeyword"+id)
				.text(keyword);
			$.ajax({
		        url: '/index.cfm/admin:language.changekeyword',
		        method:'POST',
		        dataType: 'json',
		        data: {
		        	id: id,
		        	key:keyword
		        },
		        success: 	function(data) {
		        		if(data)
		        			$("#spankeyword"+id).addClass("green");
		        	}	
		        });
		    window.setTimeout(function(){$("#spankeyword"+id).removeClass("green")},2000);
		}
		$("#spankeyword"+id).removeClass("hide");
		var element = document.getElementById("input"+id);
		element.parentNode.removeChild(element);
	}

	function changekeyword(id){
		var stringId = $("#spankeyword"+id).text();
		$("#spankeyword"+id).addClass("hide");
		$("#keyword"+id).prepend('<input id="input'+id+'" type="text" style="width:90%" onBlur="closeInput1('+id+')">');
		$("#input"+id)
			.attr('value',$("#keyword"+id).text() )
			.focus();
	}

	function closeInput2(id)
	{
		var value = $("#input"+id).val();
		if($("#spanvalue"+id).text() != value){
			$("#spanvalue"+id)
				.text(value);
			$.ajax({
		        url: '/index.cfm/admin:language.changevalue',
		        method:'POST',
		        dataType: 'json',
		        data: {
		        	id: id,
		        	key:value
		        },
		        success: 	function(data) {
		        		if(data)
		        			$("#spanvalue"+id).addClass("green");
		        	}	
		        });
		    window.setTimeout(function(){$("#spanvalue"+id).removeClass("green")},2000);
		}
		$("#spanvalue"+id).removeClass("hide");
		var element = document.getElementById("input"+id);
		element.parentNode.removeChild(element);
	}

	function changevalue(id){
		var stringId = $("#spanvalue"+id).text();
		$("#spanvalue"+id).addClass("hide");
		$("#value"+id).prepend('<input id="input'+id+'" type="text" style="width:90%" onBlur="closeInput2('+id+')">');
		$("#input"+id)
			.attr('value',$("#value"+id).text() )
			.focus();
	}
</script>
