<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	function chkUser(theUser){
	  $.getJSON("../admin/controllers/checkuser.cfc", {
	   method: 'chkUser',
	   user: theUser,
	   returnformat: 'json'
	   }, 

	   function(isUserUnique){  
	   if (isUserUnique == true) {
	   	var lb='#getLabel("is exists, Please select a new Username")#';
	   $("##theErrorDivIDUser").html('['+theUser+'] '+lb);
	 	  $('##username').val("");
	   }
	   else {
	   $("##theErrorDivIDUser").html('');
	   }
	   });
	};

	function chkEmail(theEmail){
		if("#rc.users.email#" != theEmail){
		$.getJSON("../admin/controllers/checkuser.cfc", {
		   method: 'chkEmail',
		   email: theEmail,
		   returnformat: 'json'
		   }, 

		   function(isEmailUnique){  
		   if (isEmailUnique == true) {
		   	var lb='#getLabel("is exists, Please select a new Email")#';
		   $("##theErrorDivIDEmail").html('['+theEmail+'] '+lb);
		 	  $('##email').val("");
		   }
		   else {
		   $("##theErrorDivIDEmail").html('');
		   }
		   });
		}
		else{
			$("##theErrorDivIDEmail").html('');
		}
	};

	//set value when checkbox change
	$(document).ready(function(){
		if(#rc.users.isActive#==0)
		{
			$("##isActive").val('0');
		}
		else
		{
			$("##isActive").val('1');
		}
	$('##isActive').change(function(){
	    if($(this).prop('checked')){
	          $(this).val('1');
	     }else{
	          $(this).val('0');
	     }
	 });
	});
</script>
<div class="page-header">
  <h1>
	#getLabel("Users")#
  </h1>
</div>
<form class="form-horizontal" role="form" action="" method="POST">
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Company name")#</label>
		<div class="col-sm-10">
			<input type="text" id="shopname" name="shopname" class="col-xs-10 col-sm-5" value="#rc.shop.name#" readonly/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("First name")#</label>
		<div class="col-sm-10">
			<input type="text" id="firstname" name="firstname" placeholder="#getLabel("First name")#" class="col-xs-10 col-sm-5" value="#rc.users.firstname#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Last name")#</label>
		<div class="col-sm-10">
			<input type="text" id="lastname" name="lastname" placeholder="#getLabel("Last name")#" class="col-xs-10 col-sm-5" value="#rc.users.lastname#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Phone")#</label>
		<div class="col-sm-10">
			<input type="number" id="phone" name="phone" placeholder="#getLabel("Phone")#" class="col-xs-10 col-sm-5" value="#rc.users.phone#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Email")#</label>
		<div class="col-sm-10">
			<input type="email" id="email" name="email" placeholder="#getLabel("Email")#" class="col-xs-10 col-sm-5" value="#rc.users.email#" required onchange="return chkEmail(this.value);"/>
		</div>
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<p id="theErrorDivIDEmail"></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Username")#</label>
		<div class="col-sm-10">
			<input type="text" id="username" name="username" placeholder="#getLabel("Username")#" class="col-xs-10 col-sm-5" value="#rc.users.username#" onchange="return chkUser(this.value);" required />
		</div>
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<p id="theErrorDivIDUser"></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Password")#</label>
		<div class="col-sm-10">
			<input type="password" id="password" name="password" placeholder="#getLabel("Password")#" class="col-xs-10 col-sm-5" value="#rc.users.password#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Type")#</label>
		<div class="col-sm-10">
			<select name="tag" id="tag" class="col-xs-10 col-sm-5" >
				<cfloop array="#rc.ListType#" item="iType">
					<cfif #rc.users.typeId# eq #iType.typeId#>
						<cfset selected="selected"/>
					<cfelse>
						<cfset selected=""/>
					</cfif>
					<option value="#iType.typeId#" #selected#>#iType.name#</option>
				</cfloop>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right">#getLabel("Active")#</label>
		<div class="col-sm-10">
			<input name="isActive" id="isActive" value="#rc.users.isActive#" class="ace ace-switch ace-switch-4" type="checkbox" <cfif #rc.users.isActive# >
				checked
			</cfif>>
			<span class="lbl"></span>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<button class="btn btn-info" type="submit">
				<i class="ace-icon fa fa-check bigger-110"></i>
				#getLabel("Submit")#
			</button>
		</div>
	</div>

</form>
</cfoutput>