<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	function isNumberKey(evt)
	    {
	       var charCode = (evt.which) ? evt.which : event.keyCode
	       if (charCode > 31 && (charCode < 48 || charCode > 57))
	          return false;

	       return true;
	}
	function format(input)
	{
	    var nStr = input.value + '';
	    nStr = nStr.replace( /\,/g, "");
	    x = nStr.split( '.' );
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while ( rgx.test(x1) ) {
	        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
	    }
	    input.value = x1 + x2;
	}
	function chkUser(theUser){
	  $.getJSON("../admin/controllers/checkuser.cfc", {
	   method: 'chkUser',
	   user: theUser,
	   returnformat: 'json'
	   }, 

	   function(isUserUnique){  
	   if (isUserUnique == true) {
	   var lb='#getLabel("is exists, Please select a new Username")#';
	   $("##theErrorDivIDUser").html('['+theUser+'] '+lb);
	 	  $('##username').val("");
	   }
	   else {
	   $("##theErrorDivIDUser").html('');
	   }
	   });
	};

	function chkEmail(theEmail){
		if("#rc.users.email#" != theEmail){
		$.getJSON("../admin/controllers/checkuser.cfc", {
		   method: 'chkEmail',
		   email: theEmail,
		   returnformat: 'json'
		   }, 

		   function(isEmailUnique){  
		   if (isEmailUnique == true) {
		   	var lb='#getLabel("is exists, Please select a new Email")#';
		   $("##theErrorDivIDEmail").html('['+theEmail+'] '+lb);
		 	  $('##email').val("");
		   }
		   else {
		   $("##theErrorDivIDEmail").html('');
		   }
		   });
		}
		else{
			$("##theErrorDivIDEmail").html('');
		}
	};
	//set value when checkbox change
	$(document).ready(function(){
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		if(#rc.users.isActive#==0)
		{
			$("##isActive").val('0');
		}
		else
		{
			$("##isActive").val('1');
		}
	$('##isActive').change(function(){
	    if($(this).prop('checked')){
	          $(this).val('1');
	     }else{
	          $(this).val('0');
	     }

	 });

	});
</script>
<div class="page-header">
  <h1>
	Users
  </h1>
</div>
<form class="form-horizontal" role="form" action="" method="POST">
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Company name")#</label>
		<div class="col-sm-10">
			<input type="text" id="shopname" name="shopname" class="col-xs-10 col-sm-5" value="#rc.shop.name#" readonly/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Last name")#</label>
		<div class="col-sm-10">
			<input type="text" id="firstname" name="firstname" placeholder="#getLabel("First name")#" class="col-xs-10 col-sm-5" value="#rc.users.firstname#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("First name")#</label>
		<div class="col-sm-10">
			<input type="text" id="lastname" name="lastname" placeholder="#getLabel("Last name")#" class="col-xs-10 col-sm-5" value="#rc.users.lastname#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("cmnd")#</label>
		<div class="col-sm-10">
			<input type="text" id="cmnd" name="cmnd" placeholder="#getLabel("cmnd")#" class="col-xs-10 col-sm-5" value="#rc.users.cmnd#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("dateofbirth")#</label>
		<div class="col-sm-10 form-inline">
			<input class="form-control date-picker" name="dateofbirth" id="dateofbirth" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(rc.users.dateofbirth,"dd-mm-yyyy","pt_PT")#" placeholder="#getLabel("dateofbirth")#" required/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Phone")#</label>
		<div class="col-sm-10">
			<input type="number" id="phone" name="phone" placeholder="#getLabel("Phone")#" class="col-xs-10 col-sm-5" value="#rc.users.phone#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("datestart")#</label>
		<div class="col-sm-10 form-inline">
			<input class="form-control date-picker" name="datestart" id="datestart" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(rc.users.datestart,"dd-mm-yyyy","pt_PT")#" placeholder="#getLabel("datestart")#" required/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("currentsalary")#</label>
		<div class="col-sm-10">
			<input type="text" id="currentsalary" name="currentsalary" placeholder="#getLabel("currentsalary")#" class="col-xs-10 col-sm-5" value="#NumberFormat(rc.users.currentsalary)#" required  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("holdsalary")#</label>
		<div class="col-sm-10">
			<input type="text" id="holdsalary" name="holdsalary" placeholder="#getLabel("holdsalary")#" class="col-xs-10 col-sm-5" value="#NumberFormat(rc.users.holdsalary)#" required  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Email")#</label>
		<div class="col-sm-10">
			<input type="email" id="email" name="email" placeholder="#getLabel("Email")#" class="col-xs-10 col-sm-5" value="#rc.users.email#" onchange="return chkEmail(this.value);"/>
		</div>
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<p id="theErrorDivIDEmail"></p>
		</div>	
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Username")#</label>
		<div class="col-sm-10">
			<input type="text" id="username" name="username" placeholder="#getLabel("Username")#" class="col-xs-10 col-sm-5" value="#rc.users.username#" onchange="return chkUser(this.value);" <cfif URL.id GT 0>required readonly</cfif> />
		</div>
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<p id="theErrorDivIDUser"></p>
		</div>		
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Password")#</label>
		<div class="col-sm-10">
			<input type="password" id="password" name="password" placeholder="#getLabel("Password")#" class="col-xs-10 col-sm-5" value="#rc.users.password#" required />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Type")#</label>
		<div class="col-sm-10">
			<select name="tag" id="tag" class="col-xs-10 col-sm-5" >
				<cfloop array="#rc.ListType#" item="iType">
					<cfif #rc.users.typeId# eq #iType.typeId#>
						<cfset selected="selected"/>
					<cfelse>
						<cfset selected=""/>
					</cfif>
					<option value="#iType.typeId#" #selected#>#iType.name#</option>
				</cfloop>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Language")#</label>
		<div class="col-sm-10">
			<select name="languageId" id="languageId" class="col-xs-10 col-sm-5" >
				<cfloop array="#rc.ListLanguage#" item="iLanguage">
					<cfif #rc.users.languageId# eq #iLanguage.languageId#>
						<cfset selected="selected"/>
					<cfelse>
						<cfset selected=""/>
					</cfif>
					<option value="#iLanguage.languageId#" #selected#>#iLanguage.language#</option>
				</cfloop>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right">#getLabel("Active")#</label>
		<div class="col-sm-10">
			<input name="isActive" id="isActive" value="#rc.users.isActive#" class="ace ace-switch ace-switch-4" type="checkbox" <cfif #rc.users.isActive# >
				checked
			</cfif>>
			<span class="lbl"></span>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<button class="btn btn-info" type="submit">
				<i class="ace-icon fa fa-check bigger-110"></i>
				#getLabel("Submit")#
			</button>
		</div>
	</div>

</form>
</cfoutput>