<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
    jQuery(function($) {
		var oTable1 = $('##list_user')
		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,null,{ "bSortable": false }]
	    } );
	   })
</script>

<div class="page-header">
  <h1>
	#getLabel("Users")# 
  </h1>
</div>
<div class="col-xs-12">
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:user.form">
		<i class="ace-icon fa fa-plus"></i> #getLabel("Add New")# 
	</a>
	<p></p>
		<table id="list_user" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("ID")#</th>
					<th>#getLabel("Name")#</th>
					<th>#getLabel("Phone")#</th>
					<th>#getLabel("Username")#</th>
					<th>#getLabel("Active")#</th>
					<th>#getLabel("Type")#</th>
					<th></th>
					<!--- <th></th> --->
				</tr>
			</thead>

			<tbody>
				<cfset i=1/>
				<cfloop query="rc.users">
				<tr>
					<td>
						#i#
					</td>
					<td>
						<a class="blue" href="/index.cfm/admin:user.form?id=#rc.users.userId#">
						#rc.users.firstname# #rc.users.lastname#
					</td>
					<td>
						#rc.users.phone#
					</td>
					<td>
						#rc.users.username#
					</td>
					<td>
						<cfif #rc.users.isactive# EQ true>
							<i class="ace-icon fa fa-check-circle-o green bigger-130"></i>
							<cfelse>
							<i class="ace-icon fa fa-circle red bigger-130"></i>
						</cfif>
					</td>
					<td>
						#rc.users.typeUser#
					</td>
					<td style="text-align:center">
						<a class="green" href="/index.cfm/admin:user.form?id=#rc.users.userId#">
							<i class="ace-icon fa fa-pencil bigger-130"></i>
						</a>
					</td>	
					<!--- <td style="text-align:center">
						<a class="red" href="" onclick="return checkDelete()">
							<i class="ace-icon fa fa-trash-o bigger-130"></i>
						</a>
					</td> --->	
				</tr>
				<cfset i=i+1/>
				</cfloop>
			</tbody>
		</table>
	</div>
</cfoutput>