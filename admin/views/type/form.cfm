<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	//set value when checkbox change
	$(document).ready(function(){
		if(#rc.types.isActive#==0)
		{
			$("##isActive").val('0');
		}
		else
		{
			$("##isActive").val('1');
		}
	$('##isActive').change(function(){
	    if($(this).prop('checked')){
	          $(this).val('1');
	     }else{
	          $(this).val('0');
	     }

	 });

	});
</script>

<div class="page-header">
  <h1>
	#getLabel("Type")#
  </h1>
</div>
<div class="row">
	<div class="col-xs-12">
		<form id="cms-form" class="form-horizontal" role="form" action="##" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Name")#</label>
				<div class="col-sm-10">
					<input type="text" id="name" name="name" value="#rc.types.name#" placeholder="#getLabel("Name")#" class="col-xs-10 col-sm-5" required/>
				</div>
			</div>
		<!--- 	<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">Tag</label>
				<div class="col-sm-10">
					<cfswitch expression="#rc.types.tag#">
						<cfcase value="user">
							<cfset selected="selected">
							<cfset selected1="">
						</cfcase>
						<cfcase value="price">
							<cfset selected="">
							<cfset selected1="selected">
						</cfcase>
						<cfdefaultcase>
							<cfset selected="">
							<cfset selected1="">
						</cfdefaultcase>
					</cfswitch>
					<select name="tag" id="tag" class="col-xs-10 col-sm-5" >
						<option value="user" #selected#>User</option>
						<option value="price" #selected1#>Price</option>
					</select>
				</div>
			</div> --->
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Active")#</label>
				<div class="col-sm-10">
					<input name="isActive" id="isActive" value="#rc.types.isActive#" class="ace ace-switch ace-switch-4" type="checkbox" <cfif #rc.types.isActive# >
						checked
					</cfif>>
					<span class="lbl"></span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right"></label>
				<div class="col-sm-10">
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						#getLabel("Submit")#
					</button>
				</div>
			</div>			
		</form>
	</div>
</div>
</cfoutput>