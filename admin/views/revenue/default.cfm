<cfoutput>
<script type="text/javascript">
  function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
jQuery(function($) {
	//datepicker plugin
	//link
	$('.date-picker').datepicker({
		autoclose: true,
		todayHighlight: true
	})
	//show datepicker when clicking on the icon
	.next().on(ace.click_event, function(){
		$(this).prev().focus();
	});
	//or change it into a date range picker
	$('.input-daterange').datepicker({autoclose:true});
	//to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
	$('input[name=date-range-picker]').daterangepicker({
		'applyClass' : 'btn-sm btn-success',
		'cancelClass' : 'btn-sm btn-default',
		locale: {
			applyLabel: 'Apply',
			cancelLabel: 'Cancel',
		}
	})
	.prev().on(ace.click_event, function(){
		$(this).next().focus();
	});
});
function getTotalByDate(){
	var date = $('##date1').val();
    $.ajax({
      type: 'POST',
      url: '#getConTextRoot()#/index.cfm/admin:revenue.getTotalByDate/',
      data: {'idate':date},
      dataType: 'JSON',
      success: function(data) {
        $('##total').html('Revenue: '+commaSeparateNumber(data)+ ' #currency#');
      }
      }); 
}

function getTotalByRangeDate(){
	var date = $('##daterange').val().split('-');
	var fdate=date[0];
	var tdate=date[1];
    $.ajax({
      type: 'POST',
      url: '#getConTextRoot()#/index.cfm/admin:revenue.getTotalByRangeDate/',
      data: {'fdate':fdate,'tdate':tdate},
      dataType: 'JSON',
      success: function(data) {
        $('##totalRange').html('Revenue: '+commaSeparateNumber(data)+ ' #currency#');
      }
      }); 
}
</script>
<div class="container">
	<div class="row">
	    <div class="col-md-6">
	      	<div class="row">
	      		<label for="date1">Date</label>
				<div class="row">
					<div class="col-xs-8 col-sm-11">
						<div class="input-group">
							<input class="form-control date-picker" id="date1" type="text" data-date-format="dd-mm-yyyy" />
							<span class="input-group-addon">
								<input type="button" id="btnRdate" value="Go" onclick="return getTotalByDate();">
							</span>
						</div>
					</div>
				</div>
				<div id="total">Revenue:</div>
				<label for="daterange">Date Range</label>
				<div class="row">
					<div class="col-xs-8 col-sm-11">
						<div class="input-group">
							<input class="form-control" type="text"  name="date-range-picker" id="daterange" />
							<span class="input-group-addon">
								<input type="button" id="btndate" value="Go" onclick="return getTotalByRangeDate();">
							</span>
						</div>
					</div>
				</div>
				<div id="totalRange">Revenue:</div>
	      	</div>
	    </div>
	      <div class="col-md-6">
	      	<div class="row">
	      		
	      	</div>
	    </div>
    </div>
</div>

</cfoutput>