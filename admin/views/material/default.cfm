<cfoutput>
<cfscript>
	setLayout('stock');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
 function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}
function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
	$.fn.dataTable.TableTools.defaults.aButtons = [ "copy", "csv", "xls" ];
    jQuery(function($) {
		var oTable1 = $('##list_link')
		.dataTable( {
			dom: 'T<"clear">lfrtip',
			"oTableTools": {
			    "aButtons": [
			        {
			            "sExtends": "copy",
			            "sButtonText": "Copy",
			            "oSelectorOpts": {
			                filter: 'applied'
			            }
			        },
			         {
			            "sExtends": "print",
			            "sButtonText": "Print",
			            "oSelectorOpts": {
			                filter: 'applied'
			            }
			        }
			        ,
			         {
			            "sExtends": "csv",
			            "sButtonText": "CSV",
			            "oSelectorOpts": {
			                filter: 'applied'
			            }
			        }
			        ,
			         {
			            "sExtends": "xls",
			            "sButtonText": "Excel",
			            "oSelectorOpts": {
			                filter: 'applied'
			            }
			        }
			        ,
			         {
			            "sExtends": "pdf",
			            "sButtonText": "PDF",
			            "oSelectorOpts": {
			                 filter: 'applied'
			            }
			        }
			    ]
			}
	    } );

		$('.DTTT').css({"float":"right","margin-bottom":"5px"});
	})

	function change_unitprice(id){
		var unitprice = $('.unitprice'+id).val();
		$.ajax({
		  type: 'POST',
		  url: '/index.cfm/admin:material.changeunitprice/',
		  data: {'id':id,'unitprice':unitprice},
		  dataType: 'JSON',
		  success: function(data) {
		  }
		}); 
	}

</script>

<div class="page-header">
  <h1>
	<cfswitch expression="#URL.type#">
		<cfcase value="0">
			#getLabel("List of Materials")#
		</cfcase>
		<cfcase value="1">
			#getLabel("List of under-needed materials")#
		</cfcase>
		<cfcase value="2">
			#getLabel("List of under-minimum materials")#
		</cfcase>
	</cfswitch> 
  </h1>
</div>
<div class="col-xs-12">
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:material.form?id=0">
		<i class="ace-icon fa fa-plus"></i> 
		#getLabel("Add New")# 
	</a>
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:material">
		<i class="ace-icon fa fa-list-alt"></i> 
		#getLabel("List of Materials")#
	</a>
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:material?type=1">
		<i class="ace-icon fa fa-list"></i> 
		#getLabel("List of under-needed materials")#
	</a>
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:material?type=2">
		<i class="ace-icon fa fa-th-list"></i> 
		#getLabel("List of under-minimum materials")#
	</a>
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:material?loadDelItems=0">
		<i class="ace-icon fa fa-list-ul"></i>#getLabel("List of Deleted Items")# 
	</a>
		<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("ID")#</th>
					<th>#getLabel("Name")#</th>
					<th>#getLabel("Unit")#</th>
					<th>#getLabel("Actually")#</th>
					<th>#getLabel("Needed")#</th>
					<th>#getLabel("Minimum")#</th>
					<th>#getLabel("Unit price")#</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>
			<cfloop query="#rc.ListMaterial#" >
<!--- 				<cfif rc.ListMaterial.ministock gte rc.ListMaterial.actualstock>
					<cfset colorClass="##d9534f"/>
				<cfelse>
					<cfif rc.ListMaterial.needstock gte rc.ListMaterial.actualstock>
						<cfset colorClass="##f0ad4e"/>
					<cfelse>
						<cfset colorClass=""/>
					</cfif>
				</cfif> --->
				<!--- <tr style="background-color:#colorClass#"> --->
				<tr>
					<td>
						#i#
					</td>
					<td>
						<a class="blue" href="/index.cfm/admin:material.form?id=#rc.ListMaterial.materialId#">
						#rc.ListMaterial.name#
					</td>
					<td>
						#rc.ListMaterial.unitName#
					</td>
					<td>
						#rc.ListMaterial.actualstock#
						<cfif rc.ListMaterial.ministock gte rc.ListMaterial.actualstock>
							<cfset colorClass="##d9534f"/>
							<i class="ace-icon fa fa-bell icon-animated-bell" style="color: #colorClass#; float: right"></i>
						<cfelse>
							<cfif rc.ListMaterial.needstock gte rc.ListMaterial.actualstock>
								<cfset colorClass="##f0ad4e"/>
								<i class="ace-icon fa fa-bell icon-animated-bell" style="color: #colorClass#; float: right"></i>
							<cfelse>
								<cfset colorClass=""/>
							</cfif>
						</cfif>
					</td>
					<td>
						#rc.ListMaterial.needstock#
					</td>
					<td>
						#rc.ListMaterial.ministock#
					</td>
					<!--- <td>
						<input type="text" class="control-label unitprice#rc.ListMaterial.materialId#" onchange="return change_unitprice(#rc.ListMaterial.materialId#);" onkeypress="return isNumberKey(event)" onkeyup="format(this)" value="#numberFormat(rc.ListMaterial.unitprice)#">
					</td> --->
					<td>
						#numberFormat(rc.ListMaterial.unitprice)#
					</td>
					<td style="text-align:center">
						<a class="green" href="/index.cfm/admin:material.form?id=#rc.ListMaterial.materialId#">
							<i class="ace-icon fa fa-pencil bigger-130"></i>
						</a>
					</td>	
				</tr>
				<cfset i=I+1/>
			</cfloop>	
			</tbody>
		</table>
	</div>
</cfoutput>