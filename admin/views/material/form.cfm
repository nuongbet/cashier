<cfoutput>
<cfscript>
	setLayout('stock');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
 function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}
function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}
</script>
<div class="page-header">
  <h1>
	#getLabel("Material")#
  </h1>
</div>
<div class="row">
	<div class="col-xs-12">
		<form id="cms-form" class="form-horizontal" role="form" action="##" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Name")#</label>
				<div class="col-sm-10">
					<input type="text" id="name" name="name" value="#rc.material.name#" placeholder="#getLabel("Name")#" class="col-xs-10 col-sm-5" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Unit")#</label>
				<div class="col-sm-10">
					<select name="unitId" id="unitId" class="col-xs-10 col-sm-5" >
						<cfloop array="#rc.ListUnit#" item="iUnit">
							<cfif #iUnit.unitId# eq #rc.material.unitId#>
								<cfset selected="selected"/>
							<cfelse>
								<cfset selected=""/>
							</cfif>
							<option value="#iUnit.unitId#" #selected#>#iUnit.name#</option>
						</cfloop>
					</select>
				</div>
			</div>
		<!--- 	<div class="form-group">
				<label class="col-sm-2 control-label">Actual</label>
				<div class="col-sm-10">
					<input type="number" step="any" min="0" id="actual" name="actual" placeholder="Actual" class="col-xs-10 col-sm-5" value="#rc.material.actualstock#" required/>
				</div>
			</div> --->
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Needed")#</label>
				<div class="col-sm-10">
					<input type="number" step="any" min="0" id="need" name="need" placeholder="#getLabel("Needed")#" class="col-xs-10 col-sm-5" value="#rc.material.needstock#" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Minimum")#</label>
				<div class="col-sm-10">
					<input type="number" step="any" min="0" id="mini" name="mini" placeholder="#getLabel("Minimum")#" class="col-xs-10 col-sm-5" value="#rc.material.ministock#" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Unit price")#</label>
				<div class="col-sm-10">
					<input type="text" step="any" min="0" id="unitprice" name="unitprice" placeholder="#getLabel("Unit price")#" class="col-xs-10 col-sm-5" value="#numberFormat(rc.material.unitprice)#" onkeypress="return isNumberKey(event)" onkeyup="format(this)" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Active")#</label>
				<div class="col-sm-10">
					<label>
						<cfif #rc.material.isactive# EQ false>
							<cfset checkDel = ""/>
						<cfelse>
							<cfset checkDel = "checked"/>
						</cfif>
						<input #checkDel# name="isactive" id="isactive" class="ace ace-switch ace-switch-2" type="checkbox" value="1"/>
						<span class="lbl"></span>
					</label>
				</div>
			</div>
			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						#getLabel("Submit")#
					</button>
					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						#getLabel("Reset")#
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
</cfoutput>