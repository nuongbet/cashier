<cfoutput>
<cfscript>
	setLayout('fund');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
 function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('-');
}

function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}

function search(){
	var fdate=$('##fdate').val();
	var tdate=$('##tdate').val();
	location.href="/index.cfm/admin:report.reportcash?fdate="+fdate+"&tdate="+tdate;
}
function addCashIn(){
	var datecashin=$('##datecashin').val();
	if($('##cashinput').val() != '') {
		$.ajax({
			url:'/index.cfm/admin:cashin.addcashin',
			data: { 'cashintoday':$('##cashinput').val(),
					'datecashin':datecashin
			},
			dataType: 'Json',
			type: 'post',
			success: function(data) {
				$('##cashinput').val(0);
				$('##detailcash').empty();
				$('##ngayxacdinh').empty();
				$('##ngayxacdinh').append(convertDate(data.CASHIN.DATA[0][2]));
				appenddata(data);
			}
		})
	}
	else {
		alert("Chưa nhập tiền doanh thu !");
	}
}

function addCashOut(){
	var datecashout=$('##datecashout').val();
	if($('##cashout').val() != '') {
		$.ajax({
			url:'/index.cfm/admin:cashout.addcashout',
			data: { 'cashouttoday':$('##cashout').val(),
					'datecashout':datecashout,
					'typeofcashout': $('##typecashout').val()
			},
			dataType: 'Json',
			type: 'post',
			success: function(data) {
				$('##cashout').val(0);
				$('##detailcash').empty();
				$('##ngayxacdinh').empty();
				$('##ngayxacdinh').append(convertDate(data.CASHIN.DATA[0][2]));
				appenddata(data);
			}
		})
	}
	else {
		alert("Chưa nhập tiền doanh thu !");
	}
}
function capnhatthucconyes() {
	$.ajax({
		url:'/index.cfm/admin:cash.capnhatthucconyes',
		data: { 'ngaychotyes':$('##ngaychotyes').val(),
				'thuccondauyes':$('##thuccondauyes').val(),
				'thucconcuoiyes': $('##thucconcuoiyes').val(),
				'tienlayrayes': $('##tienlayrayes').val(),
				'ghichu': $('##ghichuyes').val()
		},
		dataType: 'Json',
		type: 'post',
		success: function(data) {
			if(data) {
				location.reload();
			}
		}
	})
}

function capnhatthuccontoday() {
	$.ajax({
		url:'/index.cfm/admin:cash.capnhatthuccontoday',
		data: { 'ngaychottoday':$('##ngaychot').val(),
				'thucconcuoitoday': $('##thucconcuoi').val(),
				'ghichu': $('##ghichu').val()
		},
		dataType: 'Json',
		type: 'post',
		success: function(data) {
			if(data) {
				location.reload();
			}
		}
	})
}


function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}
	$(document).on('input','##tienlayrayes',function(e){
	   		var tienlayrayes=$(this).val().replace(/,/g , "");
	   		var thuccondauyes = $('##thuccondauyes').val().replace(/,/g , "");
		    if(isBlank(tienlayrayes)){
		      tienlayrayes=0;
		    }
		    var thuccondau=$('##thuccondau').val().replace(/,/g , "");
		    $('##thucconcuoiyes').val((thuccondauyes-tienlayrayes).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
		 });

	$(document).on('change', '##datecashin', function () {
		var datecashin=$('##datecashin').val(); 
		$('##ngayxacdinh').empty();
		$('##ngayxacdinh').append(datecashin);
		$.ajax({
			url:'/index.cfm/admin:cashin.getdetailbydate',
			data: { 'datecash':datecashin
			},
			dataType: 'Json',
			type: 'post',
			success: function(data) {
				console.log(data);
				$('##detailcash').empty();
				appenddata(data);
			}
		})
	});

	$(document).on('change', '##datecashout', function () {
		var datecashout=$('##datecashout').val();
		$('##ngayxacdinh').empty();
		$('##ngayxacdinh').append(datecashout);
		$.ajax({
			url:'/index.cfm/admin:cashout.getdetailbydate',
			data: { 'datecash':datecashout
			},
			dataType: 'Json',
			type: 'post',
			success: function(data) {
				$('##detailcash').empty();
				appenddata(data);
			}
		})
	});

	$(document).on('change', '##ngaychotyes', function () {
		var datecash = $('##ngaychotyes').val();
		$.ajax({
			url:'/index.cfm/admin:cash.getcashbydate',
			data: { 'datecash':datecash
			},
			dataType: 'Json',
			type: 'post',
			success: function(data) {
				$('##thuccondauyes').val(data.DATA[0][3].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
				$('##tienlayrayes').val(data.DATA[0][2].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
				$('##thucconcuoiyes').val(data.DATA[0][5].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
				$('##ghichuyes').val(data.DATA[0][6].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
				$('##thuccondau').val(data.DATA[0][5].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));

			}
		})
	});

	$(document).on('change', '##ngaychot', function () {
		
	});


	$.fn.dataTable.TableTools.defaults.aButtons = [ "copy", "csv", "xls" ];
    jQuery(function($) {

    	$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});
	})

	function appenddata(data) {
		var doanhthu = data.CASHIN.DATA.length > 0 ? data.CASHIN.DATA[0][1] : 0;
		var tondau   = data.TONDAU.DATA.length > 0 ? data.TONDAU.DATA[0][0] : 0;
		var ngayhomqua = data.TONDAU.DATA.length > 0 ? convertDate(data.TONDAU.DATA[0][1]) : '';
		var newele = '<span>' + tondau.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' (' + "#getLabel('thuccondau')#" + ' ' + ngayhomqua + ') + </span>';
		newele += '<span>' + doanhthu.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' (DT) </span>';
		for (var i = 0; i < data.CASHOUT.DATA.length; i++ ) {
			newele += '<span> - ' + data.CASHOUT.DATA[i][4].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + ' (' +  data.CASHOUT.DATA[i][1] + ')</span>';
		};
		newele += '<span style="font-weight:bolder; font-size:18px;"> = ' + data.EQUALCASH.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span>' ;
		$('##detailcash').append(newele)
	}

</script>

<div class="page-header">
  <h1>
	#getLabel('chitiet')#
  </h1>
</div>
<div class="col-md-12 col-xs-12 form-inline" style="margin-top:10px;">
  <h1>
	#getLabel('doanhthu')#
  </h1>
</div>
<div class="col-md-12 col-xs-12 form-inline" style="margin-top:10px;">
	
	
	<div class="col-md-12 col-xs-12 ">
		<label for="datecashin" class="col-md-2 col-xs-2 ">
			#getLabel('selectdate')#
		</label>
		<input class="form-control date-picker col-md-10 col-xs-10" name="datecashin" id="datecashin" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(now(),"dd-mm-yyyy","pt_PT")#" placeholder="Date cash in" />
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="cashinput" class="col-md-2 col-xs-2 ">
			#getLabel('inputcashin')#
		</label>
		<input class="form-control" name="cashinput" id="cashinput" type="string" value="" placeholder="Cash In Today"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
		<p class="btn btn-primary btn-white" onclick='addCashIn();'>
			<i class="ace-icon fa fa-plus"></i> 
			Add
		</p>
	</p>
	</div>
</div>
<div class="col-md-12 col-xs-12 form-inline" style="margin-top:10px;">
  <h1>
	#getLabel('tienchi')#
  </h1>
</div>
<div class="col-md-12 col-xs-12 form-inline">
	<div class="col-md-12 col-xs-12 ">
		<label for="datecashout" class="col-md-2 col-xs-2 ">
			#getLabel('selectdate')#
		</label>
		<input class="form-control date-picker col-md-10 col-xs-10" name="datecashout" id="datecashout" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(now(),"dd-mm-yyyy","pt_PT")#" placeholder="Date cash out" />
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="datecashout" class="col-md-2 col-xs-2 ">
			#getLabel('inputcashout')#
		</label>
		<input class="form-control col-md-4 col-xs-4" name="cashout" id="cashout" type="string" value="" placeholder="Cash out"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
		<input class="form-control col-md-5 col-xs-5" name="typecashout" id="typecashout" type="string" value="" placeholder="Type of Cash out" style="margin-left:10px;"/>
		<p class="btn btn-primary btn-white" onclick='addCashOut();' style="margin-left:5px;">
			<i class="ace-icon fa fa-plus"></i> 
			Add
		</p>
	</div>
</div>
<div class="col-md-12 col-xs-12 form-inline" style="margin-top:10px;">
  <h1>
	#getLabel('chitiet')# <span id="ngayxacdinh">#lsdateFormat(now(),"dd-mm-yyyy","pt_PT")#</span>
  </h1>
</div>
<div class="col-md-12 col-xs-12 form-inline" style="margin-top:10px; font-size:15px;" id="detailcash">
	<span>#numberformat(rc.tondau.thucconcuoi)# (#getLabel('thuccondau')# #lsdateFormat(now() - 1,"dd-mm-yyyy","pt_PT")#) + </span>
	<span>#numberformat(rc.todaycashIn.cashin)# (DT) </span>
	<cfloop query="rc.todaycashOut">
		<span> - #numberformat(rc.todaycashOut.cash)# (#rc.todaycashOut.typeofcash#)</span>
	</cfloop>
	<span style="font-weight:bolder; font-size:18px;"> = #numberformat(rc.equalcash)# </span>
</div>
<div class="col-md-12 col-xs-12 form-inline" style="margin-top:10px;">
  <h1>
	#getLabel('cash')#
  </h1>
</div>
<div class="col-md-12 col-xs-12" style="margin-bottom:10px;">
	<div class="col-md-6 col-xs-6 form-inline" style="font-size:14px; color: blue; text-transform: uppercase; font-weight:bolder;">
		Yesterday
	</div>
	<div class="col-md-6 col-xs-6 form-inline" style="font-size:14px; color: blue; text-transform: uppercase; font-weight:bolder;">
		Today
	</div>
</div>
<div class="col-md-6 col-xs-6 form-inline">
	<div class="col-md-12 col-xs-12 ">
		<label for="ngaychotyes" class="col-md-6 col-xs-6 ">
			#getLabel('ngaychot')#
		</label>
		<input class="form-control date-picker col-md-6 col-xs-6" name="ngaychotyes" id="ngaychotyes" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(rc.thucconyes.ngaychot,"dd-mm-yyyy","pt_PT")#" placeholder="Date cash out" />
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="thuccondauyes" class="col-md-6 col-xs-6 ">
			#getLabel('thuccondau')#
		</label>
		<input class="form-control col-md-6 col-xs-6" name="thuccondauyes" id="thuccondauyes" type="string" value="#numberformat(rc.thucconyes.thuccontondau)#" placeholder="#getLabel('thuccondau')#"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="tienlayrayes" class="col-md-6 col-xs-6 ">
			#getLabel('tienlayra')#
		</label>
		<input class="form-control col-md-6 col-xs-6" name="tienlayrayes" id="tienlayrayes" type="string" value="#numberformat(rc.thucconyes.tienlayra)#" placeholder="#getLabel('tienlayra')#"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="thucconcuoiyes" class="col-md-6 col-xs-6 ">
			#getLabel('thucconcuoi')#
		</label>
		<input class="form-control col-md-6 col-xs-6" name="thucconcuoiyes" id="thucconcuoiyes" type="string" value="#numberformat(rc.thucconyes.thucconcuoi)#" placeholder="#getLabel('thucconcuoi')#"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="ghichuyes" class="col-md-6 col-xs-6 ">
			#getLabel('ghichu')#
		</label>
		<textarea id="ghichuyes" name="ghichuyes">#rc.thucconyes.ghichu#</textarea>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<div class="col-md-6 col-xs-6 ">
		</div>
		<div class="col-md-6 col-xs-6 ">
			<p class="btn btn-primary btn-white right" onclick='capnhatthucconyes();' style="margin-left:5px;">
				<i class="ace-icon fa fa-plus"></i> 
				Add
			</p>
		</div>
	</div>
</div>
<div class="col-md-6 col-xs-6 form-inline" style="border-left: solid 2px black;">
	<div class="col-md-12 col-xs-12 ">
		<label for="ngaychot" class="col-md-6 col-xs-6 ">
			#getLabel('ngaychot')#
		</label>
		<input class="form-control date-picker col-md-6 col-xs-6" name="ngaychot" id="ngaychot" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(now(),"dd-mm-yyyy","pt_PT")#" placeholder="Date cash out" />
	</div>


	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="thuccondau" class="col-md-6 col-xs-6 ">
			#getLabel('thuccondau')#
		</label>
		<input readonly class="form-control col-md-6 col-xs-6" name="thuccondau" id="thuccondau" type="string" value="#numberformat(rc.thuccontoday.thuccontondau)#" placeholder="#getLabel('thuccondau')#"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="tienlayra" class="col-md-6 col-xs-6 ">
			#getLabel('tienlayra')#
		</label>
		<input readonly class="form-control col-md-6 col-xs-6" name="tienlayra" id="tienlayra" type="string" value="#numberformat(rc.thuccontoday.tienlayra)#" placeholder="#getLabel('tienlayra')#"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="thucconcuoi" class="col-md-6 col-xs-6 ">
			#getLabel('thucconcuoi')#
		</label>
		<input class="form-control col-md-6 col-xs-6" name="thucconcuoi" id="thucconcuoi" type="string" value="#numberformat(rc.thuccontoday.thucconcuoi)#" placeholder="#getLabel('thucconcuoi')#"  onkeypress="return isNumberKey(event)" onkeyup="format(this)"/>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<label for="ghichu" class="col-md-6 col-xs-6 ">
			#getLabel('ghichu')#
		</label>
		<textarea id="ghichu" name="ghichu">#rc.thuccontoday.ghichu#</textarea>
	</div>
	<div class="col-md-12 col-xs-12 " style="margin-top:10px;">
		<div class="col-md-6 col-xs-6 ">
		</div>
		<div class="col-md-6 col-xs-6 ">
			<p class="btn btn-primary btn-white right" onclick='capnhatthuccontoday();' style="margin-left:5px;">
				<i class="ace-icon fa fa-plus"></i> 
				Add
			</p>
		</div>
	</div>
</div>
</cfoutput> 