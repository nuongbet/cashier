<cfoutput>
<cfscript>
	setLayout('fund');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
 function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}
function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}

function search(){
	var fdate=$('##fdate').val();
	var tdate=$('##tdate').val();
	location.href="/index.cfm/admin:report.reportcash?fdate="+fdate+"&tdate="+tdate;
}

	$.fn.dataTable.TableTools.defaults.aButtons = [ "copy", "csv", "xls" ];
    jQuery(function($) {

    	$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		var oTable1 = $('##list_link')
		.dataTable( {
			"bSort": false,
			"bPaginate":false,
			dom: 'T<"clear">lfrtip',
			"oTableTools": {
			    "aButtons": [
			        {
			            "sExtends": "copy",
			            "sButtonText": "Copy",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        },
			         {
			            "sExtends": "print",
			            "sButtonText": "Print",
			            "oSelectorOpts": {
			                page: 'current',
			                filter: 'applied'
			            }
			        }
			        ,
			         {
			            "sExtends": "csv",
			            "sButtonText": "CSV",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			        ,
			         {
			            "sExtends": "xls",
			            "sButtonText": "Excel",
			            "sTitle": "Report cash from #URL.fdate# to #URL.tdate#",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			        ,
			         {
			            "sExtends": "pdf",
			            "sTitle": "Report cash from #URL.fdate# to #URL.tdate#",
			            "sButtonText": "PDF",
			            "sPdfOrientation": "landscape",
			            "oSelectorOpts": {
			                page: 'current'
			            }
			        }
			    ]
			}
	    } );

		$('.DTTT').css({"float":"right","margin-bottom":"5px"});
	})

</script>

<div class="page-header">
  <h1>
	#getLabel("Report Cash")#
  </h1>
</div>
<div class="col-md-12 form-inline">
		<input class="form-control date-picker" name="fdate" id="fdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.fdate,"dd-mm-yyyy","pt_PT")#" placeholder="From date" />
		<input class="form-control date-picker" name="tdate" id="tdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.tdate,"dd-mm-yyyy","pt_PT")#" placeholder="To date" />
		<p class="btn btn-primary btn-white" onclick='search();'>
			<i class="ace-icon fa fa-search"></i> 
		Search
		</p>
</div>
<div class="col-xs-12">
	<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th style="text-align:center;">#getLabel("No")#</th>
					<th style="text-align:center;">#getLabel("Note")#</th>
					<th style="text-align:center;">#getLabel("Payer")#</th>
					<th style="text-align:center;">#getLabel("Receiver")#</th>
					<th style="text-align:center;">#getLabel("Amount(R)")#</th>
					<th style="text-align:center;">#getLabel("Amount(P)")#</th>
					<th style="text-align:center;">#getLabel("Type")#</th>
					<th style="text-align:center;">#getLabel("Date receipt")#</th>
					<th style="text-align:center;">#getLabel("Date create")#</th>
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>
			<cfset totalReceipt=0/>
			<cfset totalPayment=0/>

		
			<cfloop query="#rc.ListCash#" >
				<tr>
					<td style="text-align:center;">
						#i#
					</td>
					<td style="text-align:left;">
						#rc.ListCash.note#
					</td>
					<td style="text-align:left;">
						#rc.ListCash.payer#
					</td>
					<td style="text-align:left;">
						#rc.ListCash.receiver#
					</td>
					<td style="text-align:right;">
						#NumberFormat(rc.ListCash.ramount)#
					</td >
					<td style="text-align:right;">
						#NumberFormat(rc.ListCash.pamount)#
					</td >
					<td style="text-align:center;">
						#rc.ListCash.type#
					</td>
					<td style="text-align:center;">
						#dateformat(rc.ListCash.datereceipt,'dd/mm/yyyy')#
					</td>
					<td style="text-align:center;">
						#dateformat(rc.ListCash.datecreate,'dd/mm/yyyy')#
					</td>
				</tr>
				<cfset i=i+1/>
				<cfif rc.ListCash.isreceipt eq 1>
					<cfset totalReceipt+=rc.ListCash.amount/>
				<cfelse>
					<cfset totalPayment+=rc.ListCash.amount/>
				</cfif>
			</cfloop>
			<cfif totalPayment gt 0 or totalReceipt gt 0>
				<tr>
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:left;">
						
					</td>
					<td style="text-align:left;">
						
					</td>
					<td style="text-align:left;font-weight:bold;font-size:14px;">
						Total Receipt
					</td>
					<td style="text-align:right;font-weight:bold;font-size:14px;">
						#NumberFormat(totalReceipt)#
					</td >
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:center;">
						
					</td>
				</tr>
					<tr>
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:left;">
						
					</td>
					<td style="text-align:left;">
						
					</td>
					<td style="text-align:left;font-weight:bold;font-size:14px;">
						Total Payment
					</td>
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:right;font-weight:bold;font-size:14px;">
						#NumberFormat(totalPayment)#
					</td >
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:center;">
						
					</td>
					<td style="text-align:center;">
						
					</td>
				</tr>
			</cfif>
			</tbody>
		</table>
	</div>
</cfoutput>