<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
    jQuery(function($) {
		var oTable1 = $('##list_link')
		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,{ "bSortable": false },{ "bSortable": false }]
	    } );
	   })

    //type price select change
function typeChange() {
  var typeId = $( "select##typepriceId" ).val();
 	location.href="#getcontextroot()#/index.cfm/admin:foodprice?typeId="+typeId;
}
</script>
<div class="page-header">
	<h1>
		#getLabel("Food Price Type")#
	</h1>
</div>
<div class="col-xs-12 row">
	<div class="col-xs-12">
		<a class="btn btn-primary btn-white" href="/index.cfm/admin:foodprice.form?id=0">
			<i class="ace-icon fa fa-plus"></i> 
			#getLabel("Add New")# 
		</a>
		<p></p>
	</div>
	<div class="col-xs-12 row">
		<!--- <label class="col-xs-1 control-label no-padding-right">Price type</label> --->
	    <div class="col-xs-4" style="padding-bottom:5px">
	      <select class="form-control" id="typepriceId" name="typepriceId" onchange="return typeChange();">
	        <option value=0>#getLabel("All")#</option>
	        <cfloop array="#rc.typeprice#" item="itype">
	        	<cfif #itype.typeId# eq #URL.typeId#>
	        		<cfset selected="selected">
	        	<cfelse>
	        		<cfset selected="">
	        	</cfif>
	           <option value="#itype.typeId#" #selected# >#itype.name#</option>
	        </cfloop>
	      </select>
	    </div>
	</div>
	<div class="col-xs-12">
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("ID")#</th>
					<th>#getLabel("Name")#</th>
					<th>#getLabel("Type")#</th>
					<th>#getLabel("Price")#</th>
					<th>#getLabel("Normal Price")#</th>
					<th></th>
					<th></th>
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>
			<cfloop query="#rc.ListFood#">
				<tr>
					<td>
						#i#
					</td>
					<td>
						<a class="blue" href="/index.cfm/admin:foodprice.form?id=#rc.ListFood.foodpriceId#">
						#rc.ListFood.fname#
					</td>
					<td>
						#rc.ListFood.tname#
					</td>
					<td>
						#NumberFormat(rc.ListFood.price)#
					</td>
					<td>
						#NumberFormat(rc.ListFood.normalprice)#
					</td>
					<td style="text-align:center">
						<div class="hidden-sm hidden-xs action-buttons">
							<a class="green" href="/index.cfm/admin:foodprice.form?id=#rc.ListFood.foodpriceId#">
								<i class="ace-icon fa fa-pencil bigger-130"></i>
							</a>
						</div>
					</td>	
					<td style="text-align:center">
						<div class="hidden-sm hidden-xs action-buttons">
							<a class="red" href="/index.cfm/admin:foodprice.delete?id=#rc.ListFood.foodpriceId#" onclick="return checkDelete()">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>
							</a>
						</div>
					</td>
				</tr>
				<cfset i=i+1/>
			</cfloop>	
			</tbody>
		</table>
	</div>
	</div>
</cfoutput>