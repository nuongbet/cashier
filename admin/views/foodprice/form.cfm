<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
 function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}
function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}
//food select change
function FoodChange() {
  var foodId = $( "select##foodId" ).val();
  $.ajax({
        type: 'POST',
        url: '#getConTextRoot()#/index.cfm/admin:foodprice.getFoodChange/',
        data: {'fId':foodId},
        dataType: 'JSON',
        success: function(data) {
        	$('##normalprice').val(commaSeparateNumber(data.price));
        }
    });
}
</script>
<cfif CGI.REQUEST_METHOD eq "POST">
	<cfswitch expression="#rc.fexists#">
		<cfcase value="0">
			<script type="text/javascript">
				alert('#getLabel("Successful.")#');
			</script>
		</cfcase>
		<cfcase value="1">
			<script type="text/javascript">
				alert('#getLabel("Food is exist!")#');
			</script>
		</cfcase>
	</cfswitch>
</cfif>
<div class="page-header">
	<h1>
		#getLabel("Food Price Type")#
	</h1>
</div>
<div class="row">
	<div class="col-xs-12">
		<form id="cms-form" class="form-horizontal" role="form" action="##" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Name")#</label>
				<div class="col-sm-10">
					<select name="foodId" id="foodId" onchange="return FoodChange();" class="col-xs-10 col-sm-5" <cfif #URL.id# gt 0>
						disabled
					</cfif> >
						<cfloop array="#rc.ListFood#" item="iFood">
		                	<cfif #iFood.FoodId# eq #rc.foodprice.foodId#>
		                		<cfset selected="selected">
		                	<cfelse>
		                		<cfset selected="">
		                	</cfif>
		                   <option value="#iFood.foodId#" #selected# >#iFood.name#</option>
                		</cfloop>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Type")#</label>
				<div class="col-sm-10">
					<select name="typeId" id="typeId" class="col-xs-10 col-sm-5" <cfif #URL.id# gt 0>
						disabled
					</cfif> >
						<cfloop array="#rc.typeprice#" item="itype">
		                	<cfif #itype.typeId# eq #URL.Id#>
		                		<cfset selected="selected">
		                	<cfelse>
		                		<cfset selected="">
		                	</cfif>
		                   <option value="#itype.typeId#" #selected# >#itype.name#</option>
                		</cfloop>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Normal price")#</label>
				<div class="col-sm-10">
					<input type="text" id="normalprice" name="normalprice" value="#numberFormat(rc.FoodById.price)#" placeholder="#getLabel("Normal price")#" class="col-xs-10 col-sm-5" readonly/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("New price")#</label>
				<div class="col-sm-10">
					<input type="text" id="price" name="price" value="#numberFormat(rc.foodprice.price)#" placeholder="#getLabel("New price")#" onkeypress="return isNumberKey(event)" onkeyup="format(this)" class="col-xs-10 col-sm-5" required/>
				</div>
			</div>
			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						#getLabel("Submit")#
					</button>
					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						#getLabel("Reset")#
					</button>
				</div>
			</div>
		</form>
	</div>
</div>
</cfoutput>