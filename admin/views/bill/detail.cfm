<cfoutput>
<cfscript>
	setLayout('fund');
</cfscript>
<div class="page-header">
  <h1>
	#getLabel("Invoice Detail")#
  </h1>
</div>
<form action="##" method="POST">
<div class="row">
	<div class="col-xs-12 row infobill">
		<div class="col-md-6">
			<div class="col-xs-12 row">
			<div class="col-xs-3">
				#getLabel("Date")#:
			</div>
			<div class="col-xs-6">
				#dateFormat(rc.Bill.datecreate[1],"dd/mm/yyyy")# #timeformat(rc.Bill.datecreate[1],"hh:mm")#
			</div>
		</div>
		<div class="col-xs-12 row">
			<div class="col-xs-3">
				#getLabel("Staff")#:
			</div>
			<div class="col-xs-6">
				#rc.Bill.staff#
			</div>
		</div>
		<div class="col-xs-12 row">
			<div class="col-xs-3">
				#getLabel("Shift")#:
			</div>
			<div class="col-xs-6">
				#rc.Bill.shift[1]#
			</div>
		</div>
		<div class="col-xs-12 row">
			<div class="col-xs-3">
				#getLabel("Order type")#: 
			</div>
			<div class="col-xs-6">
				#rc.Bill.ordername[1]#
			</div>
		</div>
		<div class="col-xs-12 row">
			<div class="col-xs-3">
				#getLabel("Price type")#: 
			</div>
			<div class="col-xs-6">
				#rc.Bill.typepriceName[1]#
			</div>
		</div>
		</div>
		<div class="col-md-6">
			<fieldset>
				<legend>#getLabel("Note")#</legend>
				<textarea rows="5" cols="30" id="txtNote" class="col-md-12 col-xs-12 col-sm-12" name="txtNote" readonly>#rc.Bill.note[1]#</textarea>
			</fieldset>	
		</div>
		
	</div>
	<div class="col-xs-12">
        <div class="tab-pane active" id="panel-current">
			<div class="panel panel-default">
	            <div class="panel-heading">#getLabel("Orders list")#</div>
	            <div class="table-responsive">
					<table id="list_billdetail" class="table">
						<thead>
							<tr>
								<th>##</th>
								<th>#getLabel("Name")#</th>
								<th>#getLabel("Price")#</th>
								<th>#getLabel("Quantity")#</th>
								<th>#getLabel("Total")#</th>
								<th>#getLabel("FOC")#</th>
							</tr>
						</thead>
						<tbody>
							<cfset i=1/>
						<cfloop query="#rc.ListOrder#">
							<tr>
								<td>
									#i#
								</td>
								<td>
									#rc.ListOrder.fname#
								</td>
								<td>
									#numberformat(rc.ListOrder.paymentprice)# #currency#
								</td>
								<td>
									#rc.ListOrder.quantity#
								</td>
								<td>
									#numberformat(rc.ListOrder.total)# #currency#
								</td>
								<td>
									#rc.ListOrder.foc#
								</td>	
							</tr>
							<cfset i += 1/>
						</cfloop>
							<tr>
								<td colspan="4">
									<label class="floatright">#getLabel("Total")#:</label>
								</td>
								<td>
									<b>#numberformat(rc.Bill.totalvalue[1])# #currency#</b>
								</td>
								<td></td>
							</tr>	
							<tr>
								<td colspan="4">
									<label class="floatright">#getLabel("Discount")#:<cfif #rc.Bill.typediscount[1]# eq 0>
										(#rc.Bill.discount[1]# %)
									</cfif></label>
								</td>
								<td>
									<b>#numberformat(rc.Bill.discountvalue[1])# #currency#</b>
								</td>
								<td></td>
							</tr>	
							<tr>
								<td colspan="4">
									<label class="floatright">#getLabel("Amount")#:</label>
								</td>
								<td>
									<b>#numberformat(rc.Bill.total[1])# #currency#</b>
								</td>
								<td></td>
							</tr>	
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</cfoutput>