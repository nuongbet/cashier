<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
    jQuery(function($) {
		var oTable1 = $('##list_link')
		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,{ "bSortable": false }]
	    } );
	   })
</script>

<div class="page-header">
  <h1>
	#getLabel("Shift")#
  </h1>
</div>
<div class="col-xs-12">
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:shift.form?id=0">
		<i class="ace-icon fa fa-plus"></i> 
		#getLabel("Add New")# 
	</a>
		<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("ID")#</th>
					<th>#getLabel("Name")#</th>
					<th>#getLabel("Start time")#</th>
					<th>#getLabel("End time")#</th>
					<th></th>
					<!--- <th></th> --->
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>
			<cfloop array="#rc.ListShift#" item="iShift">
				<tr>
					<td>
						#i#
					</td>
					<td>
						<a class="blue" href="/index.cfm/admin:shift.form?id=#iShift.shiftId#">
						#iShift.name#
					</td>
					<td>
						#TimeFormat(iShift.timestart,'HH:mm')#
					</td>
					<td>
						#TimeFormat(iShift.timeend,'HH:mm')#
					</td>
					<!--- <td>
						<cfif #iShift.isactive# EQ true>
							<i class="ace-icon fa fa-check-circle-o green bigger-130"></i>
							<cfelse>
							<i class="ace-icon fa fa-circle red bigger-130"></i>
						</cfif>	
					</td> --->
					<td style="text-align:center">
						<a class="green" href="/index.cfm/admin:shift.form?id=#iShift.shiftId#">
							<i class="ace-icon fa fa-pencil bigger-130"></i>
						</a>
					</td>	
				<!--- 	<td style="text-align:center">
						<a class="red" href="/index.cfm/admin:shift.delete?id=#iShift.shiftId#" onclick="return checkDelete()">
							<i class="ace-icon fa fa-trash-o bigger-130"></i>
						</a>
					</td>	 --->
				</tr>
				<cfset i=i+1/>
			</cfloop>	
			</tbody>
		</table>
	</div>
</cfoutput>