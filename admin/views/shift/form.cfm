<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
$(document).ready(function(){
	$('##starttime').timepicker({
		minuteStep: 1,
		showSeconds: false,
		showMeridian: false
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	$('##endtime').timepicker({
		minuteStep: 1,
		showSeconds: false,
		showMeridian: false
	}).next().on(ace.click_event, function(){
		$(this).prev().focus();
	});

	// $('##btnsubmit').click(function(){
	// 	$('##stime').value=$('##starttime').val();
	// 	$('##etime').value=$('##endtime').val();
	// });
});
</script>

<div class="page-header">
  <h1>
	#getLabel("Shift")#
  </h1>
</div>
<div class="row">
	<div class="col-xs-12">
		<form id="cms-form" class="form-horizontal" role="form" action="##" method="POST" enctype="multipart/form-data">
			<!--- <input type="hidden" id="stime" name="stime" value="#rc.shift.timestart#">
			<input type="hidden" id="etime" name="etime" value="#rc.shift.timeend#"> --->
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Name")#</label>
				<div class="col-sm-10">
					<input type="text" id="name" name="name" value="#rc.shift.name#" placeholder="#getLabel("Name")#" class="col-xs-10 col-sm-5" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Starttime")#</label>
				<div class="col-sm-10">
					<!--- <input id="timepicker1" type="text" class="col-xs-10 col-sm-5" /> --->
					<input type="text" id="starttime" name="starttime" value="#TimeFormat(rc.shift.timestart,'HH:mm')#" placeholder="#getLabel("Starttime")#" class="col-xs-10 col-sm-5" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right">#getLabel("Endtime")#</label>
				<div class="col-sm-10">
					<input type="text" id="endtime" name="endtime" value="#TimeFormat(rc.shift.timeend,'HH:mm')#" placeholder="#getLabel("Endtime")#" class="col-xs-10 col-sm-5" required/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label no-padding-right"></label>
				<div class="col-sm-10">
					<button class="btn btn-info" type="submit" id="btnsubmit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						#getLabel("Submit")#
					</button>
				</div>
			</div>			
		</form>
	</div>
</div>
</cfoutput>