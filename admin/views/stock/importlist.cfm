<cfoutput>
<cfscript>
	setLayout('stock');
</cfscript>
<script type="text/javascript">	
	
	function commaSeparateNumber(val){
		while (/(\d+)(\d{3})/.test(val.toString())){
		  val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
		}
		return val;
	}

	function showImportModal(id) {
		$.ajax({
			type: 'POST',
			url: '/index.cfm/admin:stock.importdetail/',
			data: {'importId':id},
			dataType: 'JSON',
			success: function(data) {
				$('.itemimport').empty();
				for(i=0;i<data.length;i++){
					$('.itemimport').append('<tr><td>'+data[i].materialname+'</td><td style="text-align: right">'+commaSeparateNumber(data[i].price)+' #currency#</td><td style="text-align: right">'+data[i].quantity+' '+data[i].unitname+'</td><td style="text-align: right">'+commaSeparateNumber(data[i].total)+' #currency#</td><td></td></tr>');
					$('.note').html('<textarea readonly rows="4" id="txtNote" class="col-md-12 col-xs-12 col-sm-12" name="txtNote">'+data[i].note+'</textarea>');
					$('.deliverycompany').html(data[i].deliverycompany);
					$('.deliverydate').html(data[i].deliverydate);
					$('.buyer').html(data[i].buyer);
					$('.result_amount').html(commaSeparateNumber(data[i].amount)+' #currency#');
					$('.result_actualcost').html(commaSeparateNumber(data[i].actualcost)+' #currency#');
					$('##importModal').modal('show');
				}
			}
		});		
	}

    jQuery(function($) {  

	    $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		var oTable1 = $('##importList')
		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,{ "bSortable": false }]
	    } );

		$('##datesearch').change(function(){
			var idate=$('##datesearch').val();
			location.href='/index.cfm/admin:stock.importlist?idate='+idate;
		});

	})
</script>
<div class="page-header">
  <h1>
	#getLabel("Purchase Voucher")#
  </h1>
</div>
<div class="col-xs-12">
	<div class="row">
		<div class="col-md-2">
			<a class="btn btn-primary btn-white" href="/index.cfm/admin:stock.importsheet">
				<i class="ace-icon fa fa-plus"></i> 
				#getLabel("Add New")# 
			</a>
		</div>
		<div class="col-md-3">
			<input class="form-control date-picker" name="datesearch" id="datesearch" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.idate,"dd-mm-yyyy","pt_PT")#" placeholder="Date search" />
		</div>
	</div>
	<p></p>
	<table id="importList" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>#getLabel("ID")#</th>
				<th>#getLabel("Delivery Date")#</th>
				<th>#getLabel("Delivery Company")#</th>
				<th>#getLabel("Buyer")#</th>
				<th>#getLabel("Amount")#</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<cfloop query="#rc.importlist#">
			<tr data-toggle="modal" onclick="return showImportModal(#rc.importlist.importId#);">
				<td>
					#rc.importlist.importId#
				</td>
				<td>
					#datetimeFormat(rc.importlist.datecreate,'dd-mm-yyyy')#
				</td>
				<td>
					#rc.importlist.deliverycompany#
				</td>				
				<td>
					#rc.importlist.buyer#
				</td>	
				<td>
					#numberFormat(rc.importlist.amount)# #currency#
				</td>	
				<td style="text-align:center">
					<i class="ace-icon fa fa-info bigger-130 green cursor" data-toggle="modal" onclick="return showImportModal(#rc.importlist.importId#);"></i>
				</td>
			</tr>		
			</cfloop>
		</tbody>
	</table>
</div>

<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">#getLabel("Close")#</span></button>
        <h4 class="modal-title" id="myModalLabel">#getLabel("Import detail")#</h4>
      </div>
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12 row">
				<div class="col-md-7">
					<fieldset>
						<legend>#getLabel("Information")#</legend>
						<table>
							<tr>
								<td style="width:70%"><label>#getLabel("Delivery company")#:</label></td>
								<td class="deliverycompany"></td>
							</tr>
							<tr>
								<td><label>#getLabel("Delivery Date")#:</label></td>
								<td class="deliverydate"></td>
							</tr>
							<tr>
								<td><label>#getLabel("Buyer")#:</label></td>
								<td class="buyer"></td>
							</tr>
						</table>
					</fieldset>
					<p></p>
				</div>
				<div class="col-md-5">
					<fieldset>
						<legend>#getLabel("Note")#</legend>
						<span class="note"></p>
					</fieldset>	
					<p></p>	
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-default">
		            <div class="panel-heading">#getLabel("Import list")#</div>
		            <div class="table-responsive">
		              	<table class="table" id="adjust_list">
			                <thead>
				                <tr>
				                    <th style="width: 30%">#getLabel("Name")#</th>
				                    <th style="width: 20%; text-align: right">#getLabel("Price")#</th>
				                    <th style="width: 20%; text-align: right">#getLabel("Quantity")#</th>
				                    <th style="width: 25%; text-align: right">#getLabel("Total")#</th>
				                    <th style="width: 5%"></th>
				                </tr>
			                </thead>
			                <tbody class="itemimport">
			                </tbody>
			                	<tr>
									<td colspan="3">
										<label class="amount">#getLabel("Amount")#</label>
									</td>
									<td class="result_amount" style="text-align: right"></td>
									<td></td>
			                	</tr>
			                	<tr>
									<td colspan="3">
										<label class="amount">#getLabel("Actual cost")#</label>
									</td>
									<td class="result_actualcost" style="text-align: right"></td>
									<td></td>
			                	</tr>
			            </table>
			        </div>
			    </div>			
			</div>
		</div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

</cfoutput>