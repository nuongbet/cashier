<cfoutput>
<cfscript>
	setLayout('stock');
</cfscript>
<script type="text/javascript">
	function searchdate(){
		var rdate=$("input:radio[name='rdate']:checked").val();
		var idate=$('##datesearch').val();
		location.href='/index.cfm/admin:stock.statisticsheet?idate='+idate+'&rdate='+rdate;
	}
	jQuery(function($) {
		var oTable1 = $('##list_link')
		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,null,null]
	    } );

		//datepicker plugin
		//link
		$('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		$('##datesearch').change(function(){
			searchdate();
		});

		$("input:radio[name='rdate']").change(function () { 
			searchdate();
		});
	});

</script>

<div class="page-header">
  <h1>
	#getLabel("Statistic Sheet")# 
  </h1>
</div>

<div class="col-xs-12">
	<form class="form-horizontal" role="form" action="" method="POST" enctype="multipart/form-data">
<div class="col-xs-12">
	<input type="submit" class="col-sm-2 btn btn-primary btn-white" value="#getLabel("Ending")#">
</div>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
				<div class="col-md-12">
					<div class="form-group">
						<fieldset>
							<legend>#getLabel("Date type")#</legend>
							
							<cfset checked1=""/>
							<cfset checked2=""/>
							<cfset checked3=""/>
							<cfswitch expression="#URL.rdate#">
								<cfcase value="1">
									<cfset checked1="checked"/>
								</cfcase>
								<cfcase value="2">
									<cfset checked2="checked"/>
								</cfcase>
								<cfcase value="3">
									<cfset checked3="checked"/>
								</cfcase>
							</cfswitch>
							<input type="radio" name="rdate" value="1" #checked1#>#getLabel("Day")#
							<input type="radio" name="rdate" value="2" #checked2#>#getLabel("Week")#
							<input type="radio" name="rdate" value="3" #checked3#>#getLabel("Month")#<br>
							<div class="form-group">
								<div  class="col-sm-8">
									<input class="form-control date-picker" id="datesearch" type="text" data-date-format="dd-mm-yyyy" placeholder="Date search" value="#lsdateFormat(URL.idate,"dd-mm-yyyy","pt_PT")#" style="margin-top:10px;"/>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
				</div>
				<div class="col-md-6">
				   <div class="col-md-12">
				   		<fieldset>
							<legend>#getLabel("Information")#</legend>
	                        <div class="form-group">
	                          <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Total cost")#</label>
	                          <div class="col-xs-7 col-md-9">
	                            <input type="text" id="totalcost" name="totalcost" readonly class="col-xs-12 col-sm-12" value="#NumberFormat(rc.totalcost.inputtotal[1])# #currency#"/> 
	                          </div>
	                        </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                          <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Revenue")#</label>
                          <div class="col-xs-7 col-md-9">
                            <input type="text" id="revenue" name="revenue" readonly class="col-xs-12 col-sm-12" value="#NumberFormat(rc.revenue.revenue[1])# #currency#"/> 
                          </div>
                        </div>
                    </div>
				</div>
			</div>
			
		</div>
		<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("ID")#</th>
					<th>#getLabel("Material")#</th>
					<th>#getLabel("Begin balance")#</th>
					<th>#getLabel("Input")#</th>
					<th>#getLabel("Adjust")#</th>
					<th>#getLabel("Output")#</th>
					<th>#getLabel("Ending balance")#</th>
				</tr>
			</thead>

			<tbody>
			<cfset id=1/>
			
			<cfloop query="#rc.ListStatisticSheet#">
				<tr>
					<td>
						#id#
					</td>
					<td>
						#rc.ListStatisticSheet.name#
					</td>
					<td>
						#rc.ListStatisticSheet.beginbalance#
						<cfset ebalance=#rc.ListStatisticSheet.beginbalance#/>
					</td>
					<cfif #dateFormat(rc.ListStatisticSheet.datecreate,"dd-mm-yyyy")# eq #dateFormat(now(),"dd-mm-yyyy")#>
						<td>
							<cfquery name="qGetInputNow">
								select sum(quantity) as quantity from importsheet where date(datecreate)=date(#now()#) and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#"> group by materialId
							</cfquery>
							<cfif #qGetInputNow.recordcount# eq 0>
								0
							<cfelse>
								#qGetInputNow.quantity[1]#
								<cfset ebalance=ebalance+#qGetInputNow.quantity[1]#/>
							</cfif>
						</td>
						<td>
							<cfquery name="qGetAdjustNow">
								select sum(fixnumber) as quantity from adjustsheet where date(datecreate)=date(#now()#) and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#"> group by materialId
							</cfquery>
							<cfif #qGetAdjustNow.recordcount# eq 0>
								0
							<cfelse>
								#qGetAdjustNow.quantity[1]#
								<cfset ebalance=ebalance+#qGetAdjustNow.quantity[1]#/>
							</cfif>
						</td>
						<td>
							<cfquery name="qGetOutputNow">
								select (actualtemp-ordertemp)as quantity from material where actualtemp <> ordertemp and materialId=<cfqueryparam sqltype="integer" value="#rc.ListStatisticSheet.materialId#"> group by materialId
							</cfquery>
							<cfif #qGetOutputNow.recordcount# eq 0>
								#rc.ListStatisticSheet.outputquantity#
								<cfset ebalance=ebalance-#rc.ListStatisticSheet.outputquantity#/>
							<cfelse>
								#rc.ListStatisticSheet.outputquantity+qGetOutputNow.quantity[1]#
									<cfset ebalance=ebalance-#rc.ListStatisticSheet.outputquantity+qGetOutputNow.quantity[1]#/>
							</cfif>
						</td>
						<td>
							#ebalance#
						</td>
					<cfelse>
						<td>
							#rc.ListStatisticSheet.inputquantity#
						</td>
						<td>
							#rc.ListStatisticSheet.adjustquantity#
						</td>
						<td>
							#rc.ListStatisticSheet.outputquantity#
						</td>
						<td>
							#rc.ListStatisticSheet.endingbalance#
						</td>
					</cfif>
				</tr>
				<cfset id += 1/>
			</cfloop>	
			</tbody>
		</table>
	</div>
	</form>
</cfoutput>