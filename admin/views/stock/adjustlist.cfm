<script type="text/javascript">
	
	function showAdjustModal(id) {
		$.ajax({
			type: 'POST',
			url: '/index.cfm/admin:stock.adjustdetail/',
			data: {'adjustId':id},
			dataType: 'JSON',
			success: function(data) {
				$('.itemadjust').empty();
				for(i=0;i<data.length;i++){
					$('.datecreate').html(data[i].datecreate);
					$('.checker').html(data[i].checker);
					$('.shift').html(data[i].shift);
					$('.note').html('<textarea readonly rows="4" id="txtNote" class="col-md-12 col-xs-12 col-sm-12" name="txtNote">'+data[i].note+'</textarea>');
					$('.itemadjust').append('<tr><td>'+data[i].materialname+'</td><td>'+data[i].fixnumber+' '+data[i].unitname+'</td></tr>');
					$('#adjustModal').modal('show');
				}
			}
		});		
	}

    jQuery(function($) {  	

	    $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		var oTable1 = $('#importList')
		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,{ "bSortable": false }]
	    } );

		$('#datesearch').change(function(){
			var idate=$('#datesearch').val();
			location.href='/index.cfm/admin:stock.adjustlist?idate='+idate;
		});

	})
</script>
<cfoutput>
<cfscript>
	setLayout('stock');
</cfscript>
<div class="page-header">
  <h1>
	#getLabel("Adjust")#
  </h1>
</div>
<div class="col-xs-12">
	<div class="row">
		<div class="col-md-2">
			<a class="btn btn-primary btn-white" href="/index.cfm/admin:stock.adjustsheet">
				<i class="ace-icon fa fa-plus"></i> 
				#getLabel("Add New")# 
			</a>
		</div>
		<div class="col-md-3">
			<input class="form-control date-picker" name="datesearch" id="datesearch" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.idate,"dd-mm-yyyy","pt_PT")#" placeholder="Date search" />
		</div>
	</div>
	<p></p>

	<table id="importList" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>#getLabel("ID")#</th>
				<th>#getLabel("Checker")#</th>
				<th>#getLabel("Shift")#</th>
				<th>#getLabel("Date")#</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<cfloop query="#rc.adjustlist#">
			<tr data-toggle="modal" onclick="return showAdjustModal(#rc.adjustlist.adjustId#);">
				<td>
					#rc.adjustlist.adjustId#
				</td>
				<td>
					#rc.adjustlist.checker#
				</td>
				<td>
					#rc.adjustlist.shift#
				</td>				
				<td>
					#datetimeFormat(rc.adjustlist.datecreate,'dd-mm-yyyy')#
				</td>
				<td style="text-align:center">
					<i class="ace-icon fa fa-info bigger-130 green cursor" data-toggle="modal" onclick="return showAdjustModal(#rc.adjustlist.adjustId#);"></i>
				</td>
			</tr>		
			</cfloop>
		</tbody>
	</table>
</div>

<div class="modal fade" id="adjustModal" tabindex="-1" role="dialog" aria-labelledby="adjustModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">#getLabel("Close")#</span></button>
        <h4 class="modal-title" id="myModalLabel">#getLabel("Adjust detail")#</h4>
      </div>
      <div class="modal-body">
		<div class="row">
			<div class="col-md-12 row">
				<div class="col-md-7">
					<fieldset>
						<legend>#getLabel("Information")#</legend>
						<table>
							<tr>
								<td style="width:70%"><label>#getLabel("Date create")#:</label></td>
								<td class="datecreate"></td>
							</tr>							
							<tr>
								<td><label>#getLabel("Checker")#:</label></td>
								<td class="checker"></td>
							</tr>
							<tr>
								<td><label>#getLabel("Shift")#:</label></td>
								<td class="shift"></td>
							</tr>
						</table>
					</fieldset>
					<p></p>
				</div>
				<div class="col-md-5">
					<fieldset>
						<legend>#getLabel("Note")#</legend>
						<span class="note"></p>
					</fieldset>	
					<p></p>	
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-default">
		            <div class="panel-heading">#getLabel("Adjust list")#</div>
		            <div class="table-responsive">
		              	<table class="table" id="adjust_list">
			                <thead>
				                <tr>
				                    <th>#getLabel("Name")#</th>
				                    <th>#getLabel("Quantity")#</th>
				                </tr>
			                </thead>
			                <tbody class="itemadjust">
			                </tbody>
			            </table>
			        </div>
			    </div>			
			</div>
		</div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

</cfoutput>