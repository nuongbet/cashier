<cfoutput>
<cfscript>
	setLayout('fund');
</cfscript>
<script type="text/javascript">
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
    }
    function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
	}
	function format(input)
	{
	    var nStr = input.value + '';
	    nStr = nStr.replace( /\,/g, "");
	    x = nStr.split( '.' );
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while ( rgx.test(x1) ) {
	        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
	    }
	    input.value = x1 + x2;
	}
	$(document).ready(function(){
		 $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});
	});
</script>
<div class="page-header">
  <h1>
  	<cfif URL.isreceipt eq 1>
  		#getLabel("Receipt Voucher")#
  	<cfelse>
  		#getLabel("Payment Voucher")#
  	</cfif>
  </h1>
</div>

<form class="form-horizontal" role="form" action="" method="POST">
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Date")#</label>
		<div class="col-sm-10">
			<input type="text" class="col-xs-10 col-sm-5 date-picker" name="datereceipt" id="datereceipt" type="text" data-date-format="dd-mm-yyyy" value="#dateFormat(now(),'dd-mm-yyyy')#" required>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">
			<cfif URL.isreceipt eq 1>
		  		#getLabel("Receiver")#
		  	<cfelse>
		  		#getLabel("Payer")#
		  	</cfif></label>
		<div class="col-sm-10">
			<input type="text" id="person1" name="person1" class="col-xs-10 col-sm-5" value="" required/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">
		  	<cfif URL.isreceipt eq 1>
		  		#getLabel("Payer")#
		  	<cfelse>
		  		#getLabel("Receiver")#
		  	</cfif>
		</label>
		<div class="col-sm-10">
			<input type="text" id="person2" name="person2" class="col-xs-10 col-sm-5" value="" required/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Amount")#</label>
		<div class="col-sm-10">
			<input type="text" id="amount" name="amount" class="col-xs-10 col-sm-5" value="" onkeypress="return isNumberKey(event)" onkeyup="format(this)" required/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Note")#</label>
		<div class="col-sm-10">
			<textarea rows="5" cols="30" id="txtNote"  name="txtNote" class="col-xs-10 col-sm-5" required></textarea>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Add another")#</label>
		<div class="col-sm-10">
			<label>
				<input name="acontinue" id="acontinue" class="ace ace-switch ace-switch-2" type="checkbox" value="1" checked/>
				<span class="lbl"></span>
			</label>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<button class="btn btn-info" type="submit">
				<i class="ace-icon fa fa-check bigger-110"></i>
				#getLabel("Submit")#
			</button>
		</div>
	</div>
</form>
</cfoutput>