<cfoutput>
<cfscript>
	setLayout('stock');
</cfscript>
<script type="text/javascript">

	function isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		  return false;

		return true;
	}


	function format(input)
	{
	    var nStr = input.value + '';
	    nStr = nStr.replace( /\,/g, "");
	    x = nStr.split( '.' );
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while ( rgx.test(x1) ) {
	        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
	    }
	    input.value = x1 + x2;
	}


	function commaSeparateNumber(val){
		while (/(\d+)(\d{3})/.test(val.toString())){
		  val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
		}
		return val;
	}

	function removeitem(item){
		var materialId 		= $('##mId'+item).val();
		var materialname 	= $('##mName'+item).val();
		var total 			= parseFloat($('##iTotal'+item).val().replace(/,/g , ""));
		var amount 		 	= parseFloat($('##amount').val().replace(/,/g , ""));
		amount 				= commaSeparateNumber(amount - total);
		$('.item'+item).remove();
		$('##amount').val(amount);
		$("##materialname").append('<option value="'+materialId+'">'+materialname+'</option>');
	}

	function getunitnameandbalance(materialId){
		$.ajax({
			type: 'POST',
			url: '/index.cfm/admin:stock.getunitnameandbalance/',
			data: {'materialId':materialId},
			dataType: 'JSON',
			success: function(data) {
				$('##unitname').val(data[0].name);
				$('##price').val(commaSeparateNumber(data[0].unitprice));
			}
		}); 
	}

	$(document).ready(function(){
		var i = 1;

		$('##price').val(commaSeparateNumber($('##price').val()));

	    $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		$('##materialname').change(function(){
			var materialId = $(this).val();
			getunitnameandbalance(materialId);
		})

		$('##quantity').change(function(){
			var quantity     = parseFloat($('##quantity').val());
			var price        = parseFloat($('##price').val().replace(/,/g , ""));
			var total 		 = parseFloat(price * quantity);
			total = commaSeparateNumber(total);
			$('##total').val(total);
		})

		$('.additemimport').on('click',function(e){

			var materialname = $('select[name="materialname"] option:selected').text();
			var materialId   = $('##materialname').val();
			var quantity     = parseFloat($('##quantity').val());
			var unitname     = $('##unitname').val();
			var price	 	 = $('##price').val();
			var total        = $('##total').val();
			var amount 		 = $('##amount').val();

			$('.itemimport').append('<tr class="item'+i+'"><td><input type="text" name="mId[]" id="mId'+i+'" value="'+materialId+'" hidden/><input type="text" id="mName'+i+'" value="'+materialname+'" readonly/></td><td><input type="text" name="iPrice[]" value="'+price+'" readonly /></td><td><input type="text" class="col-sm-3" name="mQuantity[]" value="'+quantity+'" readonly /><input type="text" class="col-sm-4" value="'+unitname+'" readonly /></td><td><input type="text" id="iTotal'+i+'" name="iTotal[]" value="'+total+'" readonly /></td><td><button class="btn btn-danger" type="button" onclick="return removeitem('+i+');"><i class="ace-icon fa fa-times bigger-110"></i>Remove</button></td></tr>');
			//clear input after add
			$('##quantity').val("0");
			$('##total').val("0");
			//calculate amount
			var amount = parseFloat(parseFloat(amount.replace(/,/g , "")) + parseFloat(total.replace(/,/g , "")));
			$('##amount').val(commaSeparateNumber(amount));
			//remove name material and load balance 
			$("##materialname option[value='"+materialId+"']").remove();
			var materialId=$('##materialname').val();
			getunitnameandbalance(materialId);
			i++;
		})
	})
</script>

<div class="page-header">
  <h1>
	#getLabel("Purchase Voucher Sheet")# (#datetimeFormat(now(),'dd-mm-yyyy')#)
  </h1>
</div>
<div class="row" style="margin:auto">
	<form action="" method="POST">

	<div class="col-xs-12 row">
		<div class="col-xs-6">
			<fieldset>
				<legend>#getLabel("Information")#</legend>
				<div class="row">
					<div class="col-xs-4">
						<label>#getLabel("Delivery company")#</label>
					</div>
					<div class="col-xs-8">
						<input type="text" class="form-control" name="deliverycompany" required>
						<p></p>
					</div>
					<div class="col-xs-4">
						<label>#getLabel("Delivery Date")#</label>
					</div>
					<div class="col-xs-8">
						<input type="text" class="form-control date-picker" name="deliverydate" id="deliverydate" type="text" data-date-format="dd-mm-yyyy" value="#dateFormat(now(),'dd-mm-yyyy')#" required>
						<p></p>
					</div>
					<div class="col-xs-4">
						<label>#getLabel("Buyer")#</label>
					</div>
					<div class="col-xs-8">
						<input type="text" class="form-control" name="buyer" required>
						<p></p>
					</div>
				</div>
			</fieldset>
		</div>
		<div class="col-xs-6">
			<fieldset>
				<legend>#getLabel("Note")#</legend>
				<textarea rows="5" cols="30" id="txtNote" class="col-md-12 col-xs-12 col-sm-12" name="txtNote"></textarea>
			</fieldset>			
		</div>

	</div>
	<div class="col-xs-12">
        <div class="tab-pane active" id="panel-current">
			<div class="panel panel-default">
	            <div class="panel-heading">#getLabel("Import list")#</div>
	            <div class="table-responsive">
	              	<table class="table" id="adjust_list">
		                <thead>
			                <tr>
			                    <th>#getLabel("Name")#</th>
			                    <th>#getLabel("Price")# (#currency#)</th>
			                    <th>#getLabel("Quantity")#</th>
			                    <th>#getLabel("Total")# (#currency#)</th>
			                    <th></th>
			                </tr>
		                </thead>
		                <tbody class="itemimport">
							<tr>
								<td>
									<select name="materialname" id="materialname">
										<cfloop query="#rc.ListNameMaterial#">
											<option value="#rc.ListNameMaterial.materialId#">#rc.ListNameMaterial.materialname#</option>
										</cfloop>
									</select>
								</td>
								<td>
									<input type="text" step="any" id="price" name="price" value="#rc.ListNameMaterial.unitprice#" readonly />
								</td>
								<td>
									<input type="number" step="any" id="quantity" min="0" name="quantity" value="0" class="col-sm-3" onkeypress="return isNumberKey(event)"/>
									<input type="text" id="unitname" name="unitname" value="#rc.ListNameMaterial.unitname#" class="col-sm-4" readonly/>
								</td>
								<td>
									<input type="text" step="any" id="total" name="total" value="0" readonly />
								</td>
								<td>
									<button class="btn btn-info additemimport" type="button">
										<i class="ace-icon fa fa-plus bigger-110"></i>
										#getLabel("Add")#
									</button>
								</td>
							</tr>
		                </tbody>
							<tr>
								<td colspan="3">
									<label class="amount">#getLabel("Amount")# (#currency#)</label>
								</td>
								<td colspan="2">
									<input type="text" step="any" id="amount" name="amount" value="0" readonly />
								</td>
			                </tr>
							<tr>
								<td colspan="3">
									<label class="amount">#getLabel("Actual cost")# (#currency#)</label>
								</td>
								<td colspan="2">
									<input type="text" step="any" id="actualcost" name="actualcost" value="" onkeyup="return format(this);" onkeypress="return isNumberKey(event)" required />
								</td>
			                </tr>
		            </table>
		        </div>
		    </div>
        </div>
    </div>
	<div class="col-xs-12">
	    <div class="clearfix form-actions">
			<div class="col-md-10">
				<button class="btn btn-info" type="submit">
					<i class="ace-icon fa fa-check bigger-110"></i>
					#getLabel("Submit")#
				</button>
			</div>
		</div>
	</div>
	</form>
</div>


</cfoutput>