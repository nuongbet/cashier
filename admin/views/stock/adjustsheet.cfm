<cfoutput>

<script type="text/javascript">

	function removeitem(item){
		var materialId = $('##mId'+item).val();
		var materialname = $('##mName'+item).val();
		$('.item'+item).remove();
		$("##materialname").append('<option value="'+materialId+'">'+materialname+'</option>');
	}

	function getunitnameandbalance(materialId){
		$.ajax({
			type: 'POST',
			url: '/index.cfm/admin:stock.getunitnameandbalance/',
			data: {'materialId':materialId},
			dataType: 'JSON',
			success: function(data) {
			$('##unitname').val(data[0].name);
			$('##endingbalance').val(data[0].actualstock);
			}
		}); 
	}

	$(document).ready(function(){
		var i = 1;

		$('##materialname').change(function(){
			var materialId = $(this).val();
			getunitnameandbalance(materialId);
		})

		$('##stockvalue').change(function(){
			var endingbalance = $('##endingbalance').val();
			var stockvalue = $('##stockvalue').val();
			var fixnumber = parseFloat(stockvalue - endingbalance).toFixed(2);
			$('##addFixnumber').val(fixnumber);
		})
		
		$('.additemadjust').click(function(){
			var materialname=$('select[name="materialname"] option:selected').text();
			var materialId=$('##materialname').val();
			var endingbalance = $('##endingbalance').val();
			var stockvalue = $('##stockvalue').val();
			var fixnumber = parseFloat(stockvalue - endingbalance).toFixed(2);
			$('.itemadjust').append('<tr class="item'+i+'"><td><input name="mId[]" id="mId'+i+'" type="text" value="'+materialId+'" hidden /><input type="text" id="mName'+i+'" value="'+materialname+'" readonly/></td><td><input type="text" value="'+endingbalance+'" readonly /></td><td><input type="text" value="'+stockvalue+'" readonly /></td><td><input type="text" name="mFixnumber[]" id="Fixnumber" value="'+fixnumber+'" readonly></td><td><button class="btn btn-danger" type="button" onclick="return removeitem('+i+');"><i class="ace-icon fa fa-times bigger-110"></i>Remove</button></td></tr>');
			$('##stockvalue').val("");
			$('##addFixnumber').val("");

			//remove name material and load balance 
			$("##materialname option[value='"+materialId+"']").remove();
			var materialId=$('##materialname').val();
			getunitnameandbalance(materialId);
			i++;
		})
	})
</script>
<cfscript>
	setLayout('stock');
</cfscript>
<div class="page-header">
  <h1>
	#getLabel("Adjust Sheet")# (#datetimeFormat(now(),'dd-mm-yyyy')#)
  </h1>
</div>
<div class="row" style="margin:auto">
	<form action="" method="POST">

	<div class="col-xs-12 row">
		<div class="col-xs-6">
			<fieldset>
				<legend>#getLabel("Information")#</legend>
				<div class="row">
					<div class="col-xs-4">
						<label>#getLabel("Checker")#</label>
					</div>
					<div class="col-xs-8">
						<input type="text" class="form-control" name="checker" id="checker">
						<p></p>
					</div>
					<div class="col-xs-4">
						<label>#getLabel("Shift")#</label>
					</div>
					<div class="col-xs-8">
						<select name="shift" id="shift" class="form-control">
							<cfloop query="#rc.ListShift#">
								<option value="#rc.ListShift.name#">#rc.ListShift.name#</option>
							</cfloop>
						</select>
						<p></p>
					</div>
				</div>
			</fieldset>
		</div>
		<div class="col-xs-6">
			<fieldset>
				<legend>#getLabel("Note")#</legend>
				<textarea rows="5" cols="30" id="txtNote" class="col-md-12 col-xs-12 col-sm-12" name="txtNote" style="margin-bottom:5px;"></textarea>
			</fieldset>			
		</div>

	</div>

	<div class="col-xs-12">
        <div class="tab-pane active" id="panel-current">
			<div class="panel panel-default">
	            <div class="panel-heading">#getLabel("Adjust list")#</div>
	            <div class="table-responsive">
	              	<table class="table" id="adjust_list">
		                <thead>
			                <tr>
			                    <th>#getLabel("Name")#</th>
			                    <th>#getLabel("Ending balance")#</th>
			                    <th>#getLabel("Stock value")#</th>
			                    <th>#getLabel("Fix number")#</th>
			                    <th></th>
			                </tr>
		                </thead>
		                <tbody class="itemadjust">
							<tr>
								<td>
									<select name="materialname" id="materialname">
										<cfloop query="#rc.ListNameMaterial#">
											<option value="#rc.ListNameMaterial.materialId#">#rc.ListNameMaterial.materialname#</option>
										</cfloop>
									</select>
								</td>
								<td>
									<input type="number" id="endingbalance" name="endingbalance" value="#rc.ListNameMaterial.actualstock#" class="col-md-4" readonly/>
									<input type="text" id="unitname" name="unitname" value="#rc.ListNameMaterial.unitname#" class="col-md-3" readonly/>
								</td>
								<td>
									<input type="number" id="stockvalue" step="any">
								</td>
								<td>
									<input type="text" id="addFixnumber" readonly>
								</td>
								<td>
									<button class="btn btn-info additemadjust" type="button">
										<i class="ace-icon fa fa-plus bigger-110"></i>
										#getLabel("Add")#
									</button>
								</td>
							</tr>
		                </tbody>
		            </table>
		        </div>
		    </div>
        </div>
    </div>
	<div class="col-xs-12">
	    <div class="clearfix form-actions">
			<div class="col-md-10">
				<button class="btn btn-info" type="submit">
					<i class="ace-icon fa fa-check bigger-110"></i>
					#getLabel("Submit")#
				</button>
			</div>
		</div>
	</div>
	</form>
</div>

</cfoutput>