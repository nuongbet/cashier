<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	$(document).ready(function(){
		if(#rc.category.isActive# == 0){
			$("##isActive").val('0');
		}else{
			$("##isActive").val('1');
		}
		
		$("##isActive").change(function(){
		    if($(this).prop('checked')){
		          $(this).val('1');
		     }else{
		          $(this).val('0');
		     }
		}); 
	});

    function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
	}
	function format(input)
	{
	    var nStr = input.value + '';
	    nStr = nStr.replace( /\,/g, "");
	    x = nStr.split( '.' );
	    x1 = x[0];
	    x2 = x.length > 1 ? '.' + x[1] : '';
	    var rgx = /(\d+)(\d{3})/;
	    while ( rgx.test(x1) ) {
	        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
	    }
	    input.value = x1 + x2;
	}
</script>
	
<!--- <cfquery name="listCategory">
	SELECT ifnull(pr.parentid,ifnull(c.parentid,c.categoryId)) as rid, ifnull(c.parentid, c.categoryId) as pid ,c.categoryname, c.categoryId, c.ilevel
	FROM category c
	left join category pr on pr.categoryId = c.parentId where c.shopId=<cfqueryparam sqltype="integer" value="0"/> or c.shopId=<cfqueryparam sqltype="integer" value="#SESSION.ShopId#"/>
	order by rid, (case when rid = pid then c.categoryId else c.parentId end) , c.categoryid
</cfquery> --->
<div class="page-header">
  <h1>
	#getLabel("Categories")#
  </h1>
</div><div class="row">
	<div class="col-xs-12">
		<form class="form-horizontal" role="form" action="" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">#getLabel("Name")#</label>
				<div class="col-sm-9">
					<input type="text" id="name" name="name" placeholder="#getLabel("Name")#" class="col-xs-10 col-sm-5" value="#rc.category.categoryname#" required/>
				</div>
			</div>
 
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">#getLabel("Description")#</label>

				<div class="col-sm-9">
					<input type="text" id="description" name="description" placeholder="#getLabel("Description")#" class="col-xs-10 col-sm-5" value="#rc.category.description#" />
				</div>
			</div>

		<!--- 	<div class="form-group">
				<label class="col-sm-3 control-label">Parent</label>

				<div class="col-sm-4">
					<select class="form-control" name="parent" id="form-field-select-2 parent" multiple="multiple" style="height: 250px">
						<cfloop query="listCategory">
							<option value="#listCategory.categoryId#" <cfif #listCategory.categoryId# EQ rc.category.parentId>selected</cfif> <cfif #listCategory.ilevel# EQ 4>disabled</cfif>>
								<cfif listCategory.ilevel EQ 1>
									#listCategory.categoryname#
									<cfelseif listCategory.ilevel EQ 2>
										&nbsp; &nbsp;-- #listCategory.categoryname#	
									<cfelse>
										&nbsp; &nbsp; &nbsp; &nbsp;-- #listCategory.categoryname#
								</cfif>
							</option>
						</cfloop>
					</select>
				</div>
			</div> --->
			<cfif #URL.ca# eq 6>
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">#getLabel("Cost")#</label>

					<div class="col-sm-9">
						<input type="text" id="cost" name="cost" placeholder="#getLabel("Cost")#" class="col-xs-10 col-sm-5" value="#numberFormat(rc.category.cost)#" onkeypress="return isNumberKey(event)" onkeyup="format(this)" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right">#getLabel("Profit margin")#</label>

					<div class="col-sm-9">
						<input type="text" id="profitmargin" name="profitmargin" placeholder="#getLabel("Profit margin")#" class="col-xs-10 col-sm-5" value="#numberFormat(rc.category.profitmargin)#" onkeypress="return isNumberKey(event)" onkeyup="format(this)" />
					</div>
				</div>
			</cfif>
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">#getLabel("Sorted")#</label>

				<div class="col-sm-9">
					<input type="text" id="Sorted" name="Sorted" placeholder="#getLabel("Sorted")#" class="col-xs-10 col-sm-5" value="#rc.category.Sorted#" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right">#getLabel("Active")#</label>
				<div class="col-sm-9">
					<input name="isActive" id="isActive" class="ace ace-switch ace-switch-4" type="checkbox" value="#rc.category.isActive#" <cfif #rc.category.isActive# eq 1>checked</cfif>>
					<span class="lbl"></span>
				</div>
			</div>
			<cfif url.id eq 0>
				<div class="form-group">
					<label class="col-sm-3 control-label no-padding-right" for="_addAnother">
						Add another
					</label>
					<div class="col-sm-9">
						<div class="clearfix">
							<input class="ace ace-switch ace-switch-5" type="checkbox" id="_addAnother" name="_addAnother" value="1" <cfif structKeyExists(url,"_addAnother") and url._addAnother eq true>checked</cfif>>
							<span class="lbl"></span>
						</div>
					</div>
				</div>
			</cfif>
			<div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						#getLabel("Submit")#
					</button>
					<!--- &nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button> --->
				</div>
			</div>
		</form>
	</div>
</div>
</cfoutput>