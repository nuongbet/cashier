<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<script type="text/javascript">
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
    jQuery(function($) {
    	
    	if(#URL.ca# ==6){
    		var oTable1 = $('##list_link')
    		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,null,null,{ "bSortable": false }]
	    } );
    	}
    	else{
    		var oTable1 = $('##list_link')
    			.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,{ "bSortable": false }]
	    } );
    	}
		
	   })
</script>

<div class="page-header">
  <h1>
  	<cfif #URL.ca# eq 6>
  		#getLabel("Food Category")#
  	<cfelse>
  		#getLabel("Table Category")#
  	</cfif>
  </h1>
</div>
<div class="col-xs-12">
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:category.form?ca=#URL.ca#&id=0">
		<i class="ace-icon fa fa-plus"></i>#getLabel("Add New")# 
	</a>
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:category?ca=#URL.ca#&loadDelItems=0">
		<i class="ace-icon fa fa-list-ul"></i>#getLabel("List of Deleted Items")# 
	</a>
	<p></p>
		<table id="list_link" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("ID")#</th>
					<th>#getLabel("Name")#</th>
					<th>#getLabel("Description")#</th>
					<!--- <th>Parent</th> --->
					<cfif #URL.ca# eq 6>
						<th>Cost</th>
						<th>Profit margin</th>
					<cfelse>
					<!--- 	<th></th>
						<th></th> --->
					</cfif>
					<th>#getLabel("Sorted")#</th>
					<th>#getLabel("Active")#</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
			<cfset i=1/>
			<cfloop query="rc.category">
				<tr>
					<td>
						#i#
					</td>
					<td>
						<a class="blue" href="/index.cfm/admin:category.form?ca=#URL.ca#&id=#rc.category.categoryId#">
						#rc.category.categoryname#
					</td>
					<td>
						#rc.category.description#
					</td>
				<!--- 	<td>
						#entityLoadbyPK("category",rc.category.parentId).categoryName#
					</td> --->
					<cfif #URL.ca# eq 6>
						<td>
							#numberFormat(rc.category.cost)#
						</td>
						<td>
							#numberFormat(rc.category.profitmargin)#
						</td>
					<cfelse>
						<!--- <td></td>
						<td></td> --->
					</cfif>
					<td>
						#rc.category.sorted#
					</td>
					<td>
						<cfif #rc.category.isActive# EQ true>
							<i class="ace-icon fa fa-check-circle-o green bigger-130"></i>
							<cfelse>
							<i class="ace-icon fa fa-circle red bigger-130"></i>
						</cfif>				
					</td>
					<td style="text-align:center">
						<div class="hidden-sm hidden-xs action-buttons">
							<a class="green" href="/index.cfm/admin:category.form?ca=#URL.ca#&id=#rc.category.categoryId#">
								<i class="ace-icon fa fa-pencil bigger-130"></i>
							</a>
						</div>
					</td>
					<!--- <td style="text-align:center">
						<div class="hidden-sm hidden-xs action-buttons">
							<a class="red" href="/index.cfm/admin:category.delete?id=#rc.category.categoryId#" onclick="return checkDelete()">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>
							</a>
						</div>
					</td> --->		
				</tr>
				<cfset i=i+1/>
			</cfloop>	
			</tbody>
		</table>
	</div>
</cfoutput>