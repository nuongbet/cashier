<cfoutput>
<style type="text/css" media="screen">
	.resize {
	    width: 100%;
		height: 100%;
		display: block;
		margin: auto auto;
	}
	.resize-65 {
	    width: 65%;
		height: 65%;
		display: block;
		margin: auto auto;
	}
</style>
<div class="container main-icon">
	<div class="col-md-12  img-main">
		<div class="col-md-4">
			<a href="/index.cfm/home:chief.default" >
				<img class="img-responsive resize" src="/images/warehouse.png">
				<p>#getLabel("chiefscreen")#</p>
			</a>
		</div>
		<div class="col-md-4">
			<a href="/index.cfm/admin:main.acashier" >
				<img class="img-responsive resize" src="/images/cashier.png">
				<p>#getLabel("cashierscreen")#</p>
			</a>
		</div>
		<div class="col-md-4">
			<a href="/index.cfm/admin:main.afund" >
				<img class="img-responsive resize" src="/images/cash.png">
				<p>#getLabel("cashscreen")#</p>
			</a>
		</div>
	</div>
	<div class="col-md-12  img-main">
		<div class="col-md-6">
			<a href="/index.cfm/admin:main.astock" >
				<img class="img-responsive resize-65" src="/images/warehouse.png">
				<p>Warehouse</p>
			</a>
		</div>
		<div class="col-md-6">
			<a href="/index.cfm/admin:main.admin" >
				<img class="img-responsive resize-65" src="/images/admin.png">
				<p>Admin</p>
			</a>
		</div>
	</div>
</div>
</cfoutput>
