<cfoutput>
<!---<script language="javascript">
$(function () {
    $('##topfoodsell').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Top Food sell in week'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Top Food',
            
            data: 
            [
                <cfloop query="rc.topFoodSell">
                    ['#rc.topFoodSell.name#', #rc.topFoodSell.quantity#],
                </cfloop>
            ]         
        }]
    });
});
 </script>
<div id="topfoodsell"></div>
 </div> --->



<cfscript>
    public array function queryToArray( required query qry ) {
    var columns = arguments.qry.getColumnNames();
    var ofTheJedi = [];

    for( var i = 1; i LTE qry.recordCount; i++ ) {
        var obj = {};

        for( var k = 1; k LTE arrayLen( columns ); k++ ) {
            structInsert( obj, columns[ k ], arguments.qry[ columns[ k ] ][ i ] );
        }

        arrayAppend( ofTheJedi, obj );
    }

    return ofTheJedi;
}
</cfscript>

<!--- report food --->
<cfset data3=#replace(serializejson(queryToArray(rc.topFoodSell)),'"name":',"","All")#>
<cfset data4=#replace(data3,'"quantity":',"","All")#>
<cfset data5=#replace(data4,'{',"[","All")#>
<cfset data6=#replace(data5,'}',"]","All")#>
<cfdump eval=#data3#/>
<cfset stChart = {
        chart: {
            renderTo:"container",
            plotBackgroundColor: 'null',
            plotBorderWidth: 'null',
            plotShadow: false
        },
        title: {
            text: 'Top Food sell in week'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: 'black',
                    connectorColor: 'black',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Cashier',
            data: 
                #data6#
            
        }]
    }>
    <cfhighchart attributeCollection="#stChart#" createContainer="false">
<!--- end report food --->

<!--- report revenue monthly --->
<cfquery name="qGetTotalMonth">
    SELECT 
     sum(case when month(datecreate) =  1 then total else 0 end)m1
    ,sum(case when month(datecreate) =  2 then total else 0 end)m2
    ,sum(case when month(datecreate) =  3 then total else 0 end)m3
    ,sum(case when month(datecreate) =  4 then total else 0 end)m4
    ,sum(case when month(datecreate) =  5 then total else 0 end)m5
    ,sum(case when month(datecreate) =  6 then total else 0 end)m6
    ,sum(case when month(datecreate) =  7 then total else 0 end)m7
    ,sum(case when month(datecreate) =  8 then total else 0 end)m8
    ,sum(case when month(datecreate) =  9 then total else 0 end)m9
    ,sum(case when month(datecreate) =  10 then total else 0 end)m10
    ,sum(case when month(datecreate) =  11 then total else 0 end)m11
    ,sum(case when month(datecreate) =  12 then total else 0 end)m12
    FROM bill where year(datecreate)=year(now())
</cfquery>
<cfset myArray = [qGetTotalMonth.m1[1],qGetTotalMonth.m2[1],qGetTotalMonth.m3[1],qGetTotalMonth.m4[1],qGetTotalMonth.m5[1],qGetTotalMonth.m6[1],qGetTotalMonth.m7[1],qGetTotalMonth.m8[1],qGetTotalMonth.m9[1],qGetTotalMonth.m10[1],qGetTotalMonth.m11[1],qGetTotalMonth.m12[1]]>
<cfset stChart1 = {
            chart: {
                renderTo:"container1",
                type: 'column'
            },
            title: {
                text: 'Monthly Revenue'
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Revenue (#currency#)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.1f} #currency#</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Cashier',
                data: #myArray#
            }]
        }>
    <cfhighchart attributeCollection="#stChart1#" createContainer="false">
<!--- end report revenue monthly --->

<!--- report stock --->
<cfquery name="qGetStock">
    SELECT materialId,concat(material.name,'(',unit.name,')') as name,actualstock,needstock,ministock 
    FROM material,unit 
    WHERE material.unitId=unit.unitId;
</cfquery>
<cfset ListStock=#ListToArray(valueList(qGetStock.name, ','))#/>
<cfset ListActual=#ListToArray(valueList(qGetStock.actualstock, ','))#/>
<cfset ListNeed=#ListToArray(valueList(qGetStock.needstock, ','))#/>
<cfset ListMini=#ListToArray(valueList(qGetStock.ministock, ','))#/>
<cfset stChart2 = {
            chart: {
                renderTo:"container2",
                type: 'column'
            },
            title: {
                text: 'Report Stock'
            },
            subtitle: {
                text: 'Source: Cashier'
            },
            xAxis: {
                categories: #ListStock#
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Quantity'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td><td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Actual',
                data: #ListActual#
    
            }, {
                name: 'Need',
                data: #ListNeed#
    
            }, {
                name: 'Minimum',
                data: #ListMini#
    
            }]
        }>
    <cfhighchart attributeCollection="#stChart2#" createContainer="false">
<!--- end report stock --->

<!--- report cost vs revenue in 30 recent day --->
<cfquery name="qGetCostvsRevenue">
    select day(temp.datecreate)as datecreate,ifnull(ip.amount,0)as cost,ifnull(bo.total,0)as revenue 
    from
    (
        SELECT date_sub(curdate(),interval 30 day) + INTERVAL a + b DAY datecreate,0 as amount
            FROM
            (SELECT 0 a UNION SELECT 1 a UNION SELECT 2 UNION SELECT 3
            UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7
            UNION SELECT 8 UNION SELECT 9 ) d,
            (SELECT 0 b UNION SELECT 10 UNION SELECT 20 
            UNION SELECT 30 UNION SELECT 40) m
            WHERE date_sub(curdate(),interval 30 day) + INTERVAL a + b DAY  < curdate() + interval 1 day ORDER BY a + b)as temp
        left join 
        (select date(datecreate)as datecreate,sum(amount)as amount from import
        where datecreate  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datecreate))as ip
        on temp.datecreate=ip.datecreate
        left join 
        (select date(datecreate)as datecreate,sum(total)as total from bill
        where datecreate  BETWEEN DATE_SUB(CURDATE(), INTERVAL 30 DAY) AND CURDATE() + interval 1 day group by date(datecreate))as bo
        on temp.datecreate=bo.datecreate
</cfquery>
<!--- <cfset ListDate=#ListToArray(replace(replace(valueList(qGetCostvsRevenue.datecreate, ','),"{ts '","","All")," 00:00:00'}","","All"),',')#/> --->
<cfset ListDate=#ListToArray(valueList(qGetCostvsRevenue.datecreate, ','))#/>
<cfset ListCost=#ListToArray(valueList(qGetCostvsRevenue.cost, ','))#/>
<cfset ListRevenue=#ListToArray(valueList(qGetCostvsRevenue.revenue, ','))#/>
<cfset stChart3 = {
            chart: {
                renderTo:"container3",
                type: 'line'
            },
            title: {
                text: 'Cost vs Revenue in 30 recent day',
                x: -20 //center
            },
            subtitle: {
                text: 'Source: Cashier',
                x: -20
            },
            xAxis: {
                categories: #ListDate#
            },
            yAxis: {
                title: {
                    text: 'Amount (#currency#)'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '##808080'
                }]
            },
            tooltip: {
                valueSuffix: '#currency#'
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Revenue',
                data: #ListRevenue#
            }, {
                name: 'Cost',
                data: #ListCost#
            }]
        }>
    <cfhighchart attributeCollection="#stChart3#" createContainer="false">
<!--- end report cost vs revenue in 30 recent day --->


    <div class="col-md-12 column">
        <!--- <div class="tabbable col-md-9 column" id="tabs-111222"> --->
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="##panel-current" data-toggle="tab">Top Food sell in week</a>
                </li>
                <li>
                    <a href="##panel-last" data-toggle="tab">Monthly Revenue</a>
                </li>
                <li>
                    <a href="##reportstock" data-toggle="tab">Report Stock</a>
                </li>
                 <li>
                    <a href="##costvsrevenue" data-toggle="tab">Cost vs Revenue in 30 recent day</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="panel-current">
                    <div id="container" style="min-width: 700px; height: 400px; max-width: 600px; margin: 0 auto"> </div>
                </div>
                <div class="tab-pane" id="panel-last">
                    <div id="container1" style="min-width: 700px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <div class="tab-pane" id="reportstock">
                    <div id="container2" style="min-width: 700px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <div class="tab-pane row" id="costvsrevenue">
                    <div id="container3" style="min-width: 800px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        <!--- </div> --->
    </div>

</cfoutput>