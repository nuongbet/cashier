<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<cfif SESSION.shopId NEQ 0>
	<cflocation url="/index.cfm/admin:"/>
</cfif>
<script type="text/javascript">
	$(document).ready(function(){

		//image logo
		function readURLlogo(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $('##currentImage').attr('src', e.target.result);
		        };
		        reader.readAsDataURL(input.files[0]);
		    }
		};
		$("##logo").change(function() {
			  readURLlogo(this);
		});
		//get filename of fileupload
		$('##logo').change(function() {
	    var filename = $(this).val();
	    var lastIndex = filename.lastIndexOf("\\");
	    if (lastIndex >= 0) {
	        filename = filename.substring(lastIndex + 1);
	    }
	    $('##fileimage').val(filename);
	    readURLlogo(this);
		});

		//image logo bill
		function readURLlogobill(input) {
		    if (input.files && input.files[0]) {
		        var reader = new FileReader();
		        reader.onload = function (e) {
		            $('##currentImage1').attr('src', e.target.result);
		        };
		        reader.readAsDataURL(input.files[0]);
		    }
		};

		$("##logobill").change(function() {
			  readURLlogobill(this);
		});

		//get filename of fileupload
		$('##logobill').change(function() {
	    var filename = $(this).val();
	    var lastIndex = filename.lastIndexOf("\\");
	    if (lastIndex >= 0) {
	        filename = filename.substring(lastIndex + 1);
	    }
	    $('##fileimage1').val(filename);
	    readURLlogobill(this);
		});
	})
</script>

<div class="page-header">
  <h1>
	#getLabel("Company")#
  </h1>
</div>
<div class="col-xs-12 row">
	<div class="col-xs-7">
		<form class="form-horizontal" role="form" action="" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="fileimage" id="fileimage" value="">
			<input type="hidden" name="fileimage1" id="fileimage1" value="">
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Name")#</label>
				<div class="col-sm-10">
					<input type="text" id="name" name="name" required placeholder="#getLabel("Name")#" class="col-xs-10 col-sm-10" value="#rc.shop.name#" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Address")#</label>
				<div class="col-sm-10">
					<input type="text" id="address" name="address" required placeholder="#getLabel("Address")#" class="col-xs-10 col-sm-10" value="#rc.shop.address#" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Phone")#</label>
				<div class="col-sm-10">
					<input type="number" id="phone" name="phone" required placeholder="#getLabel("Phone")#" class="col-xs-10 col-sm-10" value="#rc.shop.phone#" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Email")#</label>
				<div class="col-sm-10">
					<input type="text" id="email" name="email" required placeholder="#getLabel("Email")#" class="col-xs-10 col-sm-10" value="#rc.shop.email#" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Tax code")#</label>
				<div class="col-sm-10">
					<input type="text" id="taxcode" name="taxcode" required placeholder="#getLabel("Tax code")#" class="col-xs-10 col-sm-10" value="#rc.shop.taxcode#" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Manager name")#</label>
				<div class="col-sm-10">
					<input type="text" id="managername" name="managername" required placeholder="Manager name" class="col-xs-10 col-sm-10" value="#rc.shop.managername#" />
				</div>
			</div>		
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Logo")#</label>
				<div class="col-sm-10">
					<input type="file" id="logo" name="logo" <cfif #rc.shop.logo# eq "">required</cfif>>
				</div>
				<div class="col-sm-10">
					<img id="currentImage" src="/images/shop/#rc.shop.logo#" width="144" height="144">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">#getLabel("Logo bill")#</label>
				<div class="col-sm-10">
					<input type="file" id="logobill" name="logobill" <cfif #rc.shop.logobill# eq "">required</cfif>>
				</div>
				<div class="col-sm-10">
					<img id="currentImage1" src="/images/shop/#rc.shop.logobill#" width="144" height="144">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"></label>
				<div class="col-sm-10">
					<button class="btn btn-info" type="submit">
						<i class="ace-icon fa fa-check bigger-110"></i>
						#getLabel("Submit")#
					</button>
				</div>
			</div>

		</form>
	</div>
	<div class="col-xs-5">
		<cfif URL.shopid GT 0>
			<a class="btn btn-primary btn-white" href="/index.cfm/admin:user.formadmin?shopid=#rc.shop.shopid#">
				<i class="ace-icon fa fa-plus"></i> #getLabel("Add User")# 
			</a>
			<p></p>	
		</cfif>
		<div class="panel panel-default">
	        <div class="panel-heading">#getLabel("User list")#</div>
	        <div class="table-responsive">
		        <table class="table" id="list_user">
		            <thead>
		              	<tr>
			                <th>#getLabel("Name")#</th>
			                <th>#getLabel("Phone")#</th>
			                <th>#getLabel("Username")#</th>
			                <th>#getLabel("Type")#</th>
			                <th>#getLabel("Active")#</th>
			                <th></th>
		              	</tr>
		            </thead>
		            <tbody>
		            	<cfloop query="#rc.user#">
			            	<tr>
			            		<td>#rc.user.firstname# #rc.user.lastname#</td>
			            		<td>#rc.user.phone#</td>
			            		<td>#rc.user.username#</td>
			            		<td>#rc.user.typeUser#</td>
			            		<td>
									<cfif #rc.user.isactive# EQ true>
										<i class="ace-icon fa fa-check-circle-o green bigger-130"></i>
										<cfelse>
										<i class="ace-icon fa fa-circle red bigger-130"></i>
									</cfif>
			            		</td>
			            		<td>						
			            			<a class="green" href="/index.cfm/admin:user.formadmin?shopid=#rc.shop.shopid#&userid=#rc.user.userId#">
										<i class="ace-icon fa fa-pencil bigger-130"></i>
									</a>
								</td>
			            	</tr>
			            </cfloop>            		
		            </tbody>
		        </table>
		    </div>
	    </div>
	</div>
</div>
</cfoutput>