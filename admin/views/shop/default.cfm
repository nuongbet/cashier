<cfoutput>
<cfscript>
	setLayout('admin');
</cfscript>
<cfif SESSION.shopId NEQ 0>
	<cflocation url="/index.cfm/admin:"/>
</cfif>
<script type="text/javascript">
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
    jQuery(function($) {
		var oTable1 = $('##list_shop')
		.dataTable( {
			bAutoWidth: false,
			"aoColumns": [null,null,null,null,null,null,{ "bSortable": false }]
	    } );
	   })
</script>
<div class="page-header">
  <h1>
	#getLabel("Company")#
  </h1>
</div>
<div class="col-xs-12">
	<a class="btn btn-primary btn-white" href="/index.cfm/admin:shop.form?shopid=0">
		<i class="ace-icon fa fa-plus"></i> 
		#getLabel("Add New")#
	</a>
		<p></p>	
	<table id="list_shop" class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>#getLabel("ID")#</th>
				<th>#getLabel("Name")#</th>
				<th>#getLabel("Address")#</th>
				<th>#getLabel("Phone")#</th>
				<th>#getLabel("Email")#</th>
				<th>#getLabel("Manager name")#</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
		<cfset i=1/>
		<cfloop query="rc.shop">
			<tr>
				<td>
					#i#
				</td>
				<td>
					<a class="blue" href="/index.cfm/admin:shop.form?shopid=#rc.shop.shopId#">
					#rc.shop.name#
				</td>
				<td>
					#rc.shop.address#
				</td>
				<td>
					#rc.shop.phone#
				</td>
				<td>
					#rc.shop.email#
				</td>
				<td>
					#rc.shop.managername#
				</td>
				<td style="text-align:center">
					<a class="green" href="/index.cfm/admin:shop.form?shopid=#rc.shop.shopId#">
						<i class="ace-icon fa fa-pencil bigger-130"></i>
					</a>
				</td>	
			</tr>
			<cfset i=i+1/>
		</cfloop>	
		</tbody>
	</table>
</div>
</cfoutput>