/**
 * methods page
 */

$(document).ready( function(){
  var $container = $('.isotope').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
    transitionDuration: '0.6s',
    getSortData: {
      name: '.name',
      symbol: '.symbol',
      number: '.number parseInt',
      category: '[data-category]',
      weight: function( itemElem ) {
        var weight = $( itemElem ).find('.weight').text();
        return parseFloat( weight.replace( /[\(\)]/g, '') );
      }
    }
  });

  var filterFns = {
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
    ium: function() {
      var name = $(this).find('.name').text();
      return name.match( /ium$/ );
    }
  };

  var filterFnsDisplay = {
    numberGreaterThan50: "function() {\n  var number = $(this).find('.number').text();\n  return parseInt( number, 10 ) > 50;\n}",
    ium: 'function() {\n  var name = $(this).find(\'.name\').text();\n  return name.match( /ium$/ );\n}'
  };

  var $codeDisplay = $('.code-display code');

  var filterValue = $(this).attr('data-filter');
  var isoFilterValue = filterFns[ filterValue ] || filterValue;
  var displayFilterValue = filterFnsDisplay[ filterValue ] || filterValue;
  $container.isotope({ filter: isoFilterValue });
  $("#hdCurrentFilter").val(filterValue);
  $("#hdCurrentPage").val(1);
  $(".page2").addClass("hidden");

  $('.filters').on( 'click', '.isotopeBtn', function() {
    $(".page2").removeClass("hidden");
    var filterValue = $(this).attr('data-filter');
    var isoFilterValue = filterFns[ filterValue ] || filterValue;
    var displayFilterValue = filterFnsDisplay[ filterValue ] || filterValue;
    $container.isotope({ filter: isoFilterValue });
    $("#hdCurrentFilter").val(filterValue);
    $("#hdCurrentPage").val(1);
    setPageNumber(filterValue);
  });

  $('#btnNext').on( 'click', function() {
    $(".page2").removeClass("hidden");
    var filterValue = ".page2";
    var isoFilterValue = filterFns[ filterValue ] || filterValue;
    var displayFilterValue = filterFnsDisplay[ filterValue ] || filterValue;
    $container.isotope({ filter: isoFilterValue });
    $("#hdCurrentFilter").val(filterValue);
    $("#hdCurrentPage").val(parseInt($("#hdCurrentPage").val())+1);
    if ($("#hdCurrentPage").val() == $("#hdLastPage").val()){
      $("#btnNext").addClass("disabled");
      $("#btnBack").removeClass("disabled");
    }
  });

  $('#btnBack').on( 'click', function() {
    var filterValue = ".page1";
    var isoFilterValue = filterFns[ filterValue ] || filterValue;
    var displayFilterValue = filterFnsDisplay[ filterValue ] || filterValue;
    $container.isotope({ filter: isoFilterValue });
    $("#hdCurrentFilter").val(filterValue);
    $("#hdCurrentPage").val(parseInt($("#hdCurrentPage").val())-1);
    if (parseInt($("#hdCurrentPage").val()) == 1){
      $("#btnBack").addClass("disabled");
      $("#btnNext").removeClass("disabled");
    } 
  });

  $.fn.radioButtonGroup = function() {
    this.each( function( i, buttonGroup ) {
      var $buttonGroup = $( buttonGroup );
      $buttonGroup.find(':checked').parent().addClass('active');
      $buttonGroup.on( 'click', 'input, button', function() {
        $buttonGroup.find('.active').removeClass('active');
        var $this = $( this );
        var $clickedButton = $this.hasClass('btn') ? $this :
          $this.parents('.btn');
        $clickedButton.addClass('active');
      });
    });
    return this;
  };
  $('.js-radio-button-group').radioButtonGroup();
});

function setPageNumber(filterValue){
  $(".element-item").attr("data-page","0");
  var itemPerPage = $("#hdItemperPage").val();
  var i = 0;
  if (filterValue == "*"){
    $(".page2").addClass("hidden");
    filterValue = ".element-item";
  }
  $("section " + filterValue).each(function(index){
    
    $(this).attr("data-page", Math.ceil((index+1)/itemPerPage));
    if ($(this).attr("data-page") > 1)
      // $(this).addClass("hidden");
    i=index+1;
  });

  $("#btnNext").removeClass("disabled");
  $("#btnBack").removeClass("disabled");
  if (i > itemPerPage){
    $("#btnNext").removeClass("disabled");
    $("#btnBack").addClass("disabled");
  } else {
    $("#btnNext").addClass("disabled");
    $("#btnBack").addClass("disabled");
  }


}

