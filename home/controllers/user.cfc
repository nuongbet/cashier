component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function profile(struct rc) {
		rc.LoadProfile = entityLoad("user",{userId:URL.id},true);
		rc.ListLanguage=entityLoad("language");
		if(CGI.REQUEST_METHOD eq "POST")
		{
			rc.LoadProfile.setfirstname(rc.firstname);
			rc.LoadProfile.setlastname(rc.lastname);
			rc.LoadProfile.setphone(rc.phone);
			rc.LoadProfile.setemail(rc.email);
			if(rc.LoadProfile.getpassword()!=rc.password){
				rc.LoadProfile.setpassword(#hash(rc.password)#);
			}
			rc.LoadProfile.setlanguageId(rc.languageId);
			SESSION.languageId=rc.languageid;
			entitySave(rc.LoadProfile);
		}
	}
}