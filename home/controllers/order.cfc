component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function updateHost(struct rc) {
		var issuccess=false;
		try {
			var desk_user = entityLoad("desk_user",{deskId=rc.desk,isactive=1},true);
			if(!isnull(desk_user)) {
				desk_user.setusername(rc.host);
				desk_user.setdeskId(lsparsenumber(rc.desk));
				desk_user.setdatecreate(now());
			}
			else {
				desk_user = entityNew('desk_user');
				desk_user.setusername(rc.host);
				desk_user.setdeskId(lsparsenumber(rc.desk));
				desk_user.setdatecreate(now());
			}
			entitySave(desk_user);
			ormflush();
			issuccess=true;
		}
		catch(type variable) {
			issuccess=false;
		}
		return variables.fw.RenderData('json',issuccess);
	}
	public any function waiter(struct rc){	
		param name = "URL.id" default = 0;
		rc.table = QueryExecute("select name from desk where deskId=:id", {id=URL.id});
		rc.ListFood = QueryExecute("select f.* from food f 
								inner join category c 
								on f.categoryId=c.categoryId
								and c.isactive=1
								and f.isactive=1
								and f.shopId=:shopId 
								and c.shopId=:shopId
								order by f.fsorted",{shopId=#SESSION.ShopId#});
		rc.ListMenuFood = QueryExecute("select categoryId, categoryname from category where isActive=1 and parentId=6 and shopId=#SESSION.shopId# order by categoryname ASC");
		rc.ListOrder = QueryExecute("select o.foodId,o.orderId,o.starttime as starttime,o.fname as name,paymentprice as price,o.quantity, orderId, note 
										from `order` o 
										where o.status=0 
										and deskId=:id 
										and o.shopId=#SESSION.ShopId#
										and o.quantity > 0
										group by o.foodId
										order by o.orderId asc
										",{id=URL.id});
		var totalprice = QueryExecute("select sum(paymentprice * quantity) as total 
										from `order` o 
										where o.deskid=:id 
										and o.shopId=#SESSION.ShopId#
										and o.status =0",{id=URL.id});
		rc.total = isnull(totalprice)?0:totalprice.total[1];

		var getBillId = QueryExecute("SELECT billId 
										FROM `order` 
										where deskId=:dId 
										and status=0 
										and shopId=#SESSION.ShopId#
										group by billId",{dId:URL.id});
		rc.billId="";
		if(getBillId.recordcount >0){
			rc.billId=getBillId.billId[1];
		} 
		else{
			rc.billId=createUUID();
		}
		rc.curr_desk_user = QueryExecute("SELECT username from desk_user where deskId=:deskId and isactive=1",{deskId=URL.id});
		rc.users = QueryExecute("SELECT firstname, lastname from user where user.shopId=:shopId and user.isactive=:isactive",{shopId=SESSION.ShopId,isactive = 1});
	}

	public any function cashier(struct rc){
		rc.table = entityLoad("desk",{deskId:URL.id},true);

		param name = "URL.id" default = 0;
		rc.ListOrder = QueryExecute("select o.orderId,o.foodId,o.fname as name,paymentprice as price
									,o.quantity,(paymentprice*o.quantity)as total, orderId, note,foc 
									from `order` o 
									where o.status=0 
									and deskId=:id 
									and o.shopId=#SESSION.ShopId#
									and o.quantity > 0
									group by o.foodId
									order by o.orderId asc
									",{id=URL.id});
		var totalprice = QueryExecute("select sum(paymentprice * quantity) as total 
										from `order` o 
										where o.deskid=:id 
										and o.shopId=#SESSION.ShopId#
										and o.status =0",{id=URL.id});
		rc.total = isnull(totalprice)?0:totalprice.total[1];
		rc.typeprice=entityLoad("type",{isActive:1,tag:'price',shopId:#SESSION.ShopId#});
		rc.typeorder=entityLoad("type",{isActive:1,tag:'order'});
		var getBillId = QueryExecute("SELECT billId 
										FROM `order` 
										where deskId=:dId 
										and status=0 
										and shopId=#SESSION.ShopId#
										group by billId",{dId:URL.id});
		rc.billId=getBillId.billId[1];
		rc.liststockneed=QueryExecute("SELECT material.*,unit.name as uname FROM material 
									inner join unit on material.unitId=unit.unitId 
									where actualstock < needstock 
									and actualstock > ministock
									and material.shopId=:shopId",{shopId=SESSION.ShopId});
		rc.liststockmini=QueryExecute("SELECT material.*,unit.name as uname FROM material 
									inner join unit on material.unitId=unit.unitId 
									where actualstock < ministock
									and material.shopId=:shopId",{shopId=SESSION.ShopId});
		rc.users = QueryExecute("SELECT firstname, lastname from user where user.shopId=:shopId and user.isactive=:isactive",{shopId=SESSION.ShopId,isactive = 1});
		rc.curr_desk_user = entityLoad("desk_user",{deskId=URL.id,isactive=1},true);
		if(CGI.REQUEST_METHOD == "POST" and rc.billId != ""){
			QueryExecute("delete from bill where billId=:bId",{bId=rc.billId});
			var bill=entityNew("bill");
			var discount=replace(rc.discount,",","","All");
			if(discount == ""){
				discount=0;
			}
			bill.setbillId(rc.billId);
			bill.settotal(replace(rc.hgettotal,",","","All"));
			bill.setdiscount(discount);
			bill.setdiscountvalue(replace(rc.discountvalue,",","","All"));
			bill.settotalvalue(replace(rc.totalTemp,",","","All"));
			bill.settypediscount(1);
			bill.settypepriceId(rc.typepriceId);
			bill.settypepriceName(rc.typepriceName);
			bill.setwaiter(rc.host);
			bill.setdatecreate(now());
			// var shift="Evening";
			// var chour=hour(now());
			// var cminute=minute(now());
			// var qgetShift=QueryExecute("SELECT name 
			// 							FROM shift WHERE :stime >= timestart 
			// 							AND :stime < timeend and shopId=:shopId limit 0,1",{stime='#chour#:#cminute#',shopId=SESSION.ShopId});
			// if(qgetShift.recordCount>0){
			// 	shift=qgetShift.name;
			// }
			// if(chour >= 7 and chour <=11){
			// 	shift="Morning";
			// }
			// else if(chour >11 and chour <= 17){
			// 	shift="Day";
			// }
			// else{
			// 	shift="Evening";
			// }
			bill.setshift("Evening");
			bill.setnote(rc.txtNote);
			bill.setuserId(SESSION.UserID);
			bill.setshopId(SESSION.ShopId);
			bill.settypeid(rc.ordertypeId);
			bill.settablename(rc.tenban);
			entitySave(bill);
			ormflush();
			//save bill to receipt
			savebilltoreceipt(SESSION.UserName,'Customer',rc.hgettotal,'Receipt from bill',rc.billId);

			//stockPayment
			stockPayment(URL.id);
			QueryExecute("update desk set status=0,isPrintedReceipt=0 where deskId=:id",{id:#URL.id#});
			rc.FoodPrice=QueryExecute("update `order` t1,(select foodprice.price as price ,foodprice.foodId as foodId  
						from `order`,foodprice 
						where `order`.foodId=foodprice.foodId 
						and status=0 and deskId=:dId and typeId=:tId 
						and `order`.shopId=#SESSION.ShopId#
						group by foodprice.foodId 
						UNION   
						select food.price as price ,food.foodId as foodId  from `order`,food 
						where `order`.foodId=food.foodId 
						and status=0 and deskId=:dId 
						and `order`.shopId=#SESSION.ShopId#
						and food.foodId not in(select foodId from foodprice where typeId=:tId) 
						group by food.foodId) t2
						set t1.paymentprice=t2.price,status=1
						where  t1.foodId=t2.foodId  and t1.deskId=:dId and t1.status=0",{dId=#URL.id#,tId=#rc.typepriceId#});
			var desk_user = entityLoad("desk_user",{username=rc.host,deskId=URL.id,isactive=1},true);
			if(!isnull(desk_user)) {
				desk_user.setisactive(0);
				desk_user.setdatecreate(now());
				entitySave(desk_user);
				ormflush();
			}
		}
	}

	private function savebilltoreceipt(required any person1,required any person2,required any amount,required any txtNote,required any billId){
		
		try {
			var receipt = EntityNew("receipt");
			receipt.setDatereceipt(lsdateFormat(now(),'yyyy-mm-dd',"pt_PT"));
			receipt.setPerson1(person1);
			receipt.setPerson2(person2);
			receipt.setDatecreate(now());
			receipt.setAmount(replace('#amount#',',','','ALL'));
			receipt.setNote(txtNote);
			receipt.setIsreceipt(1);
			receipt.setTag(1);
			receipt.setTempId(billId);
			receipt.setUserId(SESSION.UserID);
			receipt.setshopId(SESSION.shopId);
			entitySave(receipt);
			ormflush();
		}
		catch(ex) {
			dump(ex);
			abort;
		}
	}
	public  function getFoodPrice(struct rc) {
		var	bb=QueryExecute("
						select foodprice.price as price ,foodprice.foodId as foodId  
						from `order`,foodprice 
						where `order`.foodId=foodprice.foodId 
						and status=0 and deskId=:dId and typeId=:tId 
						and `order`.shopId=#SESSION.ShopId#
						group by foodprice.foodId 
						UNION   
						select food.price as price ,food.foodId as foodId  from `order`,food 
						where `order`.foodId=food.foodId 
						and status=0 and deskId=:dId 
						and `order`.shopId=#SESSION.ShopId#
						and food.foodId not in(select foodId from foodprice where typeId=:tId) 
						group by food.foodId",{dId=dId,tId=tId});
		
		variables.fw.RenderData("json",bb);
	}
	
	

	public  function getChangeTotal(struct rc) {
		var total1=QueryExecute("select sum(food.price*`order`.quantity) as total 
									from `order`,food 
									where `order`.foodId=food.foodId 
									and status=0 and deskId=:dId 
									and `order`.shopId=#SESSION.ShopId#
									and `order`.foodId not in (select foodprice.foodId from foodprice where typeId=:tId)",{dId:deskId,tId:typeId});
		var total2=QueryExecute("select sum(foodprice.price*`order`.quantity) as total 
									from `order`,foodprice 
									where `order`.foodId=foodprice.foodId 
									and status=0 
									and deskId=:dId 
									and `order`.shopId=#SESSION.ShopId#
									and typeId=:tId",{dId:deskId,tId:typeId});
		if(len(total1.total[1])==0){
			t1=0;
		}
		else{
			t1=total1.total[1];
		}
		if(len(total2.total[1])==0){
			t2=0;
		}
		else{
			t2=total2.total[1];
		}
		var total=t1+t2;
		variables.fw.RenderData("json",total);
	}
	
	
	public any function FOCChange(struct rc) {
		var currentQuantity = QueryExecute("select `order`.quantity from `order` where orderId=:OId",{OId=orderId});
		var finalQuantity = currentQuantity.quantity - foc;
		if(finalQuantity <= 0) {
			QueryExecute("delete from `order` where orderId = :OId",{OId=orderId});
		}
		else {
			QueryExecute("update `order` set quantity=quantity-:foc,foc=:foc where orderId=:OId",{foc=foc,OId=orderId});
		}
		variables.fw.RenderData("json",true);
	}
	
	

	public any function updateNote(struct rc) {
		var note = QueryExecute("update `order` set note=:txtNote where orderId=:id",{txtNote=rc.txtNote,id=rc.orderId});
		variables.fw.RenderData("json",true);
	}

	public any function insertOrder(struct rc) {
		var oId="";
		var loadOrder = QueryExecute("select count(`order`.orderId) as isorder, `order`.orderId, `order`.quantity from `order` 
		where foodId='#rc.foodId#' and billId='#rc.billId#' and status=0");
		if(loadOrder.isorder > 0) {
			QueryExecute("update `order` set quantity = #(loadOrder.quantity + quantity)# 
			where orderId=#loadOrder.orderId#");
			oId=loadOrder.orderId;
		}
		else {
			query = new query();
	        query.name = "queryResult";
	        query.setDataSource('cashier');
	        sql = "

	        insert into `order`(deskId, foodId, userId, starttime, note, billId, quantity, fname, paymentprice, shopId)
		    values(:deskId, :foodId, :userId, :starttime, :note, :billId, :quantity, :fname, :paymentprice, :shopId)

	        ";
	        query.setSQL(sql);

	        query.addParam(name="deskId", cfsqltype="CF_SQL_DOUBLE", value=deskId);
	        query.addParam(name="foodId", cfsqltype="CF_SQL_DOUBLE", value=foodId);
	        query.addParam(name="userId", cfsqltype="CF_SQL_DOUBLE", value=SESSION.UserID);
	        query.addParam(name="starttime", cfsqltype="CF_SQL_TIMESTAMP", value=now());
	        query.addParam(name="note", cfsqltype="CF_SQL_VARCHAR", value="");
	        query.addParam(name="billId", cfsqltype="CF_SQL_VARCHAR", value=billId);
	        query.addParam(name="quantity", cfsqltype="CF_SQL_DOUBLE", value=quantity);
	        query.addParam(name="fname", cfsqltype="CF_SQL_VARCHAR", value=fname);
	        query.addParam(name="paymentprice", cfsqltype="CF_SQL_DECIMAL", value=price);
	        query.addParam(name="shopId", cfsqltype="CF_SQL_VARCHAR", value=SESSION.ShopId);
	        result = query.execute();
			QueryExecute("update desk set status=1 where deskId=:id",{id:deskId});
			oId=result.getPrefix().generatedkey;
		}
		ormflush();
		variables.fw.RenderData("json",oId);
	}

	//function for down or up stock with downup: 0 for down and 1 for up.
	public  function stockDownOrUp(required any oId,required any unitId,required any estimate,required any quantity,required any foodId,required any materialId,required any downup) {
		if(oId != ""){
			var nChange=1;
			var nQuantity=(estimate/nChange)*quantity;
			var stock =entityLoad("material",{materialId:materialId,shopId:#SESSION.ShopId#},true);
			if(downup ==0){
				stock.setactualstock(stock.getactualstock()-nQuantity);
				stock.setordertemp(stock.getordertemp()-nQuantity);
			}
			else{
				stock.setactualstock(stock.getactualstock()+nQuantity);
				stock.setordertemp(stock.getordertemp()+nQuantity);
			}
			
		}
	}

	public  function stockPayment(required any deskId) {
		var orders=QueryExecute("SELECT o.* FROM `order` as o
								where o.status=0 and o.deskId=:dId and o.shopId=#SESSION.ShopId#",{dId=deskId});
		for(iOder in orders){
			var foodmaterials=QueryExecute("select * from foodmaterial as fm where fm.foodId=:fId and fm.shopId=#SESSION.ShopId#",{fId=iOder.foodId});
			for(iMaterial in foodmaterials){
				stockDownOrUp("order",iMaterial.unitId,iMaterial.estimate,iOder.quantity+iOder.foc,iOder.foodId,iMaterial.materialId,0);
			}
		}
	}
	
	

	
	public function deleteOrderById(struct rc) {
		QueryExecute("delete from `order` where orderId=:Id and status=0",{Id=orderId});
		var CountOrder = entityLoad("order",{deskId:deskId,status=0});
		if(arrayLen(CountOrder) == 0){
				QueryExecute("update desk set status=0 where deskId=:id",{id:deskId});
		}
		variables.fw.RenderData("json",true);

	}

	public any function printbill(struct rc) {
		rc.nameTable = URL.tenban;
		rc.host = URL.host;
		// rc.shop = QueryExecute("select name, address, phone, email, taxcode, logobill from shop where shopId=:shopid",{shopid:SESSION.shopid});
		rc.printbill = QueryExecute("
			SELECT o.billId, o.quantity, o.fname as nameFood, o.paymentprice as priceFood, 
					bill.total,bill.discount,bill.typediscount,bill.datecreate
			from `order` o
			left outer join bill on o.billId = bill.billId
			where o.billId = :billId
			group by o.foodId
		",{billId:URL.billId});
	}

	// public any function printbill(struct rc) {
	// 	rc.shop = QueryExecute("select name, address, phone, email, taxcode, logobill from shop where shopId=:shopid",{shopid:SESSION.shopid});
	// 	rc.printbill = QueryExecute("
	// 		SELECT o.billId, o.quantity, desk.name as nameTable, o.fname as nameFood, o.paymentprice as priceFood, user.firstname, user.lastname,bill.total,bill.discount,bill.typediscount,bill.datecreate
	// 		from `order` o, desk, user,bill
	// 		where o.deskId = desk.deskId 
	// 			and o.userId = user.userId
	// 			and o.billId =:billId
	// 			and o.billId= bill.billId
	// 			and o.quantity > 0
	// 		group by o.foodId
	// 		union all
	// 		SELECT o.billId, o.foc as quantity, desk.name as nameTable, o.fname as nameFood, 0 as priceFood, user.firstname, user.lastname,0 as total,bill.discount,bill.typediscount,bill.datecreate
	// 		from `order` o, desk, user,bill
	// 		where o.deskId = desk.deskId 
	// 			and o.userId = user.userId
	// 			and o.billId =:billId
	// 			and o.billId= bill.billId
	// 			and o.foc >0
	// 		group by o.foodId
	// 	",{billId:URL.billId});
	// }


	public  function billTemp(struct rc) {
		var rs = true;
		var discountT=replace(discount,",","","All");
		if(discountT == ""){
			discountT=0;
		}
		try {
			QueryExecute("delete from bill where billId=:bId",{bId=billId});
			QueryExecute("insert into bill(billId, total, discount, typediscount, typepriceId, datecreate, userId)
						  values(:billId, :total, :discount, :typediscount, :typepriceId, :datecreate, :userId)",
						  {billId=billId, total=replace(hgettotal,",","","All"), discount=discountT, typediscount=1, typepriceId=typepriceId, datecreate=lsdateFormat(now(),"yyyy-mm-dd","pt_PT"), userId=SESSION.UserID})
			QueryExecute("update desk set isPrintedReceipt=1 where deskId=:id",{id:#rc.deskId#});
			rc.FoodPrice=QueryExecute("update `order` t1,(select foodprice.price as price ,foodprice.foodId as foodId  
						from `order`,foodprice 
						where `order`.foodId=foodprice.foodId 
						and status=0 and deskId=:dId and typeId=:tId 
						and `order`.shopId=#SESSION.ShopId#
						group by foodprice.foodId 
						UNION   
						select food.price as price ,food.foodId as foodId  from `order`,food 
						where `order`.foodId=food.foodId 
						and status=0 and deskId=:dId 
						and `order`.shopId=#SESSION.ShopId#
						and food.foodId not in(select foodId from foodprice where typeId=:tId) 
						group by food.foodId) t2
						set t1.paymentprice=t2.price
						where  t1.foodId=t2.foodId  and t1.deskId=:dId and t1.status=0",{dId=deskId,tId=typepriceId});
		}
		catch(type variable) {
			rs = false;
		}
		variables.fw.RenderData("json",rs);

	}
	
	
	

	public any function upQuantityJson(struct rc) {
		var order = entityLoad("order",{orderId:rc.orderId},true);
		order.setquantity(order.getquantity()+1);
		variables.fw.RenderData("json",true);
	}

	public any function downQuantityJson(struct rc) {
		var order = entityLoad("order",{orderId:rc.orderId},true);
		var total = order.getquantity()-1;
		if(total <= 0) {
			QueryExecute("delete from `order` where orderId = :OId",{OId=rc.orderId});
		}
		else {
			order.setquantity(total);
		}
		variables.fw.RenderData("json",true);
	}


	public any function ChangeQuantityJson(struct rc) {
		var order = entityLoad("order",{orderId:rc.orderId},true);
		var oldQuantity=order.getquantity();
		if(oldQuantity != quantity and quantity > 0){
			order.setquantity(quantity);
		}
		else {
			QueryExecute("delete from `order` where orderId = :OId",{OId=rc.orderId});
		}
	
		variables.fw.RenderData("json",true);
	}

	public array function queryToArray( required query qry ) {
	    var columns = arguments.qry.getColumnNames();
	    var ofTheJedi = [];

	    for( var i = 1; i LTE qry.recordCount; i++ ) {
	        var obj = {};

	        for( var k = 1; k LTE arrayLen( columns ); k++ ) {
	            structInsert( obj, columns[ k ], arguments.qry[ columns[ k ] ][ i ] );
	        }

	        arrayAppend( ofTheJedi, obj );
	    }

	    return ofTheJedi;
	}

	public  function getFoodByFoodTypeId(struct rc) {
		// var food="";
		// if(typeId =="All"){
		// 	food=entityLoad("food",{isActive:1,shopId:#SESSION.ShopId#});
		// }
		// else{
		// 	food=entityLoad("food",{isActive:1,categoryId:typeId,shopId:#SESSION.ShopId#});
		// }
		// variables.fw.RenderData("json",food);

		var food="";
		if(typeId =="All"){
			food=QueryExecute("select f.* from food f 
								inner join category c 
								on f.categoryId=c.categoryId
								and c.isactive=1
								and f.isactive=1
								and f.shopId=:shopId 
								and c.shopId=:shopId
								order by f.fsorted",{shopId=#SESSION.ShopId#});
		}
		else{
			food=QueryExecute("select f.* from food f 
								inner join category c 
								on f.categoryId=c.categoryId
								and c.isactive=1
								and f.isactive=1
								and f.shopId=:shopId 
								and c.shopId=:shopId
								and f.categoryId=:typeId
								order by f.fsorted",{shopId=#SESSION.ShopId#,typeId=typeId});
		}
		variables.fw.RenderData("json",queryToArray(food));
	}
	
	


	public  function getFoodByFSearch(struct rc) {
		var food=entityLoad("food",{fcode:fcode,isActive:1,shopId:#SESSION.ShopId#},true);
		var oId="";
		if(!isnull(food))
		{
			var foodId=food.getfoodId();
			var loadOrder = entityLoad("order",{foodId:foodId,billId:rc.billId},true);
			if (isnull(loadOrder))
			{
				var order = entityNew("order");
				order.setdeskId(deskId);
				order.setfoodId(foodId);
				order.setuserId(SESSION.UserID);
				order.setstarttime(now());
				order.setnote("");
				order.setbillId(rc.billId);
				order.setquantity(quantity);
				order.setfname(food.getname());
				order.setpaymentprice(food.getprice());
				order.setshopId(SESSION.ShopId);
				entitySave(order);
				oId=order.getorderId()&";;"&food.getprice()&";;"&food.getname()&";;"&foodId;
				QueryExecute("update desk set status=1 where deskId=:id",{id:deskId});
				
			}
			else 
			{
				loadOrder.setquantity(loadOrder.getquantity() + quantity );
				oId=loadOrder.getorderId()&";;"&food.getprice()&";;"&food.getname()&";;"&foodId;
			}
		}
		variables.fw.RenderData("json",oId);
	}
	
	public any function getfood(struct rc) {
		var food=entityLoad("food",{keycode:keycode,isActive:1,shopId:#SESSION.ShopId#},true);
		variables.fw.RenderData("json",food);
		
	}
	
}