component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public any function default(struct rc){
		rc.deskPos = QueryExecute("select *, d.sorted as dsorted from category as c 
									join desk as d 
									where c.categoryId=d.categoryId 
									and c.shopId=#SESSION.ShopId#
									and d.isActive=1
									and c.isactive=1
									order by categoryname asc");
		rc.ListDeskActive=QueryExecute("select c.categoryname, c.categoryId, d.name, d.deskId 
										from category as c 
										join desk as d 
										where c.categoryId=d.categoryId 
										and d.status=0 
										and d.isactive=1
										and c.isactive=1 
										and d.shopId=#SESSION.ShopId#
										order by c.sorted asc");
		rc.ListDeskInActive=QueryExecute("select c.categoryname, c.categoryId, d.name, d.deskId 
											from category as c 
											join desk as d 
											where c.categoryId=d.categoryId 
											and d.status=1 
											and d.isactive=1 
											and c.isactive=1 
											and d.shopId=#SESSION.ShopId#
											order by c.sorted asc");
		if(CGI.REQUEST_METHOD == "POST"){
			rc.check=false;
			transaction{
			try{
				QueryExecute("update cashier.order set deskId=:toDId where deskId=:fromDId and status=0",{toDId=rc.toDeskId,fromDId=rc.fromDeskId});
				QueryExecute("update desk set status=0 where deskId=:fromDId",{fromDId=rc.fromDeskId});
				QueryExecute("update desk set status=1 where deskId=:toDId",{toDId=rc.toDeskId});
				QueryExecute("update desk_user set deskId=:toDId where deskId=:fromDId and isactive=1",{toDId=rc.toDeskId,fromDId=rc.fromDeskId});
				rc.check=true;
				transactionCommit();
				variables.fw.redirect("main");
			}
			catch(any e) {
				transactionRollback();
				rc.check=false;
			}
		}

		}
	}

	public any function defaultOld(struct rc){
		rc.deskPos = QueryExecute("select *, d.sorted as dsorted from category as c 
									join desk as d 
									where c.categoryId=d.categoryId 
									and c.shopId=#SESSION.ShopId#
									and d.isActive=1
									and c.isactive=1
									order by c.sorted, d.sorted asc");
		rc.ListDeskActive=QueryExecute("select c.categoryname, c.categoryId, d.name, d.deskId 
										from category as c 
										join desk as d 
										where c.categoryId=d.categoryId 
										and d.status=0 
										and d.isactive=1
										and c.isactive=1 
										and d.shopId=#SESSION.ShopId#
										order by c.sorted asc");
		rc.ListDeskInActive=QueryExecute("select c.categoryname, c.categoryId, d.name, d.deskId 
											from category as c 
											join desk as d 
											where c.categoryId=d.categoryId 
											and d.status=1 
											and d.isactive=1 
											and c.isactive=1 
											and d.shopId=#SESSION.ShopId#
											order by c.sorted asc");
		if(CGI.REQUEST_METHOD == "POST"){
			rc.check=false;
			transaction{
			try{
				QueryExecute("update cashier.order set deskId=:toDId where deskId=:fromDId and status=0",{toDId=rc.toDeskId,fromDId=rc.fromDeskId});
				QueryExecute("update desk set status=0 where deskId=:fromDId",{fromDId=rc.fromDeskId});
				QueryExecute("update desk set status=1 where deskId=:toDId",{toDId=rc.toDeskId});
				rc.check=true;
				transactionCommit();
				variables.fw.redirect("main");
			}
			catch(any e) {
				transactionRollback();
				rc.check=false;
			}
		}

		}
	}
	
	public any function getAllDesk(struct rc){
		var bb = entityLoad("desk",{isActive=1,shopId=#SESSION.ShopId#},'status');
		return  variables.fw.RenderData("json",bb);
	}

	public any function getDeskByPos(struct rc){
		var bb = entityLoad("desk",{isActive=1,pos=rc.pos,shopId=#SESSION.ShopId#},'status');
		return  variables.fw.RenderData("json",bb);
	}

	public any function movelefttable(struct rc) {
		var csorted = QueryExecute("update desk set sorted=:csorted where deskId=:cdeskid", {csorted=psorted, cdeskid=cdeskid});
		var psorted = QueryExecute("update desk set sorted=:psorted where deskId=:pdeskid", {psorted=csorted, pdeskid=pdeskid});
		variables.fw.renderData("JSON", true);
	}
	
	public any function moverighttable(struct rc) {
		var csorted = QueryExecute("update desk set sorted=:csorted where deskId=:cdeskid", {csorted=nsorted, cdeskid=cdeskid});
		var nsorted = QueryExecute("update desk set sorted=:nsorted where deskId=:ndeskid", {nsorted=csorted, ndeskid=ndeskid});
		variables.fw.renderData("JSON", true);
	}	


}