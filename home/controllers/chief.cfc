component  output="false" displayname=""  {

	public function init(required any fw){
		variables.fw =arguments.fw;
		return this;
	}

	public function default (struct rc) {

		rc.lstbbq = QueryExecute("select GROUP_CONCAT(orderId) as orderId, sum(a.quantity) as quantity, fname, GROUP_CONCAT(tname) as tname, starttime
									from  (
									select cashier.order.orderId, cashier.order.quantity, cashier.order.fname, desk.name as tname, cashier.order.isDone,cashier.order.starttime
									from cashier.order
									inner join food on cashier.order.foodId = food.foodId
									inner join category on food.categoryId = category.categoryId
									inner join desk on cashier.order.deskId = desk.deskId
									where cashier.order.status = 0 and cashier.order.isDone = 0 and cashier.order.foodId not in (select food.foodId from food where food.categoryId in (56,57,71,77) or foodId in (317,246))
									group by cashier.order.fname, cashier.order.deskId
									) as a
									group by fname
									order by starttime");


		rc.lsthotpot = QueryExecute("select GROUP_CONCAT(orderId) as orderId, sum(a.quantity) as quantity, fname, GROUP_CONCAT(tname) as tname, starttime
									from  (
									select cashier.order.orderId, cashier.order.quantity, cashier.order.fname, desk.name as tname, cashier.order.isDone,cashier.order.starttime
									from cashier.order
									inner join food on cashier.order.foodId = food.foodId
									inner join category on food.categoryId = category.categoryId
									inner join desk on cashier.order.deskId = desk.deskId
									where cashier.order.status = 0 and cashier.order.isDone = 0 and category.categoryId = 71
									group by cashier.order.fname, cashier.order.deskId
									) as a
									group by fname
									order by starttime");
	}

	public function setDoneFoodOrdered(struct rc) {
		try {
			if(findNoCase(",", rc.orderId, 1)  > 0) {
				QueryExecute("update cashier.order set isDone=1 where cashier.order.orderId in (#rc.orderId#)");
			}
			else {
				QueryExecute("update cashier.order set isDone=1 where cashier.order.orderId = :id", {id=rc.orderId});
			}
			variables.fw.RenderData('json', true);
		}
		catch(type variable) {
			variables.fw.RenderData('json', false);
		}
		
	}
}