component output="false" displayname=""  {

	public function init(required any fw){
		variables.fw = arguments.fw;
		return this;
	}

	public any function stockneed(struct rc) {
		
		rc.liststockneed=QueryExecute("SELECT material.*,unit.name as uname FROM material 
									inner join unit on material.unitId=unit.unitId 
									where actualstock < needstock 
									and actualstock > ministock
									and isactive=1
									and material.shopId=:shopId",{shopId=SESSION.ShopId});
	}
	public any function stockmini(struct rc) {
		
		rc.liststockmini=QueryExecute("SELECT material.*,unit.name as uname FROM material 
									inner join unit on material.unitId=unit.unitId 
									where actualstock < ministock
									and isactive=1
									and material.shopId=:shopId",{shopId=SESSION.ShopId});
	}
	
}