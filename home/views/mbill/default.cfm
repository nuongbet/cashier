<cfoutput>
<script type="text/javascript">
	function checkDelete() {
	    return confirm('#getLabel("Are you sure you want delete it?")#');
	}
    jQuery(function($) {
    	 $('.date-picker').datepicker({
			autoclose: true,
			todayHighlight: true
		});

		var oTable1 = $('##list_bill')
		.dataTable( {
			"bAutoWidth": false,
			"aoColumns": [null,null,null,null,null,null,{ "bSortable": false }]
	    } );
	   });

function search(){
	var fdate=$('##fdate').val();
	var tdate=$('##tdate').val();
	var userid=$('##userId').val();
	location.href="/index.cfm/home:mbill?fdate="+fdate+"&tdate="+tdate+"&userId="+userid;
}
</script>
<div class="page-header">
  <h1>
	#getLabel("Invoice")#
  </h1>
</div>
<div class="col-md-12 form-inline">
		<input class="form-control date-picker" name="fdate" id="fdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.fdate,"dd-mm-yyyy","pt_PT")#" placeholder="From date" />
		<input class="form-control date-picker" name="tdate" id="tdate" type="text" data-date-format="dd-mm-yyyy" value="#lsdateFormat(URL.tdate,"dd-mm-yyyy","pt_PT")#" placeholder="To date" />
		<select name="userId" id="userId" class="form-control" >
			<cfif #URL.userId# eq 'All'>
					<cfset selected="selected"/>
					<cfelse>
						<cfset selected=""/>
					</cfif>
			<option 		  	
					value="All" #selected#>All
					</option>
			<cfloop query="#rc.ListUser#">
				<cfif #URL.userId# eq #rc.ListUser.userId#>
					<cfset selected="selected"/>
					<cfelse>
						<cfset selected=""/>
					</cfif>
					<option 		  	
					value="#rc.ListUser.userId#" #selected#>#rc.ListUser.username#
					</option>
			</cfloop>
		</select>
		<p class="btn btn-primary btn-white" onclick='search();'>
		<i class="ace-icon fa fa-search"></i> 
		Search
	</p>
</div>
<div class="col-xs-12">
		<strong style="font-size:18px;">
			Amount: <cfif !rc.amount.recordcount>
					0
				<cfelse>
					#numberformat(rc.amount.amount)#
				</cfif> #currency#
		</strong>
		<table id="list_bill" class="table table-striped table-bordered table-hover">
			<thead>
				<tr>
					<th>#getLabel("ID")#</th>
					<th>#getLabel("Total")#</th>
					<th>#getLabel("Shift")#</th>
					<th>#getLabel("Order Type")#</th>
					<th>#getLabel("Staff")#</th>
					<th>#getLabel("Date")#</th>
					<th></th>
				</tr>
			</thead>

			<tbody>
			<cfset id=1/>
			<cfloop query="#rc.ListBill#">
				<tr>
					<td>
						#id#
					</td>
					<td>
						<a class="blue" href="/index.cfm/home:mbill.detail?id=#rc.ListBill.billId#">
						#NumberFormat(rc.ListBill.total)#
					</td>
					<td>
						#rc.ListBill.shift#
					</td>
					<td>
						#rc.ListBill.ordername#
					</td>
					<td>
						#rc.ListBill.staff#
					</td>
					<td>
						#timeformat(rc.ListBill.datecreate,"hh:mm")# #lsdateFormat(rc.ListBill.datecreate,"dd/mm/yyyy","pt_PT")#
					</td>
					<td style="text-align:center">
						<div class="hidden-sm hidden-xs action-buttons">
							<a class="green" href="/index.cfm/home:mbill.detail?id=#rc.ListBill.billId#">
								<i class="ace-icon fa fa-pencil bigger-130"></i>
							</a>

							<!--- <a class="red" href="/index.cfm/home:mbill.delete?id=#rc.ListBill.billId#" onclick="return checkDelete()">
								<i class="ace-icon fa fa-trash-o bigger-130"></i>
							</a> --->
						</div>
					</td>	
				</tr>
				<cfset id += 1/>
			</cfloop>	
			</tbody>
		</table>
	</div>
</cfoutput>