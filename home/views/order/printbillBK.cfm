<style type="text/css">
.main-content {
	padding-top: 0px;
}
.textprice {
	font-size:9px;
}
</style>
<cfoutput>
<div class="row" style="font-size:9px; padding:20px; padding-top:0px; font-weight:bold; margin-right:0px;">
	<div class="col-xs-4 col-md-4" style="text-align: center;">
		<img src="/images/shop/#rc.shop.logobill#" class="img-responsive" width="50px">
	</div>
	<div class="col-xs-8 col-md-8" style="text-align:center; text-transform: uppercase; font-size: 16px; font-weight: bold; padding:0px; padding-left:5px;">
		<p>#getLabel("nuongbet")#</p>
	</div>
	<div class="col-xs-12 col-md-12 titlePrintBill">
		<h2 style="margin: 0px; font-size: 15px;">#getLabel("INVOICE")#</h2>
		<span>#getLabel("Date")#:</span> 
		<span>#dateTimeFormat(rc.printbill.datecreate,"dd/mm/yyyy - HH:NN:ss")#</span>
		<p></p>
	</div>
	<br>
	<div class="col-xs-6 col-md-6">
		<span>#getLabel("Staff")#: </span>
		<span>#SESSION.UserName#</span>
	</div>
	<div class="col-xs-6 col-md-6 alignright">
		<span>#getLabel("Tables")#: </span>
		<span>#rc.printbill.nameTable#</span>
	</div>
	<div class="col-md-12 col-xs-12" style="font-weight:bold;">
		<table class="printBill">
			<tr class="lineDashed">
				<th style="width: 50%">#getLabel("Name")#</th>
				<th class="alignright" style="width: 20%;">Sl</th>
				<th class="alignright" style="width: 30%;">TT</th>
			</tr>
			<cfset local.total=0 />
			<cfloop query="#rc.printbill#">
				<cfset total += rc.printbill.priceFood * rc.printbill.quantity/>
			<tr class="lineDashed">
				<td style="width:50%">
					#rc.printbill.nameFood#
					<br>
					#numberFormat(rc.printbill.priceFood)# #currency#
				</td>
				<td class="alignright" style="width:20%">
					#rc.printbill.quantity#
				</td>
				<td class="alignright" style="width:30%">
					#numberFormat(rc.printbill.priceFood * rc.printbill.quantity)#
				</td>
			</tr>
			</cfloop>
			<cfset discoun = 0/>
			<cfset discoun = rc.printbill.discount ?: 0/>
			<tr style="font-size:11px; font-weight:bold;">
				<td colspan="3" class="totalPrintBill">
					<div class="row">
						<div class="col-xs-12 col-md-12 alignright">
							<div class="col-xs-6 textprice">#getLabel("Total")#: </div>
							<div class="col-xs-6">#numberFormat(rc.printbill.total + discoun)#</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 alignright">
							<div class="col-xs-6 textprice">#getLabel("Discount")#: </div>
							<div class="col-xs-6">
								#numberFormat(discoun)#
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 alignright">
							<div class="col-xs-6 textprice">#getLabel("Payment")#: </div>
							<cfset tot = rc.printbill.total ?: 0/>
							<div class="col-xs-6">#numberFormat(tot)#</div>
						</div>
					</div>
				</td>
			</tr>	
		</table>	
	</div>

	<div class="col-md-12 col-xs-12" style="font-weight:bold; font-size: 11px; text-transform: uppercase; margin-top: 15px; text-align: center;">
		#getLabel("thanks")#
	</div>
	<div class="col-md-12 col-xs-12" style="font-weight:bold; font-size: 8px;">
		#getLabel("Address")#: #rc.shop.address#
	</div>
	<div class="col-md-12 col-xs-12" style="font-weight:bold; font-size: 8px;">
		Facebook: www.facebook.com/nuongbet
	</div>
	<div class="col-md-12 col-xs-12" style="font-weight:bold; font-size: 8px;">
		#getLabel("Hotline")#: #rc.shop.phone#
	</div>
</div>

</cfoutput>

