﻿<cfset itemPerPage = 24>

<cfoutput>
<style type="text/css">
  @media screen and (min-width: 1012px){
    .btn-container{
      margin-top: -12px !important;
    }
  }
</style>
<script type="text/javascript">
  function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
  function isNumberKey(evt){
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}

  function oquantityChange(i){
    $('##OrderQuantity').val(i);
    $('.delColor').removeClass('btnSelect').addClass('btnUnSelect');
     $('##oq'+i).removeClass('btnUnSelect').addClass('btnSelect');
  }

  function isBlank(str) {
    return (!str || /^\s*$/.test(str));
  }

  function addfood_enter (event) {
      if (event.which == 13 || event.keyCode == 13) {
          fsearch();
          return false;
      }
      return true;
  };

  function fsearch(){

    var f=$('##fsearch').val();
    var fcode=f.split("*")[1];
    var quantity=parseInt(f.split("*")[0]);

    if(isBlank(fcode)) {
      fcode = quantity;
      quantity = 1;
    } 

    $.ajax({
      type: 'POST',
      url: '#getConTextRoot()#/index.cfm/home:order.getFoodByFSearch/',
      data: {'fcode':fcode,'deskId':#URL.id#,'quantity':quantity,'billId':'#rc.billId#'},
      dataType: 'JSON',
      success: function(data) {
        if(data != ""){
          var price=parseFloat(data.split(";;")[1]);
          var fname=data.split(";;")[2];
          var orderId=data.split(";;")[0];
          var foodId=data.split(";;")[3];
          AppendRow(foodId,fname,price,quantity,orderId);
          if(quantity != 1) {
            $('##fsearch').val('');
          }
          $('##fsearch').focus();
        }
        else{
          alert("#getLabel("Food is not exists!")#");
        }
      }
    }); 
  }

  function changeHost() {
    var host = $('##host').val();
    $.ajax({
        type: 'POST',
        url: '#getConTextRoot()#/index.cfm/home:order.updateHost/',
        data: {'host':host, desk: #URL.id#},
        dataType: 'JSON',
        success: function(data) {
          if(data==true) {
            alert('Người phụ trách bàn đã được cập nhật !');
          }
          else {
            alert('Người phụ trách bàn chưa được cập nhật !');
          }
        }
    });
  }

  function delay() {
    setTimeout(function () {
      $('.iconFood').css('pointer-events', 'all');
      $('.iconFood').css('opacity', '1');
    }, 100);
  }


  function AddRow(foodId,name,price)
  {
    // $('.foodId'+foodId).animate({'background-color': '##d15b47','border-color':'##d15b47'},"slow");
    name=unescape(name);
    var quantity=parseInt($('##OrderQuantity').val());
    $('.iconFood').css('pointer-events', 'none');
    $('.iconFood').css('opacity', '0.5');

    $.ajax({
      type: 'POST',
      url: '#rootLink#/index.cfm/home:order.insertOrder/',
      data: {'foodId':foodId,'deskId':#URL.id#,'billId':'#rc.billId#','quantity':quantity,'fname':name,'price':price,'username':$('##host').val()},
      dataType: 'JSON',
     success: function(data) {
        AppendRow(foodId,name,price,quantity,data);
        delay();
      },
      error: function(){
        alert("Order [" + name + "]" + ' not success. Please try again.');
        delay('foodId' + foodId);
      }
    }); 
  }

  function AppendRow(foodId,fname,price,quantity,orderId){
      // $('.foodId'+foodId).animate({'background-color': '##438eb9', 'border-color':'##438eb9'},"slow");
      //update total
      var totalprice=$('##total').val().replace(/,/g , "");
      $('##total').val(commaSeparateNumber(+totalprice+(quantity*price)));
      
      var vstop=false;

      $(".fId").each(function(){
        if($(this).val() == foodId){
          //append add quantity when foodId exists.
          $('##quantity'+foodId).val(+ $('##quantity'+foodId).val()+quantity);
          vstop=true;
          return false;
        }
      });

      if(vstop == false){
         var no=$('##list_order tr').length;
         var priceComma=commaSeparateNumber(price);
        $('##list_order tbody').append('<tr id="tr'+orderId+'"><td id="stt" class="visible-lg">'+no+'</td><input type="hidden" name="fId" id="fId" class="fId" value="'+foodId+'"><td>'+fname+'</td><td name="quantity'+foodId+'"><button class="btn btnQuantity" onclick="return downQuantity('+orderId+','+price+');">-</button> <input id="quantity'+foodId+'" class="inputQuantity inputQuantity'+orderId+'" type="text" min="1" value="'+quantity+'" onfocus="this.oldvalue = this.value;" onchange="return quantityChange(this,'+orderId+','+price+');this.oldvalue = this.value;" onkeypress="return isNumberKey(event)"> <button class=" btn btnQuantity" onclick="return upQuantity('+orderId+','+price+');">+</button></td><td class="visible-lg">'+priceComma+'</td><cfif SESSION.UserType neq 3><td><button type="button" class="btn-danger" onclick="return RemoveRow('+orderId+','+quantity+','+price+');">Remove</button></td><td></td></cfif></tr>');
      }
  }

  function RemoveRow(id,quantity,price){
      $.ajax({
        type: 'POST',
        url: '#getConTextRoot()#/index.cfm/home:order.deleteOrderById/',
        data: {'orderId':id,'deskId':#URL.id#},
        dataType: 'JSON',
        success: function(data) {
          var ctotal=parseFloat($('##total').val().replace(/,/g , ""));
          var pricetotal=parseInt(quantity)*parseFloat(price);
          $('##total').val(commaSeparateNumber(ctotal-pricetotal));
          $('##tr'+id).remove();
          $('##list_order').find("td##stt").each(function(index){
              $(this).html(index+1);
          });
            
        }
      }); 
    }

  function completefood(orderId,btn) {

    $(btn).remove();
  }

  function showNoteModal(i,txt) {
    $('##noteModal').modal('show');
    $('##orderId').val(i);
    $('##txtNote').val(txt);
  }
  function quantityChange(txt,i,price){
    var noQuantity=$('.inputQuantity'+i).val();
    var addedQuantity = noQuantity - txt.oldvalue;
    $.ajax({
      type: 'POST',
      url: '/index.cfm/home:order.ChangeQuantityJson/',
      data: {'orderId':i,'quantity':noQuantity},
      dataType: 'JSON',
      success: function(data) {
        if(noQuantity <= 0) {
          $('.inputQuantity'+i).parent().parent().remove();
        }
        //update total
        var totalprice=$('##total').val().replace(/,/g , "");
        var oldprice=txt.oldvalue*price;
        $('##total').val(commaSeparateNumber(+totalprice-oldprice+(noQuantity*price)));
        var j = 1;
      }
    }); 
  }

  function downQuantity(i,price,orderId){
    $(this).css('pointer-events', 'none');
    $.ajax({
      type: 'POST',
      url: '/index.cfm/home:order.downQuantityJson/',
      data: {'orderId':i},
      dataType: 'JSON',
      success: function(data) {
        var quantity = $('.inputQuantity'+i).val();
        var down = quantity - 1;
        $('.inputQuantity'+i).val(down);
        if(down <= 0) {
          $('.inputQuantity'+i).parent().parent().remove();
        }
        //update total
        var totalprice=$('##total').val().replace(/,/g , "");
        $('##total').val(commaSeparateNumber(+totalprice-price));
        $(this).css('pointer-events', 'all');
      }
    }); 

  }

  function upQuantity(i,price,orderId){
    $(this).css('pointer-events', 'none');
    $.ajax({
      type: 'POST',
      url: '/index.cfm/home:order.upQuantityJson/',
      data: {'orderId':i},
      dataType: 'JSON',
      success: function(data) {
        var quantity = $('.inputQuantity'+i).val();
        var up = +quantity+ + 1;
        $('.inputQuantity'+i).val(up);
        var quantity = $('.inputQuantity'+i).val();
        //update total
        var totalprice=$('##total').val().replace(/,/g , "");
        $('##total').val(commaSeparateNumber(+totalprice+price));
        $(this).css('pointer-events', 'all');
      }
    }); 
  }

  //type food change
  function typeFoodChange() {
    var typeId = $( "select##typefoodId" ).val();
    $.ajax({
      type: 'POST',
      url: '/index.cfm/home:order.getFoodByFoodTypeId/',
      data: {'typeId':typeId},
      dataType: 'JSON',
      success: function(data) {
        var i = 1;
        var iDataPage = 1;
        var iLastPage = Math.ceil(data.length/#itemPerPage#);

        $("##hdCurrentPage").val(1);
        $("##hdLastPage").val(iLastPage);

        if (data.length < #itemPerPage#) {
          $("##btnBack").addClass("disabled");
          $("##btnNext").addClass("disabled");
        }
        else {
          $("##btnNext").removeClass("disabled");
        }

        $('##foodchange').empty();
        $('##foodchange').append('<br>');

       $.each(data,function(index,obj){
          var sStyle = "";
          if (data.length > #itemPerPage#){
            iDataPage = Math.ceil(i/#itemPerPage#);
            if( iDataPage > 1){
              sStyle = "style='display:none'";
            }
          }

          $('##foodchange').append('<div class="col-xs-6 col-sm-3 col-md-2 ifood" data-page="' + iDataPage + '" '+ sStyle + '><div class="iconFood foodId'+obj.foodId+'" onclick="return AddRow('+obj.foodId+','+'\''+escape(obj.name)+'\''+','+obj.price+')" style="background:'+(isBlank(obj.color) ? '##438eb9' :  obj.color)+';font-size: 15px; font-weight: bolder;">'+obj.name+'</div></div>');
           i++;
       });   
    }
    });
  }

  $(document).ready(function(){

    $( "##fsearch" ).focus();


    $('##oq'+1).removeClass('btnUnSelect').addClass('btnSelect');
    $('##noteModal').on('hidden.bs.modal', function (e) {
      var orderId = $('##orderId').val();
      var txtNote = $('##txtNote').val();
      $.ajax({
          type: 'POST',
          url: '#getConTextRoot()#/index.cfm/home:order.updateNote/',
          data: {'orderId':orderId,'txtNote':txtNote},
          dataType: 'JSON',
          success: function(data) {
            $('##note'+orderId).html(txtNote);
          }
      }); 
    });


    $('body').on('keydown', function (e) {
      if(e.keyCode == 123) {
        e.preventDefault();
        location.href="/index.cfm/order.cashier?id=#URL.id#";
      } else {
        $.ajax({
        type: 'POST',
        url: '/index.cfm/home:order.getfood/',
        data: {'keycode':e.keyCode},
        dataType: 'JSON',
        success: function(data) {
          AddRow(data.foodId, escape(data.name) , data.price);
        }
        });
      }

      if(! $('##fsearch').is(":focus") && ! $(".inputQuantity").is(":focus")){
      var key1=e.keyCode || e.which;
      var key=String.fromCharCode((96 <= key1 && key1 <= 105)? key1-48 : key1);
      if(parseInt(key)>=1 && parseInt(key)<=9){
        oquantityChange(key);
      }
     }
    });

  });
</script>

<div class="container-fluid">
  <div class="page-header row">
    <div class="col-md-5 col-sm-6 col-xs-12">
      <h1>
        #rc.table.name#
        <a class="btn btn-primary btn-white" href="#rootLink#/index.cfm/main" accesskey="t">
          <i class="ace-icon fa fa-angle-double-left"></i>
          #getLabel("Select Table")#
        </a>
        <cfif #SESSION.UserType# EQ 2 OR #SESSION.UserType# EQ 1>
          <a class="btn btn-primary btn-white" href="#rootLink#/index.cfm/order.cashier?id=#URL.id#" accesskey="b">
            <i class="ace-icon fa fa-keyboard-o"></i>
            #getLabel("Bill")#
          </a>
        </cfif>
      </h1>
    </div>
    <div class="col-md-7 col-sm-6 col-xs-12">
      <input type="hidden" id="OrderQuantity" value="1"> 
      <cfloop from="1" to="10" step="1" index="i">  
        <div class="col-xs-2 col-sm-2 col-md-1">
          <button type="button" class="btn-group btnSelectQuantity delColor col-xs-12 col-md-12" id="oq#i#" onclick="return oquantityChange(#i#)">
                #i#
          </button>
        </div>
      </cfloop>
    </div>
  </div>
  <div class="row">
    <div class="col-md-5">
      <div class="row">
      
        <div class="col-md-12">
           <!---  <label class="col-xs-4 col-md-2 control-label">#getLabel("Add item")#:</label>
            <input type="text" class="col-xs-8 col-md-10" id="fsearch" name="fsearch" placeholder="Quantity*FoodID" value="" inputmode="numeric"  onkeypress="return addfood_enter(event);"> ---> 
            <label class="col-xs-5 col-md-2 control-label no-padding-right">#getLabel("Total")#</label>
            <input type="text" id="total" name="total" class="col-xs-7 col-md-6" readonly value="#NumberFormat(rc.total)#"/>
        </div>
        
        <div class="col-md-12" style="clear:both">
          <br>
          <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">#getLabel("Orders list")#</div>

            <!-- Table -->
            <div class="table-responsive">
              <table class="table" id="list_order">
                <thead>
                  <tr>
                    <!--- <th></th> --->
                    <th class="visible-lg">##</th>
                    <th>#getLabel("Name")#</th>
                    <th>#getLabel("Quantity")#</th>
                    <th class="visible-lg">#getLabel("Price")#</th>
                    <!--- <th>Note</th> --->
                    <cfif SESSION.UserType neq 3>
                      <th></th>
                      <th></th>
                    </cfif>
                  </tr>
                </thead>
                <tbody>
                  <cfset i=1/>
                  <cfloop query="#rc.ListOrder#">
                  <tr id="tr#rc.ListOrder.orderId#">
<!---                 <td>
                        <i class="fa fa-tags iconEdit" onclick="return showNoteModal(#rc.ListOrder.orderId#,'#rc.ListOrder.note#');"></i>
                      </td> --->
                      <td id="stt" class="visible-lg">#i#

                      </td>
                      <input type="hidden" name="fId" id="fId" class="fId" value="#rc.ListOrder.foodId#">
                      <td>#rc.ListOrder.name#</td>
                      <td name="quantity#rc.ListOrder.foodId#">

                        <button class="btn btnQuantity" onclick="return downQuantity(#rc.ListOrder.orderId#,#rc.ListOrder.price#);">
                          -
                        </button>

                        <input id="quantity#rc.ListOrder.foodId#" class="inputQuantity inputQuantity#rc.ListOrder.orderId#" type="text" min="1" value="#rc.ListOrder.quantity#" onfocus="this.oldvalue = this.value;" onchange="return quantityChange(this,#rc.ListOrder.orderId#,#rc.ListOrder.price#);this.oldvalue = this.value;" onkeypress="return isNumberKey(event)">

                        <button class=" btn btnQuantity" onclick="return upQuantity(#rc.ListOrder.orderId#,#rc.ListOrder.price#);">
                          +
                        </button>

                      </td>
                      <td class="visible-lg">#NumberFormat(rc.ListOrder.price)#</td>
                      <!--- <td id="note#rc.ListOrder.orderId#">#rc.ListOrder.note#</td> --->
                      <cfif SESSION.UserType neq 3>
                        <td>
                          <!--- <button type="button" class="btn btnDelete" onclick="return RemoveRow(#rc.ListOrder.orderId#,#rc.ListOrder.quantity#,#rc.ListOrder.price#);">
                            X
                          </button>
 --->
                          <button type="button" class="btn-danger" onclick="return RemoveRow(#rc.ListOrder.orderId#,#rc.ListOrder.quantity#,#rc.ListOrder.price#);">
                            #getLabel("Remove")#
                          </button>
                        </td>
                        <td></td>
                      </cfif>
                    </tr>
                    <cfset i=i+1/>
                  </cfloop>
                </tbody>
               </table>
             </div>
            </div>
          </div>

      </div>
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <label class="col-xs-5 col-md-3 control-label no-padding-right" style="font-weight: bolder;">#getLabel("Host")#</label>
            <div class="col-xs-7 col-md-9">
              <select class="form-control" id="host" name="host" onchange="changeHost();">
                <cfloop query="#rc.users#">
                  <cfset fullname = "#rc.users.lastname# #rc.users.firstname#">
                  <cfif !isnull(rc.curr_desk_user)>
                    <option value="#fullname#" #(fullname eq rc.curr_desk_user.getusername())? 'selected': ''#>#rc.users.lastname# #rc.users.firstname#</option>
                  <cfelse>
                    <option value="#fullname#">#rc.users.lastname# #rc.users.firstname#</option>
                  </cfif>
                </cfloop>
              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="form-group" style="padding-top:25px">
            <label class="col-xs-5 col-md-2 control-label no-padding-right" style="font-size: 15px; font-weight: bolder;">#getLabel("Category")#</label>
            <select class="col-xs-7 col-md-6" id="typefoodId" name="typefoodId" onchange="return typeFoodChange();" style="font-size: 15px; font-weight: bolder;">
                <option value="#getLabel("All")#" style="font-size: 15px; font-weight: bolder;">#getLabel("All")#</option>
                <cfloop array="#rc.ListMenuFood#" item="iMFood">
                    <option value="#iMFood.categoryId#" style="font-size: 16px; font-weight: bolder;">#iMFood.categoryname#</option>
                </cfloop>
            </select>
            <div class="col-xs-12 col-md-4 btn-container" style="text-align:right;padding-right:0px; margin-top:2%;">
              <button class="btn btn-primary btn-md" id="btnBack">
                <i class="ace-icon fa fa-arrow-left align-top"></i>Back
              </button>
              <button class="btn btn-primary btn-md" id="btnNext">
                  <i class="ace-icon fa fa-arrow-right align-top"></i>Next
              </button>
            </div>
            
          </div>
          
        </div>
        <div class="row">

          <div class="col-md-12">
            <div class="col-xs-12 col-md-12">
              <!--- List Menu --->
              <div class="row" id="foodchange">
              <br>
              <cfset i = 1>
              <cfloop query="#rc.ListFood#">
                <div class="col-xs-6 col-sm-3 col-md-2 ifood"
                  <cfif rc.ListFood.recordcount gt #itemPerPage#>
                    data-page="#ceiling(i/#itemPerPage#)#"
                  </cfif>
                >
                    <div class="iconFood foodId#rc.ListFood.foodId#" onclick="return AddRow(#rc.ListFood.foodId#,'#replace(rc.ListFood.name,"'","\'","All")#',#rc.ListFood.price#);" 
                    style="background:
                    <cfif #rc.ListFood.color# NEQ ''>
                      #rc.ListFood.color#
                      <cfelse>
                        ##438eb9
                    </cfif>; font-size: 15px; font-weight: bolder;">
                        #rc.ListFood.name#
                    </div>
                </div>
                <cfset i++>
              </cfloop>
              </div>
              <!--- End List Menu --->
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal-content -->
<div class="modal fade" id="noteModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">#getLabel("Close")#</span></button>
        <h4 class="modal-title">#getLabel("Note")#</h4>
      </div>
      <div class="modal-body">
        <textarea rows="5" cols="65" name="txtNote" id="txtNote"></textarea>
        <input type="hidden" name="orderId" id="orderId"/>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- /.Modal-content -->

<!-- /.Khanh Code -->

<input type="hidden" id="hdCurrentPage" value="1">
<input type="hidden" id="hdLastPage" value="#ceiling(arrayLen(rc.ListFood)/#itemPerPage#)#">

<script type="text/javascript">

  $(document).ready(function(){
    <cfif arrayLen(rc.ListFood) gt #itemPerPage#>
      $(".ifood[data-page='1']").attr("style","display:block");
      $(".ifood[data-page!='1']").attr("style","display:none");
      $("##btnBack").addClass("disabled");
    <cfelse>
      $("##btnBack").addClass("disabled");
      $("##btnNext").addClass("disabled");
    </cfif>

    $("body").keydown(function(e) {
      if(e.keyCode == 37) { // left
        buttonBackClick();
      }
      else if(e.keyCode == 39) { // right
        buttonNextClick(); 
      }
    });
  });
  
 $("##btnBack").click(function(){
    buttonBackClick();
 });

  $("##btnNext").click(function(){
    buttonNextClick();
 });

  function buttonBackClick(){
    var iCurrentPage = $("##hdCurrentPage").val();

    if ( iCurrentPage > 1){
      $(".ifood[data-page='" + iCurrentPage +"']").attr("style","display:none");
      iCurrentPage--;
      $(".ifood[data-page='" + iCurrentPage + "']").attr("style","display:block");

      $("##hdCurrentPage").val(iCurrentPage);

      $("##btnNext").removeClass("disabled");
      $("##btnBack").removeClass("disabled");
    } 
    if (iCurrentPage == 1){
      $("##btnBack").addClass("disabled");
      $("##btnNext").removeClass("disabled");
    }

  }

  function buttonNextClick(){
    var iCurrentPage = $("##hdCurrentPage").val();
    var iLastPage = $("##hdLastPage").val();

    if (iCurrentPage < iLastPage) {
      $(".ifood[data-page='" + iCurrentPage +"']").attr("style","display:none");
      iCurrentPage++;
      $(".ifood[data-page='" + iCurrentPage + "']").attr("style","display:block");

      $("##hdCurrentPage").val(iCurrentPage);

      $("##btnNext").removeClass("disabled");
      $("##btnBack").removeClass("disabled");
    } 
    if (iCurrentPage == iLastPage) {
      $("##btnBack").removeClass("disabled");
      $("##btnNext").addClass("disabled");
    }
  }

</script>
<!-- /.Khanh Code End -->
</cfoutput>