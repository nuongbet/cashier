<cfoutput>
  <script type="text/javascript">
function closePrint () {
  document.body.removeChild(this.__container__);
}

function closePrintView() { //this function simply runs something you want it to do
    window.location.assign("/index.cfm/main"); //in this instance, I'm doing a re-direct
 }

function setPrint () {
  this.contentWindow.__container__ = this;
  this.contentWindow.onbeforeunload = closePrint;
  this.contentWindow.onafterprint = closePrint;
  this.contentWindow.focus(); // Required for IE
  this.contentWindow.print();
  setTimeout("closePrintView()", 3000); //delay required for IE to realise what's going on
  window.onafterprint = closePrintView(); //this is the thing that makes it work i
}

function printPage (sURL) {
  var oHiddFrame = document.createElement("iframe");
  oHiddFrame.onload = setPrint;
  oHiddFrame.style.visibility = "hidden";
  oHiddFrame.style.position = "fixed";
  oHiddFrame.style.right = "0";
  oHiddFrame.style.bottom = "0";
  oHiddFrame.src = sURL;
  document.body.appendChild(oHiddFrame);

}
</script>
<script type="text/javascript">
 function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }
function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}
function isNumberKey(evt)
    {
       var charCode = (evt.which) ? evt.which : event.keyCode
       if (charCode > 31 && (charCode < 48 || charCode > 57))
          return false;

       return true;
}
function format(input)
{
    var nStr = input.value + '';
    nStr = nStr.replace( /\,/g, "");
    x = nStr.split( '.' );
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while ( rgx.test(x1) ) {
        x1 = x1.replace( rgx, '$1' + ',' + '$2' );
    }
    input.value = x1 + x2;
}


function RemoveRow(id,quantity,price){
    $.ajax({
      type: 'POST',
      url: '#getConTextRoot()#/index.cfm/home:order.deleteOrderById/',
      data: {'orderId':id,'deskId':#URL.id#},
      dataType: 'JSON',
      success: function(data) {
      location.reload();
      }
    }); 
}

function FOCChange(orderId,quantity){
  var foc=$('##foc'+orderId).val();
  if(parseInt(foc)>quantity){
    alert('#getLabel("Can not FOC. Please choose FOC <= Quantity.")#');
  }
  else{
       $.ajax({
      type: 'POST',
      url: '#getConTextRoot()#/index.cfm/home:order.FOCChange/',
      data: {'orderId':orderId,'foc':foc},
      dataType: 'JSON',
      success: function(data) {
      location.reload();
      }
  }); 
  }
}

function showNoteModal(i,txt) {
  $('##noteModal').modal('show');
  $('##orderId').val(i);
  $('##txtNote').val(txt);
}

//bill print temp
function  billTemp(){
  if(!isBlank('#rc.billId#')){
  var typeId = $( "select##typepriceId" ).val();
  var discount=$('##discount').val();
  var hgettotal=$('##total').val();
  var radioDiscount=$('##rId').val();
  var received=$('##received').val();
  var refund=$('##refund').val();
  $.ajax({
      type: 'POST',
      url: '#getConTextRoot()#/index.cfm/home:order.billTemp/',
      data: {'typepriceId':typeId,'radioDiscount':radioDiscount,'discount':discount,'hgettotal':hgettotal,'billId':'#rc.billId#','deskId':#URL.id#, 'isPrintedReceipt': true},
      dataType: 'JSON',
      success: function(data) {
        var fullname = $("##host").val();
        var arrfull = fullname.split(" ");
        var lastname = arrfull[arrfull.length - 1];
         printPage('#rootLink#/index.cfm/order.printbill?billId=#rc.billId#&tenban=#rc.table.name#&received='+received+'&refund='+refund+'&host='+lastname);
      }
      }); 
  }
}

//type price select change
function typeChange() {
  var typeId = $( "select##typepriceId" ).val();
  var typeName= $( "select##typepriceId option:selected" ).text();
  $('##typepriceName').val(typeName);
  $.ajax({
        type: 'POST',
        url: '#getConTextRoot()#/index.cfm/home:order.getChangeTotal/',
        data: {'deskId':#URL.id#,'typeId':typeId},
        dataType: 'JSON',
        success: function(data) {
          $('##htotal').val(data);
          $('##totalTemp').val(commaSeparateNumber(data));
          var tdiscount=$('input[name=radioDiscount]:checked').val();
          var discount=$('##discount').val().replace(/,/g , "");
          if(isBlank(discount)){
            discount=0;
          }
          var ctotal=$('##htotal').val();
          if(tdiscount==0){
            var percent=100-discount;
            $('##total').val(commaSeparateNumber(ctotal*percent/100));
            $('##discountvalue').val(commaSeparateNumber(ctotal*discount/100));
          }
          else{
            $('##total').val(commaSeparateNumber(ctotal-discount));
             $('##discountvalue').val(commaSeparateNumber(discount));
          }

          //update refund
          var received=$('##received').val().replace(/,/g , "");
          var totaltemp=$('##total').val().replace(/,/g , "");
          $('##refund').val(commaSeparateNumber(received-totaltemp));
          disabledPayment(received,totaltemp);
          //append price
          $.getJSON('#getConTextRoot()#/index.cfm/home:order.getFoodPrice/',{ dId: #URL.id#, tId: typeId }, function(data) {
            for(var i=0;i<data.DATA.length;i++){
                $(".fId").each(function(){
                if($(this).val() == data.DATA[i][1]){
                  //append add quantity when foodId exists.
                  $('##price'+$(this).val()).html(commaSeparateNumber(data.DATA[i][0]));
                  var totalchange=data.DATA[i][0]*$('##quantity'+$(this).val()).html();;
                  $('##totalchange'+$(this).val()).html(commaSeparateNumber(totalchange));
                  return false;
                }
              });
            }
          });

        }
    }); 
}
function disabledPayment(received,ctotal){
    if(isBlank(received)){
      received=0;
    }
    if(isBlank(ctotal)){
      ctotal=0;
    }
    if(parseFloat(received) < parseFloat(ctotal)){
      $('##btnsubmit').prop("disabled", true);
    }
    else{
       $('##btnsubmit').prop("disabled", false);
    }
}

$(document).ready(function(){



    $('body').on('keydown', function (e) {
      if(e.keyCode == 123) {
        e.preventDefault();
        location.href="/index.cfm/order.waiter?id=#URL.id#";
      }
    });


  $('##noteModal').on('hidden.bs.modal', function (e) {
    var orderId = $('##orderId').val();
    var txtNote = $('##txtNote').val();
    $.ajax({
        type: 'POST',
        url: '#getConTextRoot()#/index.cfm/home:order.updateNote/',
        data: {'orderId':orderId,'txtNote':txtNote},
        dataType: 'JSON',
        success: function(data) {
          $('##note'+orderId).html(txtNote);
        }
    }); 
  });

  //radio discount change
  $('input[type=radio][name=radioDiscount]').on('change', function(){
    var discount=$('##discount').val().replace(/,/g , "");
    if(isBlank(discount)){
      discount=0;
    }
    var ctotal=$('##htotal').val();
     $('##rId').val($(this).val());
    switch($(this).val()){
        case '0' :
            var percent=100-discount;
            $('##total').val(commaSeparateNumber(ctotal*percent/100));
            $('##discountvalue').val(commaSeparateNumber(ctotal*discount/100));
            break;
        case '1' :
            $('##total').val(commaSeparateNumber(ctotal-discount));
             $('##discountvalue').val(commaSeparateNumber(discount));
            break;
    }
    //update refund
    var received=$('##received').val().replace(/,/g , "");
    var totaltemp=$('##total').val().replace(/,/g , "");
    $('##refund').val(commaSeparateNumber(received-totaltemp));  
     disabledPayment(received,totaltemp);          
});

  //input discount change
  $('##discount').on('input',function(e){

    var tdiscount=$('input[name=radioDiscount]:checked').val();
    var discount=$(this).val().replace(/,/g , "");
    if(isBlank(discount)){
      discount=0;
    }
    var ctotal=$('##htotal').val();

    if(tdiscount==0){
      var percent=100-discount;
      $('##total').val(commaSeparateNumber(ctotal*percent/100));
      $('##discountvalue').val(commaSeparateNumber(ctotal*discount/100));
    }
    else{
      $('##total').val(commaSeparateNumber(ctotal-discount));
       $('##discountvalue').val(commaSeparateNumber(discount));
    }

    //update refund
    var received=$('##received').val().replace(/,/g , "");
    var totaltemp=$('##total').val().replace(/,/g , "");
    $('##refund').val(commaSeparateNumber(received-totaltemp));
    disabledPayment(received,totaltemp);
 });

    //input Received change
  $('##received').on('input',function(e){
    var received=$(this).val().replace(/,/g , "");
    if(isBlank(received)){
      received=0;
    }
    var totaltemp=$('##total').val().replace(/,/g , "");
    $('##refund').val(commaSeparateNumber(received-totaltemp));
    disabledPayment(received,totaltemp);
 });

  //submit
  $('##btnsubmit').click(function(){
    $('##hgettotal').val($('##total').val());
  });



});

</script>
<cfif CGI.REQUEST_METHOD eq "POST" and rc.billId neq "">
    <cfquery name="qCheckSendEmail">
      select datecreate from alertmail
      where date(datecreate) ="#lsdateFormat(now(),"yyyy-mm-dd","pt_PT")#" 
      and shopId=#SESSION.shopId#
    </cfquery>

    <!--- get email template --->
    <cfquery name="emailContain">
      select * from email 
      where emailKey = "Stock"
    </cfquery>
    <cfset eSubject = replace(emailContain.subject,"$date$",lsdateFormat(now(),"dd-mm-yyyy","pt_PT"))>
    <cfset lEmailContainer = listToArray(emailContain.container,"$container$")>
    <cfif arrayLen(lEmailContainer) lt 2>
      <cfset lEmailContainer[2] = "</br>">
      <cfif arrayLen(lEmailContainer) lt 1>
        <cfset lEmailContainer[1] = "<br>">
      </cfif>
    </cfif>
    <!--- end get email template --->

    <cfif #qCheckSendEmail.recordcount# eq 0 and #rc.liststockneed.recordcount# gt 0 >
        <cfquery name="qGetInfoShop">
            select * from shop where shopId=#SESSION.ShopId#
        </cfquery>
        <cfmail
            from="testrasia@gmail.com" 
            to="#qGetInfoShop.email[1]#"
            subject="#eSubject#" type="html" >
            #lEmailContainer[1]#
            <cfinclude template="../liststock/stockneed.cfm"/>
            <br>
            <cfinclude template="../liststock/stockmini.cfm"/>
            #lEmailContainer[2]#
        </cfmail>
        <cfquery name="insertAlertMail">
          insert into alertmail(datecreate,shopId) values("#lsdateformat(now(),"yyyy-mm-dd","pt_PT")#",#SESSION.shopId#)
        </cfquery>
    <cfelse>
        <cfif #qCheckSendEmail.recordcount# eq 1 and #rc.liststockmini.recordcount# gt 0 >
          <cfquery name="qGetInfoShop">
              select * from shop where shopId=#SESSION.ShopId#
          </cfquery>
          <cfmail
              from="testrasia@gmail.com" 
              to="#qGetInfoShop.email[1]#"
              subject="#eSubject#" type="html" >
              #lEmailContainer[1]#
              <cfinclude template="../liststock/stockneed.cfm"/>
              <br>
              <cfinclude template="../liststock/stockmini.cfm"/>
              #lEmailContainer[2]#
          </cfmail>
          <cfquery name="insertAlertMail">
            insert into alertmail(datecreate,shopId) values("#lsdateformat(now(),"yyyy-mm-dd","pt_PT")#",#SESSION.shopId#)
          </cfquery>
        </cfif>
    </cfif>
  <script type="text/javascript">
    $(document).ready(function() {
      var fullname = $("##host").val();
      var arrfull = fullname.split(" ");
      var lastname = arrfull[arrfull.length - 1];
      printPage('#rootLink#/index.cfm/order.printbill?billId=#rc.billId#&tenban=#rc.table.name#&received=#rc.received#&refund=#rc.refund#&host='+lastname);
    })
  </script>
 
</cfif>
<div class="container-fluid">
<div class="page-header">
  <h1>
    #rc.table.name#
    <a class="btn btn-primary btn-white" href="#rootLink#/index.cfm/main" accesskey="t">
      <i class="ace-icon fa fa-angle-double-left"></i>
      #getLabel("Select Table")#
    </a>
    <a class="btn btn-primary btn-white" href="#rootLink#/index.cfm/order.waiter?id=#URL.id#" accesskey="o">
      <i class="ace-icon fa fa-glass"></i>
      #getLabel("Order")#
    </a>
  </h1>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="row">      
      <div class="col-xs-12">
        <div class="panel panel-default">
          <!-- Default panel contents -->
          <div class="panel-heading">#getLabel("Orders list")#</div>

          <!-- Table -->
          <div class="table-responsive">
            <table class="table" id="list_order">
              <thead>
                <tr>
                  <th>##</th>
                  <th>#getLabel("Name")#</th>
                  <th>#getLabel("Price")#</th>
                  <th>#getLabel("Quantity")#</th>
                  <th>#getLabel("Total")#</th>
                  <!--- <th>Note</th> --->
                  <th>#getLabel("FOC")#</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <cfset i=1/>
                <cfloop query="#rc.ListOrder#">
                <tr id="tr#rc.ListOrder.orderId#">
                      <td id="stt">#i#
                      <input type="hidden" name="fId" id="fId" class="fId" value="#rc.ListOrder.foodId#">
                    </td>
                      <td>#rc.ListOrder.name#</td>
                      <td id="price#rc.ListOrder.foodId#" name="price#rc.ListOrder.foodId#">#NumberFormat(rc.ListOrder.price)#</td>
                      <td id="quantity#rc.ListOrder.foodId#" name="quantity#rc.ListOrder.foodId#">#rc.ListOrder.quantity+rc.ListOrder.foc#</td>
                      <!--- <td id="note#rc.ListOrder.orderId#">#rc.ListOrder.note#</td> --->
                      <td id="totalchange#rc.ListOrder.foodId#">#numberformat(rc.ListOrder.total)#</td>
                      <td>
                        <input id="foc#rc.ListOrder.orderId#" class="inputQuantity" type="text" min="0" value="#rc.ListOrder.foc#" maxlength="2" onchange="return FOCChange(#rc.ListOrder.orderId#,#rc.ListOrder.quantity#);"onkeypress="return isNumberKey(event)">
                      </td>
                      <td>
                        <button type="button" class="btn btn-danger visible-xs" onclick="return RemoveRow(#rc.ListOrder.orderId#,#rc.ListOrder.quantity#,#rc.ListOrder.price#);">
                          X
                        </button>

                        <button type="button" class="btn btn-danger visible-lg visible-sm" onclick="return RemoveRow(#rc.ListOrder.orderId#,#rc.ListOrder.quantity#,#rc.ListOrder.price#);">
                          #getLabel("Remove")#
                        </button>

                      </td>
                    </tr>
                    <cfset i=i+1/>
                </cfloop>
              </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
  </div>
  <div class="col-md-6">
    <form class="form-horizontal" role="form" action="" method="POST">
      <input type="hidden" id="htotal" name="htotal" value="#rc.total#">
      <input type="hidden" id="hgettotal" name="hgettotal" value="">
      <input type="hidden" id="bId" name="bId" value="#rc.billId#">
      <input type="hidden" id="rId" name="rId" value="0">
      <input type="hidden" id="tenban" name="tenban" value="#rc.table.name#">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="##panel-current" data-toggle="tab"><i class="ace-icon fa fa-dollar bigger-110"></i>#getLabel("Payment")#</a>
            </li>
            <li>
                <a href="##panel-last" data-toggle="tab"><i class="ace-icon fa fa-edit bigger-110"></i>#getLabel("Note")#</a>
            </li>
        </ul>
       <div class="tab-content">
            <div class="tab-pane active" id="panel-current">
                   <div class="row">
               <!---      <div class="col-md-12">
                      <div class="form-group">
                        <label class="col-xs-5 col-md-3 control-label no-padding-right">Total</label>
                        <div class="col-xs-7 col-md-9">
                          <input type="text" id="btotal" name="btotal" class="col-xs-12 col-sm-12" readonly value="#NumberFormat(rc.total)#"/>
                          <p></p>
                        </div>
                      </div>
                    </div> --->
                    <div class="col-md-12">
                        <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Choose a price type")#</label>
                              <div class="col-xs-7 col-md-9">
                                <select class="form-control" id="typepriceId" name="typepriceId" onchange="return typeChange();">
                                  <option value=0>#getLabel("Normal")#</option>
                                  <cfloop array="#rc.typeprice#" item="itype">
                                     <option value="#itype.typeId#">#itype.name#</option>
                                  </cfloop>
                                </select>
                                <input type="hidden" id="typepriceName" name="typepriceName" value="#getLabel("Normal")#">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Discount")#</label>
                              <div class="col-xs-7 col-md-9">
                                <input type="text" id="discount" name="discount" onkeypress="return isNumberKey(event)" onkeyup="format(this)" class="col-xs-12 col-sm-5"/>&nbsp; 
                                <!---<input type="radio" name="radioDiscount" id="radioDiscount" value="0" checked> #getLabel("percent")# &nbsp;
                                <input type="radio" name="radioDiscount" id="radioDiscount" value="1"> #getLabel("cash")#--->
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Total")#</label>
                              <div class="col-xs-7 col-md-9">
                                <input type="text" id="totalTemp" name="totalTemp" class="col-xs-12 col-sm-12" readonly value="#NumberFormat(rc.total)#"/>
                                <p></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Discount value")#</label>
                              <div class="col-xs-7 col-md-9">
                                <input type="text" id="discountvalue" name="discountvalue" class="col-xs-12 col-sm-12 txtDisplay txtSize" readonly value="0"/>
                                <p></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Amount")#</label>
                              <div class="col-xs-7 col-md-9">
                                <input type="text" id="total" name="total" class="col-xs-12 col-sm-12 txtDisplay txtSize" readonly value="#NumberFormat(rc.total)#"/>
                                <p></p>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Received")#</label>
                              <div class="col-xs-7 col-md-9">
                                <input type="text" id="received" name="received" onkeypress="return isNumberKey(event)" onkeyup="format(this)" class="col-xs-12 col-sm-12"/> 
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Refund")#</label>
                              <div class="col-xs-7 col-md-9">
                                <input type="text" id="refund" name="refund" readonly class="col-xs-12 col-sm-12 txtDisplay txtSize"/> 
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Order type")#</label>
                              <div class="col-xs-7 col-md-9">
                                <select class="form-control" id="ordertypeId" name="ordertypeId">
                                  <cfloop array="#rc.typeorder#" item="iorder">
                                     <option value="#iorder.typeId#">#iorder.name#</option>
                                  </cfloop>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-xs-5 col-md-3 control-label no-padding-right">#getLabel("Host")#</label>
                              <div class="col-xs-7 col-md-9">
                                <select class="form-control host" id="host" name="host">
                                  <cfloop query="#rc.users#">
                                    <cfset fullname = "#rc.users.lastname# #rc.users.firstname#">
                                    <cfif !isnull(rc.curr_desk_user)>
                                      <option lastname="#rc.users.lastname#" value="#fullname#" #(fullname eq rc.curr_desk_user.getusername())? 'selected': ''#>#rc.users.lastname# #rc.users.firstname#</option>
                                    <cfelse>
                                      <option value="#fullname#">#rc.users.lastname# #rc.users.firstname#</option>
                                    </cfif>
                                  </cfloop>
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group" style="float: right">
                              <div class="col-md-12">
                                <button class="btn btn-info" type="button" id="btnprint" onclick="billTemp();">
                                  <i class="ace-icon fa fa-file-text-o bigger-110"></i>
                                  #getLabel("Print")#
                                </button>
                                <button class="btn btn-info" type="submit" id="btnsubmit" disabled>
                                  <i class="ace-icon fa fa-dollar bigger-110"></i>
                                  #getLabel("Payment")#
                                </button>
                              </div>
                            </div>
                          </div>

                        </div>
                    </div>
                  </div>
            </div>
            <div class="tab-pane" id="panel-last">
              <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12">
                  <textarea rows="10" cols="80" id="txtNote" class="col-md-12 col-xs-12 col-sm-12" name="txtNote"></textarea>
                </div>
              </div>
            </div>
        </div>
 
    </form>
  </div>
</div>
</div>
<!-- Modal-content -->
<div class="modal fade" id="noteModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">#getLabel("Close")#</span></button>
        <h4 class="modal-title">#getLabel("Note")#</h4>
      </div>
      <div class="modal-body">
        <textarea rows="5" cols="65" name="txtNote" id="txtNote"></textarea>
        <input type="hidden" name="orderId" id="orderId"/>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- /.Modal-content -->

</cfoutput>

