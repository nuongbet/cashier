<cfoutput>
	<div class="col-xs-12">
        <div class="tab-pane active" id="panel-current">
			<div class="panel panel-default">
	            <div class="panel-heading">#getLabel("List of under-needed materials")#</div>
	            <div class="table-responsive">
					<table id="list_stockneed" class="table">
						<thead>
							<tr>
								<th>##</th>
								<th>#getLabel("Name")#</th>
								<th>#getLabel("Unit")#</th>
								<th>#getLabel("Actually")#</th>
								<th>#getLabel("Needed")#</th>
								<th>#getLabel("Min")#</th>
							</tr>
						</thead>
						<tbody>
							<cfset i=1/>
						<cfloop query="#rc.liststockneed#">
							<tr>
								<td>
									#i#
								</td>
								<td>
									#rc.liststockneed.name#
								</td>
								<td>
									#rc.liststockneed.uname#
								</td>
								<td>
									#rc.liststockneed.actualstock#
								</td>
								<td>
									#rc.liststockneed.needstock#
								</td>
								<td>
									#rc.liststockneed.ministock#
								</td>		
							</tr>
							<cfset i += 1/>
						</cfloop>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</cfoutput>