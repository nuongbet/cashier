<cfoutput>
<div class="page-header">
  <h1>
	#getLabel("Profile")#
  </h1>
</div>
<form class="form-horizontal" role="form" action="" method="POST">
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("First name")#</label>
		<div class="col-sm-10">
			<input type="text" id="firstname" name="firstname" placeholder="#getLabel("First name")#" class="col-xs-10 col-sm-5" value="#rc.LoadProfile.firstname#" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Last name")#</label>
		<div class="col-sm-10">
			<input type="text" id="lastname" name="lastname" placeholder="#getLabel("Last name")#" class="col-xs-10 col-sm-5" value="#rc.LoadProfile.lastname#" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Phone")#</label>
		<div class="col-sm-10">
			<input type="number" id="phone" name="phone" placeholder="#getLabel("Phone")#" class="col-xs-10 col-sm-5" value="#rc.LoadProfile.phone#" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Email")#</label>
		<div class="col-sm-10">
			<input type="email" id="email" name="email" placeholder="#getLabel("Email")#" class="col-xs-10 col-sm-5" value="#rc.LoadProfile.email#" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Username")#</label>
		<div class="col-sm-10">
			<input type="text" id="username" name="username" placeholder="#getLabel("Username")#" class="col-xs-10 col-sm-5" value="#rc.LoadProfile.username#" readonly/>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Password")#</label>
		<div class="col-sm-10">
			<input type="password" id="password" name="password" placeholder="#getLabel("Password")#" class="col-xs-10 col-sm-5" value="#rc.LoadProfile.password#" />
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">#getLabel("Language")#</label>
		<div class="col-sm-10">
			<select name="languageId" id="languageId" class="col-xs-10 col-sm-5" >
				<cfloop array="#rc.ListLanguage#" item="iLanguage">
					<cfif #rc.LoadProfile.languageId# eq #iLanguage.languageId#>
						<cfset selected="selected"/>
					<cfelse>
						<cfset selected=""/>
					</cfif>
					<option value="#iLanguage.languageId#" #selected#>#iLanguage.language#</option>
				</cfloop>
			</select>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-10">
			<button class="btn btn-info" type="submit">
				<i class="ace-icon fa fa-check bigger-110"></i>
				#getLabel("Submit")#
			</button>
		</div>
	</div>

</form>
</cfoutput>