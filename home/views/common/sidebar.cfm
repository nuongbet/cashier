<cfoutput>
<div id="sidebar" class="sidebar h-sidebar navbar-collapse collapse">	

	<ul class="nav nav-list">
		<li class="active hover">
			<a href="#rootLink#/index.cfm/main">
				<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> #getLabel("Waiter")# </span>
			</a>

			<b class="arrow"></b>
		</li>
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>

	<script type="text/javascript">
		try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
	</script>
</div>
</cfoutput>