<cfoutput>
<cfquery name="qGetlogoandname">
	SELECT name,logo FROM shop WHERE shopId=#SESSION.ShopId#
</cfquery>
<div id="navbar" class="navbar navbar-default    navbar-collapse       h-navbar">
	<script type="text/javascript">
		try{ace.settings.check('navbar' , 'fixed')}catch(e){}
	</script>
	<div class="navbar-container" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="/index.cfm/admin:" class="navbar-brand">
				<small>
					<cfif SESSION.shopId NEQ 0>
						<img src="/images/shop/#qGetlogoandname.logo#" class="logo">
						#qGetlogoandname.name#
						<cfelse>
						<i class="fa fa-paw"></i>
						Cashier
					</cfif>
				</small>
			</a>
		</div>
		<div>
	  </div>
		<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
			<ul class="nav ace-nav">
				<li class="light-blue user-min">
					<a data-toggle="dropdown" href="##" class="dropdown-toggle">
						<span>
							<small>#getLabel("Welcome,")#</small>
							#SESSION.UserName#
						</span>
						<i class="ace-icon fa fa-caret-down"></i>
					</a>
					<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<cfif #SESSION.UserType EQ 1#>
							<li>
								<a href="/index.cfm/admin:">
									<i class="ace-icon fa fa-external-link"></i>
									#getLabel("Home page")#
								</a>
							</li>
						</cfif>
						<li>
							<a href="/index.cfm/home:mbill">
								<i class="ace-icon fa fa-file-text-o"></i>
								#getLabel("Invoice")#
							</a>
						</li>
						<li>
							<a href="/index.cfm/user/profile?id=#SESSION.UserID#">
								<i class="ace-icon fa fa-user"></i>
								#getLabel("Profile")#
							</a>
						</li>

						<li class="divider"></li>

						<li>
							<a href="/index.cfm/common/logout">
								<i class="ace-icon fa fa-power-off"></i>
								#getLabel("Logout")#
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
</cfoutput>