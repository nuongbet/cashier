<cfset itemPerPage = 1000
>

<style type="text/css">
	.col-xs-6, .col-sm-3, .col-md-2 {
		padding-right: 0px;
		padding-left: 0px;
	}
	.btn.active:after {
		border-bottom: none;
	}

	.show {
		/*top: 0px !important;*/
		/*transform:translateY(0px) !important;*/
	}

	.element-item .col-md-12 {
		padding-right: 0px;
	}
</style>

<cfoutput>
<script type="text/javascript">

	$(document).ready(function(){
		var n = $('.deskBox').length;
		for(var i=1;i<=n;i++) 
		{
			showDivedit(i);
		}		
		$('section:first-child .btnMoveleftsection').prop('disabled', true);
		$('section:last-child .btnMoverightsection').prop('disabled', true);
	});

	function showDivedit(i){
	    $(document).on('mouseenter', '.t'+i, function () {
	        $(this).find(".btnedit").show();
	    }).on('mouseleave', '.t'+i, function () {
	    $(this).find(".btnedit").hide();
	    });
	}

	function moveleftsection(id){                         
		div1 = $('.t'+id);
		div2 = $('.t'+id).prev();

		tdiv1 = div1.clone();
		tdiv2 = div2.clone();

		if(!div2.is(':empty')){
		    div1.replaceWith(tdiv2);
		    div2.replaceWith(tdiv1);
		}
		var idprev = div2[0].className;
		idprev = idprev.replace('t','');
		var csorted = parseInt($('.dsorted'+id).val());
		var psorted = parseInt($('.dsorted'+idprev).val());
		var cdeskid = $('.deskid'+id).val();
		var pdeskid = $('.deskid'+idprev).val();
	    $.ajax({
	      type: 'POST',
	      url: '/index.cfm/home:main.movelefttable/',
	      data: {'csorted':csorted,'cdeskid':cdeskid,'psorted':psorted,'pdeskid':pdeskid},
	      dataType: 'JSON',
	      success: function(data) {
	      	location.reload();
	      }
	    }); 

	}

	function moverightsection(id){
		div1 = $('.t'+id);
		div2 = $('.t'+id).next();

		tdiv1 = div1.clone();
		tdiv2 = div2.clone();

		if(!div2.is(':empty')){
		    div1.replaceWith(tdiv2);
		    div2.replaceWith(tdiv1);
		}

		var idnext = div2[0].className;
		idnext = idnext.replace('t','');
		var csorted = parseInt($('.dsorted'+id).val());
		var nsorted = parseInt($('.dsorted'+idnext).val());
		var cdeskid = $('.deskid'+id).val();
		var ndeskid = $('.deskid'+idnext).val();
	    $.ajax({
	      type: 'POST',
	      url: '/index.cfm/home:main.moverighttable/',
	      data: {'csorted':csorted,'cdeskid':cdeskid,'nsorted':nsorted,'ndeskid':ndeskid},
	      dataType: 'JSON',
	      success: function(data) {
	      	location.reload();
	      }
	    }); 

	}
                
</script>
<cfif CGI.REQUEST_METHOD eq "POST">
	<cfif rc.check eq false>
		<script type="text/javascript">
			alert('#getLabel("Can not change table. Please change again.")#');
		</script>
	</cfif>
</cfif>
<div class="container-fluid">
<cfset currPos=0>
<cfset currPos1=0>
<cfset currPos2=0>
	<div class="page-header row">
		<div class="col-md-6 col-xs-12 col-sm-6">
			<div class="btn-group filters js-radio-button-group">
				<button class="btn btn-primary isotopeBtn active" data-filter="*">#getLabel("All Tables")#</button>
				<cfloop query="rc.deskPos">
					<cfif rc.deskPos.categoryId NEQ currPos>
						<button class="btn btn-primary isotopeBtn" data-filter=".#reReplaceNoCase(rc.deskPos.categoryname, "[^a-z0-9\-\_]","","all")#">#rc.deskPos.categoryname#</button>
						<cfset currPos=rc.deskPos.categoryId>
					</cfif>		
				</cfloop>
			</div>
		</div>
		<div class="col-md-6 col-xs-12 col-sm-6 no-padding">
			<div class="row no-margin">
			<form action="" method="POST">
			<select name="fromDeskId" id="fromDeskId" class="col-md-3 col-xs-4 col-sm-4 selectpicker col-md-offset-4" >
				<cfloop query="#rc.ListDeskInActive#">
					<cfif rc.ListDeskInActive.categoryId NEQ currPos1>
						</optgroup>
						<optgroup label="#rc.ListDeskInActive.categoryname#">
						<cfset currPos1= rc.ListDeskInActive.categoryId/>
					</cfif>
					<cfif rc.ListDeskInActive.categoryId EQ currPos1>
						<cfquery name="qGetInfo">
							SELECT count(*) AS items, ifnull(sum(price * quantity),0) AS total 
							FROM food f 
							INNER JOIN  cashier.order o 
							ON f.foodid = o.foodid 
							WHERE o.deskid=#rc.ListDeskInActive.deskId# 
							AND o.status =0
							AND o.shopId=#SESSION.ShopId#
							</cfquery>
						<option value="#rc.ListDeskInActive.deskId#">
							#rc.ListDeskInActive.name# [#NumberFormat(qGetInfo.total)# #currency# (#qGetInfo.items#)]
						</option>
					</cfif>
				</cfloop>
			</select>
			
			<div class="col-md-1 col-xs-2 col-sm-2">
				<div class="icon-move">
					<i class="ace-icon fa fa-long-arrow-right bigger-130"></i>
				</div>		
			</div>
			<select name="toDeskId" id="toDeskId" class="col-md-2 col-xs-4 col-sm-4 " >
				<cfloop query="#rc.ListDeskActive#">
					<cfif rc.ListDeskActive.categoryId NEQ currPos2>
						</optgroup>
						<optgroup label="#rc.ListDeskActive.categoryname#">
						<cfset currPos2= rc.ListDeskActive.categoryId/>
					</cfif>
					<cfif rc.ListDeskActive.categoryId EQ currPos2>
						<option value="#rc.ListDeskActive.deskId#">
							#rc.ListDeskActive.name#
						</option>
					</cfif>
				</cfloop>
			</select>
			<div class="col-md-2 col-xs-2 col-sm-2" style="padding-left: 2%">
				<button type="submit" class="btn btn-primary btn-change-table" style="width:100%">#getLabel("Change")#</button>
			</div>
			</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="container demo">
			<div class="isotope">
				<cfset i=1/>
				<cfset j=1/>
				<cfset catalog = 0>
				<cfloop query="rc.deskPos">

					<cfset catalog = rc.deskPos.categoryname>
					<cfif rc.deskPos.categoryId NEQ currPos>
						<cfset currPos=rc.deskPos.categoryId>
						<cfset j=1/>
					</cfif>

					<cfif rc.deskPos.categoryId EQ currPos>	
						<section class="t#i#">
							<div class="element-item transition #reReplaceNoCase(rc.deskPos.categoryname, "[^a-z0-9\-\_]","","all")# col-xs-6 col-sm-3 col-md-2 div#i# <cfif rc.deskPos.recordCount gt #itemPerPage#>
				                    page#ceiling(i/#itemPerPage#)#
				                </cfif>"
								<cfif rc.deskPos.recordCount gt #itemPerPage#>
				                    data-page="#ceiling(i/#itemPerPage#)#"
				                </cfif>
							>
								<!--- set link permission user --->
								<cfif #SESSION.UserType# EQ 2 AND #rc.deskPos.status# EQ 1>
									<a href="/index.cfm/order.cashier?id=#rc.deskPos.deskId#">
									<cfelseif #SESSION.UserType# EQ 2 AND #rc.deskPos.status# EQ 0>
									<a href="/index.cfm/order.waiter?id=#rc.deskPos.deskId#">	
									<cfelseif #SESSION.UserType# EQ 3>
									<a href="/index.cfm/order.waiter?id=#rc.deskPos.deskId#">	
									<cfelseif #SESSION.UserType# EQ 1 AND #rc.deskPos.status# EQ 1>
									<a href="/index.cfm/order.cashier?id=#rc.deskPos.deskId#">
									<cfelseif #SESSION.UserType# EQ 1 AND #rc.deskPos.status# EQ 0>
									<a href="/index.cfm/order.waiter?id=#rc.deskPos.deskId#">	
								</cfif>	
								<!--- end set link permission user --->
									<div class="col-md-12">
									<cfif #rc.deskPos.status# EQ 0>
										<div class="deskBox colorDeskBoxBlue">
									<cfelse>
										<div class="deskBox colorDeskBoxRed">
									</cfif>						
											<p style="padding:20px; font-size:20px; text-transform:uppercase;">
												#rc.deskPos.name#
												<input type="hidden" class="dsorted#i#" value="#rc.deskPos.dsorted#">
												<input type="hidden" class="deskid#i#" value="#rc.deskPos.deskId#">
											</p>
											<!--- <p>
												(#rc.deskPos.type#)
											</p>
										
											<p>
												<cfquery name="qGetTotalByDeskId">
													SELECT count(*) AS items, ifnull(sum(price * quantity),0) AS total 
													FROM food f 
													INNER JOIN  cashier.order o 
													ON f.foodid = o.foodid 
													WHERE o.deskid=#rc.deskPos.deskId# 
													AND o.status =0
													AND o.shopId = #SESSION.ShopId#
												</cfquery>
												
												#NumberFormat(qGetTotalByDeskId.total)# #currency# (#qGetTotalByDeskId.items#)
											</p> --->
										</div>
									</div>
									</a>
							</div>
						</section>
						<cfset i=i+1/>
						<cfset j++/>	
					</cfif>

				</cfloop>
			</div>
		</div>
	</div>		
</div>

<!-- /.Khanh Code -->
<script src="/assets/js/isotope.pkgd.js"></script>
<script src="/assets/js/index.js"></script>

<input type="hidden" id="hdCurrentPage" value="1">
<input type="hidden" id="hdLastPage" value="#ceiling(rc.deskPos.recordCount/#itemPerPage#)#">
<input type="hidden" id="hdCurrentFilter" value="*">
<input type="hidden" id="hdItemperPage" value="#itemPerPage#">

<script type="text/javascript">

  $(document).ready(function(){
    <cfif rc.deskPos.recordCount gt #itemPerPage#>
      $("##btnBack").addClass("disabled");
    <cfelse>
      $("##btnBack").addClass("disabled");
      $("##btnNext").addClass("disabled");
    </cfif>

    $("body").keydown(function(e) {
      if(e.keyCode == 37) { // left
        buttonBackClick();
      }
      else if(e.keyCode == 39) { // right
        buttonNextClick(); 
      }
    });
  });

  function buttonBackClick(){

  }

  function buttonNextClick(){

  }

</script>
<!-- /.Khanh Code End -->

</cfoutput>