
<script type="text/javascript">
	$(document).ready(function () {
		setTimeout(function () {
			location.reload();
		}, 7000);
	});

	$(document).on('click','#btnMarkDone', function() {
		var currParentTR = $(this).parent().parent();
		$.ajax({
	      type: 'POST',
	      url: '/index.cfm/chief.setDoneFoodOrdered',
	      data: {'orderId':currParentTR.attr('orderid')},
	      dataType: 'JSON',
	      success: function(data) {
	      	if(data == true) {
				currParentTR.remove();
	      	}
	      }
	    }); 
	});
</script>
<cfoutput>
	<div class="page-content container-fluid">
		<div class="container-fluid">
			<div class="page-header row">
			</div>
			<div class="row">
			  <div class="col-md-6">
			    <div class="row">      
			      <div class="col-xs-12">
			        <div class="panel panel-default">
			          <!-- Default panel contents -->
			          <div class="panel-heading">#getLabel("lstBBQ")#</div>
			          <!-- Table -->
			          <div class="table-responsive">
			            <table class="table" id="list_bbq">
			              <thead>
			                <tr>
			                  <th>#getLabel("Name")#</th>
			                  <th>#getLabel("Quantity")#</th>
			                  <th>#getLabel("Tables")#</th>
			                  <th>#getLabel("Status")#</th>
			                </tr>
			              </thead>
			              <tbody>
			              	<cfloop query="rc.lstbbq">
		              			<cfset styles = ""/>
		              			<cfset minutes = DateDiff("n", rc.lstbbq.starttime, now())/>
			              		<cfif minutes lt 5>
			              			<cfset styles = "background: white;"/>
			              		<cfelseif minutes gte 5 and minutes lt 10>
			              			<cfset styles = "background: orange;"/>
			              		<cfelse>
			              			<cfset styles = "background: red; color: white;"/>
			              		</cfif>
			              		<cfif findNoCase(",", rc.lstbbq.tname, 1) gt 0>
			              			<cfset styles = "background: aqua;"/>
			              		</cfif>
			              		<tr style="#styles#" orderid="#rc.lstbbq.orderId#">
				                      <td>#rc.lstbbq.fname#</td>
				                      <td>#rc.lstbbq.quantity#</td>
				                      <td>#rc.lstbbq.tname#</td>
				                      <td>
				                      	<button type="button" class="btn btn-sm" id="btnMarkDone">
											<i class="ace-icon fa fa-check"></i>
										</button>
				                      </td>
				                </tr>
			              	</cfloop>
			              </tbody>
			            </table>
			          </div>
			          </div>
			        </div>
			      </div>
			  </div>
			  <div class="col-md-6">
			    <div class="row">      
			      <div class="col-xs-12">
			        <div class="panel panel-default">
			          <!-- Default panel contents -->
			          <div class="panel-heading">#getLabel("lstHotPot")#</div>
			          <!-- Table -->
			          <div class="table-responsive">
			            <table class="table" id="list_hotpot">
			              <thead>
			                <tr>
			                  <th>#getLabel("Name")#</th>
			                  <th>#getLabel("Quantity")#</th>
			                  <th>#getLabel("Tables")#</th>
			                  <th>#getLabel("Status")#</th>
			                </tr>
			              </thead>
			              <tbody>
			              	<cfloop query="rc.lsthotpot">
			              		<cfset styles = ""/>
		              			<cfset minutes = DateDiff("n", rc.lsthotpot.starttime, now())/>
			              		<cfif minutes lt 5>
			              			<cfset styles = "background: white;"/>
			              		<cfelseif minutes gte 5 and minutes lt 10>
			              			<cfset styles = "background: orange;"/>
			              		<cfelse>
			              			<cfset styles = "background: red; color: white;"/>
			              		</cfif>
			              		<cfif findNoCase(",", rc.lsthotpot.tname, 1) gt 0>
			              			<cfset styles = "background: aqua;"/>
			              		</cfif>
			              		<tr style="#styles#" orderid="#rc.lsthotpot.orderId#">
				                      <td>#rc.lsthotpot.fname#</td>
				                      <td>#rc.lsthotpot.quantity#</td>
				                      <td>#rc.lsthotpot.tname#</td>
				                      <td>
				                      	<button type="button" class="btn btn-sm" id="btnMarkDone">
											<i class="ace-icon fa fa-check"></i>
										</button>
				                      </td>
				                </tr>
			              	</cfloop>
			              </tbody>
			            </table>
			          </div>
			          </div>
			        </div>
			      </div>
			  </div>
			</div>
		</div>
	</div>
</cfoutput>