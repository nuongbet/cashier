component output="false" displayname=""  {

	public function init(){
		return this;
	}

	public any function login(boolean islogged, boolean isadmin, string userid, string username, string usertype, string shopid) {
		SESSION.isAdmin       = isadmin;
		SESSION.isLogin    	  = islogged;
		SESSION.UserID        = userid;
		SESSION.UserName      = username;
		SESSION.UserType      = usertype;
		SESSION.ShopId        = shopid;
		return;
	}

	public function default(struct rc)
	{
		if(CGI.REQUEST_METHOD == "post")
		{
			var user=QueryExecute("select * from user where (username=:uname or email=:uname) and password=:upass and isActive=1",{uname=rc.username,upass=hash(rc.password)});

			if (user.recordcount>0){
			
				if(user.typeId[1] == 1){
					login(true, true, user.userId[1], user.firstname[1], user.typeId[1], user.shopId[1]);
	              	SESSION.languageId=user.languageId[1];
	              	location("/index.cfm/admin:",false);
				}
				else
				{
					login(true, false, user.userId[1], user.firstname[1], user.typeId[1], user.shopId[1]);
					SESSION.languageId=user.languageId[1];
					location("/index.cfm",false);
				}
			}
			else
			{
				rc.message = "Login failure ! Please check your login account";
			}
		}
	} 
}