<cfoutput>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Cashier Login Page</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/assets/css/font-awesome.min.css" />
		<link rel="stylesheet" href="/assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="/assets/css/ace.min.css" id="main-ace-style" />
		<link rel="stylesheet" href="/css/style.css"/>
		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/assets/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="/assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="/assets/css/ace-rtl.min.css" />
		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="/assets/css/ace-ie.min.css" />
		<![endif]-->
		<script src="/js/jquery.js"></script>
		<script src="/js/jquery.animate-colors.js"></script>
		<script src="/assets/js/ace-extra.min.js"></script>
		<!--[if lte IE 8]>
		<script src="/assets/js/html5shiv.min.js"></script>
		<script src="/assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="login-layout light-login">
		<div class="main-container">
			#body#
		</div>
		<script type="text/javascript">
			window.jQuery || document.write("<script src='/assets/js/jquery.min.js'>"+"<"+"/script>");
		</script>
		<!--[if IE]>
		<script type="text/javascript">
		 window.jQuery || document.write("<script src='/assets/js/jquery1x.min.js'>"+"<"+"/script>");
		</script>
		<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript">
			jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
			//you don't need this, just used for changing background
			jQuery(function($) {
			 $('##btn-login-dark').on('click', function(e) {
				$('body').attr('class', 'login-layout');
				$('##id-text2').attr('class', 'white');
				$('##id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('##btn-login-light').on('click', function(e) {
				$('body').attr('class', 'login-layout light-login');
				$('##id-text2').attr('class', 'grey');
				$('##id-company-text').attr('class', 'blue');
				
				e.preventDefault();
			 });
			 $('##btn-login-blur').on('click', function(e) {
				$('body').attr('class', 'login-layout blur-login');
				$('##id-text2').attr('class', 'white');
				$('##id-company-text').attr('class', 'light-blue');
				
				e.preventDefault();
			 });
			 
			});
		</script>
	</body>
</html>
</cfoutput>