<cfoutput>
	<div class="main-content">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1">
				<div class="login-container">
					<div class="center">
<!--- 						<h2 class="blue" id="id-company-text">
							<i class="fa fa-paw"></i>
							Cashier Login Page
						</h2> --->
					</div>

					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>
					<div class="space-12"></div>


					<div class="position-relative">
						<div id="login-box" class="login-box visible widget-box no-border">
							<div class="widget-body">
								
								<div class="widget-main">
									<h4 class="header blue lighter bigger">
										<i class="ace-icon glyphicon glyphicon-user blue"></i>
										Please Enter Your Information
									</h4>

									<div class="space-6"></div>
									<cfif structKeyExists(rc, "message")>
										<div class="alert alert-danger">

											<i class="ace-icon fa fa-times"></i>
											#rc.message#
											<br>

										</div>
										<div class="space-6"></div>
									</cfif>
									<form method ="post" enctype="multipart/form-data">
										<fieldset>
											<label class="block clearfix">
												<span class="block input-icon input-icon-right">
													<input type="text" class="form-control" placeholder="User Name" name="username" id="username"/>
													<i class="ace-icon fa fa-user"></i>
												</span>
											</label>

											<label class="block clearfix">
												<span class="block input-icon input-icon-right">
													<input type="password" class="form-control" placeholder="Password" name="password" id="password"/>
													<i class="ace-icon fa fa-lock"></i>
												</span>
											</label>

											<div class="space"></div>

											<div class="clearfix">
												<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
													<i class="ace-icon fa fa-key"></i>
													<span class="bigger-110">LogIn</span>
												</button>
											</div>
											<div class="space-4"></div>
										</fieldset>
									</form>

								</div>
							</div>
						</div>
					</div>

					<div class="navbar-fixed-top align-right">
						<br />
						&nbsp;
						<a id="btn-login-dark" href="##">Dark</a>
						&nbsp;
						<span class="blue">/</span>
						&nbsp;
						<a id="btn-login-blur" href="##">Blur</a>
						&nbsp;
						<span class="blue">/</span>
						&nbsp;
						<a id="btn-login-light" href="##">Light</a>
						&nbsp; &nbsp; &nbsp;
					</div>
				</div>
			</div>
		</div>
	</div>
</cfoutput>