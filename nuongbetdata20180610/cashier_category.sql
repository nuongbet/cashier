CREATE DATABASE  IF NOT EXISTS `cashier` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cashier`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: localhost    Database: cashier
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryname` varchar(255) DEFAULT NULL,
  `description` longtext,
  `parentId` int(11) DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  `ilevel` double DEFAULT NULL,
  `shopId` double DEFAULT NULL,
  `sorted` varchar(255) DEFAULT NULL,
  `cost` decimal(19,2) DEFAULT NULL,
  `profitmargin` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (56,'Bia','Thức uống có cồn',6,'',3,1,'1',7000.00,3000.00),(57,'Nước ngọt','Thức uống có gas',6,'',3,1,'2',6000.00,3000.00),(58,'Bò','Các món ăn từ bò',6,'',3,1,'3',20000.00,10000.00),(59,'Heo','Các món ăn từ heo',6,'',3,1,'4',20000.00,10000.00),(60,'Cá','Các món ăn từ cá',6,'',3,1,'5',10000.00,5000.00),(61,'Gà','Các món ăn từ gà',6,'',3,1,'6',10000.00,5000.00),(62,'Bạch tuộc - Mực','Các món ăn từ bạch tuộc và mực',6,'',3,1,'7',10000.00,5000.00),(63,'Tôm','Các món ăn từ tôm',6,'',3,1,'8',20000.00,10000.00),(64,'Ếch','Các món ăn từ ếch',6,'',3,1,'9',10000.00,5000.00),(65,'Dà điểu','Các món ăn từ đà điểu',6,'',3,1,'10',30000.00,10000.00),(66,'Dê','Các món ăn từ dê',6,'',3,1,'11',30000.00,10000.00),(67,'Ốc','Các món ăn từ ốc',6,'',3,1,'12',10000.00,5000.00),(68,'Rau','Các món rau',6,'',3,1,'13',10000.00,5000.00),(69,'Món thêm','Các món dùng thêm',6,'',3,1,'14',10000.00,5000.00),(70,'Tráng miệng','Món tráng miệng',6,'\0',3,1,'15',10000.00,5000.00),(71,'Lẩu','Các món ăn từ lẩu',6,'',3,1,'16',10000.00,5000.00),(72,'Bệt','Ngồi trên sân thượng bằng các tấm pallet',2,'',3,1,'1',0.00,0.00),(73,'Khu A','Ngồi dưới tầng trệt ngoài trời',2,'',3,1,'2',0.00,0.00),(74,'Trệt trong nhà','Ngồi dưới tầng trệt trong nhà',2,'',3,1,'3',0.00,0.00),(75,'Mang về','Mang về',2,'',3,1,'4',0.00,0.00),(76,'Khu B','Tầng trệt ngồi ngoài trời',2,'',3,1,'5',0.00,0.00),(77,'Thuốc lá','Thuốc lá',6,'',3,1,'',0.00,0.00),(78,'Z-Phụ thu','Tính thêm tiền trong trường hợp ngoại lệ',6,'',3,1,'',0.00,0.00),(79,'Khu C','Bên nhà riêng',2,'\0',3,1,'6',0.00,0.00),(80,'Khu C','',2,'',3,1,'',0.00,0.00),(81,'Tiger lon','lon',6,'\0',3,1,'',18000.00,0.00);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-10 16:01:46
