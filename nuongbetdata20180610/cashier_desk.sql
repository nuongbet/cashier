CREATE DATABASE  IF NOT EXISTS `cashier` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `cashier`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win32 (AMD64)
--
-- Host: localhost    Database: cashier
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `desk`
--

DROP TABLE IF EXISTS `desk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desk` (
  `deskId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) DEFAULT NULL,
  `status` double DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `isactive` bit(1) DEFAULT NULL,
  `pos` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `shopId` double DEFAULT NULL,
  `sorted` double DEFAULT NULL,
  `isPrintedReceipt` bit(1) DEFAULT NULL,
  PRIMARY KEY (`deskId`),
  KEY `desk.categoryId_category.categoryId` (`categoryId`),
  CONSTRAINT `desk.categoryId_category.categoryId` FOREIGN KEY (`categoryId`) REFERENCES `category` (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desk`
--

LOCK TABLES `desk` WRITE;
/*!40000 ALTER TABLE `desk` DISABLE KEYS */;
INSERT INTO `desk` VALUES (41,72,0,'1-4 người','',NULL,'Bệt 1',1,1,'\0'),(42,72,0,'1-4 người','',NULL,'Bệt 2',1,2,'\0'),(43,72,0,'1-4 người','',NULL,'Bệt 3',1,3,'\0'),(44,72,0,'1-4 người','',NULL,'Bệt 4',1,4,'\0'),(45,72,0,'1-4 người','',NULL,'Bệt 5',1,5,'\0'),(46,72,0,'1-4 người','',NULL,'Bệt 6',1,6,'\0'),(47,72,0,'1-4 người','',NULL,'Bệt 7',1,7,'\0'),(48,72,0,'1-4 người','',NULL,'Bệt 8',1,8,'\0'),(49,72,0,'1-4 người','',NULL,'Bệt 9',1,9,'\0'),(50,72,0,'1-4 người','',NULL,'Bệt 10',1,10,'\0'),(51,72,0,'1-10 người','',NULL,'Bệt vip 1',1,11,'\0'),(52,73,0,'1-4 người','',NULL,'Bàn A1',1,1,'\0'),(53,73,0,'1-4 người','',NULL,'Bàn A2',1,2,'\0'),(54,73,0,'1-4 người','',NULL,'Bàn A3',1,3,'\0'),(55,73,0,'1-4 người','',NULL,'Bàn A4',1,4,'\0'),(56,73,0,'1-4 người','',NULL,'Bàn A5',1,5,'\0'),(57,73,0,'1-4 người','',NULL,'Bàn A6',1,6,'\0'),(58,73,0,'1-4 người','',NULL,'Bàn A7',1,7,'\0'),(59,73,0,'1-4 người','',NULL,'Bàn A8',1,8,'\0'),(60,73,0,'1-4 người','',NULL,'Bàn A9',1,9,'\0'),(61,73,0,'1-4 người','',NULL,'Bàn A10',1,10,'\0'),(62,74,0,'1-4 người','',NULL,'Bàn N1',1,1,'\0'),(63,74,0,'1-4 người','',NULL,'Bàn N2',1,2,'\0'),(64,74,0,'1-4 người','',NULL,'Bàn N3',1,3,'\0'),(65,74,0,'1-4 người','',NULL,'Bàn N4',1,4,'\0'),(66,76,0,'1 - 4 người','',NULL,'Bàn B1',1,11,'\0'),(67,76,0,'1 - 4 người','',NULL,'Bàn B2',1,12,'\0'),(68,76,0,'1 - 4 người','',NULL,'Bàn B3',1,13,'\0'),(69,76,0,'1 - 4 người','',NULL,'Bàn B4',1,14,'\0'),(70,76,0,'1 - 4 người','',NULL,'Bàn B5',1,15,'\0'),(71,76,0,'1 - 4 người','',NULL,'Bàn B6',1,16,'\0'),(72,76,0,'1 - 4 người','',NULL,'Bàn B7',1,17,'\0'),(73,76,0,'1 - 4 người','',NULL,'Bàn B8',1,18,'\0'),(74,76,0,'1 - 4 người','',NULL,'Bàn B9',1,19,'\0'),(75,76,0,'1 - 4 người','\0',NULL,'Bàn B10',1,20,'\0'),(76,75,0,'Mang về','',NULL,'Mang về 1',1,1,'\0'),(77,75,0,'Mang về','',NULL,'Mang về 2',1,2,'\0'),(78,75,0,'Cà phê','',NULL,'Cà phê 1',1,3,'\0'),(79,75,0,'Cà phê','',NULL,'Cà phê 2',1,4,'\0'),(80,80,0,'Bàn 1-4 người','\0',NULL,'Bàn C1',1,1,'\0'),(81,80,0,'Bàn 1-4 người','\0',NULL,'Bàn C2',1,2,'\0'),(82,80,0,'Bàn 1-4 người','\0',NULL,'Bàn C3',1,3,'\0'),(83,80,0,'Bàn 1-4 người','\0',NULL,'Bàn C4',1,4,'\0'),(84,80,0,'Bàn 1-4 người','\0',NULL,'Bàn C5',1,5,'\0'),(85,80,0,'Bàn 1-4 người','\0',NULL,'Bàn C6',1,12,'\0'),(86,76,0,'Bàn 1-4 người','\0',NULL,'Bàn B9',1,21,'\0'),(87,76,0,'Bàn 1-4 người','\0',NULL,'Bàn B10',1,22,'\0'),(88,76,0,'Bàn 1-4 người','\0',NULL,'Bàn B11',1,23,'\0'),(89,76,0,'Bàn 1-4 người','\0',NULL,'Bàn B12',1,24,'\0'),(90,74,0,'Bàn 1-4 người','',NULL,'Bàn N5',1,5,'\0'),(91,74,0,'Bàn 1-4 người','',NULL,'Bàn N6',1,6,'\0'),(92,73,0,'Bàn 1-4 người','',NULL,'Bàn A11',1,11,'\0'),(93,73,0,'Bàn 1-4 người','',NULL,'Bàn A12',1,12,'\0'),(94,72,0,'','',NULL,'Bệt vip 2',1,12,'\0'),(95,76,0,'1-4 người','\0',NULL,'Bàn B9',1,25,'\0'),(96,76,0,'','',NULL,'Bàn B10',1,26,'\0'),(97,80,0,'','',NULL,'Bàn C1',1,13,'\0'),(98,80,0,'','',NULL,'Bàn C2',1,14,'\0'),(99,80,0,'','',NULL,'Bàn C3',1,15,'\0'),(100,80,0,'','',NULL,'Bàn C4',1,16,'\0'),(101,80,0,'','',NULL,'Bàn C5',1,17,'\0'),(102,80,0,'','',NULL,'Bàn C6',1,18,'\0'),(103,80,0,'','',NULL,'Bàn C7',1,19,'\0'),(104,80,0,'','',NULL,'Bàn C8',1,20,'\0'),(105,80,0,'Bàn 1-4 người','',NULL,'Bàn C9',1,21,'\0'),(106,80,0,'Bàn 1-4 người','',NULL,'Bàn C10',1,22,'\0'),(107,74,0,'','',NULL,'Cà phê 3',1,7,'\0');
/*!40000 ALTER TABLE `desk` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-10 16:01:45
