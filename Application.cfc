component extends="lib.framework" {

	this.name = "Cashier"; // name of the application context
	variables.serverName ="Cashier";
	this.datasource = "cashier";

	this.ormEnabled = true;
	this.ormSettings = { logsql : true };
	this.invokeImplicitAccessor = true;

	// regional
	// default locale used for formating dates, numbers ...
	this.locale = "en_US"; 
	// default timezone used
	// this.timezone = "Asia/Bangkok"; 

// scope handling
	// lifespan of a untouched application scope
	this.applicationTimeout = createTimeSpan( 1, 0, 0, 0 ); 
	function setupSession() {
		SESSION.isAdmin       = false;
		SESSION.isLogin    	  = false;
		SESSION.UserID        ="";
		SESSION.UserName      ="";
		SESSION.UserType      ="";
		SESSION.ShopId      ="";
		SESSION.languageId = 2;
	}
	// session handling enabled or not
	this.sessionManagement = true; 
	// cfml or jee based sessions
	this.sessionType = "j2ee"; 
	// untouched session lifespan
	this.sessionTimeout = createTimeSpan( 0, 1, 0, 0 ); 
	this.sessionStorage = "memory";

	// client scope enabled or not
	this.clientManagement = false; 
	this.clientTimeout = createTimeSpan( 90, 0, 0, 0 );
	this.clientStorage = "cookie";
						
	// using domain cookies or not
	this.setDomainCookies = false; 
	this.setClientCookies = false;

	// prefer the local scope at unscoped write
	this.localMode = "classic"; 
	
	// buffer the output of a tag/function body to output in case of a exception
	this.bufferOutput = true; 
	this.compression = false;
	this.suppressRemoteComponentContent = false;
	
	// If set to false Railo ignores type defintions with function arguments and return values
	this.typeChecking = true;

	//request timeout
	this.requestTimeout=createTimeSpan(0,0,10,0);

	
	// request
	// max lifespan of a running request
	this.requestTimeout=createTimeSpan(0,0,0,50); 

	// charset
	this.charset.web="UTF-8";
	this.charset.resource="windows-1252";
	this.scopeCascading = "standard";
	
	variables.rootLink = getContextRoot();
	variables.currency = "VND";
	variables.framework = {};
	// setup subsystems
	variables.framework.usingSubsystems = true;
	variables.framework.siteWideLayoutSybsystem = 'common';
	variables.framework.defaultSubsystem = 'home'
	variables.framework.defaultSection = 'main';
	variables.framework.defaultItem = 'default';
	variables.framework.reloadApplicationOnEveryRequest = false;

	function setupApplication() {
		// manage model and controllers with DI/1:
		var bf = new lib.ioc( "model, controllers" );
		setBeanFactory( bf );
		//multi language
		application.ListLanguage = entityLoad("language");
		var qgetdefaultLanguage = QueryExecute("select languageId from language where defaultlanguage = 1");
		application.languageId = qgetdefaultLanguage.languageId;

		var helper = createObject("component", "admin.helper.resource");
		helper.loadResource();
		// ORMReload();
		APPLICATION.Resources = {};
		return;
	}


    function setupRequest()
    {
    	if (structKeyExists(SESSION, "isLogin") &&  SESSION.isLogin == true)
    	{
    		if(structKeyExists(SESSION, "isAdmin") && SESSION.isAdmin == false && FindNoCase("admin",rc.action) == 1)
    		{
				 GetPageContext().getResponse().sendRedirect("/index.cfm/");
    		}
		}
		if(structKeyExists(SESSION, "isLogin") && SESSION.isLogin == false && FindNoCase("admin",rc.action) == 1)
		{
			 GetPageContext().getResponse().sendRedirect("/index.cfm/login:login");
		}
    }

	this.tag.cflocation.addtoken = false;		
}
